export class PolicyVerificationVm {
    constructor() {
        this.userId = 0;
        this.fACode = '';
        this.fullName = '';
        this.maxPolicy = 0;
        this.policyPendingDetail = '';
        this.policyApprovedDetail = '';
        this.policyRejectDetail = '';
    }

    userId: number;
    fACode: string;
    trainingCompletionDate: Date;
    trainingExamPassingDate: Date;
    dateOfFirstPolicySubmission: Date;
    dateOfFinalPolicySubmission: Date;
    fullName: string;
    maxPolicy: number;
    policyPendingDetail: string;
    policyApprovedDetail: string;
    policyRejectDetail: string;
}
