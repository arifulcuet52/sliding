import { ActionTypeEum } from "./enums/lead-action.enum";

export class ReferenceActivity {
    constructor(){
        this.activityId = 0;
        this.actionId = ActionTypeEum.call;
        this.subActionStatusId = 0;
    }
    activityId: number;
    referenceInfoId: number;
    actionId: number;
    actionName: string;
    subActionStatusId: number;
    statusName: string;
    remarks: string;
}
