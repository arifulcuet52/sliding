export class Campaign {
    constructor(){
        this.id = 0;
    }
    id: number;
    campaignName: string;
    campaignCode: string;
    campaignLink: string;
    campaignStartDate: Date;
    campaignStartTime: Date;
    campaignEndTime: Date;
    campaignEndDate: Date;
    campaignDuration: number;
    adminVerificationEndDate: Date;
    adminVerificationTime: Date;
    isPolicyPrivileged: boolean;
    minPolicyRequired: number;
    isTrainingFeeExempted: boolean;
    premiumRequired: number;
    noOfCandidateRegiWithCode: number;
}
