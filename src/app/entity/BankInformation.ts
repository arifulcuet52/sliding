export class BankInformation {
    constructor () {
        this.id = 0;
        this.bankCode = '0';
        this.branchCode = '0';
    }
    id: number;
    candidateId: number;
    accountNumber: string;
    bankId: number;
    bankCode: string;
    bank: any;
    branchCode: string;
    branchId: number;
    branch: any;
}
