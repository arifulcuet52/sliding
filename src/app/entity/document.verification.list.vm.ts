export class DocumentVerificationListVm {
  candidateId: number;
  candidateName: string;
  faCode: string;
  trainingCompletionDate: Date | string;
  trainingExamPassingDate: Date | string;
  dateOfFinalDocumentSubmission: Date | string;
  docCount: number;
  approveDocCount: number;
  rejectDocCount: number;
  verificationPendingCount: number;
  resendDocCount: number;
}
