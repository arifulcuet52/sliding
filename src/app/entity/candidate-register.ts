export class CandidateRegister {
    constructor() {
        this.FirstName = '';
        this.MiddleName = '';
        this.LastName = '';
        this.Gender = 'Female';
        this.DateofBirth = new Date();
        this.Age = 0;
        this.Email = '';
        this.Password = '';
        this.ConfirmPassword = '';
        this.HighestQualification = '';
        this.PreferredWorkLocationDistrict = '';
        this.PreferredWorkLocationArea = '';
        this.MobileNo = '';
        this.CampaignCode = '';
        this.RefereeAgentCode = '';
        this.IsExperienced = true;
        this.IsInvolved = false;
        this.HasCrime = false;
    }
    FirstName: string;
    MiddleName: string;
    LastName: string;
    Gender: string;
    DateofBirth: Date;
    Age: number;
    Email: string;
    Password: string;
    ConfirmPassword: string;
    HighestQualification: string;
    PreferredWorkLocationDistrict: string;
    PreferredWorkLocationArea: string;
    MobileNo: string;
    CampaignCode: string;
    RefereeAgentCode: string;
    IsExperienced: boolean;
    IsInvolved: boolean;
    HasCrime: boolean;
}
