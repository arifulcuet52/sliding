export class Role {
    constructor(name: string, status: boolean) {
        this.name = name;
        this.status = status;
    }
    id: string;
    name: string;
    description: string;
    status: boolean;
}
