export class RefVerificationQueue {
    constructor(){
        this.lockedBy = 0;
    }
    candidateId: number;
    lockedBy: number;
}
