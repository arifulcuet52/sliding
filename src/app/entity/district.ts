import { Area } from './area';
import { CountryInfo } from './country';

export class District {
    /**
     *
     */
    constructor(countryId: number, districtId: number, districtName: string) {
        this.countryId = countryId;
        this.districtId = districtId;
        this.districtName = districtName;
    }
    districtId: number;
    districtName: string;
    countryId: number;
    countryInfo: CountryInfo;
    areas: Area[];
}
