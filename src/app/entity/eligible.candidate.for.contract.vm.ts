export class EligibleCandidateForContractVm {
  candidateId: number;
  candidateName: string;
  fACode: string;
  taggedUmBmName: string;
  campaignDate: Date | string | null;
  contractDate: Date | string | null;
  dateOfAckowledgement: Date | string | null;
  action: string;
  totalRow: number;
}
