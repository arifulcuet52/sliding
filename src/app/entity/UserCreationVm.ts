import { User } from './user';
import { UserPermission } from './user-permission';

export class UserCreationVm extends User {
  constructor() {
    super();
    this.dob = null;
    this.tagUserList = [];
  }
  gender: string;
  dob: Date; // = new Date();dateofBirth: Date;
  age: number;
  mobileNo: string;
  email: string;
  roles: UserPermission[];
  profilePicSrc: string;
  profilePicUrl: string;
  tagUserList: string[];
}
