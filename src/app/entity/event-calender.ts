import { Time } from '@angular/common';

export class EventCalender{
  constructor(){
    this.eventCalenderId = 0;
    this.capacity = 0;
    this.availableCapacity = 0;
  }
  eventUrl: string;
  eventImageUrl: string;
  eventCalenderId: number;
  eventTitle: string;
  startDate: Date;
  endDate: Date;
  startTime: Time;
  startTimeString: string;
  endTimeString: string;
  endTime: Time;
  districtId: number;
  areaId: number;
  address: string;
  description: string;
  capacity: number;
  availableCapacity: number;
  statusId: number;
}
