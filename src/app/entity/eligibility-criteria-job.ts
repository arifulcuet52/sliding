export class EligibilityCriteriaJob {
    constructor(){
        this.id = 0;
        this.status = true;
    }
    id: number;
    title: string;
    score: number;
    sortOrder: number;
    status: boolean;
}
