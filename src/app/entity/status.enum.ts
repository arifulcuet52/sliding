export enum StatusEnum {
    Active = 1,
    InActive = 2,
    Lock = 3,
    UnLock = 4,
    Issue = 5,
    ReIssue = 6,
    Acivated = 7,
    Send = 8,
    Failed = 9,
    Expired = 10,
    Pending = 11,
    Accepted = 12,
    Rejected = 13
}

export enum StatusFeeEnum {
    Pending = 17,
    Accepted = 18,
    Rejected = 19
}

export enum LeadTypeEnum {
    ActiveLead = 1,
    InActiveLead = 2,
    All = 3
}
export enum LeadTypeEnumUntagged {
    ActiveLead = 1,
    InActiveLead = 2,
    All = 3,
    MyActiveLeads = 4,
    MyInactiveLeads = 5
}

export enum TrainingCertificateListFilterEnum{
  All = 1,
  Completed = 2,
  Pending = 3
}
