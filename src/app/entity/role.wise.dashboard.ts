interface RoleWiseDashboard {
  id: number;
  roleId: number;
  dashboardTitle: string;
  fieldSetTitle: string;
  sortOrder: number;
  roleWiseDashboardDetailses: RoleWiseDashboardDetailse[];
}
