export class QuestionVm {
    constructor(title: string) {
        this.title = title;
    }

    id: number;
    moduleId: number;
    moduleName: string;
    title: string;
    questionTypeId: number;
    questionTypeName: string;
    score: number;
    isActive: boolean;
    createDateTime: string;
    questionOptions: any;
    questionType: any;
}
