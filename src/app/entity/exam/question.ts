import { QuestionOption } from './question-option';
import { QuestionType } from './question-type';

export class Question {
    constructor(title: string) {
        this.title = title;
        this.score = 0;
        this.questionOptions = [];
        this.questionType = new QuestionType();
    }

    id: number;
    trainingModuleId: number;
    title: string;
    questionTypeId: number;
    score: number;
    isActive: boolean;
    createDateTime: string;
    questionOptions: QuestionOption[];
    questionType: QuestionType;
}
