export class TrainingCertificate {
    candidateId: number;
    candidateExamId: number;
    examScheduleId: number;
    candidateName: string;
    faCode: string;
    trainingExamDate?: any;
    documentSubmissionDate: string;
    certificateFeeDateTime: string;
    atpCreateDateTime: string;
    summaryFieldWorkCreateDateTime: string;
    certificateFilePath: string;
    certificateUploadDate?: any;
}
