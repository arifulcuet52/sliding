export class QuestionOption {
    id: number;
    text: string;
    score: number;
    isAnswer: boolean;
}
