import { QuestionPerModule } from './question-per-module';

export class ExamSetting {
  constructor() {
    this.questionPerModules = [];
  }

  id: number;
  totalNoOfQuestion: number;
  questionPerPage: number;
  totalScore: number;
  passScore: number;
  status: boolean;
  userId: number;
  createdOn: Date;
  questionPerModules: QuestionPerModule[];
}
