import { ExamSetting } from './exam-setting';
import { TrainingModule } from '../training-module';

export class QuestionPerModule {
    id: number;
    examSettingId: number;
    moduleId: number;
    numberOfQuestion: number;
    status: boolean;
    userId: number;
    createdOn: string;
    examSetting: ExamSetting;
    module: TrainingModule;
}
