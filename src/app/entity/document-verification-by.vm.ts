export class DocumentVerificationBy {
    candidateId: number;
    candidateName: string;
    faCode: string;
    trainingCompletionDate: string;
    trainingExamPassingDate: string;
    dateOfFinalDocumentSubmission: string;
    docCount: number;
    approveDocCount: number;
    rejectDocCount: number;
    verificationPendingCount: number;
    isLocked: boolean;
    lockedBy: number;
    lockedByName: string;
    lockedDate: string;
    statusName: string;
    authorizedBy?: any;
    authorizedByName: string;
    authorizedDate: string;
}
