import { District } from './district';

export class CountryInfo {
    countryId: number;
    name: string;
    districts: District[];
}
