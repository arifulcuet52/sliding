export class RefrenceVerificationVm {
  candidateId: number;
  name: string;
  faCode: string;
  dateofFinalrefsubmission: Date | string;
  totalRef: number;
  pendingcount: number;
  approvecount: number;
  rejectcount: number;
  resendRefCount: number;
}
