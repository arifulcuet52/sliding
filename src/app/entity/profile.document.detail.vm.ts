export class ProfileDocumentDetailVm {
  id: number | null;
  userId: number;
  globalKeyId: number;
  value: string;
  statusId: number | null;
  remarks: string;
  status: string;
  globalKey: string;
  title: string;
  url: string;
  spanText: string;
  showUploadButton: boolean;
  uploadLabel: string;
}
