export class EligibilityCriteria {
    constructor() {
        this.eligibilityCriteriaId = 0;
        // this.minimumAge = 0;
        // this.maximumAge = 0;
        // this.minLevelOfEducation = 0;
        // this.isExAgent = false;
        // this.isInvolved = false;
        // this.hasCriminalOffence = false;
        // this.minTotalScore = 0;
        this.status = false;
    }
    eligibilityCriteriaId: number;
    minimumAge: number;
    maximumAge: number;
    minLevelOfEducation: number;
    isExAgent: boolean;
    isInvolved: boolean;
    hasCriminalOffence: boolean;
    minTotalScore: number;
    status: boolean;
    ageCriteriaId: number;
}
