export class Pager<T> {
  totalItem: number;
  itemPerPage: number;
  hasNextPage: boolean;
  hasPreviousPage: boolean;
  data: T[];
}
