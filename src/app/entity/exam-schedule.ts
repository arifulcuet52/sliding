import { Area } from "./area";
import { District } from "./district";

export class ExamSchedule {
    constructor(location: string){
        this.id = 0;
        this.venueLocation = location;
        this.areaId = 0;
        this.districtId = 0;/* 
        this.venueSeatingCapacity = 0;
        this.availableSeats = 0; */
    }
    id: number;
    examDate: Date;
    startTime: Date;
    endTime: Date;
    examDuration: number;
    durationStr: string;
    venueLocation: string = '';
    venueSeatingCapacity: number;
    availableSeats: number;
    districtId: number;
    areaId: number;
    area: Area;
    district: District;
}
