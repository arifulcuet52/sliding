export class AccountActivationVm{
    constructor(){
        this.email = '';
        this.password = '',
        this.confirmPassword = '';
    }
    email: string;
    password: string;
    confirmPassword: string;
}
