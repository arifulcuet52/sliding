export class RolePermission{
    roleId:String;
    menuId:String;
    menuName:String;
    access:boolean;
    delete:boolean;
    edit:boolean;
    insert:boolean;
}