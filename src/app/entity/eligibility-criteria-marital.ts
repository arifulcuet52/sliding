export class EligibilityCriteriaMarital {
    constructor(){
        this.id = 0;
        this.status = true;
    }
    id: number;
    maritalStatus: string;
    score: number;
    status: boolean;
}
