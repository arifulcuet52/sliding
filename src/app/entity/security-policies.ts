export class SecurityPolicies {
  id: number;
  requiredLength: number;
  requireNonLetterOrDigit = false;
  requireDigit = false;
  requireLowercase = false;
  requireUppercase = false;
  defaultPassword: string;
  maxNumOfInvalidAttemAllowed: number;
  accPassValidityDuration: number;
  userHisPassCheckCount: number;
  otpLength: number;
  otpExpirationInSecond: number;
  otpGenerationDelayInSecond: number;
  maxNumOfInvOTPAttempAllow: number;
  passResetURLDurInMin: number;
  // AssignedAdmin: string;
  // AssignedDate?: Date;
  // CompletedDate?: Date;
}
