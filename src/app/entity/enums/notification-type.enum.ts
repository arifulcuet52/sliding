export enum NotificationTypeEnum {
    Email = 1,
    SMS = 2,
    OnSite = 3
}
