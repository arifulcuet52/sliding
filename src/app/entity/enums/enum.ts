export enum policyEnum {
    pending = 20,
    accepted = 21,
    rejected = 22,
}

export enum commonEnum {
    pass = 53,
    fail = 54
}

export enum ReferenceStatusEnum {
    pending = 38,
    accepted = 39,
    rejected = 40,
    resend = 66
}

export enum ProfileVerificationGlobalKeys {
    policyNumberVerificationStatus = "PolicyNumberVerificationStatus",
    reqDocVerificationStatus = "ReqDocVerificationStatus",
    referenceInfoVerificationStatus = "ReferenceInfoVerificationStatus"
}

export enum ReferenceVerificationStatus{
    complete = "Completed",
    incomplete = "InComplete"
}

export enum DesignationEnum{
    um = "U",
    bm = "A",
    ae = "E",
    fa = "F"
}

export enum DesignationDescription{
    um = "Unit Manager",
    bm = "Branch Manager",
    ae = "Agency Executive",
    fa = "Financial Associate"
}

export enum UrlSettingEnum{
    cmpCode = "CampaignUrlPrefix",
    eventUrlPrefix = 'EventUrlPrefix'
}

export enum RoleEnum
{
    Admin = 9,
    BM = 15,
    Candidate = 10,
    CollectionsAdmin = 18,
    Invigilator = 16,
    TrainingAdmin = 17,
    UM = 14,
    AgencyServiceAdmin=19,
    CallCenterAdmin = 20
}

export enum TrainingModuleEnum  {
    title= 1,
    module= 2,
    section= 3
}



