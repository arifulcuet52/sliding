import { Status } from './status';

export class TagRequest
{
    tagRequestId: number;
    candidateUserId: number;
    umBmUserId: number;
    requestDateTime: Date | string;
    reponseDateTime: Date | string | null;
    statusId: number;
    status: Status;
}
