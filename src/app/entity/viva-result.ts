export class VivaResult {
    constructor()
    {
        this.id = 0;
        this.userId = 0;
        this.remarks = '';
        this.dateOfAcknowledgement = new Date();
    }
    id: number;
    userId: number;
    statusId: number;
    remarks: string;
    dateOfAcknowledgement: Date;
}
