export class UserPermission {
    constructor() { }
    roleId: string;
    name: string;
    description: string;
    userExists: boolean;
}
