export class ProfileStatusVm {
    profileStatusId: number = 0;
    userId: number = 0;
    globalKeyId: number = 0;
    statusId: number = 0;
    status: string = "";
    globalKey: string = "";
}
