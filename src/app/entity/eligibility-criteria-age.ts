export class EligibilityCriteriaAge {
    constructor()
    {
        this.id = 0;
        this.status = false;
    }
    id: number;
    minAge: number;
    maxAge: number;
    score: number;
    status: boolean;
}
