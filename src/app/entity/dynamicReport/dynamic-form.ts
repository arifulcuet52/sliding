export class DynamicForm {
    id: number;
    type: string;
    name: string;
    label: string;
    value: string;
    required: boolean;
    onChangeDependency: boolean;
    childDropdownId: number;
    execCascadingQuery: string;
    options: DynamicOption[];
}

export class DynamicOption {
    key: any;
    label: string;
}