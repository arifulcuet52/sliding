 export enum OTPIssueReasonEnum {
        ForgetPassword = 1,
        UMBMSelection = 2,
        UserActivation = 3
    }
