export class TrainingModule {
  constructor() {
    this.id = 0;
    this.moduleName = '';
    this.moduleType = 0;
    this.url = '';
    this.contentType = '';
    this.parentId = 0;
    this.childrens = [];
    this.isActive = true;
    this.isShow = true;
    this.title = '';
  }

  id: number;
  moduleName: string;
  moduleType: number;
  url: string;
  contentType: string;
  parentId: number;
  childrens: any;
  isActive: boolean;
  isShow: boolean;
  title: string;
  totalQuestions: number;
}
