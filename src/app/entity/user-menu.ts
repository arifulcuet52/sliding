export class UserMenuVm {
    menuId: number;
    menuName: string;
    component: string;
    path: string;
    parentMenu: number | null;
    sortOrder: number;
    access: boolean;
    insert: boolean;
    edit: boolean;
    delete: boolean;
    list: UserMenuVm[] = [];
    showInMenu: boolean;
    portalName: string;
}