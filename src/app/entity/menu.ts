export class Menu {
    constructor(menuName: string) {
        this.menuName = menuName;
    }
    menuId: number;
    menuName: string;
    component: string;
    path: string;
    parentId: number | null;
    sortOrder: number;
    parentMenu: number;
    showInMenu: boolean;
    portalName: string;
}
