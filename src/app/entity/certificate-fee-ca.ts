export class CertificateFeeCa {
    certificateFeeId: number;
    candidateId: number;
    candidateName?: string;
    faCode: string;
    receiptNumber: string;
    createDateTime: string;
    amountPaid: number;
    paymentDate: string;
    depositPointId: number;
    depositPointName: string;
    lockedBy?: any;
    lockedByName?: any;
    lockedDate: string;
    statusId: number;
    status: string;
    authorizedBy: number;
    authorizedByName?: string;
    authorizedDate?: string;
}
