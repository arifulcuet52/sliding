interface RoleWiseDashboardDetailse {
  id: number;
  roleWiseDashboardId: number;
  cssClassName?: string;
  label: string;
  routeLink?: string;
  sortOrder: number;
}
