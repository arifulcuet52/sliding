export class UserRoleVm {
    email: string;
    firstName: string;
    fullName: string;
    lastName: string;
    middleName: string;
    mobileNo: string;
    roleId: number;
    userId: number;
}
