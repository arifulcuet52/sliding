declare global {
  interface Array<T> {
    orderByPropertyNames(...prop): Array<T>;
  }
}
Array.prototype.orderByPropertyNames = function(...prop) {
  this.sort((a, b) => temp(a, b, ...prop));
  return this;
};
export default 'anything';
export function temp(a, b, ...prop) {
  let value1 = '';
  let value2 = '';
  // console.log(prop.length, new Date());
  for (let index = 0; index < prop.length; index++) {
    const element = prop[index];
    value1 += a[element].toString().toLocaleLowerCase();
    value2 += b[element].toString().toLocaleLowerCase();
  }
  if (value1 > value2) {
    return 1;
  }
  if (value1 < value2) {
    return -1;
  }
}
