export class CandidateProfileInfo {
    constructor() {
        this.firstName = '';

        this.middleName = '';

        this.lastName = '';

        this.fatherName = '';

        this.motherName = '';

        this.husbandName = '';

        this.gender = '';

        this.dob = new Date();

        this.maritalStatusId = 0;

        this.presentAddress = '';

        this.permanentAddress = '';

        this.mobileNo = '';

        this.countryId = 0;

        this.designationId = 0;

        this.nationalIdCardSrc = '';

        this.profilePictureSrc = '';

        this.highestEducationId = 0;

        this.faCode = '';

        this.nidNo = '';
    }
    candidateProfileInfoId: number;

    candidateId: number;

    designationId: number;

    firstName: string;

    middleName: string;

    lastName: string;

    fatherName: string;

    motherName: string;

    husbandName: string;

    gender: string;

    dob: Date;

    maritalStatusId: number;

    presentAddress: string;

    permanentAddress: string;

    mobileNo: string;

    countryId: number;

    nationalIdCardSrc: string;

    profilePictureSrc: string;

    highestEducationId: number | null;

    faCode: string;

    nidNo: string;
    profilePictureUrl: string;
}
