import { PagingModel } from "./PagingModel";

export class ResponseMessage<T>{
    msgCode: string;
    msg: string;
    data: T;
}
