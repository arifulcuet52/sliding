export class GeneralSetting {
  constructor() {
    this.id = 0;
    this.imageType = '';
    this.imageFileSizeLimit = 0;
    this.userIdleTimeConfiguration = 0;
    this.uploadedFileSizeLimit = 0;
    this.maxExamScheduleDisplay = 0;
    this.otpLength = 0;
    this.otpExpirationInSecond = 0;
    this.otpGenerationDelayInSecond = 0;
    this.maxNumOfInvOTPAttempAllow = 0;
    this.sessionExpiryPeriod = 0;
    this.fieldWorkAfterPassingTrainingDays = 0;
  }
  id: number;
  imageType: string;
  imageFileSizeLimit: number;
  userIdleTimeConfiguration: number;
  uploadedFileSizeLimit: number;
  maxExamScheduleDisplay: number;
  otpLength: number;
  otpExpirationInSecond: number;
  otpGenerationDelayInSecond: number;
  otpLockDurationInSec: number;
  maxNumOfInvOTPAttempAllow: number;
  sessionExpiryPeriod: number;
  quotaLimit: number;
  maxDistance: number;
  ageDifferenceUM: number;
  ageDifferenceBM: number;
  noOfUMBMShow: number;
  fieldWorkAfterPassingTrainingDays: number;
  minReqPolicyNo: number;
  minReqReferenceNo: number;
  slaVerificationDuration: number;
}
