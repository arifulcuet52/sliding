import { ReferenceActivity } from "./reference-activity";

export class Reference {
    constructor() {
        this.id = 0;
        this.statusId = 0;
        this.remarks = "";
        this.isPending = true;
        this.isActionHistoryShown = false;
        this.actionList = [];
    }
    id: number;
    candidateId: number;
    statusId: number;
    rafereeName: string;
    contactNo: string;
    address: string;
    occupation: string;
    companyName: string;
    remarks: string;
    isPending: boolean;
    isActionHistoryShown: boolean;
    actionList: ReferenceActivity[];
}
