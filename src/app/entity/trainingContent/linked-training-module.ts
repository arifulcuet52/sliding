export class LinkedTrainingModule{
    id?:number;
    moduleId:number;
    presentationId:number; 
    subModuleInfo:LinkedSubModule[]=[]
    
}

export class LinkedSubModule{
    linkedSubModuleId?:number;
    subModuleId?:number; 
    startSlideSerialNo?:number;
    endSlideSerialNo?:number;
    statusId?:number;
    createdBy?:number;
    created?:Date;
    updatedBy?:number;
    updated?:Date;   
}
