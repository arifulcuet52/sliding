
export class slides {
    id: number;
    slideContent: string;
    order: number;
    presentationId: number;
}
export class presentation {
    id: number;
    title: string;
    slideDetails: slides[] = [];
}

export class Property {
    id: string;
    startTime: string;
    endTime: string;
    display: boolean;
    backgroundColor: string;
    changeText: string;
    opacity: number;
    initialDisplayProp: string;
    borderColor: string;
    borderLine: number;
    delay: number;
    sequence: number;
    linkedSlideNumber: number;
    activeBgColor: string;
    visitedBgColor: string;
    inactiveBgColor: string;
    actionAfterClick: string;
    type: string;
    animationProp: string;
    dynamicDragDrop: string;
    enabledDisabledProperty: string;
    nextEnabledButtonSequenseNo: number;
    nextButtonEnabledDelay: number;
    selfButtonEnabledDelay: number;
    textColor: string;
    dynamicDragDropShowHide:string;

}

export class NextButtonProperty {
    nextSlideNo: number;
    prevSlideNo: number;
    nextButtonEnabledDelayTime:number;
    sequenceNoClickingNextButtonEnabled:number;
    selfEnabledDelayTime:number;
}