export interface Status
{
    statusId: number;
    name: string;
    sortOrder: number;

}