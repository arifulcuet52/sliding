import { Designation } from './designation';
import { Department } from './department';

export class ColleactionAdminVm {
          id: number;
          firstName: string;
          departmentId: number;
          designationId: number;
          email: string;
          profilePicSrc: string;
          mobileNumber: string;
          designation: Designation;
          department: Department;
}
