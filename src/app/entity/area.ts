import { District } from './district';

export class Area {
    /**
     *
     */
    constructor(districtId: number, areaId: number, areaName: string) {
        this.areaId = areaId;
        this.areaName = areaName;
        this.districtId = districtId;
    }
    areaId: number;
    areaName: string;
    distances: number;
    districtId: number;
    district: District;
}
