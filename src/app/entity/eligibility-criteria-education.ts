export class EligibilityCriteriaEducation {
constructor(){
    this.id = 0;
    this.status = false;
}
id: number;
title: string;
score: number;
level: number;
status: boolean;
sortOrder: number;
shortCode: string;
}
