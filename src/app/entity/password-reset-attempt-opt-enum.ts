export enum PasswordResetAttemptOptsEnum {
  UnlockAccount = 1,
  ForgotPassword = 2,
  ChangePassword = 3,
  DefaultPasswordChange = 4
}
