export class FAQ{
    constructor(){
        this.id = 0;
        this.question = '';
        this.answer = '';
        this.statusId = 0;
        this.categoryId = 0;
    }
    id: number;
    question: string;
    answer: string;
    statusId: number;
    categoryId: number;
}