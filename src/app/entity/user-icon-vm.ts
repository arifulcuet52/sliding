export class UserIconVm {
    agentId: number;
    designation: string;
    firstName: string;
    profilePicSrc: string;
}
