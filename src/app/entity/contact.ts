export class Contact {
  contactId: number;
  firstName: string;
  middleName: string;
  lastName: string;
  mobile: string;
  email?: any;
  comment?: any;
  createDateTime: string;
}