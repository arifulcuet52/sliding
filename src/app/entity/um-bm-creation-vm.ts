
import { UserPermission } from './user-permission';
export class UmBmCreationVm {
  constructor() {
    this.areaId = 0;
    this.districtId = 0;
    this.educationId = 0;
    this.designationId = 0;
    this.id = 0;
    this.statusId = 0;
    this.shownInRecSearch = true;
    this.profilePicUrl = '';
    /* this.yearOfExp = 0;
        this.requirementRanking = 0;
        this.productionBasedRanking = 0;
        this.activeManPower = 0;  */
  }
  id: number;
  fullName: string;
  firstName: string;
  middleName: string;
  dateOfBirth: Date;
  lastName: string;
  status: string;
  statusId: number;
  userId: number;
  designationId: number;
  designation: string;
  agencyName: string;
  districtId: number;
  district: string;
  areaId: number;
  area: string;
  address: string;
  teamMember: number;
  achievements: string;
  quotaLimit: number;
  quotaRemaining: number;
  yearOfExp: number;
  requirementRanking: number;
  productionBasedRanking: number;
  activeManPower: number;
  totalScore: number;
  profilePicSrc: string;
  profilePicUrl: string;
  education: string;
  educationId: number;
  quotaConsume: number;
  agentId: string;
  shownInRecSearch: boolean;
  mobileNumber: string;
  email: string;
  achievementNo: number;
  roles: UserPermission[];
  userCode: string;
}
