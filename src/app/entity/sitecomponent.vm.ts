interface SiteComponentVm {
  id: number;
  name: string;
  appName: string;
  parentComponentId?: any;
  siteComponentTypeId: number;
  parentComponent?: any;
  typeId: number;
  type: string;
  children: SiteComponentVm[];
}
