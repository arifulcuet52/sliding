
export class PagingModel<T> {
  totalItem: number;
  hasNextPage: boolean;
  hasPreviousPage: boolean;
  data: T[];
}

