export class CertificateFee {
    certificateFeeId: number;
    firstName: string;
    middleName: string;
    lastName?: string;
    fullName: string;
    candidateName: string;
    userId: number;
    faCode: string;
    receiptNumber: string;
    amountPaid: number;
    paymentDate: string;
    isLocked: boolean;
    lockedBy?: any;
    verificationDate: string;
    remarks?: string;
    authorizedBy: number;
    authorizedDate?: string;
    createdBy: number;
    statusId: number;
    candidateId: number;
    depositPointName: string;
    createDateTime: string;
    depositPointId: number;
}
