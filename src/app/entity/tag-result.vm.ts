export class  TagRequestVm {
  tagRequestId: number;
  candidateUserId: number;
  firstName: string;
  middleName: string;
  lastName: string;
  fullName: string;
  age: number;
  educationId: number;
  educationName: string;
  umBmUserId: number;
  requestDateTime: string;
  reponseDateTime?: any;
  statusId: number;
  statusName: string;
}


