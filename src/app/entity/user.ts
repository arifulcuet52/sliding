export class User {
    constructor() {
        this.designationId = 0;
    }
    id: string;
    fullName: string;
    firstName: string;
    middleName: string;
    lastName: string;
    statusId: number;
    userId: number;
    designationId: number;
    departmentId: number;
    designation: string;
    department: string;
    profilePicSrc: string;
    profilePicUrl: string;

}


