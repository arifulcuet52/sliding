import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountService } from '../shared/service/remote/authentication/account.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  isError: boolean;
  constructor(private router: Router, private accountSer: AccountService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    //this.isError = true;
    const code = this.activatedRoute.snapshot.queryParamMap.get('key');
    //console.log(code);
    this.accountSer.validateShortURL(code).subscribe(
      (success) => {
        console.log(success);
        // console.log(success['fullCode'] );
        this.accountSer.setTempEmail(success['email']);
        this.router.navigate(['/ap/change-password'], {
          queryParams: { 'code': success['fullCode'] }
        });
      },
      (error) => { console.log(error); this.isError = true; }
    );
  }

}
