import { Component, OnInit } from '@angular/core';
import { MultilingualService } from './../../../shared/service/multilingual-service'
import { Values } from '../../../shared/models/multilingual'
import { catchError, map, tap } from 'rxjs/operators';
//import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import swal from 'sweetalert2';

@Component({
  selector: 'app-multilingual',
  templateUrl: './multilingual.component.html',
  styleUrls: ['./multilingual.component.css']
})
export class MultilingualComponent implements OnInit {
  public IsEdit: boolean = false;
  public multilanguageKeyValuesList: Values[] = [];
  public activeModule: string = "values";
  public values: Values = new Values();
  public listOfLanguage: any[] = ['bn', 'en', 'fr'];
  swal: any;
  constructor(private multilingualService: MultilingualService) { }

  ngOnInit() {
    this.values.language = "en";
    this.fnGetMultilingualData();
  }

  public fnActiveModule(module: string) {
    this.activeModule = module;
  }

  public fnChangeLanguagetoLoadData(event: any) {
    console.log(event.target.value);
    this.values.language = event.target.value;
    this.fnGetMultilingualData();
  }

  public fnPostMultilingualValues() {
    let key: any = { KeyId: this.values.keyId, KeyName: this.values.keyName }
    this.values.key = key;
    this.multilingualService.fnPostValue(this.values).subscribe(
      (data: any) => {
        if (data) {
          swal({
            title: '',
            text: data.msg,
            type: 'success',
            confirmButtonText: 'Ok'
          });
          this.fnGetMultilingualData();
          this.fnCreateNewRecord();
        }
      },
      (error: any) => {
        swal({
          title: 'Error!',
          text: "System has failed to save the information.",
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    )
  }

  public fnGetMultilingualData() {
    this.multilingualService.fnGetValues(this.values.language, 'Depend on Language').subscribe(
      (data: any) => {
        if (data) {
          this.multilanguageKeyValuesList = data.data || [];
        }
        (error: any) => { }
      },
    );
  }
  public fnGetLanguageList() {
    this.multilingualService.fnGetLanguageList().subscribe(
      (data: any[]) => {
        this.listOfLanguage = data;
      },
      (error: any) => {
        //show the alert message
      }
    )
  }

  fnPtableCallBack(event: any) {
    if (event.action == "edit-item") {
      this.values = event.record;
    } else if (event.action == "delete-item") {
      swal({
        title: "warning",
        text: 'Are you sure you want to delete key '+event.record.keyName+' for language '+event.record.language+'?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        console.log(result);
        if (result.value) {
          this.multilingualService.fnDeleteValue(event.record.id).subscribe(
            (data: any) => {
              //alert("success");
              this.fnGetMultilingualData();
            },
            (error: any) => {
              swal({
                title: 'Error!',
                text: "System has failed to delete the information.",
                type: 'error',
                confirmButtonText: 'Close'
              });
            }
          );
        }
      });      
    }
  }

  fnCreateNewRecord() {
    let language = this.values.language
    this.values = new Values();
    this.values.language = language;

  }

  public multilanguageKeyValuesTableSetting = {
    tableID: "key-value-table",
    tableClass: "table table-border ",
    tableName: 'Key value information of different languages',
    tableRowIDInternalName: "ID",
    tableColDef: [
      { headerName: 'Language', width: '10%', internalName: 'language', sort: true, type: "" },
      { headerName: 'Key', width: '30%', internalName: 'keyName', sort: true, type: "" },
      { headerName: 'Values', width: '60%', internalName: 'values', sort: true, type: "" }
    ],
    enabledSearch: true,
    enabledSerialNo: true,
    pageSize: 15,
    enabledPagination: true,
    enabledEditBtn: true,
    enabledCellClick: true,
    enabledColumnFilter: false,
  };
}
