import { MultilingualModule } from './multilingual.module';

describe('MultilingualModule', () => {
  let multilingualModule: MultilingualModule;

  beforeEach(() => {
    multilingualModule = new MultilingualModule();
  });

  it('should create an instance', () => {
    expect(multilingualModule).toBeTruthy();
  });
});
