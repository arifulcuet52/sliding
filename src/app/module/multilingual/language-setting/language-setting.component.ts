import { Component, OnInit } from '@angular/core';
import { MultilingualService } from './../../../shared/service/multilingual-service'
import { LanguageSetting } from '../../../shared/models/multilingual'
import { catchError, map, tap } from 'rxjs/operators';
//import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import swal from 'sweetalert2';

@Component({
  selector: 'app-language-setting',
  templateUrl: './language-setting.component.html',
  styleUrls: ['./language-setting.component.css']
})
export class LanguageSettingComponent implements OnInit {
  public IsEdit: boolean = false;
  public languageSetting: LanguageSetting = new LanguageSetting();
  public languageSettingData:LanguageSetting[]=[];
  public listOfLanguage: any[] = ['bn', 'en', 'fr'];
  public listOfPortal: any[] = ['AP','CP'];
  swal: any;
  constructor(private multilingualService: MultilingualService) { }

  ngOnInit() {
    this.languageSetting.languageCode = "en";
    this.languageSetting.portalCode="CP";    
    this.fnGetLanguageSettingData();
  }

  public fnPostLanguageSetting() { 
    this.multilingualService.fnPostLanguageSetting(this.languageSetting).subscribe(
      (data: any) => {
        if (data) {
          swal({
            title: '',
            text: data.msg,
            type: 'success',
            confirmButtonText: 'Ok'
          });
          this.fnGetLanguageSettingData();
          this.fnCreateNewRecord();
        }
      },
      (error: any) => {
        swal({
          title: 'Error!',
          text: "System has failed to save the information.",
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    )
  }

  public fnGetLanguageSettingData() {
    this.multilingualService.fnGetLanguageSettings().subscribe(
      (data: any) => {
        if (data) {
          this.languageSettingData = data.data || [];
        }
        (error: any) => { }
      },
    );
  }
  public fnGetLanguageList() {
    this.multilingualService.fnGetLanguageList().subscribe(
      (data: any[]) => {
        this.listOfLanguage = data;
      },
      (error: any) => {
        //show the alert message
      }
    )
  }

  fnPtableCallBack(event: any) {
    if (event.action == "edit-item") {
      this.languageSetting = event.record;
    } else if (event.action == "delete-item") {
      swal({
        title: "warning",
        text: 'Are you sure you want to delete the information?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        console.log(result);
        if (result.value) {
          this.multilingualService.fnDeleteLanguageSetting(event.record.id).subscribe(
            (data: any) => {
              //alert("success");
              this.fnGetLanguageSettingData();
            },
            (error: any) => {
              swal({
                title: 'Error!',
                text: "System has failed to delete the information.",
                type: 'error',
                confirmButtonText: 'Close'
              });
            }
          );
        }
      });      
    }
  }

  fnCreateNewRecord() {
    let language = this.languageSetting.languageCode
    let portalCode = this.languageSetting.portalCode;
    this.languageSetting = new LanguageSetting();
    this.languageSetting.languageCode = language;
    this.languageSetting.portalCode = portalCode;
  }

  public languageSettingTableSetting = {
    tableID: "key-value-table",
    tableClass: "table table-border ",
    tableName: 'Information of language setting',
    tableRowIDInternalName: "id",
    tableColDef: [
      { headerName: 'Portal', width: '10%', internalName: 'portalCode', sort: true, type: "" },
      { headerName: 'Language', width: '30%', internalName: 'languageCode', sort: true, type: "" },
      { headerName: 'CP Source', width: '60%', internalName: 'cpLnSource', sort: true, type: "" }
    ],
    enabledSearch: true,
    enabledSerialNo: true,
    pageSize: 15,
    enabledPagination: true,
    enabledEditBtn: true,
    enabledCellClick: true,
    enabledColumnFilter: false,
  };
}