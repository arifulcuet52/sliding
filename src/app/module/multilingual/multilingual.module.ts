import { NgModule } from '@angular/core';
import {Routes,RouterModule,Route} from '@angular/router'
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MultilingualComponent } from './multilingual/multilingual.component';
import{PTableModule} from './../../shared/modules';
import {MultilingualService} from './../../shared/service/multilingual-service';
import { LanguageSettingComponent } from './language-setting/language-setting.component'
const routes:Routes=[
  {        
      path: '',
      children: [
          {path:'',component:MultilingualComponent}, 
          {path:'language-setting',component:LanguageSettingComponent},  
      ]
  } 
]
@NgModule({
  imports: [
    CommonModule,RouterModule.forChild(routes),PTableModule,FormsModule
  ],
  declarations: [MultilingualComponent, LanguageSettingComponent],
  providers:[MultilingualService]
})
export class MultilingualModule { }


