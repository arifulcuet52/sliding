import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/service/remote/user/user.service';
import { User } from '../../entity/user';
import * as feather from 'feather-icons';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private userService: UserService) { }
  models: User[] = [];
  errorMessage:string;
  ngOnInit() {
    feather.replace();
    this.getAll();
  }
  getAll() {
    // test purpose
    this.userService.getAll("").subscribe(
      (success: User[]) => {      
        this.models = success;     
      },
      (error) => { console.log("asas")},

    );
  }
}
