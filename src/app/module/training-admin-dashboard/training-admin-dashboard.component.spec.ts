import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingAdminDashboardComponent } from './training-admin-dashboard.component';

describe('TrainingAdminDashboardComponent', () => {
  let component: TrainingAdminDashboardComponent;
  let fixture: ComponentFixture<TrainingAdminDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingAdminDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingAdminDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
