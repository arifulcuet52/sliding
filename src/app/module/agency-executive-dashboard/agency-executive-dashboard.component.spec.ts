import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgencyExecutiveDashboardComponent } from './agency-executive-dashboard.component';

describe('AgencyExecutiveDashboardComponent', () => {
  let component: AgencyExecutiveDashboardComponent;
  let fixture: ComponentFixture<AgencyExecutiveDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgencyExecutiveDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgencyExecutiveDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
