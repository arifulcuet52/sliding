import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdelModalComponent } from './idel-modal.component';

describe('IdelModalComponent', () => {
  let component: IdelModalComponent;
  let fixture: ComponentFixture<IdelModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdelModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdelModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
