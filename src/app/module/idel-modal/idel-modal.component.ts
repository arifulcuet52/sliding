import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-idel-modal',
  templateUrl: './idel-modal.component.html',
  styleUrls: ['./idel-modal.component.css']
})
export class IdelModalComponent implements OnInit {

  constructor(public bsModalRef: BsModalRef) { }
  msg: string;

  ngOnInit() {
  }

}
