import { SalesPolicyFileUploadService } from './../../shared/service/remote/sales-policy-file-upload/sales-policy-file-upload.service';
import { UtilityService } from 'src/app/shared/service/utility/utility.service';
import { Component, OnInit } from '@angular/core';
import { DocumentUploadSettingVm } from 'src/app/entity/document.upload.setting.vm';
import swal from 'sweetalert2';

@Component({
  selector: 'app-sales-policy-file-upload',
  templateUrl: './sales-policy-file-upload.component.html',
  styleUrls: ['./sales-policy-file-upload.component.scss']
})
export class SalesPolicyFileUploadComponent implements OnInit {

  fileType: string;
  fileObject: File = null;
  fileName: string;
  fileSetting: any;
  formData: FormData = new FormData();

  constructor(private utilityService: UtilityService,
              private salesPolicyFileUploadService: SalesPolicyFileUploadService) { }

  ngOnInit() {
    this.loadComponent();
  }

  loadComponent(){

    this.utilityService.get_file_settings().subscribe(res => {
      this.fileSetting = res.data;
    });

    this.utilityService.get_document_upload_setting().subscribe(
      (sucees: DocumentUploadSettingVm[]) => {
        // this.profilePictureFileType = sucees.filter(x => x.gloablKey === "NationalIDCard")[0].imageType;
        this.fileType = sucees.filter(x => x.gloablKey === 'SalesPolicyFileUpload')[0].imageType;
       // console.log('hi' + this.fileType);
      },
      error => {
        console.log(error);
      }
    );
  }

  save(){
    this.salesPolicyFileUploadService.saveFile(this.formData).subscribe(
      (success) => {
        console.log(success);
        swal({
          title: 'success!',
          type: 'success',
          confirmButtonText: 'Close',
          text: success.msg
        });
      },
      (error) => {
        console.log(error);
        swal({
          title: 'Warning!',
          type: 'warning',
          confirmButtonText: 'Close',
          text: error.error.message
        });
      }
    );
  }

  onFileChange(event) {
    if (event.target.files.length > 0
      && this.fileValidate(event.target.files[0], this.fileType, this.fileSetting.fileSize)) {
      this.fileObject = event.target.files[0];
      if (this.formData.has('fileId')) {
        this.formData.delete('fileId');
      }
      this.formData.append('fileId', this.fileObject, this.fileObject['name']);
      this.fileName = this.fileObject.name;
    }
  }

  fileValidate(file: File, allowFileType?: string, allowMaxSize?: number) {
    let extention = '';
    let size = 0;
    let valid = false;

    if (file.name) {
      extention = this.fileExtention(file.name);
    }

    if (file.size) {
      size = this.fileToMB(file.size);
    }

    if (!allowFileType || (extention && allowFileType.includes(extention))) {
      if (!allowMaxSize || (size && size <= allowMaxSize)) {
        valid = true;
      }
    } else {
      swal({
        title: 'Warning!',
        type: 'warning',
        confirmButtonText: 'Close',
        text: `File type should be ${allowFileType} & max ${allowMaxSize} MB size of file are supported`
      });
    }

    return valid;
  }

  fileExtention(filename: string) {
    return filename.substring(filename.lastIndexOf('.') + 1, filename.length) || filename;
  }

  fileToMB(size: number) {
    if (size <= 0) {
      return 0;
    }

    return Math.ceil(size / (1024 * 1024));
  }

}
