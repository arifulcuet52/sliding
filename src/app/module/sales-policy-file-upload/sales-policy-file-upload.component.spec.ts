import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesPolicyFileUploadComponent } from './sales-policy-file-upload.component';

describe('SalesPolicyFileUploadComponent', () => {
  let component: SalesPolicyFileUploadComponent;
  let fixture: ComponentFixture<SalesPolicyFileUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesPolicyFileUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesPolicyFileUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
