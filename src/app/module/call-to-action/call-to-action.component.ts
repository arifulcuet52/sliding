import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/app/shared/service/utility/utility.service';
import { Router } from '@angular/router';
import '../../entity/extension/array.extension';

@Component({
  selector: 'app-call-to-action',
  templateUrl: './call-to-action.component.html',
  styleUrls: ['./call-to-action.component.scss']
})
export class CallToActionComponent implements OnInit {
  callToActions: any = [];
  constructor(private utilityService: UtilityService, private router: Router) {}

  ngOnInit() {
    this.utilityService.get_call_to_actions('AP').subscribe(res => {
      let data = res.data as Array<any>;
      data = data.filter(x => x['isShow'] === true);
      this.callToActions = data.orderByPropertyNames('sortOrder', 'actionName');
    });
  }

  routeTo(routeLink: string) {
    if (routeLink) {
      if (routeLink.includes('?')) {
        const params = {};
        const allQuery = routeLink.split('&');
        for (let index = 0; index < allQuery.length; index++) {
          const element = allQuery[index];

          let key = '';
          let value = '';
          if (index === 0) {
            const firstElement = allQuery[0].substring(
              allQuery[0].indexOf('?') + 1
            );
            key = firstElement.substring(0, firstElement.indexOf('='));
            value = firstElement.substring(firstElement.indexOf('=') + 1);
          } else {
            key = element.substring(0, element.indexOf('='));
            value = element.substring(element.indexOf('=') + 1);
          }
          params[key] = value;
        }
        routeLink = routeLink.substring(0, routeLink.indexOf('?'));
        this.router.navigate([routeLink], {
          queryParams: params
        });
      } else {
        this.router.navigate([routeLink]);
      }
    }
  }
}
