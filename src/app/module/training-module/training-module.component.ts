import { Component, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable, of as observableOf } from 'rxjs';
import { TrainingModuleService } from '../../shared/service/remote/exam/training-module.service';
import { TrainingModule } from '../../entity/training-module';
import {
  MatSort,
  MatDialog,
  MatDialogConfig,
  MatTableDataSource
} from '@angular/material';
import { DialogTrainingModuleComponent } from '../../dialog/dialog-training-module/dialog-training-module.component';
import { ActivatedRoute, Router } from '@angular/router';
import swal from 'sweetalert2';
import * as $ from 'jquery';
@Component({
  selector: 'app-training-module-list',
  templateUrl: './training-module.component.html',
  styleUrls: ['./training-module.component.css']
})
export class TrainingModuleComponent implements OnInit {
  @ViewChild(MatSort)
  sort: MatSort;
  searchText = '';
  pageSize = 10;
  pageIndex = 1;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  dataSource: any;
  tempData: any;
  displayedColumns: string[] = ['sl', 'moduleName',  'title', 'isShow', 'moduleType', 'action'];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private tms: TrainingModuleService
  ) {}

  ngOnInit() {
    this.search();
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.search();
  }

  search() {
    this.tms
      .getall(this.searchText, this.pageSize, this.pageIndex)
      .subscribe(res => {
        this.dataSource = new MatTableDataSource(res['data'].data);
        this.dataSource.sort = this.sort;
        this.length = res['data'].totalItem;
      });
  }

  filter(searchText: string) {
    this.searchText = searchText;
    this.pageIndex = 1;
    this.search();
  }

  /** ------------------------- */

  open_dialog(model: any) {
    console.log(model, 'open');
    window.scrollTo(0, 0);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { model: model };
    dialogConfig.width = '400px';
    return this.dialog.open(DialogTrainingModuleComponent, dialogConfig);
  }
  add() {
    this.open_dialog(new TrainingModule())
      .afterClosed()
      .subscribe(data => {
        this.search();
      });
  }
  edit(id, index) {
    const bbb = Object.assign({}, this.dataSource.data[index]);
    this.open_dialog(bbb)
      .afterClosed()
      .subscribe(data => {
        this.search();
      });
  }

  delete(id, index) {
    this.tms.delete(id).subscribe(
      success => {
        this.search();
        swal({
          title: '',
          text: 'Delete successfully',
          type: 'success',
          confirmButtonText: 'Close'
        });
      },
      error => {
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).error.message,
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    );
  }
  onClick() {
    $('#imgupload').trigger('click');
  }


  changeSortOrder(obj, index, isLevelUp){
    this.tms.changeSortOrder(obj.id, {'isLevelUp': isLevelUp}).subscribe(res => {
      if (res.data.length === 2){
        const data = this.dataSource.data;
        if (isLevelUp){
          data[index - 1] = res.data[0];
          data[index] = res.data[1];
        }else{
          data[index] = res.data[0];
          data[index + 1] = res.data[1];
        }
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
      }

    });
  }
}
