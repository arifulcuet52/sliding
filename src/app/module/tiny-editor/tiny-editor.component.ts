import {
  Component,
  AfterViewInit,
  EventEmitter,
  OnDestroy,
  Input,
  Output,
  OnInit
} from '@angular/core';
import 'tinymce/tinymce.min';
import 'tinymce/themes/modern/theme.min';

import 'tinymce/plugins/table/plugin.min';
import 'tinymce/plugins/link/plugin.min';
import 'tinymce/plugins/lists/plugin.min';

declare var tinymce: any;

@Component({
  selector: 'app-tiny-editor',
  template: `
    <textarea id="{{elementId}}">{{ inputValue }}</textarea>
  `
})
export class TinyEditorComponent implements AfterViewInit, OnDestroy, OnInit {
  @Input() elementId: String;
  @Input() inputValue: String;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onEditorContentChange = new EventEmitter();

  editor;

  ngOnInit(): void {}
  ngAfterViewInit() {
    tinymce.init({
      menubar: false,
      selector: '#' + this.elementId,
      plugins: ['link', 'table', 'lists'],
      skin_url: 'assets/skins/lightgray',
      // tslint:disable-next-line:max-line-length
      toolbar: [
        ' undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat'
      ],
      setup: editor => {
        this.editor = editor;
        editor.on('keyup change', () => {
          const content = editor.getContent();
          this.onEditorContentChange.emit(content);
        });
      }
    });
  }

  ngOnDestroy() {
    tinymce.remove(this.editor);
  }

  keyupHandler($event) {
    console.log($event);
  }
}
