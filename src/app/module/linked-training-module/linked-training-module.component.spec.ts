import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkedTrainingModuleComponent } from './linked-training-module.component';

describe('LinkedTrainingModuleComponent', () => {
  let component: LinkedTrainingModuleComponent;
  let fixture: ComponentFixture<LinkedTrainingModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkedTrainingModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkedTrainingModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
