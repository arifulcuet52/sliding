import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { TrainingModuleService } from '../../shared/service/remote/exam/training-module.service';
import { LinkedTrainingModule, LinkedSubModule } from '../../entity/trainingContent/linked-training-module';
import { TrainingModule } from '../../entity/training-module';
import { TrainingContentService } from '../../shared/service/remote/training-content/training-content.service';
import { presentation, slides } from '../../entity/trainingContent/Presentation';
import swal from 'sweetalert2';
import { TrainingModuleEnum } from '../../entity/enums/enum';

@Component({
  selector: 'app-linked-training-module',
  templateUrl: './linked-training-module.component.html',
  styleUrls: ['./linked-training-module.component.scss']
})
export class LinkedTrainingModuleComponent implements OnInit {
  public moduleLinkingForm: FormGroup;
  public linkedSubModuleForm: FormGroup;
  public allModuleSubModuleInfo: TrainingModule[] = [];
  public moduleDDList: TrainingModule[] = [];
  public subModuleDDList: TrainingModule[] = [];
  public presentationDDList: presentation[] = [];
  public slideSerial: slides[] = [];
  public displayedColumns: string[] = ['sl', 'moduleName', 'presentationName', 'action'];
  public moduleSettingsData: any[] = [];

  public linkedModuleModel: LinkedTrainingModule = new LinkedTrainingModule();
  constructor(private trainingModuleService: TrainingModuleService, private trainingContentService: TrainingContentService, private fb: FormBuilder) {
    this.fnSetDefaultForm();
  }

  createSubModule(): FormGroup {
    return this.fb.group({
      subModuleId: null,
      startSlideSerialNo: null,
      endSlideSerialNo: null,
      linkedSubModuleId: null
    });
  }

  ngOnInit() {
    this.fnGetAllModuleSettingsList();
    this.getModule();
    this.getPresentation();
    if (this.linkedModuleModel.presentationId > 0) {
      this.fnGetPresentationSlide(this.linkedModuleModel.presentationId);
    }
  }

  getModule() {
    this.moduleDDList = [];
    this.trainingModuleService.getDropdown().subscribe(
      (response: any) => {
        console.log('response:', response);
        this.allModuleSubModuleInfo = response || [];
        this.moduleDDList = this.allModuleSubModuleInfo.filter((rec: TrainingModule) => {
          if (rec.moduleType == TrainingModuleEnum.module) {
            return true;
          } else {
            return false;
          }
        });

        this.subModuleDDList = this.allModuleSubModuleInfo.filter((rec: TrainingModule) => {
          if (rec.parentId == this.linkedModuleModel.moduleId) {
            return true;
          } else {
            return false;
          }
        });
      },
      error => { }
    );
  }



  getPresentation() {
    this.trainingContentService.getAllPresentation().subscribe((rec: any) => {
      this.presentationDDList = rec.data || [];
      console.log('presention name', rec)
    });
  }

  fnGetPresentationSlide(presentaionId: number) {
    if (presentaionId > 0) {
      this.trainingContentService.getAllSlidesByPresentationId(presentaionId).subscribe((rec: any) => {
        console.log('slide details: ', rec);
        this.slideSerial = this.fnColumnSorting(rec.data.slideDetails || [], 'order', 'asc');
      });
    }
  }

  fnGetSubModuleDropDownByModuleId(moduleId: number) {
    this.subModuleDDList = this.allModuleSubModuleInfo.filter((rec: TrainingModule) => {
      if (rec.parentId == moduleId) {
        return true;
      } else {
        return false;
      }
    }) || [];
  }


  fnChangeModuleName(moduleId: number) {
    this.fnGetSubModuleDropDownByModuleId(moduleId);

    let arrayLength = (<FormArray>this.moduleLinkingForm.get('subModuleInfo')).length;
    for (let i = 0; i < arrayLength; i++) {
      (<FormArray>this.moduleLinkingForm.get('subModuleInfo')).removeAt(i);
    }

    if ((<FormArray>this.moduleLinkingForm.get('subModuleInfo')).length == 0) {
      (<FormArray>this.moduleLinkingForm.get('subModuleInfo'))
        .push(this.createSubModule());
    }
  }

  fnRemoveRecord(index: number) {
    let subModule: LinkedTrainingModule = this.moduleLinkingForm.value;
    var removeableRecord: LinkedSubModule = subModule.subModuleInfo[index];
    console.log(removeableRecord);
    if (removeableRecord.linkedSubModuleId > 0) {
      this.trainingModuleService.fnCheckDependencyBeforeDeleteSubModule(removeableRecord.linkedSubModuleId).subscribe(
        (rec: any) => {
          if (rec.msgCode == 'err') {
            alert(rec.msg);
          } else {
            (<FormArray>this.moduleLinkingForm.get('subModuleInfo')).removeAt(index);
          }
        }
      );
    } else {
      (<FormArray>this.moduleLinkingForm.get('subModuleInfo')).removeAt(index);
    }

  }

  fnGetSubModuleDropdown(index: number): TrainingModule[] {
    let subModule: LinkedTrainingModule = this.moduleLinkingForm.value;
    var selectedRecord: LinkedSubModule = subModule.subModuleInfo[index];
    let filteredRecord: TrainingModule[] = this.subModuleDDList.filter(
      (rec: TrainingModule) => {
        let filterdSubmoduleRecord = subModule.subModuleInfo.filter(x => x.subModuleId == rec.id && x.subModuleId != selectedRecord.subModuleId) || null;
        if (filterdSubmoduleRecord.length != 0) {
          return false;
        } else {
          return true;
        }
      }
    );
    return filteredRecord;
  }

  fnSubmitForm() {
    console.log(this.moduleLinkingForm.value);
    let subModule: LinkedTrainingModule = this.moduleLinkingForm.value;  
    let finalModel: LinkedTrainingModule = new LinkedTrainingModule();
    finalModel.moduleId = subModule.moduleId;
    finalModel.presentationId = subModule.presentationId;
    subModule.subModuleInfo.forEach(
      (rec: LinkedSubModule) => {
        let prevRecord = this.linkedModuleModel.subModuleInfo.filter(x => x.linkedSubModuleId == rec.linkedSubModuleId);
        if (rec.linkedSubModuleId > 0 && prevRecord != null) {
          finalModel.subModuleInfo.push(
            {
              linkedSubModuleId: rec.linkedSubModuleId, subModuleId: rec.subModuleId, startSlideSerialNo: rec.startSlideSerialNo,
              endSlideSerialNo: rec.endSlideSerialNo, statusId: prevRecord[0].statusId, createdBy: prevRecord[0].createdBy,
              created: prevRecord[0].created, updatedBy: prevRecord[0].updatedBy, updated: prevRecord[0].updated
            });
        } else {
          finalModel.subModuleInfo.push(rec);
        }
      }
    );

    this.trainingModuleService.fnAddLinkedSubModule(finalModel).subscribe(
      (rec: any) => {
        console.log("success", rec);
        this.fnGetAllModuleSettingsList();
        this.fnSetDefaultForm();
        swal({
          title: 'Success!',
          text: rec.msg,
          type: 'success',
          confirmButtonText: 'Close'
        });
      },
      (error: any) => {
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).error,
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    );
  }

  fnGetAllModuleSettingsList() {
    this.trainingModuleService.fnGetAllModuleSettins().subscribe(
      (rec: any) => {
        console.log(rec);
        this.moduleSettingsData = rec.data || [];
      }
    );
  }

  fnGetSubModuleSettingInfo(moduleId: number) {
    this.trainingModuleService.fnGetSubModuleInfoByModuleId(moduleId).subscribe(
      (rec: any) => {
        console.log('success data: ', rec.data || new LinkedTrainingModule());
        this.linkedModuleModel = rec.data || new LinkedTrainingModule();
        this.fnSetPreviousData();
        if (this.linkedModuleModel.presentationId > 0) {
          this.fnGetPresentationSlide(this.linkedModuleModel.presentationId);
        }

        if (this.linkedModuleModel.moduleId > 0) {
          this.fnGetSubModuleDropDownByModuleId(this.linkedModuleModel.moduleId);
        }
      },
      (error: any) => {
        console.log('error');
      }
    );
  }

  add(index: number) {
    console.log(index);
    (this.moduleLinkingForm.get('subModuleInfo') as FormArray).push(this.createSubModule());
  }

  fnSetPreviousData() {
    this.fnSetDefaultForm();
    this.moduleLinkingForm.patchValue(this.linkedModuleModel); // to set previous values..
    for (let index = 0; index < this.linkedModuleModel.subModuleInfo.length; index++) {
      (<FormArray>this.moduleLinkingForm.get('subModuleInfo'))
        .push(this.createSubModule());

      (<FormArray>this.moduleLinkingForm.get('subModuleInfo'))
        .controls[index].get('subModuleId').setValue(this.linkedModuleModel.subModuleInfo[index].subModuleId);
      (<FormArray>this.moduleLinkingForm.get('subModuleInfo'))
        .controls[index].get('startSlideSerialNo').setValue(this.linkedModuleModel.subModuleInfo[index].startSlideSerialNo);
      (<FormArray>this.moduleLinkingForm.get('subModuleInfo'))
        .controls[index].get('endSlideSerialNo').setValue(this.linkedModuleModel.subModuleInfo[index].endSlideSerialNo);

      (<FormArray>this.moduleLinkingForm.get('subModuleInfo'))
        .controls[index].get('linkedSubModuleId').setValue(this.linkedModuleModel.subModuleInfo[index].linkedSubModuleId);
    }
  }


  fnSetDefaultForm() {
    this.moduleLinkingForm = this.moduleLinkingForm = this.fb.group({
      moduleId: [null, [Validators.required]],
      presentationId: [null, [Validators.required]],
      subModuleInfo: this.fb.array([])
    });
  }

  fnColumnSorting(dataset: any, colName: any, sortingOrder: string = 'asc'): any[] {
    if (dataset.length < 1) {
      return dataset;
    }

    if (sortingOrder === 'asc') {
      dataset = dataset.sort((n1, n2) => {
        if (n1[colName] > n2[colName]) { return 1; }
        if (n1[colName] < n2[colName]) { return -1; }

        return 0;
      });

    } else {
      dataset = dataset.sort((n1, n2) => {
        if (n1[colName] < n2[colName]) { return 1; }
        if (n1[colName] > n2[colName]) { return -1; }
        return 0;
      });
    }

    return dataset;
  }

  fnEditedit(moduleId: number) {
    this.fnGetSubModuleSettingInfo(moduleId);
  }

  fnDelete(moduleId: number) {
    swal({
      title: 'Warning!',
      text: 'Are you sure you want to delete this?',
      type: 'warning',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
    }).then(res => {
      if (res.value) {
        this.trainingModuleService.fnDeleteModuleSettingsById(moduleId).subscribe(
          (success) => {
            this.fnGetAllModuleSettingsList();
          },
          (error) => {
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error,
              type: 'error',
              confirmButtonText: 'Close'
            });
          }
        );
      }
    });
  }

  fnResetForm() {
    this.fnSetDefaultForm();
  }

}

