import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CandidateReferenceService } from '../../../shared/service/remote/candidate-reference/candidate-reference.service';
import { Reference } from '../../../entity/reference';
import {
  ReferenceStatusEnum,
  ProfileVerificationGlobalKeys,
  ReferenceVerificationStatus
} from '../../../entity/enums/enum';
import swal from 'sweetalert2';
import { NotificationService } from '../../../shared/service/remote/notification/notification.service';
import { GeneralSettingsService } from '../../../shared/service/remote/general-settings/general-settings.service';
import { ProfileStatusService } from '../../../shared/service/remote/profile-status/profile-status.service';
import { ProfileStatusVm } from '../../../entity/profile-status-vm';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { DialogReferenceRejectComponent } from '../../../dialog/dialog-reference-reject/dialog-reference-reject.component';
import { AccountService } from 'src/app/shared/service/remote/authentication/account.service';
import { DialogReferenceActionsComponent } from '../../../dialog/dialog-reference-actions/dialog-reference-actions.component';
import { ReferenceActivity } from '../../../entity/reference-activity';

@Component({
  selector: 'app-reference',
  templateUrl: './reference.component.html',
  styleUrls: ['./reference.component.scss']
})
export class ReferenceComponent implements OnInit {
  public candidateId: number;
  public references: Reference[] = [];
  public rejected = 0;
  public accepted = 0;
  public verificationNumber = 0;
  public minReqRef: number;
  public minApprovedRef: number;
  public minRejectRef: number;
  public docVerificationStatus: ProfileStatusVm;
  public policyVerificationStatus: ProfileStatusVm;
  public refVerificationStatus: ProfileStatusVm;
  public listOfStatus: ProfileStatusVm[] = [];
  public isActionHistoryShown: boolean = false;
  public actionList: ReferenceActivity[] = [];
  public filteredActions: ReferenceActivity[] = [];

  constructor(
    private _route: ActivatedRoute,
    private candidateReferenceService: CandidateReferenceService,
    private notificationService: NotificationService,
    private dialog: MatDialog,
    private generalSettingService: GeneralSettingsService,
    private profileStatusService: ProfileStatusService,
    public account: AccountService
  ) {}

  ngOnInit() {
    this._route.params.subscribe(params => {
      this.candidateId = parseInt(params.id);
    });
    this.getCandidateReferences();
    this.getActionList(this.candidateId);
    this.getRequiredNumbers();
  }

  getRequiredNumbers() {
    this.generalSettingService.get().subscribe(
      success => {
        this.minReqRef = success.data.minReqReferenceNo;
        this.minApprovedRef = success.data.minRefApprovalRequired;
        this.minRejectRef = this.minReqRef - this.minApprovedRef;
      },
      error => {
        console.log(error);
      }
    );
  }

  approvePressed(model: Reference) {
    const tempStatus = model.statusId;

    const swalWithButtons = swal.mixin({
      confirmButtonClass: 'btn btn-success mx-3',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    });

    swalWithButtons({
      title: 'Are you sure?',
      text: 'Are you sure the referee information is valid?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, approve it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: false
    }).then(result => {
      if (result.value) {
        model.statusId = ReferenceStatusEnum.accepted;
        this.updateReference(model, tempStatus);
      } else if (result.dismiss === swal.DismissReason.cancel) {
      }
    });
  }

  rejectPressed(model: Reference) {
    // const model = this.set_model_property(index);
    const tempStatus = model.statusId;

    this.open_dialog(model)
      .afterClosed()
      .subscribe(remarks => {
        console.log(remarks);
        if (remarks) {
          model.remarks = remarks;
          model.statusId = ReferenceStatusEnum.rejected;
          this.updateReference(model, tempStatus);
        }
      });
  }

  open_dialog(data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { model: data };
    dialogConfig.width = '400px';
    return this.dialog.open(DialogReferenceRejectComponent, dialogConfig);
  }

  takeActions(model){
    this.open_take_action_dialog(model)
      .afterClosed()
      .subscribe((res=>{
        if(res){
          this.getActionList(model.candidateId);
        }
      }));
  }

  open_take_action_dialog(data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { model: data };
    dialogConfig.width = '400px';
    return this.dialog.open(DialogReferenceActionsComponent, dialogConfig);
  }

  showActionHistories(index){
    this.references[index].isActionHistoryShown = !this.references[index].isActionHistoryShown;
  }

  getActionList(candidateId: number){
    this.candidateReferenceService.getReferenceActionsByCandidate(candidateId).subscribe(
      (succ) => {
        console.log(succ);
        this.actionList = succ.data;
        this.references.forEach(element => {
          element.actionList = this.actionList.filter(
            action => action.referenceInfoId === element.id
          );
        });
        console.log(this.references);
      },
      (err) =>{
        console.log(err);
      }
    );
  }

  checkForCompletion() {
    this.profileStatusService
      .getAllProfileStatusByCandidate(this.candidateId)
      .subscribe(
        (success: ProfileStatusVm[]) => {
          this.listOfStatus = success;
          this.refVerificationStatus = this.getSpecifiedProfileStatusVm(this.listOfStatus,ProfileVerificationGlobalKeys.referenceInfoVerificationStatus
          );
          /* this.docVerificationStatus = this.getSpecifiedProfileStatusVm(this.listOfStatus, ProfileVerificationGlobalKeys.reqDocVerificationStatus);
        this.policyVerificationStatus = this.getSpecifiedProfileStatusVm(this.listOfStatus, ProfileVerificationGlobalKeys.policyNumberVerificationStatus); */

          if (
            this.refVerificationStatus.status ==
            ReferenceVerificationStatus.complete /* &&
           this.docVerificationStatus.globalKey == ReferenceVerificationStatus.complete &&
           this.policyVerificationStatus.globalKey == ReferenceVerificationStatus.complete */
          ) {
            this.notifyOnVerificationCompletion();
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  notifyOnVerificationCompletion() {
    this.notificationService
      .notifyOnVerificationComplete(this.candidateId)
      .subscribe(
        success => {
          console.log(success);
        },
        error => {
          console.log(error);
        }
      );
  }

  getSpecifiedProfileStatusVm(
    listOfprofileStatusVm: ProfileStatusVm[],
    key: string
  ): ProfileStatusVm {
    let specifiedProfileStatusVm: ProfileStatusVm = new ProfileStatusVm();

    listOfprofileStatusVm.forEach(element => {
      if (element.globalKey == key) {
        specifiedProfileStatusVm = element;
      }
    });
    return specifiedProfileStatusVm;
  }

  updateReference(model: Reference, tempStatus) {
    this.candidateReferenceService.updateReferenceStatus(model).subscribe(
      success => {
        // this.verificationNumber += 1;
        this.notify(success.results);
      },
      error => {
        model.statusId = tempStatus;
        model.remarks = null;
        console.log(error);
        this.show_error(error, 'Unable to update.');
      }
    );
  }

  notify(model) {
    this.accepted = 0;
    this.rejected = 0;
    this.candidateReferenceService
      .getReferenceInfoByCandidateId(model.candidateId)
      .subscribe(
        success => {
          console.log(success.data);
          success.data.forEach(model => {
            if (model.statusId === ReferenceStatusEnum.accepted) {
              this.accepted += 1;
            }
            if (model.statusId === ReferenceStatusEnum.rejected) {
              this.rejected += 1;
            }
          });

          if (this.accepted == this.minApprovedRef) {
            //send accept noti
            // this.accepted = 0;
            this.notificationService
              .notifyOnRefAccept(model.candidateId)
              .subscribe(
                success => {
                  console.log(success);
                },
                error => {
                  console.log(error);
                }
              );

            this.checkForCompletion();
          } else if (this.minRejectRef + 1 == this.rejected) {
            //send reject noti
            // this.rejected = 0;
            this.notificationService
              .notifyOnRefReject(model.candidateId)
              .subscribe(
                success => {
                  console.log(success);
                },
                error => {
                  console.log(error);
                }
              );
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  getStatus(model: Reference) {
    const class_attr = 'custom-control custom-radio';
    if (model.statusId === ReferenceStatusEnum.pending || model.statusId === ReferenceStatusEnum.resend) {
      model.isPending = true;
      return class_attr + ' my-checkbox my-checkbox-2';
    } else if (model.statusId === ReferenceStatusEnum.accepted) {
      model.isPending = false;
      return class_attr + ' my-checkbox my-checkbox-1';
    } else if (model.statusId === ReferenceStatusEnum.rejected) {
      model.isPending = false;
      return class_attr + ' my-checkbox my-checkbox-3';
    } 
    else {
      model.isPending = false;
      return class_attr;
    }
  }

  getCandidateReferences() {
    this.candidateReferenceService
      .getReferenceInfoByCandidateId(this.candidateId)
      .subscribe(
        success => {
          this.references = success.data;
        },
        error => {
          console.log(error);
        }
      );
  }

  show_error(error: any, title) {
    swal({
      title: title,
      text: JSON.parse(JSON.stringify(error)).error.message,
      type: 'warning',
      confirmButtonText: 'Close'
    });
  }

  show_success(message) {
    swal({
      title: 'Success!',
      text: message,
      type: 'success',
      confirmButtonText: 'Close'
    });
  }
}
