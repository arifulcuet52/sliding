import { PolicyVerificationService } from './../../../shared/service/remote/policy-verification/policy-verification.service';

import { Component, OnInit, AfterViewInit } from '@angular/core';
import swal from 'sweetalert2';
import { policyEnum } from 'src/app/entity/enums/enum';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from 'src/app/shared/service/remote/authentication/account.service';

@Component({
  selector: 'app-policy-number',
  templateUrl: './policy-number.component.html',
  styleUrls: ['./policy-number.component.scss']
})
export class PolicyNumberComponent implements OnInit, AfterViewInit {
  polycies: any = [];
  minReqPolicyNo = 0;
  enumPolicy = policyEnum;
  constructor(
    private policyService: PolicyVerificationService,
    private activatedRoute: ActivatedRoute,
    public account: AccountService
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(x => {
      this.policyService.getbyuser(x['id']).subscribe(res => {
        this.polycies = res.data;
      });
    });
  }
  ngAfterViewInit(): void {
    // alert('fire');
    // this.policyService.expandEmitter.subscribe(x => {
    //   if (x == true) {
    //     alert('fire');
    //     this.expandPolicyNum();
    //   }
    // });
    this.policyService.expanPolicy();
  }
  reject(index, model) {
    swal({
      title: 'Are you sure you want to reject?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then(result => {
      if (result.value) {
        this.policyService.reject(model.id, model).subscribe(
          res => {
            this.polycies[index] = res.data;
          },
          error => {
            this.show_error(error);
          }
        );
      }
    });
  }

  accept(index, model) {
    swal({
      title: 'Are you sure you want to approve?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then(result => {
      if (result.value) {
        this.policyService.approve(model.id, model).subscribe(
          res => {
            this.polycies[index] = res.data;
          },
          error => {
            this.show_error(error);
          }
        );
      }
    });
  }

  get_class_att(statusId: number) {
    const class_attr = 'custom-control custom-radio';
    if (statusId === this.enumPolicy.pending) {
      return class_attr + ' my-checkbox my-checkbox-2';
    } else if (statusId === this.enumPolicy.accepted) {
      return class_attr + ' my-checkbox my-checkbox-1';
    } else if (statusId === this.enumPolicy.rejected) {
      return class_attr + ' my-checkbox my-checkbox-3';
    } else {
      return class_attr;
    }
  }

  show_error(error: any) {
    swal({
      title: 'Warning!',
      text: JSON.parse(JSON.stringify(error)).error.message,
      type: 'warning',
      confirmButtonText: 'Close'
    });
  }

  show_success(message) {
    swal({
      title: 'Success!',
      text: message,
      type: 'success',
      confirmButtonText: 'Close'
    });
  }
  expandPolicyNum() {
    console.log('expandPolicyNum');
    const policyDiv = document.getElementById(
      'policyNumberTitle'
    ) as HTMLElement;
    policyDiv.getElementsByTagName('span')[0].click();
  }
}
