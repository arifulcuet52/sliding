import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from 'src/app/shared/service/remote/authentication/account.service';

@Component({
  selector: 'app-candidate-profile',
  templateUrl: './candidate-profile.component.html',
  styleUrls: ['./candidate-profile.component.scss']
})
export class CandidateProfileComponent implements OnInit, AfterViewInit {

  constructor(private activatedRoute: ActivatedRoute, public account: AccountService) { }

  ngOnInit() {
    const code = this.activatedRoute.snapshot.queryParamMap.get('tab');
    this.selectNextTab(code);
  }
  selectNextTab(code: string) {
    const element = document.getElementById(code) as HTMLElement;
    if (element) {
      element.click();
    }
  }
  ngAfterViewInit(): void {
    const code = this.activatedRoute.snapshot.queryParamMap.get('tab');
    this.selectNextTab(code);
  }
}
