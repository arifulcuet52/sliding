import { UtilityCandidateService } from './../../../shared/service/remote/utility-candidate/utility-candidate.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import * as feather from 'feather-icons';

@Component({
  selector: 'app-academic-qualification',
  templateUrl: './academic-qualification.component.html',
  styleUrls: ['./academic-qualification.component.scss']
})
export class AcademicQualificationComponent implements OnInit {
  constructor(
    private activeRoute: ActivatedRoute,
    private utilityService: UtilityCandidateService
  ) { }

  public educations = [];
  public boards = [];
  public listOfQualifics = [];
  public model: any;
  public highestEduLevel: number;
  public candidateId: number;


  ngOnInit() {
    feather.replace();
    this.activeRoute.params.subscribe(params => {
      this.candidateId = parseInt(params.id, 10);
      this.getQualification(this.candidateId);
    });
    this.loadComponent();
  }

  loadComponent() {
    this.utilityService.get_criteria_edu().subscribe(res => {
      this.educations = res.data;
    });

    this.utilityService.get_all_boards().subscribe(res => {
      this.boards = res.data;
    });
  }




  getQualification(candidateId: number) {
    this.utilityService.get_qualific_by_candidateId(candidateId).subscribe(
      async (success: any) => {
        this.listOfQualifics = success.data;
        console.log(this.listOfQualifics);

        this.listOfQualifics = await this.fnColumnSorting(this.listOfQualifics, 'passingYear', 'asc');

      },
      error => {
        console.log(error);
      }
    );
  }


  async fnColumnSorting(dataset: any, colName: any, sortingOrder: string = 'asc') {
    if (dataset.length < 1) {
      return dataset;
    }

    if (sortingOrder === 'asc') {
      dataset = dataset.sort((n1, n2) => {
        if (n1[colName] > n2[colName]) { return 1; }
        if (n1[colName] < n2[colName]) { return -1; }

        return 0;
      });

    } else {
      dataset = dataset.sort((n1, n2) => {
        if (n1[colName] < n2[colName]) { return 1; }
        if (n1[colName] > n2[colName]) { return -1; }
        return 0;
      });
    }

    return dataset;
  }

}
