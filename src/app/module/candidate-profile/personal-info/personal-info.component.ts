import { Component, OnInit } from '@angular/core';
import * as feather from 'feather-icons';
import { ActivatedRoute } from '@angular/router';
import { UtilityCandidateService } from 'src/app/shared/service/remote/utility-candidate/utility-candidate.service';
@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss']
})
export class PersonalInfoComponent implements OnInit {

  model: any;
  constructor(private activeRoute: ActivatedRoute, private utilityService: UtilityCandidateService) { }

  ngOnInit() {
    feather.replace();
    this.activeRoute.params.subscribe(params => {
      const candidateId = parseInt(params.id, 10);
      this.getCandidateProfileInfo(candidateId);
    });
  }

  getCandidateProfileInfo(id: number){
    this.utilityService.get_cadidate_profile_info(id).subscribe(res => {
      this.model = res.data;
      console.log(this.model);
    });
  }
}
