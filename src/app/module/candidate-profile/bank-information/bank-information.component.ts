import { UtilityCandidateService } from './../../../shared/service/remote/utility-candidate/utility-candidate.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BankInformation } from '../../../entity/BankInformation';
import * as feather from 'feather-icons';


@Component({
  selector: 'app-bank-information',
  templateUrl: './bank-information.component.html',
  styleUrls: ['./bank-information.component.scss']
})
export class BankInformationComponent implements OnInit {

  constructor(
    private activeRoute: ActivatedRoute,
    private utilityService: UtilityCandidateService
    ) { }

  public model: BankInformation = new BankInformation();
  public banks = [];
  public branches = [];
  public routingNumber = '';

  ngOnInit() {
    feather.replace();
    this.getBanks();
  }

  getBankInfoByCandidateId(candidateId: number) {
    this.utilityService.get_bank_info_by_candidateId(candidateId).subscribe(
      (success: any) => {
        if (success.data != null) {
          this.model = success.data;
          this.getBranch(true);
          this.getRoutingNumber(true);
        }
      },
      error => {
        console.log(error);
      }
    );
  }


  getBanks() {
    this.utilityService.get_all_banks().subscribe(
      (success: any) => {
        this.banks = success.data;
        this.activeRoute.params.subscribe(params => {
          const candidateId = parseInt(params.id, 10);
          this.getBankInfoByCandidateId(candidateId);
        });
      },
      error => {
        console.log(error);
      }
    );
  }

  getBranch(isSelected = false) {
    this.branches = [];
    this.routingNumber = '';
    if (!isSelected) {
      this.model.branchCode = '0';
    }

    if (this.model.bankCode !== '0') {
      this.banks.forEach(bank => {
        if (bank.bankCode === this.model.bankCode) {
          this.branches = bank.branches;
        }
      });
    }
  }

  getRoutingNumber(isSelected = false) {
    if (!isSelected) {
      this.routingNumber = '';
    }
    if (this.model.branchCode !== '0') {
      this.branches.forEach(branch => {
        if (branch.branchCode === this.model.branchCode) {
          this.routingNumber = branch.routingNumber;
        }
      });
    }
  }


}
