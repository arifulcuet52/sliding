import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import swal from 'sweetalert2';
import { DocumentUploadSettingVm } from 'src/app/entity/document.upload.setting.vm';
import { ProfileDocumentDetailVm } from 'src/app/entity/profile.document.detail.vm';
import { UtilityService } from 'src/app/shared/service/utility/utility.service';
import { ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap';
import { ShowDocumentComponent } from '../../show-document/show-document.component';
import { AccountService } from 'src/app/shared/service/remote/authentication/account.service';
import { Subscription } from 'rxjs';
let rejectSub: Subscription;
let acceptSub: Subscription;
@Component({
  selector: 'app-candidate-document-details',
  templateUrl: './candidate-document-details.component.html',
  styleUrls: ['./candidate-document-details.component.scss']
})
export class CandidateDocumentDetailsComponent implements OnInit {
  documentUploadSetting: DocumentUploadSettingVm[] = [];
  selectdFile: string;
  value: any;
  // micrCheque: ProfileDocumentDetailVm;
  // nationalIdCard: ProfileDocumentDetailVm;
  // profilePicture: ProfileDocumentDetailVm;
  // atpApplicationForm: ProfileDocumentDetailVm;
  // certificateFeeDetailForm: ProfileDocumentDetailVm;
  // summaryofFiledwork: ProfileDocumentDetailVm;
  // examAnswerSheet: ProfileDocumentDetailVm;
  // trainingCertificate: ProfileDocumentDetailVm;
  isDisplay = false;
  //userEduLabel: ProfileDocumentDetailVm[];
  //fixedDoc: ProfileDocumentDetailVm[];
  docList: ProfileDocumentDetailVm[];
  title: string;
  userId: number;
  fixedGlobalId = [
    'NationalIDCard',
    'Profile Picture',
    'ATP Application Form',
    'Certificate Fee Detail Form',
    'Exam Answer Sheet',
    'Summary of Filed work',
    'MICR Cheque leaf',
    'TrainingCertificate'
  ];
  constructor(
    private utilityService: UtilityService,
    private activatedRoute: ActivatedRoute,
    private showDocumentModalService: BsModalService,
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.selectdFile = '';
    // this.userEduLabel = [];
    // this.fixedDoc = [];
    this.docList = [];
    this.isDisplay = false;
    this.addAllFixedDocument();
    this.updateFixedDocTitle();
    this.getDocumentSetting(false);
    $('#mandatoryDocumentsTitle > h5 >span').trigger('click');
    this.getUserId();
    this.accept();
    this.rejectdoc();




    // this.policy.expandEmitter.subscribe(x => {
    //   console.log(x, 'child');
    //   this.expandPolicyNum();
    // });
  }

  updateFixedDocTitle() {
    // for (let index = 0; index < this.fixedDoc.length; index++) {
    //   const element = this.fixedDoc[index];
    //   if (element.globalKey == 'MICR Cheque leaf') {
    //     this.fixedDoc[index].title = 'MICR Cheque ';
    //     this.fixedDoc[index].spanText = 'Leaf / Bank Certificae or Bank Statment';
    //   }
    //   else if (element.globalKey == 'NationalIDCard') {
    //     this.fixedDoc[index].title = 'National ID Card';
    //   }
    //   else if (element.globalKey == 'Profile Picture') {
    //     this.fixedDoc[index].title = 'Profile Picture';
    //   }
    //   else if (element.globalKey == 'ATP Application Form') {
    //     this.fixedDoc[index].title = 'ATP Application Form';
    //   }
    //   else if (element.globalKey == 'Certificate Fee Detail Form') {
    //     this.fixedDoc[index].title = 'Certificate Fee Details Form';
    //   }
    //   else if (element.globalKey == 'Summary of Field work') {
    //     this.fixedDoc[index].title = 'Summary of Field Work';
    //   }
    //   else if (element.globalKey == 'TrainingCertificate') {
    //     this.fixedDoc[index].title = 'Training Certificate';
    //   }
    // }
  }

  addAllFixedDocument() {
    // this.micrCheque = new ProfileDocumentDetailVm();
    // this.micrCheque.statusLabel = 'Upload Document';
    // this.micrCheque.globalKey = 'MICR Cheque leaf';


    // this.nationalIdCard = new ProfileDocumentDetailVm();
    // this.nationalIdCard.statusLabel = 'Upload Document';
    // this.nationalIdCard.globalKey = 'NationalIDCard';

    // this.profilePicture = new ProfileDocumentDetailVm();
    // this.profilePicture.statusLabel = 'Upload Document';
    // this.profilePicture.globalKey = 'Profile Picture';

    // this.atpApplicationForm = new ProfileDocumentDetailVm();
    // this.atpApplicationForm.statusLabel = 'Upload Document';
    // this.atpApplicationForm.globalKey = 'ATP Application Form';

    // this.certificateFeeDetailForm = new ProfileDocumentDetailVm();
    // this.certificateFeeDetailForm.statusLabel = 'Upload Document';
    // this.certificateFeeDetailForm.globalKey = 'Certificate Fee Detail Form';

    // this.summaryofFiledwork = new ProfileDocumentDetailVm();
    // this.summaryofFiledwork.statusLabel = 'Upload Document';
    // this.summaryofFiledwork.globalKey = 'Summary of Field work';

    // this.examAnswerSheet = new ProfileDocumentDetailVm();
    // this.examAnswerSheet.statusLabel = 'Upload Document';
    // this.examAnswerSheet.globalKey = 'Exam Answer Sheet';

    // this.trainingCertificate = new ProfileDocumentDetailVm();
    // this.trainingCertificate.statusLabel = 'Upload Document';
    // this.trainingCertificate.globalKey = 'TrainingCertificate';

    // this.fixedDoc.push(this.micrCheque);
    // this.fixedDoc.push(this.certificateFeeDetailForm);
    // this.fixedDoc.push(this.nationalIdCard);
    // this.fixedDoc.push(this.summaryofFiledwork);
    // this.fixedDoc.push(this.profilePicture);
    // this.fixedDoc.push(this.examAnswerSheet);
    // this.fixedDoc.push(this.atpApplicationForm);
    // this.fixedDoc.push(this.trainingCertificate);

  }

  getUserId() {
    this.activatedRoute.params.subscribe(x => {
      this.userId = x['id'];
    });
  }

  getUserEduLabel(sendNotification: boolean) {
    this.getProfileDocumentDetail(sendNotification);
    // this.utilityService.get_user_edu_label(this.userId).subscribe(
    //   (success: ProfileDocumentDetailVm[]) => {
    //     // console.log(success);
    //     this.userEduLabel = success;
    //     this.userEduLabel.forEach(x => this.statusLabelChange(x));
    //     this.getProfileDocumentDetail(sendNotification);
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // );
    //this.getProfileDocumentDetail(sendNotification);
  }

  getProfileDocumentDetail(sendNotification: boolean) {
    this.utilityService.get_profileDocumentDetail(this.userId).subscribe(
      (sucees: ProfileDocumentDetailVm[]) => {

        this.docList = sucees;
        // education label
        // this.userEduLabel = sucees.filter(
        //   x => !this.fixedGlobalId.includes(x.globalKey)
        // );
        // this.userEduLabel.forEach(x => this.statusLabelChange(x));
        // education label

        // for (let index = 0; index < this.fixedDoc.length; index++) {
        //   const key = this.fixedDoc[index].globalKey;
        //   //console.log(key);

        //   const data = sucees.filter(
        //     x => x.globalKey === key
        //   )[0];
        //   if (data) {
        //     this.fixedDoc[index] = data;

        //   }
        //   // console.log(this.fixedDoc[index], key);
        //   this.statusLabelChange(this.fixedDoc[index]);
        // }
        // this.updateFixedDocTitle();
        // console.table(sucees);
        //console.table(this.fixedDoc);
        // const nationalIdCard = sucees.filter(
        //   x => x.globalKey === 'NationalIDCard'
        // )[0];
        // const profilePicture = sucees.filter(
        //   x => x.globalKey === 'Profile Picture'
        // )[0];

        // const atpApplicationForm = sucees.filter(
        //   x => x.globalKey === 'ATP Application Form'
        // )[0];

        // const certificateFeeDetailForm = sucees.filter(
        //   x => x.globalKey === 'Certificate Fee Detail Form'
        // )[0];
        // const summaryofFiledwork = sucees.filter(
        //   x => x.globalKey === 'Summary of Filed work'
        // )[0];
        // const examAnswerSheet = sucees.filter(
        //   x => x.globalKey === 'Exam Answer Sheet'
        // )[0];
        // const micrCheque = sucees.filter(
        //   x => x.globalKey === 'MICR Cheque leaf'
        // )[0];

        // const trainingCer = sucees.filter(
        //   x => x.globalKey === 'TrainingCertificate'
        // )[0];
        // if (nationalIdCard) {
        //   this.nationalIdCard = nationalIdCard;
        //   this.statusLabelChange(this.nationalIdCard);
        // }
        // if (profilePicture) {
        //   this.profilePicture = profilePicture;
        //   this.statusLabelChange(this.profilePicture);
        // }
        // if (atpApplicationForm) {
        //   this.atpApplicationForm = atpApplicationForm;
        //   this.statusLabelChange(this.atpApplicationForm);
        // }
        // if (certificateFeeDetailForm) {
        //   this.certificateFeeDetailForm = certificateFeeDetailForm;
        //   this.statusLabelChange(this.certificateFeeDetailForm);
        // }
        // if (summaryofFiledwork) {
        //   this.summaryofFiledwork = summaryofFiledwork;
        //   this.statusLabelChange(this.summaryofFiledwork);
        // }
        // if (examAnswerSheet) {
        //   this.examAnswerSheet = examAnswerSheet;
        //   this.statusLabelChange(this.examAnswerSheet);
        // }
        // if (micrCheque) {
        //   this.micrCheque = micrCheque;
        //   this.statusLabelChange(this.micrCheque);
        // }
        // if (trainingCer) {
        //   this.trainingCertificate = trainingCer;
        //   this.statusLabelChange(this.trainingCertificate);
        // }
        this.notificationSend(sendNotification);
        this.isDisplay = true;
      },
      error => {
        console.log(error);
      }
    );
  }
  statusLabelChange(model: ProfileDocumentDetailVm) {
    // if (model.status === 'Pending' || model.status === 'Candidate Re Submit') {
    //   model.statusLabel = 'Reupload';
    // } else if (model.status === 'Rejected') {
    //   model.statusLabel = 'Resend';
    // } else {
    //   model.statusLabel = 'Upload Document';
    // }

  }

  getDocumentSetting(sendNotification: boolean) {
    this.utilityService.get_document_upload_setting().subscribe(
      (success: DocumentUploadSettingVm[]) => {
        this.documentUploadSetting = success;
        this.getUserEduLabel(sendNotification);
      },
      error => {
        console.log(error);
      }
    );
  }

  notificationSend(sendNotification: boolean) {
    let status = false;
    const docUploadCount = this.docList.filter(
      x =>
        (x.status === 'Rejected' ||
          x.status === 'Approved')
        && x.globalKey != 'TrainingCertificate'
    ).length;

    if (docUploadCount === this.docList.length - 1) {
      status = true;
    }


    // const eduUploadCount = this.userEduLabel.filter(
    //   x => x.status === 'Approved' || x.status === 'Rejected'
    // ).length;
    // const fixedDocCount = this.fixedDoc.filter(
    //   x => (x.status === 'Approved' || x.status === 'Rejected') && x.globalKey != 'TrainingCertificate'
    // ).length;

    // if (
    //   eduUploadCount === this.userEduLabel.length &&
    //   ((this.micrCheque.status === 'Approved' ||
    //     this.micrCheque.status === 'Rejected') &&
    //     (this.certificateFeeDetailForm.status === 'Approved' ||
    //       this.certificateFeeDetailForm.status === 'Rejected') &&
    //     (this.nationalIdCard.status === 'Approved' ||
    //       this.nationalIdCard.status === 'Rejected') &&
    //     (this.summaryofFiledwork.status === 'Approved' ||
    //       this.summaryofFiledwork.status === 'Rejected') &&
    //     (this.profilePicture.status === 'Approved' ||
    //       this.profilePicture.status === 'Rejected') &&
    //     (this.examAnswerSheet.status === 'Approved' ||
    //       this.examAnswerSheet.status === 'Rejected') &&
    //     (this.atpApplicationForm.status === 'Approved' ||
    //       this.atpApplicationForm.status === 'Rejected')
    //   )
    // ) {
    //   status = true;
    // }

    // if (
    //   eduUploadCount === this.userEduLabel.length &&
    //   fixedDocCount === this.fixedDoc.length - 1
    // ) {
    //   status = true;
    // }

    if (status && sendNotification) {
      console.log('notificationSend');
      this.utilityService
        .sendDocumentVerificationNotification(this.userId)
        .subscribe();
    }

    if (status) {
      this.expandPolicyNum();
      // this.policy.expanPolicy();
    }
  }
  expandPolicyNum() {
    //$('#policy_number').collapse('show');
    //
    const policyDiv = document.getElementById(
      'policyNumberTitle'
    ) as HTMLElement;
    const span = policyDiv.getElementsByTagName('span')[0];
    if (span.classList.contains('collapsed')) {
      policyDiv.getElementsByTagName('span')[0].click();
    } else {
      $('#mandatoryDocumentsTitle > h5 >span').trigger('click');
    }
  }
  accept() {

    if (acceptSub) {
      acceptSub.unsubscribe();
    }
    acceptSub = this.utilityService.accepEmiter.subscribe(x => {
      // console.log(x);
      this.utilityService
        .acceptDocument(this.selectdFile, this.userId)
        .subscribe(
          success => {
            this.getUserEduLabel(true);
          },
          error => {
            console.log(error);
          }
        );
      // this.getUserEduLabel(true);
    });
  }
  rejectdoc() {

    if (rejectSub) {
      rejectSub.unsubscribe();
    }
    rejectSub = this.utilityService.rejectEmiter.subscribe(x => {
      console.log(x, 'm fire');
      this.utilityService
        .rejectDocument(this.selectdFile, x, this.userId)
        .subscribe(
          success => {
            this.getUserEduLabel(true);
          },
          error => {
            console.log(error);
          }
        );
      // this.getUserEduLabel(true);
    });
  }

  getSelectedFile(globalKey: string): ProfileDocumentDetailVm {
    let selectFile: ProfileDocumentDetailVm;
    const doc = this.docList.filter(
      x => x.globalKey == globalKey
    );
    if (doc.length > 0) {
      selectFile = doc[0];
    }

    // const eduUploadCount = this.userEduLabel.filter(
    //   x => x.globalKey == globalKey
    // );
    // if (eduUploadCount.length > 0) {
    //   selectFile = eduUploadCount[0];
    // } else {
    //   const fixedUploadCount = this.fixedDoc.filter(
    //     x => x.globalKey == globalKey
    //   );
    //   if (fixedUploadCount.length > 0) {
    //     selectFile = fixedUploadCount[0];
    //   }
    // }
    return selectFile;
  }

  onViewDetailsClick(path: string, title: string, key: string) {
    //console.log(key, key.length, new Date());

    this.selectdFile = key;
    if (path) {

      const file = this.getSelectedFile(key);
      console.log(file);
      this.showDocumentModalService.show(ShowDocumentComponent, {
        initialState: {
          filePath: path,
          header: title,
          accepReject: true,
          showAcceptbtn: (this.accountService.canShow(
            'btnApprove',
            'MandatoryDocument'
          ) && (file.status === 'Pending' || file.status === 'Candidate Re Submit') && (this.selectdFile != 'Certificate Fee Detail Form')),
          showRejectbtn: (this.accountService.canShow(
            'btnReject',
            'MandatoryDocument'
          ) && (file.status === 'Pending' || file.status === 'Candidate Re Submit') && (this.selectdFile != 'Certificate Fee Detail Form'))
          ,
          globalKey: key
        },
        ignoreBackdropClick: true,
        class: 'custome-modal'
      });
      const sub = this.showDocumentModalService.onHidden.subscribe(x => {
        //  this.uploader.clearQueue();
        //  this.getUserEduLabel(true);
        sub.unsubscribe();
      });
    } else {
      swal({
        title: 'Warning!',
        type: 'warning',
        confirmButtonText: 'Close',
        text: 'File not uploaded.'
      });
    }
  }
}
