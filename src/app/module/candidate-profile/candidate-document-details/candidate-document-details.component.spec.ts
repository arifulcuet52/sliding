import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateDocumentDetailsComponent } from './candidate-document-details.component';

describe('CandidateDocumentDetailsComponent', () => {
  let component: CandidateDocumentDetailsComponent;
  let fixture: ComponentFixture<CandidateDocumentDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateDocumentDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateDocumentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
