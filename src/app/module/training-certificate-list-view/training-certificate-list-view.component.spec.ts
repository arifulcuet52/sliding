import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingCertificateListViewComponent } from './training-certificate-list-view.component';

describe('TrainingCertificateListViewComponent', () => {
  let component: TrainingCertificateListViewComponent;
  let fixture: ComponentFixture<TrainingCertificateListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingCertificateListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingCertificateListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
