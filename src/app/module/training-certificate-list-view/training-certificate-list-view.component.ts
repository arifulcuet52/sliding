import { Values } from './../../shared/models/multilingual/values';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import {
  MatSort,
  MatDialog,
  MatTableDataSource,
  MatDialogConfig
} from '@angular/material';
import { DataService } from 'src/app/shared/service/data/data.service';
import { PagingModel } from 'src/app/entity/PagingModel';
import { QuestionDialogComponent } from '../exam/question/question-dialog/question-dialog.component';
import { TrainingCertificate } from 'src/app/entity/exam/training-certificate';
import { SelectionModel } from '@angular/cdk/collections';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { FileUploader } from 'ng2-file-upload';
import { AccountService } from 'src/app/shared/service/remote/authentication/account.service';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import * as $ from 'jquery';
import { BsModalService } from 'ngx-bootstrap';
import { Ng2FileUploadComponent } from '../ng2-file-upload/ng2-file-upload.component';
import swal from 'sweetalert2';
import { DocumentViewerComponent } from '../document-viewer/document-viewer.component';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { TrainingCertificateListViewService } from 'src/app/shared/service/remote/training-certificate-list/training-certificate-list-view.service';
import { TrainingCertificateListFilterEnum } from 'src/app/entity/status.enum';

@Component({
  selector: 'app-training-certificate-list-view',
  templateUrl: './training-certificate-list-view.component.html',
  styleUrls: ['./training-certificate-list-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TrainingCertificateListViewComponent implements OnInit {
  // #region variables
  displayedColumns: string[] = [
    // 'select',
    // 'sl',
    'candidateId',
    'candidateName',
    'faCode',
    'trainingExamDate',
    'documentSubmissionDate',
    'action',
    'certificateUploadDate'
  ];
  filterItems: any = TrainingCertificateListFilterEnum;
  filterValue: any;
  dataSource: any;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  selection = new SelectionModel<TrainingCertificate>(true, []);
  @ViewChild(MatSort)
  sort: MatSort;
  datePickerConfig: Partial<BsDatepickerConfig>;
  // documentViewerService: BsModalService;

  trainingCertificates: TrainingCertificate[] = [];
  trainingCertificate: TrainingCertificate;
  errorMessage = '';
  successMessage = '';
  searchText = '';
  fromDate: Date = null;
  toDate: Date = null;
  pageSize = 10;
  pageIndex = 1;
  uploader: FileUploader;
  path: SafeResourceUrl;
  documentAllowExtention = '.jpg,.bmp,.jpeg,.pdf';
  // #endregion

  //#region
  constructor(
    private dialog: MatDialog,
    private trainingCertificateListViewService: TrainingCertificateListViewService,
    public dataService: DataService,
    private accountService: AccountService,
    private authService: AuthService,
    private modalService: BsModalService,
    private documentViewerService: BsModalService,
    private domSanitizer: DomSanitizer
  ) {
    this.uploader = new FileUploader({
      url: this.authService.APPSERVER + 'training/certificate/upload',
      authTokenHeader: 'Authorization',
      authToken: 'Bearer ' + this.accountService.getToken()
    });
  }

  ngOnInit() {
    this.datePickerConfig = Object.assign(
      {},
      {
        dateInputFormat: 'DD/MM/YYYY',
        showWeekNumbers: false
      }
    );
    console.log(this.filterItems);
    this.filterItems = Object.keys(this.filterItems);
    this.filterItems = this.filterItems.slice(this.filterItems.length / 2);
    this.filterValue = 'All';
    this.initiation();
    this.reload();
  }
  //#endregion

  // #region get data
  getAll() {
    console.log(this.searchText);
    this.trainingCertificateListViewService
      .get(this.pageSize, this.pageIndex, this.filterValue, this.searchText)
      .subscribe(
        (response: PagingModel<TrainingCertificate>) => {
          this.dataSource = new MatTableDataSource(response.data);
          this.dataSource.sort = this.sort;
          this.length = response.totalItem;
        },
        () => { }
      );
  }
  // #endregion

  // #region grid functions
  initiation() {
    this.pageIndex = 1;
    this.searchText = '';
    this.fromDate = null;
    this.toDate = null;
    this.trainingCertificate = new TrainingCertificate();
    this.successMessage = '';
    this.errorMessage = '';
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.reload();
  }

  filter(searchText: string) {
    this.pageIndex = 1;
    this.searchText = searchText;
    console.log(this.searchText);
    this.getAll();
   // this.reload();
  }

  clear() {
    this.initiation();
    this.reload();
  }

  reload() {
    this.getAll();
  }

  open_dialog(model) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = model;
    return this.dialog.open(QuestionDialogComponent, dialogConfig);
  }
  // #endregion

  // #region grid multi select
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }
  //#endregion

  // #region file upload
  uploadFile(candidateId: number, candidateExamId: number) {
    this.trainingCertificate.candidateId = candidateId;
    this.trainingCertificate.candidateExamId = candidateExamId;
    document.getElementById('certificateUpload').click();
  }

  upload(event) {
    if (!this.documentValidation(event.target.files)) {
      this.uploader.clearQueue();
      this.reload();
      return;
    }

    this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
      form.append('candidateId', this.trainingCertificate.candidateId);
      form.append('candidateExamId', this.trainingCertificate.candidateExamId);
    };
    this.modalService.show(Ng2FileUploadComponent, {
      initialState: {
        uploader: this.uploader
      },
      ignoreBackdropClick: true
    });
    this.modalService.onHide.subscribe(x => {
      this.uploader.clearQueue();
      this.reload();
    });
  }

  viewCertificate(path: string, title: string) {
    const rootPath = this.authService.APPSERVER.replace('/api/', '');
    if (path) {
      this.documentViewerService.show(DocumentViewerComponent, {
        initialState: {
          filePath: rootPath + path,
          header: `Training Certificate ${title}`
        },
        ignoreBackdropClick: true,
        class: 'custome-modal'
      });
    } else {
      swal({
        title: 'Warning!',
        type: 'warning',
        confirmButtonText: 'Close',
        text: 'File not uploaded.'
      });
    }
  }
  //#endregion

  trainingCertificatePrint(): void {
    this.trainingCertificateListViewService
      .trainingCertificatePrint(this.fromDate, this.toDate, this.searchText)
      .subscribe(response => {

        if (window.navigator && window.navigator.msSaveOrOpenBlob) { // IE workaround
          this.iePdf(response);
          return;
        }

        const pdfData = 'data:application/pdf;base64,' + response;
        this.path = this.domSanitizer.bypassSecurityTrustResourceUrl(pdfData);

        if (this.path) {
          this.documentViewerService.show(DocumentViewerComponent, {
            initialState: {
              filePath: this.path,
              header: 'Training Certificate List',
              type: 'saferesourceurl'
            },
            ignoreBackdropClick: true,
            class: 'custome-modal'
          });
        } else {
          swal({
            title: 'Warning!',
            type: 'warning',
            confirmButtonText: 'Close',
            text: 'File not uploaded.'
          });
        }
      });
  }

  documentValidation(files: FileList) {
    let valid = false;
    let file;
    let extention = '';
    let fileSize = 0;

    if (files.length > 0) {
      file = files[0];
    } else {
      return;
    }

    extention = this.fileExtention(file.name);
    fileSize = this.fileToMB(file.size);

    if (this.documentAllowExtention && this.documentAllowExtention.includes(extention)) {
      valid = true;
    } else {
      swal({
        title: 'Warning!',
        type: 'warning',
        confirmButtonText: 'Close',
        text: 'only these types(' + this.documentAllowExtention + ') of images are supported'
      });
    }

    return valid;
  }

  fileExtention(filename: string) {
    return filename.substring(filename.lastIndexOf('.') + 1, filename.length) || filename;
  }

  fileToMB(size: number) {
    if (size <= 0) {
      return 0;
    }

    return Math.ceil(size / (1024 * 1024));
  }

  iePdf(response: any, filename?: string) {
    const byteCharacters = atob(response);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/pdf' });
    filename = filename != null ? filename : 'report.pdf';
    window.navigator.msSaveOrOpenBlob(blob, filename);
  }
}
