import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource, MatDialog, MatTable } from '@angular/material';
import { LeadService } from '../../../shared/service/remote/lead/lead.service';
import { LeadTypeEnum, LeadTypeEnumUntagged } from '../../../entity/status.enum';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import * as feather from 'feather-icons';
import { DataService } from 'src/app/shared/service/data/data.service';
import { AccountService } from 'src/app/shared/service/remote/authentication/account.service';
import { Values, Keys } from 'src/app/shared/models/multilingual';
import { debounce } from 'rxjs/operators';

@Component({
  selector: 'app-untaggedlead-list',
  templateUrl: './untaggedlead-list.component.html',
  styleUrls: ['./untaggedlead-list.component.scss']
})
export class UntaggedleadListComponent implements OnInit {

  public searchText = '';
  public LeadId: Number;
  public actionId: number;
  public dataSource: any = null;
  public leadTypeEnum: any = LeadTypeEnumUntagged;
  public remarks = '';
  public selectedRecord: any;
  public dataSourceArray: any;
  public element: any;
  public actions: any;
  public userForm: FormGroup;
  public defaultActionId: any = '-1';
  // public leadTypes: any;
  public leadTypeValue: any;
  public defaultLeadTypeId: any = '-1';
  public pageSize = 10;
  public pageIndex = 1;
  public length: number;
  public leadTypeId = 3;
  public userid: any;
  public userEmail: any;
  public defaultSubActionId: any = '-1';
  public subActionId: any;
  public subActions: any;


  public displayedColumns: string [] = ['sl', 'userId', 'faCode', 'fullName', 'currentStatusName', 'nextActionName',
  'myAction', 'remarks', 'dateOfAction', 'action'];

  public  leadTypes: { id: number, name: string }[] = [
    { 'id': 3, 'name': 'All' },
    { 'id': 1, 'name': 'Active Leads' },
    { 'id': 2, 'name': 'Inactive Leads' },
    { 'id': 4, 'name': 'My Active Leads' },
    { 'id': 5, 'name': 'My Inactive Leads' }
];



  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild('openModalButton') openModalButton: ElementRef;
  @ViewChild('modalCloseButton') modalCloseButton: ElementRef;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, public leadService: LeadService
    , private dialog: MatDialog, private dataService: DataService, private accService: AccountService) {
      this.userForm = new FormGroup({
          remarks: new FormControl('', Validators.compose([Validators.required, this.noWhitespaceValidator]))
      });
      this.actionId = this.defaultActionId;
      this.leadTypeValue = this.defaultLeadTypeId;
      this.subActionId = this.defaultSubActionId;
      // this.leadTypes = Object.keys(this.leadTypeEnum);
      // this.leadTypes = this.leadTypes.slice(this.leadTypes.length / 2);
      this.userEmail = accService.getUserEmail();
    }

  ngOnInit() {
    feather.replace();
    this.getAll(this.leadTypeEnum.All, this.searchText);
    this.loadDropdown();
    this.userid = this.getCurrentUserInfo();
  }

  getCurrentUserInfo () {
    this.leadService.getCurrentUserInfo(this.userEmail)
      .subscribe(res => {
      this.userid = (res as any).userId;
    }, error => {
    });
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  selectedIndexChanged() {
    debugger;
    if (this.leadTypeValue === '-1') {
      this.leadTypeValue = 3; // all
    }
    this.leadTypeId = this.leadTypeValue;
    this.getAll(this.leadTypeId, this.searchText);
  }

  selectedActionChanged() {
    debugger;
    this.leadService.getSubActionsByActionId(this.actionId).subscribe(res => {
      if (res.length != 0) {
        this.subActions = res;
        this.subActionId = this.defaultSubActionId;
      }
      else {
        this.subActions = null;       
        this.subActionId = -5; // for disable rule
      }
      
      console.log(this.subActions);
    });   
  }

  getAll(leadType: any, searchText: string) {
     const leadGroup = 'UnTagged';
    this.leadService.getAlllead(this.pageSize, this.pageIndex, leadGroup, leadType, searchText)
      .subscribe(res => {
        this.element = res;
          this.LeadId = res.leadId;
          this.dataSourceArray = [];
          this.dataSourceArray.push(res.data);
          // this.dataSource = new MatTableDataSource(this.dataSourceArray);
          this.dataSource = new MatTableDataSource(res.data);
          this.length = res.totalItem;
          console.log(res.data);

      }, error => {

      });
  }

  pageChange($event) {
    console.log($event);
    window.scrollTo(0, 0);

    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.getAll(this.leadTypeId, this.searchText);
  }

  loadDropdown()
  {
    this.leadService.getDropdownLeadStatus().subscribe(res => {
      this.actions = res;
    });
  }


  openModal(element: any) {
    this.selectedRecord = Object.assign({}, element);
    // tslint:disable-next-line:no-unused-expression
    this.openModalButton && this.openModalButton.nativeElement.click();

  }

  accept(element: any) {
    const leadEvent: LeadEvent = this.selectedRecord;
    leadEvent.ActionId = this.actionId;
    leadEvent.remarks = this.remarks;
    leadEvent.EventType = 1;
    leadEvent.LeadType = 'UnTagged';
    leadEvent.SubActionId = this.subActionId == -5 ? null : this.subActionId;

    this.leadService.updateStatus(leadEvent)
      .subscribe(res => {
        swal({
          title: 'Success!',
          text: 'Save successfully',
          type: 'success',
          confirmButtonText: 'Close'
        });
        // tslint:disable-next-line:no-unused-expression
        this.modalCloseButton && this.modalCloseButton.nativeElement.click();
        this.closePopUp();
       this.getAll(this.leadTypeEnum.All, this.searchText);
      }, error => {
        swal({
          title: 'Warning!',
          text: error.error,
          type: 'warning',
          confirmButtonText: 'Close'
        });
      });
  }

  selfAssign (element: any) {

    this.selectedRecord = Object.assign({}, element);
    const leadMaster: LeadEvent = this.selectedRecord;

    this.leadService.SelfAssign(leadMaster)
      .subscribe(res => {
        swal({
          title: 'Success!',
          text: 'Save successfully',
          type: 'success',
          confirmButtonText: 'Close'
        });

       this.getAll(this.leadTypeEnum.All, this.searchText);
      }, error => {
        swal({
          title: 'Warning!',
          text: error.error,
          type: 'warning',
          confirmButtonText: 'Close'
        });
      });
  }

  filter(searchText: string) {
    this.searchText = searchText;
    this.getAll(this.leadTypeId, searchText);
  }

  closePopUp(){
    this.remarks = '';
    this.actionId = this.defaultActionId;
  }

}


export class LeadEvent {
  remarks: string;
  leadId: number;
  UserId: number;
  ActionId: number;
  EventType: number;
  EventBody: string;
  LeadType: string;
  SubActionId: number;
}
