import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UntaggedleadListComponent } from './untaggedlead-list.component';

describe('UntaggedleadListComponent', () => {
  let component: UntaggedleadListComponent;
  let fixture: ComponentFixture<UntaggedleadListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UntaggedleadListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UntaggedleadListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
