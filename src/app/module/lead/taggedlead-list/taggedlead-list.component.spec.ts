import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaggedleadListComponent } from './taggedlead-list.component';

describe('TaggedleadListComponent', () => {
  let component: TaggedleadListComponent;
  let fixture: ComponentFixture<TaggedleadListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaggedleadListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaggedleadListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
