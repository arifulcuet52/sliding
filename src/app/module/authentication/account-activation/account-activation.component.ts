import { AccountActivationVm } from './../../../entity/accoun-activation-vm';
import { Component, OnInit } from '@angular/core';
import { OTPIssueReasonEnum } from '../../../entity/otp-issue-reason-enum';
import { OtpService } from '../../../shared/service/remote/otp/otp.service';
import { OTPVm } from '../../../entity/otpVm';
import { timer, Subscription } from 'rxjs';
import swal from 'sweetalert2';


@Component({
  selector: 'app-account-activation',
  templateUrl: './account-activation.component.html',
  styleUrls: ['./account-activation.component.css']
})
export class AccountActivationComponent implements OnInit {

  otpdeliveryMethod = 1;
  counDown = 0;
  counDownDate: number;
  model = new AccountActivationVm();

  constructor(private otpService: OtpService) { }

  ngOnInit() {
  }


  sendOTP() {
    this.otpService.generateOTP(this.otpdeliveryMethod, OTPIssueReasonEnum.UserActivation, this.model.email).subscribe(
        (success: OTPVm) => {
          const otpVm = new OTPVm();
          otpVm.allowAfter = success.allowAfter;
          otpVm.message = success.message;
          otpVm.path = success.path;
          this.otpGenerate(otpVm);
        },
        (error) => {
        }
      );
  }

  otpGenerate(otpvm: OTPVm) {
    this.counDown = otpvm.allowAfter;
    let ngUnsubscribe = new Subscription();
    const numbers = timer(0, 1000);
    const today1 = new Date();
    const myToday = new Date(today1.getFullYear(), today1.getMonth(), today1.getDate(), 0, 0, otpvm.allowAfter);
    ngUnsubscribe = numbers.subscribe(x => {
      const temp = new OTPVm();
      temp.allowAfter = otpvm.allowAfter - x;
      temp.path = otpvm.path;
      this.counDown = this.counDown - 1;
      const tempD = new Date(myToday).getTime() - (x * 1000);
      this.counDownDate = tempD;
      if (otpvm.allowAfter <= x) {
        ngUnsubscribe.unsubscribe();
      }
    }
    );
  }
}



