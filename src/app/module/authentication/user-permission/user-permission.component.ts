import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import * as typeahead from 'jquery-typeahead';
import * as $ from 'jquery';
import { UserPermission } from 'src/app/entity/user-permission';
import { UserPermissionService } from '../../../shared/service/remote/authentication/user-permission.service';
import { AuthService } from '../../../shared/service/auth/auth.service';
import { User } from '../../../entity/user';

@Component({
  selector: 'app-user-permission',
  templateUrl: './user-permission.component.html',
  styleUrls: ['./user-permission.component.css']
})
export class UserPermissionComponent implements OnInit {
  models: UserPermission[] = [];
  model: { name: '' } = { name: '' };
  url = this.authService.APPSERVER + 'user';
  users: User[] = [];
  rolesid: string[] = [];
  errorMessage = '';
  successMessage = '';
  constructor(
    private userPermission: UserPermissionService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    console.log(typeahead);
    this.model = { name: '' };
    this.get('');
    this.search();
  }

  get(userid: string) {
    this.userPermission.Get(userid, 'all', 'userPermission').subscribe(
      (result: UserPermission[]) => {
        this.models = result;
      },
      error => {}
    );
  }
  save() {
    this.errorMessage = '';
    const usersId = this.users.map(x => x.id);

    this.userPermission
      .UserAssignToRole(
        usersId,
        this.models.filter(x => x.userExists == true).map(x => x.roleId)
      )
      .subscribe(
        success => {
          this.successMessage = JSON.parse(JSON.stringify(success));
        },
        error => {
          this.errorMessage = JSON.parse(JSON.stringify(error)).error.message;
        }
      );
  }

  roleSelect(roleid: string) {
    if (!this.rolesid.includes(roleid)) {
      this.rolesid.push(roleid);
    } else {
      this.rolesid.splice(this.rolesid.findIndex(i => i === roleid), 1);
    }
  }
  onChange(id: string) {
    this.get(id);
  }

  public search() {
    const access = this;

    $.typeahead({
      input: '.js-typeahead-hockey_v2',
      minLength: 2,
      order: 'asc',
      dynamic: true,
      delay: 300,
      offset: false,
      hint: true,
      searchOnFocus: true,
      blurOnTab: false,
      templateValue: '{{fullName}}',
      display: ['fullName'],
      cancelButton: true,
      emptyTemplate: 'No result for {{query}}',
      matcher: function(item, displayKey) {
        return true;
      },
      multiselect: {
        limit: 10,
        limitTemplate: 'You can\'t select more than 10 teams',
        matchOn: ['id'],
        cancelOnBackspace: true,
        data: function() {
          const deferred = $.Deferred();
          deferred.always(function() {
            console.log('data loaded from promise');
          });

          return deferred;
        },
        callback: {
          onClick: function(node, item, event) {
            // fire on click on item of textbox
            // console.log(item);
            //   alert(item.name + ' Clicked!');
          },
          onCancel: function(node, item, event) {
            access.users.splice(
              access.users.findIndex(i => i.id === item.id),
              1
            );

            // fire on remove item from  textbox
            //alert(item.name + ' Removed!');
          }
        }
      },
      source: {
        users: {
          ajax: function(query) {
            return {
              url: access.url + '?userName=' + query,
              method: 'get',
              dataType: 'json',
              headers: {
                Authorization:
                  'Bearer ' + JSON.parse(localStorage.getItem('AD')).token
              },
              success: function(data) {
                return data;
              }
            };
          }
        }
      },
      callback: {
        onClick: function(node, a, item, event) {
          let selectUser = new User();
          selectUser = item;
          access.users.push(selectUser);

          //for item select on search time
          // console.log(node)
          //console.log(a)
          //console.log(item)
          // console.log(event)
          // console.log('onClick function triggered');
        }
      }
    });
  }
}
