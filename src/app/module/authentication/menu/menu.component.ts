import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuService } from '../../../shared/service/remote/authentication/menu.service';
import { Menu } from '../../../entity/menu';
import {
  MatSort,
  MatDialog,
  MatTableDataSource,
  MatDialogConfig
} from '@angular/material';
import { DialogMenuCreateComponent } from '../../../dialog/dialog-menu-create/dialog-menu-create.component';
import swal from 'sweetalert2';
import { PagingModel } from 'src/app/entity/PagingModel';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  displayedColumns: string[] = [
    'sl',
    'parentMenu',
    'menuName',
    'showInMenu',
    'sortOrder',
    'action'
  ];
  dataSource: any;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  @ViewChild(MatSort)
  sort: MatSort;

  menus: Menu[] = [];
  menu: Menu;
  errorMessage = '';
  successMessage = '';
  searchText = '';
  pageSize = 10;
  pageIndex = 1;
  constructor(private dialog: MatDialog, private menuService: MenuService) {}

  ngOnInit() {
    this.initiation();
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }
  getAll(searchText: string, pageSize: number, pageIndex: number) {
    this.menuService.getAll(searchText, pageSize, pageIndex).subscribe(
      (response: PagingModel<Menu>) => {
        this.dataSource = new MatTableDataSource(response.data);
        this.dataSource.sort = this.sort;
        this.length = response.totalItem;
      },
      error => {}
    );
  }
  initiation() {
    this.searchText = '';
    this.menu = new Menu('');
    this.successMessage = '';
    this.errorMessage = '';
  }
  save() {
    if (this.menu.menuId) {
      this.menuService.update(this.menu).subscribe(
        (response: string) => {
          this.initiation();
          this.getAll(this.searchText, this.pageSize, this.pageIndex);
        },
        error => {
          this.errorMessage = JSON.parse(JSON.stringify(error)).error.message;
        }
      );
    } else {
      this.menuService.insert(this.menu).subscribe(
        (response: string) => {
          this.initiation();
          this.getAll(this.searchText, this.pageSize, this.pageIndex);
        },
        error => {
          this.errorMessage = JSON.parse(JSON.stringify(error)).error.message;
        }
      );
    }
  }

  cancel() {
    this.initiation();
  }
  delete(menuId: string) {
    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.menuService.remove(menuId).subscribe(
          (response: string) => {
            this.getAll(this.searchText, this.pageSize, this.pageIndex);
            this.initiation();
          },
          error => {
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error.message,
              type: 'error',
              confirmButtonText: 'Close'
            });
          }
        );
      }
    });
  }
  edit(menuId: string) {
    this.menuService.get(menuId).subscribe(
      (response: Menu) => {
        this.menu = response;
        this.open_dialog(this.menu)
          .afterClosed()
          .subscribe(data => {
            if (data !== undefined) {
              this.getAll(this.searchText, this.pageSize, this.pageIndex);
            }
          });
      },
      error => {
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).message,
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    );
  }

  add() {
    this.menu = new Menu('');
    this.open_dialog(this.menu)
      .afterClosed()
      .subscribe(data => {
        if (data !== undefined) {
          this.getAll(this.searchText, this.pageSize, this.pageIndex);
        }
      });
  }
  open_dialog(model) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { model: model };
    return this.dialog.open(DialogMenuCreateComponent, dialogConfig);
  }
  pageChange($event) {
    console.log($event);
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
    console.log($event);
  }
  filter(searchText: string) {
    this.searchText = searchText;
    this.pageIndex = 1;
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }
}
