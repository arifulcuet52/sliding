import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../../shared/service/remote/authentication/account.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  constructor(private accService: AccountService, private router: Router) { }
  errorMessage = '';
  successMessage = '';
  hide = false;
  accountLock = false;
  model: {
    email: '';
    password: '';
  };
  ngOnInit() {
    this.accountLock = false;
    this.model = { email: '', password: '' };
    this.successMessage = this.accService.getMessage();
    if (this.accService.isLogIn()) {
      this.router.navigate([this.accService.getDashboardPath()]);
    }

  }

  logIn() {
    this.accService.Login(this.model).subscribe(
      succ => {
        this.accService.set_token(succ);
        const res = JSON.parse(JSON.stringify(succ));
        if (res.PasswordExpire) {
          this.accService.setMessage(res.PasswordExpire);
          this.router.navigate(['ap/change-password']);
        } else if (res.default_password) {
          this.accService.setTempEmail(this.model.email);
          this.router.navigate(['/ap/change-password'], {
            queryParams: { 'chanage-password': true }
          });
        } else {
          this.router.navigate([this.accService.getDashboardPath()]);
          // if (this.accService.isTraingAdmin()) {
          //   this.router.navigate(['ap/training-admin']);
          // } else if (this.accService.isAgencyServiceAdmin()) {
          //   this.router.navigate(['ap/agent-service-admin']);
          // } else if (this.accService.isCollectionAdmin()) {
          //   this.router.navigate(['/ap/collection-admin-dashboard']);
          // } else if (
          //   this.accService.roleCheck('Call Center Admin') ||
          //   this.accService.roleCheck('Agency Service Admin')
          // ) {
          //   alert('fffff');
          //   this.router.navigate(['/ap/dashboard']);
          // } else {
          //   this.router.navigate(['ap/admin-dashboard']);
          // }
        }
      },
      err => {
        this.accountLock = false;
        if (err.error.error == 'PasswordExpire') {
          this.router.navigate(['ap/unlock-account']);
          this.accService.setMessage(err.error.error_description);
        } else if (err.error.error == 'LockAccount') {
          this.accountLock = true;
        }
        this.errorMessage = err.error.error_description;
      }
    );
  }
  forgetPassword() {
    this.accService.setMessage('');
    this.router.navigate(['ap/account-recovery']);
    // if(this.accountLock){
    //   this.router.navigate(['module/unlock-account'])
    // }else{
    //   this.router.navigate(['module/forget-password'])
    // }
  }
}
