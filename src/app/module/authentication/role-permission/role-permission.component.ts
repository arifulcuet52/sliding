import { Component, OnInit } from '@angular/core';
import { RolePermissionService } from '../../../shared/service/remote/authentication/role-permission.service';
import { RoleService } from '../../../shared/service/remote/authentication/role.service';
import { Role } from '../../../entity/role';
import { RolePermission } from 'src/app/entity/role-permission';
import { ToastrMessageService } from '../../../shared/utils/toastr-message.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-role-permission',
  templateUrl: './role-permission.component.html',
  styleUrls: ['./role-permission.component.css']
})
export class RolePermissionComponent implements OnInit {
  roles: Role[] = [];
  roleId = '';
  rolePermissionModel: RolePermission[] = [];
  errorMessage = '';
  successMessage = '';
  constructor(
    private rolePermission: RolePermissionService,
    private roleService: RoleService,
    private toastr: ToastrMessageService
  ) {}

  ngOnInit() {
    this.GetRoles();
    this.GetAllRolePermission('');
  }

  GetRoles() {
    this.roleService.getAll().subscribe(
      (success: Role[]) => {
        this.roles = success;
      },
      error => {}
    );
  }

  GetAllRolePermission(roleId: string) {
    this.rolePermission.GetAll(roleId).subscribe(
      (success: RolePermission[]) => {
        this.rolePermissionModel = success;
      },
      error => {}
    );
  }
  roleChange(roleId: string) {
    this.roleId = roleId;

    this.GetAllRolePermission(this.roleId);
  }
  save() {
    const access = this;
    this.errorMessage = '';
    this.successMessage = '';
    if (this.roleId == 'Select' || !this.roleId) {
      // this.errorMessage = "Please select a role";
      // this.toastr.showError("Please select a role");
      swal({
        title: 'Error!',
        text: 'Please select a role',
        type: 'error',
        confirmButtonText: 'Close'
      });
    } else {
      this.rolePermissionModel.forEach(function(item, index) {
        item.roleId = access.roleId;
      });
      this.rolePermission.menuAssign(this.rolePermissionModel).subscribe(
        (success: string) => {
          // this.successMessage = success;
          // this.toastr.showSuccess();
          swal({
            title: 'Success!',
            text: 'Save successfully',
            type: 'success',
            confirmButtonText: 'Close'
          });
          this.GetAllRolePermission(this.roleId);
        },
        error => {}
      );
    }
  }
  checkBoxValueChnage(menuId: string, propertyName: string) {
    const index = this.rolePermissionModel.findIndex(x => x.menuId == menuId);
    this.rolePermissionModel[index][propertyName] = !this.rolePermissionModel[
      index
    ][propertyName];
  }
}
