import { Component, OnInit } from '@angular/core';
import { RoleService } from '../../../shared/service/remote/authentication/role.service';
import { Role } from '../../../entity/role';
import swal from 'sweetalert2';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  model: Role;
  models: Role[];
  errorMessage: string;
  constructor(private rService: RoleService) { }

  ngOnInit() {
    this.initiation();
  }
  getAll() {
    this.rService.getAll().subscribe(
      (roles: Role[]) => {
        this.models = roles;
      },
      (error) => {
      },
    );
  }

  private initiation() {
    this.getAll();
    this.model = new Role('', false);
    this.errorMessage = '';
  }

  save() {

    if (this.model.id) {
      this.rService.update(this.model).subscribe(
        (success) => {
          this.initiation();
        },
        (error) => {
          this.errorMessage = JSON.parse(JSON.stringify(error)).error.message;
          swal({
            title: 'Error!',
            text: this.errorMessage,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    } else {
      this.rService.save(this.model).subscribe(
        (success) => {
          this.initiation();
        },
        (error) => {
          this.errorMessage = JSON.parse(JSON.stringify(error)).error.message;
          swal({
            title: 'Error!',
            text: this.errorMessage,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }
  edit(id: string) {
    console.log(id);
    this.rService.get(id).subscribe(
      (role: Role) => {
        this.model = role;
      },
      (error) => {
        console.log(error);
      }
    );
  }
  cancel() {
    this.initiation();
  }

  delete(id: string) {
    this.rService.remove(id).subscribe(
      (success) => {
        this.initiation();
        this.getAll();
      },
      (error) => {
        this.errorMessage = JSON.parse(JSON.stringify(error)).error.message;
      }
    );
  }
}
