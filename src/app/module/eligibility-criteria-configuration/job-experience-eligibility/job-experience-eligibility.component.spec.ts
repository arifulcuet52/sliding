import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobExperienceEligibilityComponent } from './job-experience-eligibility.component';

describe('JobExperienceEligibilityComponent', () => {
  let component: JobExperienceEligibilityComponent;
  let fixture: ComponentFixture<JobExperienceEligibilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobExperienceEligibilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobExperienceEligibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
