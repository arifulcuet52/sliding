import { Component, OnInit, ViewChild } from '@angular/core';
import { JobEligibilityService } from '../../../shared/service/remote/eligibility/job-eligibility/job-eligibility.service';
import { EligibilityCriteriaJob } from './../../../entity/eligibility-criteria-job';
import { MatSort, MatDialog, MatDialogConfig, MatTableDataSource } from '@angular/material';
import { DialogEligibilityJobComponent } from '../../../dialog/dialog-eligibility-job/dialog-eligibility-job.component';
import swal from 'sweetalert2';

@Component({
  selector: 'app-job-experience-eligibility',
  templateUrl: './job-experience-eligibility.component.html',
  styleUrls: ['./job-experience-eligibility.component.css']
})
export class JobExperienceEligibilityComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  query: any;
  displayedColumns: string[] = ['sl', 'title', 'score', 'sortOrder', 'status', 'action' ];
  dataSource: any;
  constructor(private dialog: MatDialog, private jobEligibilityService: JobEligibilityService) { }

  ngOnInit() {
    this.refresh();
  }

  open_dialog(model){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {model: model} ;
    // dialogConfig.width = '600px';
    // dialogConfig.height = '450px';
    return this.dialog.open(DialogEligibilityJobComponent, dialogConfig);
  }

  add(){
    this.open_dialog(new EligibilityCriteriaJob()).afterClosed().subscribe(data => {
      if (data !== undefined){
        this.refresh();
      }
    });
  }

  edit(id, index){
    this.open_dialog(this.dataSource.data[index]).afterClosed().subscribe(data => {
      if (data !== undefined){
        this.refresh();
      }
    });
  }

  delete(id, index){


    swal({
      title: 'Warning!',
      text: 'Are you sure you want to delete this?',
      type: 'warning',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
    }).then(res => {
      if (res.value){
        this.jobEligibilityService.delete(id).subscribe(
          (success) => {
            this.refresh();
           },
          (error) => {
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error.message,
              type: 'error',
              confirmButtonText: 'Close'
            });
           }
        );
      }
    });



  }

  change_status(index, data){

    this.jobEligibilityService.edit(data.id, data).subscribe(
      (succ) => {
        this.refresh();
      },
      (error) => {
        data.status = false;
        this.dataSource[index] = data;
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).error,
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    );
  }

  refresh(){
    this.jobEligibilityService.getall().subscribe(res => {
      this.dataSource =  new MatTableDataSource(res.data);
      this.dataSource.sort = this.sort;
    });
  }

  reload(index, model, hasRemove){
    const tempSource = this.dataSource.data;
    if (hasRemove){
      tempSource.splice(index, 1);
    }else{
      if (index > -1){
        tempSource[index] = model;
      }else{
        tempSource.push(model);
      }
    }
    this.dataSource = new MatTableDataSource(tempSource) ;
    this.dataSource.sort = this.sort;
  }

  search_list_data(){
    console.log(this.query);
    const tempSource = this.dataSource.data;
    // tempSource.filter(x => Object.keys(x).some(prop => x[prop].includes('y')) );

    // tempSource.filter(obj => Object.keys(obj).some(prop => obj[prop].includes(this.query)));
    tempSource.filter(obj => Object.keys(obj).some(this.kis));
    // tempSource.filter(obj => Object.keys(obj).forEach(prop => console.log(obj[prop])));

    // console.log(tempSource);
    // console.log(tempSource[0]['title'].includes(this.query));
    // return tempSource.filter(item =>
    //   Object.keys(item).some(k => item[k].includes(term.toLowerCase()))
    // );
  }

  kis = function(ele: any){
   console.log(ele);
   return ele;
  };
}



