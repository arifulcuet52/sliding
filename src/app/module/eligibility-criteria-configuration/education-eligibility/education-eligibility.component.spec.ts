import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationEligibilityComponent } from './education-eligibility.component';

describe('EducationEligibilityComponent', () => {
  let component: EducationEligibilityComponent;
  let fixture: ComponentFixture<EducationEligibilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducationEligibilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationEligibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
