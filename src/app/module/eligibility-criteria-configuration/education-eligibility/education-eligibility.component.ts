import { Component, OnInit, ViewChild } from '@angular/core';
import { EligibilityCriteriaEducation } from '../../../entity/eligibility-criteria-education';
import { EducationEligibilityService } from '../../../shared/service/remote/eligibility/education-eligibility/education-eligibility.service';
import { MatDialog, MatTableDataSource, MatDialogConfig, MatSort } from '@angular/material';
import { DialogEducationEligibilityComponent } from '../../../dialog/dialog-education-eligibility/dialog-education-eligibility.component';
import swal from 'sweetalert2';

@Component({
  selector: 'app-education-eligibility',
  templateUrl: './education-eligibility.component.html',
  styleUrls: ['./education-eligibility.component.css']
})
export class EducationEligibilityComponent implements OnInit {

  constructor(private eduEligibilityService: EducationEligibilityService,
    private dialog: MatDialog) { }

    @ViewChild(MatSort) sort: MatSort;
    displayedColumns: string[] = ['sl', 'title', 'score', 'status', 'level', 'action'];
    dataSource: any;
    model: EligibilityCriteriaEducation = new EligibilityCriteriaEducation();

  ngOnInit() {
    this.getAll();
  }
  
  getAll() {
    this.eduEligibilityService.getAll().subscribe(
      (success) => {
        this.dataSource = new MatTableDataSource(success.data);
        this.dataSource.sort = this.sort;
      },
      (error) => { console.log("error") },
    );
  }

  open_dialog(model){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {model: model} ;
    return this.dialog.open(DialogEducationEligibilityComponent, dialogConfig);
  }

  add(){
    this.open_dialog(this.model).afterClosed().subscribe(data => {
      this.getAll();
    });
  }

  edit(id, index){
    this.open_dialog(this.dataSource.data[index]).afterClosed().subscribe(data => {
      this.getAll();
    });
  }

  delete(id, index){

    swal({
      title: 'Warning!',
      text: 'Are you sure you want to delete this?',
      type: 'warning',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
    }).then(res => {
      if (res.value){
        this.eduEligibilityService.delete(id).subscribe(
          (success) => {
            this.getAll();
           },
          (error) => {
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error.message,
              type: 'error',
              confirmButtonText: 'Close'
            });
           }
        );
      }
    });
  }

  change_status(index, data){
    this.eduEligibilityService.edit(data.id, data).subscribe(
      (succ) => {
        console.log(succ);
      },
      (error) => {
        data.status = false;
        this.dataSource[index] = data;
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).error,
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    );
  }
}





















/* import { Component, OnInit } from '@angular/core';
import { EligibilityCriteriaEducation } from '../../../entity/eligibility-criteria-education';
import { EducationEligibilityService } from '../../../shared/service/remote/eligibility/education-eligibility/education-eligibility.service';
import { Router } from '../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-education-eligibility',
  templateUrl: './education-eligibility.component.html',
  styleUrls: ['./education-eligibility.component.css']
})
export class EducationEligibilityComponent implements OnInit {

  constructor(private eduEligibilityService: EducationEligibilityService,
    private router: Router) { }

  models: EligibilityCriteriaEducation[] = [];
  model: EligibilityCriteriaEducation;
  errorMessage = '';
  successMessage = '';

  ngOnInit() {
    this.initiation();
    this.getAll();
  }

  initiation() {
    this.model = new EligibilityCriteriaEducation();
    this.successMessage = "";
    this.errorMessage = "";
  }

  getAll() {
    this.eduEligibilityService.getAll().subscribe(
      (success) => {
        this.models = success.data;
        console.log(success.data);
      },
      (error) => { console.log("error") },
    );
  }

  edit(id) {
    this.eduEligibilityService.get(id).subscribe(
      (response) => {
        this.model = response.data;
        window.scroll(0,0);
      },
      (error) => { this.errorMessage = JSON.parse(JSON.stringify(error)).error.message;}
    )
  }

  save() {
    const m = Object.assign({}, this.model);

    if (this.model.id) {
      this.eduEligibilityService.edit(this.model.id, this.model).subscribe(
        (response) => {
          this.initiation();
          this.getAll();
        },
        (error) => {
          this.errorMessage = JSON.parse(JSON.stringify(error)).error.message;
        });
    }
    else{
      this.eduEligibilityService.save(m).subscribe(
        (success) => {
          success.results;
          this.initiation();
          this.getAll();
          window.scroll(0,0);
        },
        (error) => { this.errorMessage = JSON.parse(JSON.stringify(error)).error.message; }
      )
    }
  }

  cancel() {
    this.initiation();
  }


  delete(id, index) {
    debugger;
    var isConfirmed = confirm("Are you sure deleting this criteria?");
    if (isConfirmed) {
      this.eduEligibilityService.delete(id).subscribe(
        (success: string) => {
          this.getAll();
          this.initiation();
        },
        (error) => { this.errorMessage = JSON.parse(JSON.stringify(error)).error.message; }
      );
    }
  }

}
 */