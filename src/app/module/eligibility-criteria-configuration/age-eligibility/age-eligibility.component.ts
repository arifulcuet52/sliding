import { Component, OnInit, ViewChild } from '@angular/core';
import { AgeEligibilityService } from '../../../shared/service/remote/eligibility/age-eligibility/age-eligibility.service';
import { EligibilityCriteriaAge } from '../../../entity/eligibility-criteria-age';
import { MatDialog, MatTableDataSource, MatDialogConfig, MatSort } from '@angular/material';
import { DialogAgeEligibilityComponent } from '../../../dialog/dialog-age-eligibility/dialog-age-eligibility.component';
import swal from 'sweetalert2';

@Component({
  selector: 'app-age-eligibility',
  templateUrl: './age-eligibility.component.html',
  styleUrls: ['./age-eligibility.component.css']
})
export class AgeEligibilityComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  constructor(private ageEligibilityService: AgeEligibilityService,
    private dialog: MatDialog) { }

  displayedColumns: string[] = ['sl', 'minAge', 'maxAge', 'score', 'status', 'action'];
  dataSource: any;
  model: EligibilityCriteriaAge = new EligibilityCriteriaAge();

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.ageEligibilityService.getAll().subscribe(
      (success) => {
        this.dataSource = new MatTableDataSource(success.data);
        this.dataSource.sort = this.sort;
      },
      (error) => { console.log("error") },
    );
  }
  
  open_dialog(model){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {model: model} ;
    return this.dialog.open(DialogAgeEligibilityComponent, dialogConfig);
  }

  add(){
    console.log(this.model)
    this.open_dialog(this.model).afterClosed().subscribe(data => {
      this.getAll();
    });
  }

  edit(id, index){
    this.open_dialog(this.dataSource.data[index]).afterClosed().subscribe(data => {
      this.getAll();
    });
  }

  delete(id, index){
    this.ageEligibilityService.delete(id).subscribe(
      (success) => {
        this.getAll();
       },
      (error) => {
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).error,
          type: 'error',
          confirmButtonText: 'Close'
        });
       }
    );
  }

  change_status(index, data){
    this.ageEligibilityService.edit(data.id, data).subscribe(
      (succ) => {
        console.log(succ);
      },
      (error) => {
        data.status = false;
        this.dataSource[index] = data;
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).error,
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    );
  }
}
/* ----------------------------------------------------- */






























/* import { Component, OnInit } from '@angular/core';
import { AgeEligibilityService } from '../../../shared/service/remote/eligibility/age-eligibility/age-eligibility.service';
import { Router } from '../../../../../node_modules/@angular/router';
import { EligibilityCriteriaAge } from '../../../entity/eligibility-criteria-age';

@Component({
  selector: 'app-age-eligibility',
  templateUrl: './age-eligibility.component.html',
  styleUrls: ['./age-eligibility.component.css']
})
export class AgeEligibilityComponent implements OnInit {

  constructor(private ageEligibilityService: AgeEligibilityService,
    private router: Router) { }

  models: EligibilityCriteriaAge[] = [];
  model: EligibilityCriteriaAge;
  errorMessage = '';
  successMessage = '';
  ngOnInit() {
    this.initiation();
    this.getAll();
  }

  initiation() {
    this.model = new EligibilityCriteriaAge();
    this.successMessage = "";
    this.errorMessage = "";
  }

  cancel() {
    this.initiation();
  }

  save() {
    const m = Object.assign({}, this.model);

    if (this.model.id) {
      this.ageEligibilityService.edit(this.model.id, this.model).subscribe(
        (response) => {
          this.initiation();
          this.getAll();
        },
        (error) => {
          this.errorMessage = JSON.parse(JSON.stringify(error)).error.message;
        });
    }
    else {
      this.ageEligibilityService.save(m).subscribe(
        (success) => {
          success.results;
          this.initiation();
          this.getAll();
        },
        (error) => { this.errorMessage = JSON.parse(JSON.stringify(error)).error.message; }
      )
    }
  }

  getAll() {
    this.ageEligibilityService.getAll().subscribe(
      (success) => {
        this.models = success.data;
        console.log(success.data);
      },
      (error) => { console.log("error") },
    );
  }

  edit(id) {
    this.ageEligibilityService.get(id).subscribe(
      (response) => {
        this.model = response.data;
        window.scroll(0,0);
      },
      (error) => { this.errorMessage = JSON.parse(JSON.stringify(error)).error.message; }
    )
  }

  delete(id) {
    debugger;
    var isConfirmed = confirm("Are you sure deleting this criteria?");
    if (isConfirmed) {
      this.ageEligibilityService.delete(id).subscribe(
        (success: string) => {
          this.getAll();
          this.initiation();
        },
        (error) => { this.errorMessage = JSON.parse(JSON.stringify(error)).error.message; }
      );
    }
  }

}
 */