import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgeEligibilityComponent } from './age-eligibility.component';

describe('AgeEligibilityComponent', () => {
  let component: AgeEligibilityComponent;
  let fixture: ComponentFixture<AgeEligibilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgeEligibilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgeEligibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
