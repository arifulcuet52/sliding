import { Component, OnInit, ViewChild } from '@angular/core';
import { MaritalEligibilityService } from '../../../shared/service/remote/eligibility/marital-eligibility/marital-eligibility.service';
import { EligibilityCriteriaMarital } from './../../../entity/eligibility-criteria-marital';
import { MatSort, MatDialog, MatDialogConfig, MatTableDataSource } from '@angular/material';
import { DialogEligibilityMaritalStatusComponent } from '../../../dialog/dialog-eligibility-marital-status/dialog-eligibility-marital-status.component';
import swal from 'sweetalert2';

@Component({
  selector: 'app-marital-status-eligibility',
  templateUrl: './marital-status-eligibility.component.html',
  styleUrls: ['./marital-status-eligibility.component.css']
})
export class MaritalStatusEligibilityComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['sl', 'maritalStatus', 'score', 'status', 'action' ];
  dataSource: any;

  constructor(private dialog: MatDialog, private maritalEligibilityService: MaritalEligibilityService) { }

  ngOnInit() {
    this.refresh();
  }

  open_dialog(model){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {model: model} ;
    // dialogConfig.width = '600px';
    // dialogConfig.height = '450px';
    return this.dialog.open(DialogEligibilityMaritalStatusComponent, dialogConfig);
  }

  add(){
    this.open_dialog(new EligibilityCriteriaMarital()).afterClosed().subscribe(data => {
      if (data !== undefined){
        this.refresh();
      }
    });
  }

  edit(id, index){
    this.open_dialog(this.dataSource.data[index]).afterClosed().subscribe(data => {
      if (data !== undefined){
        this.refresh();
      }
    });
  }

  delete(id, index){
    swal({
      title: 'Warning!',
      text: 'Are you sure you want to delete this?',
      type: 'warning',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
    }).then(res => {
      if (res.value){
        this.maritalEligibilityService.delete(id).subscribe(
          (success) => {
            this.refresh();
           },
          (error) => {
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error.message,
              type: 'error',
              confirmButtonText: 'Close'
            });
           }
        );
      }
    });
  }

  change_status(index, data){

    this.maritalEligibilityService.edit(data.id, data).subscribe(
      (succ) => {
        this.refresh();
      },
      (error) => {
        data.status = false;
        this.reload(index, data, false);
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).error,
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    );
  }

  refresh(){
    this.maritalEligibilityService.getall().subscribe(res => {
      this.dataSource = new MatTableDataSource(res.data) ;
      this.dataSource.sort = this.sort;
    });
  }

  reload(index, model, hasRemove){
    const tempSource = this.dataSource.data;
    if (hasRemove){
      tempSource.splice(index, 1);
    }else{
      if (index > -1){
        tempSource[index] = model;
      }else{
        tempSource.push(model);
      }
    }
    this.dataSource = new MatTableDataSource(tempSource) ;
    this.dataSource.sort = this.sort;
  }

}
