import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaritalStatusEligibilityComponent } from './marital-status-eligibility.component';

describe('MaritalStatusEligibilityComponent', () => {
  let component: MaritalStatusEligibilityComponent;
  let fixture: ComponentFixture<MaritalStatusEligibilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaritalStatusEligibilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaritalStatusEligibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
