import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.icon-menu').on('click', function(){
      $(this).hide('slow');
      $('.icon-close,.home-megamenu').show('slow');
      $('.global-header').addClass('open-homemenu');
    });
    $('.icon-close').on('click', function(){
      $(this).add('.home-megamenu').hide('slow');
      $('.icon-menu').show('slow');
      $('.global-header').removeClass('open-homemenu');
    });
  }

}
