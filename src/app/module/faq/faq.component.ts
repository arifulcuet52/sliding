import { FAQ } from './../../entity/faq';
import { Component, OnInit } from '@angular/core';
import { FaqService } from 'src/app/shared/service/remote/fac/fac.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { DialogFaqComponent } from 'src/app/dialog/dialog-faq/dialog-faq.component';
import { FaqCategory } from 'src/app/entity/faq-category';
import swal from 'sweetalert2';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  categoryId = 0;
  categories = [];
  faqs: FAQ[] = [];
  constructor(private faqService: FaqService, private dialog: MatDialog) { }

  ngOnInit() {
    this.faqService.get_categories().subscribe(res => {
      this.categories = res.data;
    });
  }

  change_category(id: number){
    // const model = new FAQ();
    // model.categoryId = id;

    if (id === 0){
      this.faqs = [];
      // this.faqs.push(model);
    }else{
      this.faqService.get_by_category(id).subscribe(res => {
        this.faqs = res.data;
        // this.faqs.push(model);
      }, (error) => {
        this.faqs = [];
        // this.faqs.push(model);
      });
    }
  }

  add_new_faq(){
    const model = new FAQ();
    model.categoryId = this.categoryId;
    this.faqs.push(model);
  }

  delete_fac(obj: FAQ, index: number){
    swal({
      title: 'Are you sure?',
      text: 'Delete this item.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this.faqService.delete(obj.id).subscribe(res => {
          this.faqs.splice(index, 1);
          swal(
            '',
            'Delete Successfully.',
            'success'
          );
        }, (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error.message,
            type: 'error',
            confirmButtonText: 'Close'
          });
        });
      }
    });

  }

  save(obj, index){
    if (obj.id <= 0){
      this.faqService.save(obj).subscribe(res => {
        this.faqs[index] = res.data;
        swal({
          title: '',
          text: 'Save successfully.',
          type: 'success',
          confirmButtonText: 'Ok'
        });
      }, (error) => {
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).error.message,
          type: 'error',
          confirmButtonText: 'Close'
        });
      });
    }else{
      this.faqService.edit(obj.id, obj).subscribe(res => {
        this.faqs[index] = res.data;
        swal({
          title: '',
          text: 'Update successfully.',
          type: 'success',
          confirmButtonText: 'Ok'
        });
      }, (error) => {
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).error.message,
          type: 'error',
          confirmButtonText: 'Close'
        });
      });
    }
  }

  add_category(){
    this.category_setup(0);
  }

  edit_category(){
    this.category_setup(this.categoryId);
  }

  detete_category(){
    swal({
      title: 'Are you sure?',
      text: 'Delete this category.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {

        const index = this.categories.map(x => x.id).indexOf(this.categoryId);
        this.faqService.delete_category(this.categoryId).subscribe(res => {
          this.categories.splice(index, 1);
          this.faqs = [];
          swal(
            '',
            'Delete Successfully.',
            'success'
          );
        }, (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error.message,
            type: 'error',
            confirmButtonText: 'Close'
          });
        });
      }
    });


  }

  category_setup(id){
    let model = new FaqCategory();
    if (id > 0){
      model = Object.create(this.categories.filter(x => x.id === id)[0]) ;
    }
    this.open_dialog(model).afterClosed().subscribe(data => {
      if (data !== undefined){
        const index = this.categories.map(x => x.id).indexOf(model.id);
        if (index > -1){
          this.categories[index] = data;
          this.categoryId = data.id;
        }else{
          this.categories.push(data);
          this.categoryId = data.id;
        }
      }
    });
  }

  open_dialog(data){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {model: data} ;
    dialogConfig.width = '400px' ;
    return this.dialog.open(DialogFaqComponent, dialogConfig);
  }

}
