import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgencyServiceAdminComponent } from './agency-service-admin.component';

describe('AgencyServiceAdminComponent', () => {
  let component: AgencyServiceAdminComponent;
  let fixture: ComponentFixture<AgencyServiceAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgencyServiceAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgencyServiceAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
