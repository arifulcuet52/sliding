import { Component, OnInit } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import * as _ from "lodash";
import { Pager } from "src/app/entity/pager";
import { NotificationService } from "../../shared/service/remote/notification/notification.service";
import { NotificationTypeEnum } from "../../entity/enums/notification-type.enum";
import { UserNotificationVm } from "../../entity/notification.vm";

@Component({
  selector: "app-user-notification",
  templateUrl: "./user-notification.component.html",
  styleUrls: ["./user-notification.component.scss"]
})
export class UserNotificationComponent implements OnInit {
  notifications = new BehaviorSubject([]);
  notificationCount = 0;
  constructor(private userNotificationService: NotificationService) {}
  finished = true;
  notificationType = NotificationTypeEnum;
  value: number;
  pageNumber: number;
  ngOnInit() {
    this.value = NotificationTypeEnum.OnSite;
    this.notificationCount = 0;
    this.pageNumber = 0;
    this.getNotification();
  }
  onScroll() {
    console.log("scrolled");
    this.pageNumber = this.pageNumber + 1;
    this.getNotification();
    this.userNotificationService.informClickEmitter();
  }
  private getNotification() {
    this.finished = true;
    this.userNotificationService
      .getNotification(this.value, this.pageNumber)
      .subscribe(
        (success: Pager<UserNotificationVm>) => {
          this.finished = false;
          console.log(success);
          this.notificationCount = success.totalItem;
          const newNotifications = success.data;
          const currentNotifications = this.notifications.getValue();
          this.notifications.next(
            _.concat(currentNotifications, newNotifications)
          );
          console.log(this.notifications);
        },
        error => {
          console.log(error);
        }
      );
  }

  onTabChange(type: NotificationTypeEnum) {
    // this.notifications = new BehaviorSubject([]);
    // this.value = type;
    // this.notificationCount = 0;
    // this.pageNumber = 0;
    // this.getNotification();
  }
  onClickNotification(id) {
    this.userNotificationService.updateReadStatus(id).subscribe();
    this.userNotificationService.informClickEmitter();
  }
}
