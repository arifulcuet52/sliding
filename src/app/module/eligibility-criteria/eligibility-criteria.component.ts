import { AgeEligibilityService } from './../../shared/service/remote/eligibility/age-eligibility/age-eligibility.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { EligibilityService } from '../../shared/service/remote/eligibility/eligibility.service';
import { EligibilityCriteria } from './../../entity/eligibility-criteria';
import { MatSort, MatDialog, MatDialogConfig, MatTableDataSource } from '@angular/material';
import { DialogEligibilityCriteriaComponent } from '../../dialog/dialog-eligibility-criteria/dialog-eligibility-criteria.component';
import swal from 'sweetalert2';
import { debug } from 'util';

@Component({
  selector: 'app-eligibility-criteria',
  templateUrl: './eligibility-criteria.component.html',
  styleUrls: ['./eligibility-criteria.component.css']
})
export class EligibilityCriteriaComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['sl', 'minAge', 'maxAge', 'title', 'isExAgent', 'isInvolved',
  'hasCriminalOffence', 'minTotalScore', 'status', 'action' ];
  dataSource: any;
  eduList = [];
  ageList = [];

  constructor(private dialog: MatDialog, private eligibilityService: EligibilityService, private aes: AgeEligibilityService) { }

  ngOnInit() {
    this.refresh();

    this.eligibilityService.get_criteria_edu().subscribe(res => {
      this.eduList = res.data;
    });

    this.aes.gets_active().subscribe(res => {
      this.ageList = res.data;
    });
  }

  open_dialog(model){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {model: model, eduList: this.eduList, ageList: this.ageList} ;
    // dialogConfig.width = '600px';
    // dialogConfig.height = '450px';
    return this.dialog.open(DialogEligibilityCriteriaComponent, dialogConfig);
  }

  add(){
    this.open_dialog(new EligibilityCriteria()).afterClosed().subscribe(data => {
      this.refresh();
    });
  }

  edit(id, index){
    this.open_dialog(this.dataSource.data[index]).afterClosed().subscribe(data => {
      this.refresh();
    });
  }

  delete(id, index){

    this.eligibilityService.delete(id).subscribe(
      (success) => {
        this.refresh();
        swal({
          title: '',
          text: 'Delete successfully',
          type: 'success',
          confirmButtonText: 'Close'
        });
       },
      (error) => {
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).error,
          type: 'error',
          confirmButtonText: 'Close'
        });
       }
    );
  }

  change_status(index, data){

    this.eligibilityService.edit(data.eligibilityCriteriaId, data).subscribe(
      (succ) => {
        this.refresh();
      },
      (error) => {
        data.status = !data.status;
        this.reload(index, data, false);
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).error,
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    );
  }

  refresh(){
    this.eligibilityService.getall().subscribe(res => {
      this.dataSource = new MatTableDataSource(res.data);
      this.dataSource.sort = this.sort;
    });
  }

  reload(index, model, hasRemove){
    const tempSource = this.dataSource.data;
    if (hasRemove){
      tempSource.splice(index, 1);
    }else{
      if (index > -1){
        tempSource[index] = model;
      }else{
        tempSource.push(model);
      }
    }
    this.dataSource = new MatTableDataSource(tempSource) ;
    this.dataSource.sort = this.sort;
  }

}
