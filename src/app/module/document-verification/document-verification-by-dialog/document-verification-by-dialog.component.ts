import { Component, OnInit, Inject } from '@angular/core';
import { UserInfo } from '../../../entity/userInfo';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserService } from '../../../shared/service/remote/user/user.service';
import { ResponseMessage } from '../../../entity/response-message';
import swal from 'sweetalert2';
import { ProfileDocumentService } from '../../../shared/service/remote/profile-document/profile-document.service';
import { DocumentVerificationBy } from '../../../entity/document-verification-by.vm';

@Component({
  selector: 'app-document-verification-by-dialog',
  templateUrl: './document-verification-by-dialog.component.html',
  styleUrls: ['./document-verification-by-dialog.component.scss']
})
export class DocumentVerificationByDialogComponent implements OnInit {

  model: DocumentVerificationBy;
  users: UserInfo[];
  roleName = 'Agency Service Admin';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DocumentVerificationByDialogComponent>,
    public profileDocumentService: ProfileDocumentService,
    public userService: UserService
  ) { }

  ngOnInit() {
    this.model = this.data;
    this.getAgencyServiceAdmin();
  }

  getAgencyServiceAdmin() {
    this.userService.getByRole(this.roleName).subscribe(
      (response: ResponseMessage<UserInfo[]>) => {
        this.users = response.data;
      },
      () => { }
    );
  }

  reassign() {
    console.log(this.model);
    swal({
      title: 'Confirm',
      text: 'Are you sure to reassign?',
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        this.profileDocumentService.reassign(this.model).subscribe(
          (response: any) => {
            if (response.msgCode === 'ok') {
              swal({
                title: 'Success',
                text: response.msg,
                type: 'success',
                confirmButtonText: 'Ok'
              });
              this.dialogRef.close(response);
              return;
            } else {
              swal({
                title: 'Error!',
                text: response.msg,
                type: 'error',
                confirmButtonText: 'Close'
              });
              return;
            }
          },
          (error) => {
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error.message,
              type: 'error',
              confirmButtonText: 'Close'
            });
          }
        );
      }
    });
  }

  close() {
    this.dialogRef.close();
  }
}
