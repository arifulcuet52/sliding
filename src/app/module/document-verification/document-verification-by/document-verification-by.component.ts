import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatDialogConfig, MatDialog } from '@angular/material';
import { DocumentVerificationBy } from 'src/app/entity/document-verification-by.vm';
import { DataService } from 'src/app/shared/service/data/data.service';
import { ProfileDocumentService } from 'src/app/shared/service/remote/profile-document/profile-document.service';
import { DocumentVerificationByDialogComponent } from 'src/app/module/document-verification/document-verification-by-dialog/document-verification-by-dialog.component';
import { PagingModel } from 'src/app/entity/PagingModel';

@Component({
  selector: 'app-document-verification-by',
  templateUrl: './document-verification-by.component.html',
  styleUrls: ['./document-verification-by.component.scss']
})
export class DocumentVerificationByComponent implements OnInit {

  @ViewChild(MatSort)

  public filterStatus = '';
  public searchText = '';
  public completeStatus = false;
  public inProgressStatus = false;
  public filterStatuses: string[] = ['Pending', 'In Progress', 'Completed'];
  pageSize = 10;
  pageNumber = 1;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  public dataSource: any = null;
  sort: MatSort;
  displayedColumns: string[];

  constructor(private dialog: MatDialog,
    private profileDocumentService: ProfileDocumentService,
    public dataService: DataService
  ) { }

  ngOnInit() {
    this.filterStatus = this.filterStatuses[0];
    this.statusChange();
    this.getDocumentVerificationBy();
  }

  filter() {
    this.statusChange();
    this.getDocumentVerificationBy();
  }

  getDocumentVerificationBy() {
    this.profileDocumentService
      .getDocumentVerificationBy(this.filterStatus, this.searchText, this.pageNumber, this.pageSize)
      .subscribe(
        (response: PagingModel<DocumentVerificationBy>) => {
          this.dataSource = new MatTableDataSource(response.data);
          this.dataSource.sort = this.sort;
          this.length = response.totalItem;
        },
        error => {
          console.table(error);
        }
      );
  }

  search() {
    this.getDocumentVerificationBy();
  }

  statusChange() {
    this.completeStatus = this.filterStatus === 'Completed';
    this.inProgressStatus = this.filterStatus === 'In Progress';

    if (this.completeStatus) {
      this.displayedColumns = ['candidateId', 'candidateName', 'faCode', 'trainingCompletionDate', 'trainingExamPassingDate', 'dateOfFinalDocumentSubmission',
        'verificationPendingCount', 'assignedAdmin', 'assignedDate', 'completedDate'];
    }
    else if (this.inProgressStatus) {
      this.displayedColumns = ['candidateId', 'candidateName', 'faCode', 'trainingCompletionDate', 'trainingExamPassingDate', 'dateOfFinalDocumentSubmission',
        'verificationPendingCount', 'approveDocCount', 'rejectDocCount', 'assignedAdmin', 'assignedDate'];
    } else {
      this.displayedColumns = ['candidateId', 'candidateName', 'faCode', 'trainingCompletionDate', 'trainingExamPassingDate', 'dateOfFinalDocumentSubmission'];
    }
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageNumber = $event.pageIndex + 1;
    this.getDocumentVerificationBy();
  }

  reassign(documentVerificationBy) {
    this.openDialog(documentVerificationBy).afterClosed().subscribe(data => {
      if (data !== undefined) {
        this.getDocumentVerificationBy();
      }
    });
  }

  openDialog(model: DocumentVerificationBy) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = model;
    return this.dialog.open(DocumentVerificationByDialogComponent, dialogConfig);
  }
}
