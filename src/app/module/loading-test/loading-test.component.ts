import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/service/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import {
  TypeHeadSettings,
  TypeHeadResponseType
} from 'src/app/module/typehead/entity/typehead.entity';

@Component({
  selector: 'app-loading-test',
  templateUrl: './loading-test.component.html',
  styleUrls: ['./loading-test.component.css']
})
export class LoadingTestComponent implements OnInit {
  setting: TypeHeadSettings = new TypeHeadSettings();
  constructor(private authService: AuthService) {
    this.setting.showPropertyKey = 'fullName';
    this.setting.listTextFormate = '<p >{{userId}} - {{fullName}}</p>';
    this.setting.returnPropertyKey = 'id';
    this.setting.placeHolderText = 'search....';
    this.setting.responseType = TypeHeadResponseType.complex;
    this.setting.endpointUrl = this.authService.APPSERVER + 'user';
    this.setting.querystring = 'userName';
  }

  ngOnInit() {}
  itemChange(e) {
    console.log(e);
  }
}
