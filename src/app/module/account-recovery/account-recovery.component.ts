import { Component, OnInit } from '@angular/core';
import * as feather from 'feather-icons';
import { AuthService } from '../../shared/service/auth/auth.service';
import { OtpService } from '../../shared/service/remote/otp/otp.service';
import { Route, Router } from '@angular/router';
import { OTPVm } from '../../entity/otpVm';
import { OTPIssueReasonEnum } from '../../entity/otp-issue-reason-enum';
@Component({
    selector: 'app-account-recovery',
    templateUrl: './account-recovery.component.html',
    styleUrls: ['./account-recovery.component.css']
})
export class AccountRecoveryComponent implements OnInit {
    otpdeliveryMethod = 0;
    email: string;
    errorMessage = '';
    disable = true;
    constructor(private otp: OtpService, private router: Router) { }

    ngOnInit() {
        feather.replace();
    }

    getOTP() {
        this.disable = false;
        this.otp.setOTPDeliveryMethod(this.otpdeliveryMethod);
        this.otp.generateOTP(this.otpdeliveryMethod, OTPIssueReasonEnum.ForgetPassword, this.email).subscribe(
            (succces: any) => {
                console.log(succces);
                this.router.navigate(['/ap/forget-password', succces.path]);
                this.otp.setOtp({ path: succces.path, allowAfter: succces.allowAfter, message: succces.message });
                this.otp.setOTPMail(this.email);
                this.disable = true;
            },
            (error) => {
                console.log(error);
                this.errorMessage = error.error.message;

                this.otp.setOtp(new OTPVm());
                this.disable = true;
            }
        );

    }

}
