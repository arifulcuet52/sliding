import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallCenterAdminDashboardComponent } from './call-center-admin-dashboard.component';

describe('CallCenterAdminDashboardComponent', () => {
  let component: CallCenterAdminDashboardComponent;
  let fixture: ComponentFixture<CallCenterAdminDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallCenterAdminDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallCenterAdminDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
