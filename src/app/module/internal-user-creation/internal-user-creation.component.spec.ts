import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalUserCreationComponent } from './internal-user-creation.component';

describe('InternalUserCreationComponent', () => {
  let component: InternalUserCreationComponent;
  let fixture: ComponentFixture<InternalUserCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalUserCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalUserCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
