import { Component, OnInit } from '@angular/core';
import { DepartmentService } from '../../shared/service/remote/department/department.service';
import { DesignationService } from '../../shared/service/remote/designation/designation.service';
import { Department } from '../../entity/department';
import { Designation } from '../../entity/designation';
import { UserInfoService } from '../../shared/service/remote/user-info/user-info.service';
import { ColleactionAdminVm } from '../../entity/CollectionAdminCreation';

@Component({
  selector: 'app-internal-user-creation',
  templateUrl: './internal-user-creation.component.html',
  styleUrls: ['./internal-user-creation.component.scss']
})
export class InternalUserCreationComponent implements OnInit {
  departments: Department[];
  designations: Designation[];
  model: ColleactionAdminVm;
  public filePicture: File = null;
  public formData: FormData = new FormData();
  public collectionAdminPicUrl = null;
  public picture_name: string;
  public tempImage = 'images/avatar.png';
  constructor(
    private departmentService: DepartmentService,
    private designationService: DesignationService,
    private userInfoService: UserInfoService
  ) {}

  ngOnInit() {
    this.model = new ColleactionAdminVm();
    this.model.profilePicSrc = '';
    this.getDepartmentList();
    this.getDesignationList();
  }

  getDepartmentList() {
    this.departmentService.getAll().subscribe(
      (success: Department[]) => {
        this.departments = success;
        //console.table(success);
      },
      error => {
        console.log(error);
      }
    );
  }
  getDesignationList() {
    // this.designationService.getAll().subscribe(
    //   (success: Designation[]) => {
    //     // console.log(success);
    //     this.designations = success;
    //   },
    //   (error) => {
    //     console.log(error);
    //   }
    // );
  }

  onSubmit() {
    console.log(this.model.id);
    if (!this.model.id) {
      if (this.formData.has('model')) {
        this.formData.delete('model');
      }
      this.formData.append('model', JSON.stringify(this.model));
      this.userInfoService.saveCollectionAdmin(this.formData).subscribe(x => x);
    }
  }

  onPictureChange(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      this.filePicture = event.target.files[0];
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (e: any) => {
        // called once readAsDataURL is completed
        this.collectionAdminPicUrl = e.target.result;
        this.model.profilePicSrc = this.collectionAdminPicUrl;
        this.picture_name = this.filePicture.name;
      };
      if (this.formData.has('profilePicture')) {
        this.formData.delete('profilePicture');
      }
      this.formData.append(
        'profilePicture',
        this.filePicture,
        this.filePicture.name
      );
      if (this.model.id) {
        this.userInfoService
          .upload_picture(this.model.id, this.formData)
          .subscribe(res => {});
      }
    }
  }
}
