import { DialogVivaResultComponent } from './../../dialog/dialog-viva-result/dialog-viva-result.component';
import { commonEnum } from './../../entity/enums/enum';
import { VivaResultService } from './../../shared/service/remote/viva-result/viva-result.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatSort, MatTableDataSource, MatDialogConfig } from '@angular/material';
import { DataService } from 'src/app/shared/service/data/data.service';
import { VivaResult } from 'src/app/entity/viva-result';
import swal from 'sweetalert2';
import { AccountService } from 'src/app/shared/service/remote/authentication/account.service';


@Component({
  selector: 'app-viva-result',
  templateUrl: './viva-result.component.html',
  styleUrls: ['./viva-result.component.scss']
})
export class VivaResultComponent implements OnInit {
  @ViewChild(MatSort)
  sort: MatSort;
  searchText = '';
  pageSize = 10;
  pageIndex = 1;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  dataSource: any;
  tempData: any;
  displayedColumns: string[] = ['sl', 'userId', 'fullName', 'faCode', 'taggedUmBmName',
   'campaignName', 'campaignEndDate', 'statusName', 'remarks', 'dateOfAcknowledgement', 'action' ];

   gridType = 'CAMPAIGN';
   gridTypes = ['CAMPAIGN', 'NON-CAMPAIGN'];

   filterType = null;
   filterTypes = ['Pending', 'Completed'];

   isAgencyExecutiveAdmin = false;

  status = commonEnum;
  constructor(
    private router: Router,
    private dialog: MatDialog,
    private vrs: VivaResultService,
    public ds: DataService,
    private accountService: AccountService
  ) {}

  ngOnInit() {
    this.checkRole();
    this.get_grid_column();
    this.search();
  }
  checkRole(){
    this.isAgencyExecutiveAdmin = this.accountService.checkRole('AGENCY EXECUTIVE Admin');
    console.log(this.isAgencyExecutiveAdmin);
  }
  get_grid_column(){
    // todo
    if (this.gridType === 'CAMPAIGN' && this.isAgencyExecutiveAdmin){
      this.displayedColumns = ['sl', 'userId', 'fullName', 'faCode', 'taggedUmBmName',
      'campaignName', 'campaignEndDate', 'statusName', 'remarks', 'dateOfAcknowledgement', 'action' ];
    }
    else if (this.gridType === 'NON-CAMPAIGN' && this.isAgencyExecutiveAdmin){
      this.displayedColumns = ['sl', 'userId', 'fullName', 'faCode', 'taggedUmBmName',
      'statusName', 'remarks', 'dateOfAcknowledgement', 'action' ];
    }
    else if (this.gridType === 'CAMPAIGN' && !this.isAgencyExecutiveAdmin && this.filterType === 'Completed'){
      this.displayedColumns =  ['sl', 'userId', 'fullName', 'faCode', 'taggedUmBmName',
      'campaignName', 'campaignEndDate', 'aeName', 'statusName', 'remarks', 'dateOfAcknowledgement'];
    }
    else if (this.gridType === 'NON-CAMPAIGN' && !this.isAgencyExecutiveAdmin && this.filterType === 'Completed'){
      this.displayedColumns =  ['sl', 'userId', 'fullName', 'faCode', 'taggedUmBmName',
       'aeName', 'statusName', 'remarks', 'dateOfAcknowledgement']; }
    else if (this.gridType === 'CAMPAIGN' && !this.isAgencyExecutiveAdmin && this.filterType !== 'Completed'){
        this.displayedColumns =  ['sl', 'userId', 'fullName', 'faCode', 'taggedUmBmName', 'campaignName', 'campaignEndDate',
         'aeName']; }
    else if (this.gridType === 'NON-CAMPAIGN' && !this.isAgencyExecutiveAdmin && this.filterType !== 'Completed'){
      this.displayedColumns = ['sl', 'userId', 'fullName', 'faCode', 'taggedUmBmName', 'aeName'];
    }
  }

  get_filter_column(){
    if (this.filterType === 'Completed' && this.gridType === 'CAMPAIGN'){
      this.displayedColumns =  ['sl', 'userId', 'fullName', 'faCode', 'taggedUmBmName',
      'campaignName', 'campaignEndDate', 'aeName', 'statusName', 'remarks', 'dateOfAcknowledgement'];
    }
    else if (this.filterType === 'Completed' && this.gridType === 'NON-CAMPAIGN'){
      this.displayedColumns =  ['sl', 'userId', 'fullName', 'faCode', 'taggedUmBmName',
       'aeName', 'statusName', 'remarks', 'dateOfAcknowledgement'];
    }
    else if (this.gridType === 'CAMPAIGN') {
      this.displayedColumns =  ['sl', 'userId', 'fullName', 'faCode', 'taggedUmBmName',
      'campaignName', 'campaignEndDate', 'aeName'];
    }
    else{
      this.displayedColumns =  ['sl', 'userId', 'fullName', 'faCode', 'taggedUmBmName', 'aeName'];
    }
    console.log(this.filterType);
    this.search();
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.search();
  }

  search() {
    this.vrs
      .get_candidate_viva_result(this.searchText, this.pageSize, this.pageIndex, this.filterType)
      .subscribe(res => {
        this.dataSource = new MatTableDataSource(res['data'].data);
        this.dataSource.sort = this.sort;
        this.length = res['data'].totalItem;
        console.log(this.dataSource);
      });
  }

  filter(searchText: string) {
    this.searchText = searchText;
    this.pageIndex = 1;
    this.search();
  }

  pass(userId: number, index: number){

    swal({
      title: 'Are you sure?',
      text: 'Candidate pass the viva successfully.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {

        const model = this.set_model_property(index);
        model.statusId = this.status.pass;
        model.remarks = '';

        if (model.id > 0){
          this.vrs.edit(model.id, model).subscribe(res => {
            this.map_obj_value(index, res.data);
            swal(
              '',
              'Update Successfully.',
              'success'
            );
          });
        }else{
          this.vrs.save(model).subscribe(res => {
            this.map_obj_value(index, res.data);
            swal(
              '',
              'Save Successfully.',
              'success'
            );
          });
        }
      }
    });
  }

  fail(userId: number, index: number){
    const model = this.set_model_property(index);
    this.open_dialog(model).afterClosed().subscribe(data => {
      this.map_obj_value(index, data);
    });
  }

  open_dialog(data){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {model: data} ;
    dialogConfig.width = '400px' ;
    return this.dialog.open(DialogVivaResultComponent, dialogConfig);
  }

  map_obj_value(index, data){
      this.dataSource.data[index].vivaResultId = data.id;
      this.dataSource.data[index].statusId = data.statusId;
      this.dataSource.data[index].statusName = data.statusName;
      this.dataSource.data[index].remarks = data.remarks;
      this.dataSource.data[index].dateOfAcknowledgement = data.dateOfAcknowledgement;
      this.dataSource.sort = this.sort;
  }

  set_model_property(index){
    const model = new VivaResult();
    model.id = this.dataSource.data[index].vivaResultId;
    model.userId = this.dataSource.data[index].userId;
    model.statusId = this.dataSource.data[index].statusId;
    model.remarks = this.dataSource.data[index].remarks;
    model.dateOfAcknowledgement = this.dataSource.data[index].dateOfAcknowledgement;
    return model;
  }

}
