import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VivaResultComponent } from './viva-result.component';

describe('VivaResultComponent', () => {
  let component: VivaResultComponent;
  let fixture: ComponentFixture<VivaResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VivaResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VivaResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
