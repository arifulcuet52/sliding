import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SearchService } from '../../shared/service/search/search.service';
import * as typeahead from 'jquery-typeahead';
import * as $ from 'jquery';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  responses: string[] = [];
  searchText = "";
  constructor(private route: ActivatedRoute, private searchService: SearchService) { }

  ngOnInit() {
    console.log(typeahead);
    this.jqueryTypehead();
    this.route.paramMap.subscribe(parms => {
      this.searchText = parms.get("text");
      this.responses = [] = this.searchService.populateLink(this.searchText);
    })
  }
  search() {

    this.responses = [] = this.searchService.populateLink(this.searchText);
  }

  change(event) {
    let a = new  FormData();
    a.append('aaaa',event.target.files[0],event.target.files[0].name);

    console.log(a.get('aaaa'));
  }
  public jqueryTypehead() {
    const access = this;
    $.typeahead({
      input: '.js-typeahead-country_v1',
      minLength: 1,
      maxItem: 2,
      order: "asc",
      source: {
        data: function () {
          return access.searchService.populateLink(this.query).map(x => x['innerText']);
        }
      },
      callback: {
        onClick: function (node, a, item, event) {
          access.searchText = item['display'];
        },
        onInit: function (node) {
          //   console.log('Typeahead Initiated on ' + node.selector);
        }
      }
    });
  }
}
