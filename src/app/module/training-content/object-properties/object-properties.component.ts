import { Component, OnInit, Input, DoCheck, KeyValueDiffers } from '@angular/core';
import { presentation, slides, Property } from '../../../entity/trainingContent/Presentation';
import { TrainingService } from './../training-content-creation/training.service';
import { ControlName } from './../training-controls-name';
declare var $: any;
@Component({
  selector: 'app-object-properties',
  templateUrl: './object-properties.component.html',
  styleUrls: ['./object-properties.component.scss']
})
export class ObjectPropertiesComponent implements OnInit, DoCheck {
  @Input() property: Property;// = new Property();
  @Input() slides: slides[] = [];
  public nextSlideNo: number;
  public prevSlideNo: number;
  //public property: Property = new Property();
  //public propertySet: Property = new Property();
  public differ: any;
  constructor(public differs: KeyValueDiffers) { this.differ = differs.find({}).create(); }

  ngDoCheck() {
    //this.property=JSON.parse(JSON.stringify(this.propertySet));
    // console.log('do check....', this.propertySet);
    // var changes = this.differ.diff(this.propertySet);
    // if (changes) {
    //   console.log('changes.....');
    //   this.property = JSON.parse(JSON.stringify(this.propertySet));
    // }
  }

  ngOnInit() {
    TrainingService.propertySet.subscribe((rec: any) => {
      this.property = JSON.parse(rec) || new Property();
      console.log('subscribe..', this.property);
    },
      (error) => { console.log('error..'); });
  }


  fnApplyProperty() {
    console.log('aply.. ', this.property);
    $(ControlName.selectedClass).each((i, e) => {
      let elementId = $(e).attr('id');
      this.property.id=elementId;
      $(e).attr('property', JSON.stringify(this.property));
      $('#' + elementId).addClass('enable-data-property');
      // $('#'+elementId).data('property', JSON.stringify(this.property));
      if (this.property != null) {
        if (this.property.initialDisplayProp == 'Hidden') {
          $('#' + elementId).addClass('display-hidden');
        } else if (this.property.initialDisplayProp == 'Show') {
          $('#' + elementId).removeClass('display-hidden');
          $('#' + elementId).removeClass('full-time-hidden');
        } else if (this.property.initialDisplayProp == 'Full Time Hidden') {
          $('#' + elementId).removeClass('display-hidden');
          $('#' + elementId).addClass('full-time-hidden');
        } else if (this.property.dynamicDragDrop != null) {
          $('#' + elementId).removeClass('dynamic-droppable');
          $('#' + elementId).removeClass('dynamic-draggable');
          $('#' + elementId).addClass(this.property.dynamicDragDrop);
        }
        if (this.property.dynamicDragDropShowHide != null) {
          $('#' + elementId).removeClass('dynamic-hide-after-drop');
          $('#' + elementId).removeClass('dynamic-show-after-drop');
          $('#' + elementId).addClass(this.property.dynamicDragDropShowHide);
        }

        //to check text 
        if (this.property.changeText != null && this.property.changeText != "") {
          $(".ui-selected").each((i, e) => {
              let id = $(e).attr('id');
              $('#' + id + ' p').text(this.property.changeText);
          });
        } else {
          $(".ui-selected").each((i, e) => {
              let id = $(e).attr('id');
              $('#' + id + ' p').text('');
          })
        }

        //to change text color
        if (this.property.textColor != null && this.property.textColor != "") {
          $(".ui-selected").each((i, e) => {
              let id = $(e).attr('id');
              $('#' + id + ' p').css('color',this.property.textColor);
          });
        } 
        //to apply opacity
        if ((this.property.opacity || null) != null) {
          $('#' + elementId).css('opacity', this.property.opacity);
        }

        // to apply background color
        if (this.property.borderLine || null != null) {
          $('#' + elementId).css('border', this.property.borderLine + 'px solid ' + this.property.borderColor);
        } else {
          $('#' + elementId).css('border', '');
        }

        //to apply background color        
        //$('#' + elementId).css('background-color', this.property.backgroundColor || '');
        if ($('#' + elementId).hasClass('square-shape') || $('#' + elementId).hasClass('curve-button-shape')) {
          $('#' + elementId + ' > div:first-child').css('background-color', this.property.backgroundColor);
        } else {
          $('#' + elementId).css('background-color', this.property.backgroundColor);
        }

        //to apply enabled disabled property
        if ((this.property.enabledDisabledProperty || null) != null) {
          if (this.property.enabledDisabledProperty == 'clicked-disabled') {
            $('#' + elementId).addClass('disabled-dynamic-clicked-property');
          } else {
            $('#' + elementId).removeClass('disabled-dynamic-clicked-property');
          }
        } else {
          $('#' + elementId).removeClass('disabled-dynamic-clicked-property');
        }
      }
    });
    //TrainingService.showSuccessMessage('Property applied successfully.');
  }

}
