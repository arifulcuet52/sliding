import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router'
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MultilingualService } from '../../shared/service/multilingual-service';
import { TrainingContentCreationComponent } from './training-content-creation/training-content-creation.component';
import { ObjectPropertiesComponent } from './object-properties/object-properties.component';
import { ContextMenuService } from './context-menu.service';
import { TrainingContentCreationAdminComponent } from './training-content-creation-admin/training-content-creation-admin.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { MakeDraggable, MakeDroppable, Draggable } from '../../shared/modules/p-table/drag-drop-service/drag.n.drop';
import { FilterPipe } from 'src/app/shared/pipe/filterByProperty/filter-By-List.pipe';
const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: TrainingContentCreationAdminComponent },
      { path: 'old', component: TrainingContentCreationComponent },
      { path: 'admin', component: TrainingContentCreationAdminComponent },
    ]
  }
]
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), FormsModule, ColorPickerModule
  ],
  declarations: [
    TrainingContentCreationComponent,
    ObjectPropertiesComponent,
    TrainingContentCreationAdminComponent,
    MakeDraggable,
    MakeDroppable,
    Draggable,
    FilterPipe,
  ],
  providers: [MultilingualService, ContextMenuService]
})
export class TrainingModule { }


