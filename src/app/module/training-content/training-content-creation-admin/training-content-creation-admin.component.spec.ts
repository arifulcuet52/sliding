import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingContentCreationAdminComponent } from './training-content-creation-admin.component';

describe('TrainingContentCreationAdminComponent', () => {
  let component: TrainingContentCreationAdminComponent;
  let fixture: ComponentFixture<TrainingContentCreationAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingContentCreationAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingContentCreationAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
