import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthService } from '../../../shared/service/auth/auth.service';
import { TrainingAdminService } from './training-admin.service';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { TrainingContentService } from '../../../shared/service/remote/training-content/training-content.service';
import { presentation, slides, Property, NextButtonProperty } from '../../../entity/trainingContent/Presentation';
import { ContextMenuService } from '../context-menu.service';
import * as feather from 'feather-icons';
import { ControlName } from '../training-controls-name';
import { MakeDraggable } from '../../../shared/modules/p-table/drag-drop-service/drag.n.drop'
import { timer } from 'rxjs/observable/timer';
import * as html2canvas from 'html2canvas'
declare var $: any;
@Component({
  selector: 'app-training-content-creation-admin',
  templateUrl: './training-content-creation-admin.component.html',
  styleUrls: ['./training-content-creation-admin.component.scss']
})
export class TrainingContentCreationAdminComponent implements OnInit {
  public rightSideBar: boolean = false;
  public leftSidePopup: boolean = false;
  public slide: slides = new slides();
  public order: number = 1;
  public nextSlideNo: number = 0;
  public prevSlideNo: number = 0;
  //public animation: Animation = new Animation();
  public listOfTheme: any[] = [];
  public listOfAvatar: any[] = [];
  public listOfIcon: any[] = [];
  public listOfVideos: any[] = [];
  public listOfImages: any[] = [];
  public listOfAudio: any[] = [];
  public listOfSlides: slides[] = [];
  public listOfDisplaySlide: slides[] = [];
  public listOfPresentation: presentation[] = [];
  public presentation: presentation = new presentation();
  public contextMenu: ContextMenuService = new ContextMenuService();
  public selectedProperty: Property = new Property();
  public nextButtonProp: NextButtonProperty = new NextButtonProperty();
  public searchText: any;
  constructor(private trainingContentService: TrainingContentService, private authService: AuthService) { }

  async ngOnInit() {
    feather.replace();
    await this.fnGetImage('Themes');
    // await this.fnGetImage('Icons');
    // await this.fnGetImage('Avatars');
    // await this.fnGetImage('Images');
    // await this.fnGetImage('Videos');
    // await this.fnGetImage('Audio');
    // await this.addJquery();
    //this.getPresentation();
    this.getPresentationList();
    this.addJquery();
  }

  fnToggleRightBar() {
    this.rightSideBar = (this.rightSideBar == true) ? false : true;
  }

  async getPresentation(id: number) {
    await this.trainingContentService.getAllSlidesByPresentationId(id).subscribe((data: any) => {
      console.log('data of presentaion', data);
      if (data.msgCode == 'Ok') {
        this.presentation = data.data || new presentation();
        console.log('reesponsed slide: ', this.presentation.slideDetails);
        this.fnColumnSorting('order');
        if (this.presentation.slideDetails.length > 0) {// to render the 1st slide
          this.fnRenderSlide(this.presentation.slideDetails[0]);
        }

      }
    });
    this.rightSideBar = false;
  }

  getPresentationList(): void {
    this.trainingContentService.getAllPresentation().subscribe(
      (success: any) => {
        this.listOfPresentation = success.data || [];
      },
      (error: string) => {
        TrainingAdminService.showErrorMessage(JSON.parse(JSON.stringify(error)).error.message);
      },
    );
  }

  public async fnGetImage(type: string) {
    this.searchText = '';
    if (type != "Themes") {
      if (this.leftSidePopup == true && this.selectedTab != type) {
        this.leftSidePopup == true
      } else {
        this.leftSidePopup = (this.leftSidePopup == true) ? false : true;
      }
      this.selectedTab = type;
    }

    await this.trainingContentService.getAllImage(type).then((rec: any) => {
      if (rec.msgCode == "Ok") {
        if (type == "Avatars") {
          this.listOfAvatar = rec.data || [];
        } else if (type == "Icons") {
          this.listOfIcon = rec.data || [];
        } else if (type == "Themes") {
          this.listOfTheme = rec.data || [];
        } else if (type == "Images") {
          this.listOfImages = rec.data || [];
        } else if (type == 'Videos') {
          this.listOfVideos = rec.data || [];
        } else if (type == 'Audio') {
          this.listOfAudio = rec.data || [];
        }

        setTimeout(() => {
          TrainingAdminService.fnRenderDraggableObject($('.image-content .draggable-object'));
          TrainingAdminService.fnRenderDropableObject($(ControlName.outerContainerId));
        }, 1000)
      }

      console.log('image list:', this.listOfImages);
    },
      (error: any) => {
      });
  }

  fnCollectAudioFile() {
    TrainingAdminService.fnCollectAudioFile();
  }

  fnPuaseAllAudio() {
    TrainingAdminService.fnPuaseAllAudio();
  }

  addContentName(item) {

    let index1 = item.lastIndexOf('~') + 1;
    let index2 = item.lastIndexOf('-') + 1;
    return item.substring(Math.max(index1, index2), item.lastIndexOf('.'));
  }
  fnRenderSlide(slide: slides) {
    $.fn.deletedObjectsForControlZ = [];
    this.slide.id = slide.id;
    $('#outerContainer').html(slide.slideContent);
    TrainingAdminService.fnRerenderJqueryAdmin();
    $('.display-hidden').css('display', 'block');
    $('.full-time-hidden').css('display', 'block');
    setTimeout(() => {
      let nextBtnProp = $("#container").attr('data-action') || null;
      this.nextButtonProp = nextBtnProp == null ? new NextButtonProperty() : JSON.parse(nextBtnProp);
      console.log('this.nextButtonProp .............', this.nextButtonProp);
    }, 700);
  }

  fnCloseModal() {
    this.leftSidePopup = false;
  }

  fnApplyNextPrevButtonAction() {
    console.log('this.nextButtonProp: ', this.nextButtonProp);
    if (this.nextButtonProp != null) {
      $("#container").attr('data-action', JSON.stringify(this.nextButtonProp));
    }
  }

  fnDeleteSlide(slide: slides) {
    if (confirm("Do you want to delete slide?")) {
      this.trainingContentService.deleteSlideById(slide.presentationId, slide.id).subscribe(
        (res: any) => {
          if (res.msgCode == 'Ok') {
            TrainingAdminService.showSuccessMessage(res.msg);
            this.presentation.slideDetails = this.presentation.slideDetails.filter((s: slides) => s.id != slide.id);
          } else {
            TrainingAdminService.showErrorMessage(res.msg);
          }
        },
        (err) => {
          TrainingAdminService.showErrorMessage(JSON.parse(JSON.stringify(err)).error.message);
        }
      )
    }
  }

  fnCopySlide(slide: slides) {
    let selectedSlide = JSON.parse(JSON.stringify(slide));
    if (confirm("Do you want to create duplicate slide?")) {
      let currentContent: presentation = new presentation();
      selectedSlide.order = (this.presentation.slideDetails.reduce((maxOrder, val) => val.order > maxOrder ? val.order : maxOrder, 0) + 1);
      //selectedSlide.order = this.slide.order > 0 ? (this.presentation.slideDetails.reduce((maxOrder, val) => val.order > maxOrder ? val.order : maxOrder, 0) + 1) || 1 : 1;
      selectedSlide.id = 0;
      currentContent = JSON.parse(JSON.stringify(this.presentation));
      currentContent.slideDetails = [];
      currentContent.slideDetails.push(selectedSlide);
      this.trainingContentService.savePresentation(currentContent).subscribe(
        (success: any) => {
          console.log(success.data);
          TrainingAdminService.showSuccessMessage(success.msg);
          this.presentation.id = success.data.id || 0;
          this.presentation.slideDetails.push(success.data.slideDetails[0] || null);
          this.addSlideSnapShot(success.data.slideDetails[0].id);
        },
        (error: string) => {
          TrainingAdminService.showErrorMessage(JSON.parse(JSON.stringify(error)).error.message);
        },
      );
    }
  }



  savePresentation() {
    let currentContent: presentation = new presentation();
    const desData = $('#outerContainer').html();
    console.log(this.presentation);
    if ((this.presentation.title || "") == "") {
      alert("Presentation name can't empty.");
      return false;
    }

    this.slide.order = this.slide.id > 0 ? this.slide.order : this.presentation.slideDetails.length || 0 > 0 ? (this.presentation.slideDetails.reduce((maxOrder, val) => val.order > maxOrder ? val.order : maxOrder, 0) + 1) || 1 : 1;
    this.slide.slideContent = desData;
    currentContent = JSON.parse(JSON.stringify(this.presentation));
    currentContent.slideDetails = [];
    currentContent.slideDetails.push(this.slide);
    if (confirm('Do you want to save this slide?')) {
      this.trainingContentService.savePresentation(currentContent).subscribe(
        (success: any) => {
          TrainingAdminService.showSuccessMessage(success.msg);
          console.log(success.data);
          this.presentation.id = success.data.id || 0;
          if (this.slide.id > 0) {
            this.presentation.slideDetails.forEach((rec: slides) => {
              if (rec.id == this.slide.id) {
                rec.slideContent = success.data.slideDetails[0].slideContent || '';
              }
            });
          } else {
            let slide: slides = new slides();
            slide.id = success.data.slideDetails[0].id || 0;
            slide.order = success.data.slideDetails[0].order || 0;
            slide.presentationId = success.data.slideDetails[0].presentationId || 0;
            slide.slideContent = success.data.slideDetails[0].slideContent || '';
            this.slide.id = success.data.slideDetails[0].id || 0;

            //this.presentation.slideDetails.push(success.data.slideDetails[0] || null);
            this.presentation.slideDetails.push(slide || null);
          }
          this.addSlideSnapShot(success.data.slideDetails[0].id);
        },
        (error: string) => {
          TrainingAdminService.showErrorMessage(JSON.parse(JSON.stringify(error)).error.message);
        },
      );
    }
  }

  fnResetSlide() {
    this.slide.id = 0;
    $('#outerContainer').html('<div id="container" class="ui-widget-content" style="height:100%"><div id="inner-container" style=" width: 100%;"></div> </div>');
  }

  fnApplyColor() {
    var colorCode = $('.favcolor').val();
    TrainingAdminService.fnApplyColor(colorCode);
  }

  fnApplyBackgroudTheme(imageUrl: string) {
    TrainingAdminService.fnApplyTheme(imageUrl);
  }

  fnApplyBackgroundOpacity(opacityValue: number) {
    TrainingAdminService.fnApplyOpacity(opacityValue);
  }

  fnUploadImage(event, type) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append('uploadFile', file, file.name);
      this.trainingContentService.addImage(formData, type).subscribe(
        (rec: any) => {
          if (rec.msgCode == "Ok") {
            if (type == "Avatars") {
              this.listOfAvatar.push(rec.data);
            } else if (type == "Themes") {
              this.listOfTheme.push(rec.data);
            } else if (type == "Icons") {
              this.listOfIcon.push(rec.data);
            } else if (type == "Images") {
              this.listOfImages.push(rec.data);
            } else if (type == 'Videos') {
              this.listOfVideos.push(rec.data);
            } else if (type == 'Audio') {
              this.listOfAudio.push(rec.data);
            }
          }
          //this.addJquery();
          setTimeout(() => {
            TrainingAdminService.fnRenderDraggableObject($('.draggable-object'));
          }, 1000)
        },
        () => {
          alert("Failed");
        }
      );

    }
  }

  public selectedTab: string;
  fnSelectedTab(tabName) {
    if (this.selectedTab == tabName) {
      this.selectedTab = "None";
    } else {
      this.selectedTab = tabName;
    }
  }

  async onDrop(target: slides, src: slides) {
    console.log('target:', target);
    console.log('src:', src);
    //let target:slides=this.

    let sourceOrder = src.order || 1;
    let loopeingOrder = sourceOrder + 1;
    await this.presentation.slideDetails.forEach((rec: slides) => {
      if (rec.id == target.id) {
        rec.order = sourceOrder;
      } else if (rec.id == src.id) {
        rec.order = sourceOrder + 1;
      }
      else if (rec.order > sourceOrder) {
        loopeingOrder = loopeingOrder + 1;
        rec.order = loopeingOrder;
      }
    });

    await this.fnColumnSorting('order');

    if (target.order < src.order) {
      let order = 1;
      await this.presentation.slideDetails.forEach((rec: slides) => {
        rec.order = order;
        order = order + 1;
      });
    }

    /// get seperate set to update the order
    this.trainingContentService.fnChangeSlideOrder(this.presentation.slideDetails, this.presentation.id).subscribe((rec: any) => {
      console.log('Successfully reordered');
    },
      (error: any) => { console.log('Fail to reorder') });

  }



  async fnColumnSorting(colName: any, sortingOrder: string = 'asc') {
    if (this.presentation.slideDetails.length < 1) {
      return;
    }

    if (sortingOrder == 'asc') {
      this.presentation.slideDetails = this.presentation.slideDetails.sort((n1, n2) => {
        if (n1[colName] > n2[colName]) { return 1; }
        if (n1[colName] < n2[colName]) { return -1; }
        return 0;
      });

      console.log('order by:', this.presentation.slideDetails);
    } else {
      this.presentation.slideDetails = this.presentation.slideDetails.sort((n1, n2) => {
        if (n1[colName] < n2[colName]) { return 1; }
        if (n1[colName] > n2[colName]) { return -1; }
        return 0;
      });
    }

  }

  addSlideSnapShot(slideId): void {
    console.log("Slide Id: ", slideId);
    let slideCoverImage;
    var element = document.getElementById("outerContainer");
    html2canvas(element).then((canvas) => {
      slideCoverImage = canvas.toDataURL("image/png");
      slideCoverImage = slideCoverImage.replace('data:image/png;base64,', '');
      const imageBlob = this.dataURItoBlob(slideCoverImage);
      const imageFile = new File([imageBlob], "slidescover", { type: 'image/jpeg' });
      let formData: FormData = new FormData();
      formData.append('uploadFile', imageFile, imageFile.name);
      this.trainingContentService.addSlideCoverImage(formData, "Slides", slideId).subscribe(
        (rec: any) => {
          if (rec.msgCode == "Ok") {

          }
        },
        () => {
          alert("Failed");
        }
      );
    });


  }

  dataURItoBlob(dataURI) {
    const byteString = atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([arrayBuffer], { type: 'image/jpeg' });
    return blob;
  }

  addJquery(): void {
    $(document).ready(() => {
      let zindex = 1;
      let $cloneObject;
      let $cloneCanvusElement = new Array();
      let keyBoardButtonPress;
      $.fn.deletedObjectsForControlZ = [];
      $.fn.tagName = function () {
        return this.prop('tagName');
      };

      // btn click action for all
      $(document).on('click', '.btn', function (e) {
        const id = $(this).attr('id');
        $('.hidable-item').css('display', 'none')
        const show = '.' + id + '-show',
          hide = '.' + id + '-hide';
        if ($(show).hasClass('hidable-button')) {
          $(show).css('display', 'flex');
        } else {
          $(show).css('display', 'block');
        }

        $(hide).css('display', 'none');

        const btns = $('.btn');
        let color = '';
        let tag = '';
        btns.each(function (i) {
          const btnId = btns[i].id;
          if (btnId) {
            color = $('#' + btnId).attr('inActiveColor');
            if (color) {
              const first_child = $('#' + btnId).children()[0];
              if (first_child) {
                const tagName = first_child.nodeName
                if (tagName.toLowerCase() === 'div') {
                  first_child.style.background = color;
                } else {
                  $('#' + btnId).css('background', color);
                }
              }
              color = undefined;
            } else {
              const first_child = $('#' + btnId).children()[0];
              // if (first_child) {
              //   const tagName = first_child.nodeName
              //   if (tagName.toLowerCase() === 'div') {
              //     first_child.style.background = 'transparent';
              //   } else {
              //     $('#' + btnId).css('background', 'transparent');
              //   }
              // }
            }
          }
        });
        color = $('#' + id).attr('activeColor');
        if (color) {
          const first_child = $('#' + id).children()[0];
          if (first_child) {
            const tagName = first_child.nodeName
            if (tagName.toLowerCase() === 'div') {
              first_child.style.background = color;
            } else {
              $('#' + id).css('background', color);
            }
          }
        }
      });

      // to hide btns 
      $(document).on('click', '.hidable-button', function (e) {
        const id = $(this).attr('id');
        const show = '.' + id + '-show',
          hide = '.' + id + '-hide';
        if ($(show).hasClass('hidable-button')) {
          $(show).css('display', 'flex');
        } else {
          $(show).css('display', 'block');
        }

        $(hide).css('display', 'none');

        $(this).css('display', 'none');
      });

      this.contextMenu.showContextMenuAdminPart();
      this.contextMenu.hideMenu();
      this.contextMenu.showObjectMenu();
      this.contextMenu.hideObjectMenu();
      this.contextMenu.deactivateButtonAction();
      this.contextMenu.setActiveBGColor();
      this.contextMenu.setDeactiveBGColor();
      this.contextMenu.setHidableItem();
      this.contextMenu.hideItem();
      //this.contextMenu.showHiddenItem();
      this.contextMenu.showHiddenItemAdmin();
      this.contextMenu.setAsButton();
      this.contextMenu.setAsHidableButton();
      zindex = this.contextMenu.sendBack(zindex);
      zindex = this.contextMenu.bringFront(zindex);
      this.contextMenu.unselectAll();
      this.contextMenu.actionforSuccessDrop();
      this.contextMenu.makeSuccessSequenceAfterDrop();
      this.contextMenu.actionAfterUnsuccessfulDrop();


      TrainingAdminService.fnRenderDraggableObject($('.realobject, .btnObj, .draggable-object, .text-editor-draggable'));
      TrainingAdminService.fnRenderDropableObject($(ControlName.outerContainerId));

      // $(document).on('click', '.realobject,.draggable-object, .text-editor-draggable, .btnObj', (ele) => {
      //   let elementId = $(ele.currentTarget).attr('id');
      //   let evt = ele.currentTarget;
      //   let property: any = $(evt).attr('property') || null;
      //   if ($(evt).hasClass('ui-selected')) {
      //     $(evt).removeClass('ui-selected');
      //   } else {
      //     $(evt).addClass('ui-selected');
      //     //set the property
      //     this.selectedProperty = JSON.parse(property) || new Property();
      //   }
      //   //console.log(this.property);
      //   this.fnShowProperties(property);
      // });

      $(document).on('click', '.realobject,.draggable-object, .text-editor-draggable, .btnObj', (ele) => {
        let elementId = $(ele.currentTarget).attr('id');
        let evt = ele.currentTarget;
        let propertySet = $(evt).attr('property') || null;
        console.log('propety set:', propertySet);
        let property: Property = propertySet != null ? JSON.parse($(evt).attr('property')) : new Property();
        console.log('properties:', property);
        keyBoardButtonPress = keyBoardButtonPress || false;
        if (!keyBoardButtonPress || keyBoardButtonPress != 17) {//If no key press 
          if ($(evt).hasClass('ui-selected')) {
            $(evt).removeClass('ui-selected');
            this.selectedProperty = property;
          } else {
            $(ControlName.outerContainerId + ' > div').removeClass('ui-selected');
            $(evt).addClass('ui-selected');
            this.selectedProperty = property;
          }
        }
        else if (keyBoardButtonPress == 17) {//Control press 
          if ($(evt).hasClass('ui-selected')) {
            $(evt).removeClass('ui-selected');
          } else {
            $(evt).addClass('ui-selected');
            this.selectedProperty = property;
          }
        }

        $('#' + elementId).resizable({
          handles: "all",
          alsoResize: '.ui-selected',
          // containment: 'parent'
        });
        $('#' + elementId).draggable(
          { containment: "parent" }
        );
        $('.ui-resizable-se').removeClass('ui-icon');
      });

      //to delete
      $(document).keyup(function (e) {
        $.fn.deletedObjectsForControlZ=[];
        if (e.keyCode === 46) {
          $('.ui-selected').each((i, ele) => {
            let id = $(ele).attr('id');
            $cloneObject = $('#' + id).clone();
            $.fn.deletedObjectsForControlZ.push($cloneObject);
          });
          $(ControlName.outerContainerId + ' .ui-selected').remove();
          //$(ControlName.outerContainerId).remove('.ui-selected');
        }

      });

      $(document).keydown(function (e) {
        keyBoardButtonPress = e.keyCode;
        if (e.ctrlKey && e.keyCode == 86) {//past
          $.each($cloneCanvusElement, (key, value) => {
            const newId = TrainingAdminService.fnGenerateGuid();
            console.log(newId);
            value.attr('id', newId);
            $('#container').append(value);
            var position = $('#' + newId).position();
            $('#' + newId).css({ top: position.top + 10, left: position.left + 10 });
            $('#container').append($('#' + newId));
            $('#' + newId).draggable();
            $cloneCanvusElement = [];
          });
        }
        else if (e.ctrlKey && e.keyCode == 67) {//copy content
          $cloneObject = null;
          $cloneCanvusElement = [];
          console.log(e.ctrlKey, e.keyCode);
          $('.ui-selected').each((i, ele) => {
            let id = $(ele).attr('id');
            $cloneObject = $('#' + id).clone();
            $cloneCanvusElement.push($cloneObject);

          });
        }

        else if (e.ctrlKey && e.keyCode == 88) {//cut content
          $cloneObject = null;
          $cloneCanvusElement = [];
          console.log(e.ctrlKey, e.keyCode);
          $('.ui-selected').each((i, ele) => {
            let id = $(ele).attr('id');
            $cloneObject = $('#' + id).clone();
            const newId = TrainingAdminService.fnGenerateGuid();
            $cloneObject.attr('id', newId);
            $cloneCanvusElement.push($cloneObject);
          });
          $(ControlName.outerContainerId + ' .ui-selected').remove();
        }

        else if (e.ctrlKey && e.keyCode == 90) {//back content
          console.log( $.fn.deletedObjectsForControlZ);
          if ( $.fn.deletedObjectsForControlZ.length > 0) {
            $.fn.deletedObjectsForControlZ.forEach(element => {
              var lastdeleted =  element;
              let id = $(lastdeleted).attr('id');
              $('#container').append(lastdeleted);
              $('#' + id).draggable();
             });
          }
          $.fn.deletedObjectsForControlZ=[];
        }
      });

      $(document).keyup(function (e) {

        // if (e.keyCode === 46) {
        //   $('.ui-selected').each((i, ele) => {

        //     let id = $(ele).attr('id');
        //     $cloneObject = $('#' + id).clone();
        //     $cloneObject.removeClass('canvas-element');
        //     $deletedObjectsForControlZ.push($cloneObject);
        //   });
        //   $('#container > div').remove('.ui-selected');
        // }
        if (e.keyCode == 17) {
          keyBoardButtonPress = undefined;
        }
      });


    });

  }

  fnShowProperties(property: any) {
    console.log('selected prop: ', this.selectedProperty);
    TrainingAdminService.propertySet.next(JSON.parse(property));
  }

  public enableDataPropCollection: Property[] = [];
  public enabledDisabledPropList: Property[] = [];
  fnShowPreview() {
    $('.enable-data-property').bind('click', (ele) => {
      //console.log(ele);
      this.enabledDisabledPropList = [];
      let id = $(ele.currentTarget).attr('id');
      let propertySet = $(ele.currentTarget).attr('property') || null;
      $('#' + id).addClass('visited');
      let property: Property = propertySet != null ? JSON.parse(propertySet) : null;
      if (property != null && property.linkedSlideNumber != null && property.linkedSlideNumber != -1) {
        let selectedSlide = this.presentation.slideDetails.filter((rec: any) => { if (rec.id == property.linkedSlideNumber) { return true; } else { return false; } })
        this.fnRenderSlide(selectedSlide[0]);
        return false;
      }
      //enabled timer
      if (property != null && property.enabledDisabledProperty != null && property.enabledDisabledProperty == 'clicked-disabled') {
        if (property.selfButtonEnabledDelay != null && property.selfButtonEnabledDelay > 0) {
          this.enabledDisabledPropList.push(property);
          //this.fnEnabledButtonUsingTimer(property.nextEnabledButtonSequenseNo, property.nextButtonEnabledDelay);
        }
      } if (property.nextEnabledButtonSequenseNo != null || property.nextEnabledButtonSequenseNo != 0) {
        //timer for next button enabled 
        this.enabledDisabledPropList.push(property);
        //this.fnEnabledButtonUsingTimer(property.nextEnabledButtonSequenseNo, property.nextButtonEnabledDelay);
      }

      //active border
      if (property != null && property.activeBgColor != null && property.activeBgColor != '') {
        $('#' + id + ' > div:first-child').css('background-color', property.activeBgColor);
        //to remove all bg-color        
        this.fnRemoveAllActiveAction(id);
      }

      // if (property != null && property.activeBgColor != null && property.activeBgColor != "") {
      //   $(ele.currentTarget).css('background-color', property.activeBgColor);

      // }

      this.fnEnabledButtonUsingTimer();
    });

    $('.enable-data-property').each((e, elem) => {
      let prop = $(elem).attr('property') || null;
      if (prop != null) {
        this.enableDataPropCollection.push(JSON.parse(prop));
      }
    });
    TrainingAdminService.fnShowSlidePreview()
  }

  fnEnabledButtonUsingTimer() {
    let selectedProperty: Property[] = [];
    let subscribeInfo: any;
    //subscribeInfo.unsubscribe();
    const source: any = timer(1000, 2000);
    subscribeInfo = source.subscribe(val => {
      console.log('timer:...', val);
      this.enabledDisabledPropList.forEach((rec: Property) => {
        if (val == +rec.nextButtonEnabledDelay) {
          this.enableDataPropCollection.forEach((record: Property) => {
            if (+record.sequence == +rec.nextEnabledButtonSequenseNo) {
              $('#' + record.id).removeClass('disabled-dynamic-clicked-property');
            }
          });
        }
      });

      console.log('this.enabledDisabledPropList', this.enabledDisabledPropList);
      this.enabledDisabledPropList = this.enabledDisabledPropList.filter((r: any) => r.nextButtonEnabledDelay > val);
      if (this.enabledDisabledPropList.length == 0) {
        subscribeInfo.unsubscribe();
      }
    });
  }

  fnRemoveAllActiveAction(id: string) {
    $('.enable-data-property').each((i, elem) => {
      console.log('elem ::', elem);
      let domProp = $(elem).attr('property') || null;
      let prop: Property = domProp != null ? JSON.parse(domProp) : null;
      console.log('property set ::', prop);
      let eleId = $(elem).attr('id');
      if (prop != null) {
        if (eleId != id && prop.activeBgColor != null && prop.activeBgColor != '') {
          if ($(elem).hasClass('visited')) {
            $('#' + eleId + ' > div:first-child').css('background-color', prop.visitedBgColor || '');
          }
        }
      }
    });
  }

  preview() {
    //$(".animated").css('display', 'none');
    $('.animated').each((i, ele) => {
      let type = $(ele).attr('data-animation-type') || 0;
      let delay = $(ele).attr('transition-delay') || 0;
      let duration = $(ele).attr('transition-duration') || 0;
      let width = $(ele).width();
      $(ele).css('width', 0)

      if (type == 'slide-down') {
        setTimeout(() => {
          $(ele).slideDown(duration);
        }, delay);
      } else if (type == 'slide-right') {
        setTimeout(() => {
          $(ele).animate({
            width: width
          });
        }, delay);
      }
    });
  }

  fnSavePresentation() {

  }
  fnGenerateId(fileUrl: any) {
    //console.log(fileUrl);
    var urlSplitArray = fileUrl.split('/');
    var id = urlSplitArray[urlSplitArray.length - 1];
    return id;
  }

  fnGetCoverImage(slide: slides) {
    let APILink = this.authService.APPSERVER;
    if (APILink != null && APILink != undefined) {
      APILink = APILink.split('api')[0];
    }
    let url = APILink + '/Assets/Training-Content/Slides/' + slide.id + '.jpg';
    return url;
  }
}
