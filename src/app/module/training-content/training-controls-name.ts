export class ControlName {
  public static selectedClass='.ui-selected';
  public static selecteeClass='.ui-selectee';
  public static mainContainerId = '#container';
  public static outerContainerId = '#outerContainer';
  public static innerContainerId = '#inner-container';
  public static menuClass = '.menu';
  public static menuId = '#menu';
  public static mainRow = '#main-row';  
  public static uiSeletedName ='ui-selected';
  public static uiSeleteeName ='ui-selectee';
  public static dynamicDroppable='dynamic-droppable';
  public static dynamicDraggable='dynamic-draggable';
  

}