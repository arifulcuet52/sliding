import { Injectable } from '@angular/core';
import { TrainingService } from './training-content-creation/training.service';
import { ControlName } from './training-controls-name';
declare var $: any;
@Injectable({
    providedIn: 'root'
})
export class ContextMenuService {
    public menuVisible = false;

    ShowHideMenu(command): void {
        const style = command === 'show' ? 'block' : 'none';
        $('.menu').css({ 'display': style });
        this.menuVisible = command === 'show';
        $(ControlName.menuId).removeClass(ControlName.uiSeletedName);
    }

    showContextMenu(): void {
        $(ControlName.mainContainerId).contextmenu((evt) => {
            evt.preventDefault();
            const off = $(ControlName.mainContainerId).offset();
            const height = $('#main-row').height();
            const width = $('#main-row').width() - $(ControlName.mainContainerId).width();
            const menuWidth = $(ControlName.menuId).width() / 3;
            const menuHeight = $(ControlName.menuId).height() / 1.3;
            $(ControlName.menuId).css('left', `${(evt.pageX - off.left) + (width / 2) + menuWidth}px`);
            $(ControlName.menuId).css('top', `${(evt.pageY) - (height + menuHeight)}px`);
            this.ShowHideMenu('show');
            return false;
        });
    }

    showContextMenuAdminPart(): void {
        $(ControlName.outerContainerId).contextmenu((evt) => {
            evt.preventDefault();
            const off = $(ControlName.mainContainerId).offset();
            const height = $('#main-row').height();
            const width = $('#main-row').width() - $(ControlName.mainContainerId).width();
            const menuWidth = $(ControlName.menuId).width() / 3;
            const menuHeight = $(ControlName.menuId).height() / 1.3;
            $(ControlName.menuId).css('left', `${(evt.pageX - off.left) + (width / 2) + menuWidth}px`);
            //$(ControlName.menuId).css('top', `${(evt.pageY) - (height + menuHeight)}px`);
            $(ControlName.menuId).css('top', `${(evt.pageY) - (height + 30)}px`);
            this.ShowHideMenu('show');
            return false;
        });
    }

    hideMenu(): void {
        // window.addEventListener('click', (e) => {
        //     e.preventDefault();
        //     if (this.menuVisible) { this.ShowHideMenu('hide'); }
        // });
    }

    showObjectMenu(): void {
        $('#show-menu').click((e) => {
            e.preventDefault();
            e.stopPropagation();
            let btns = $('.btn.ui-selected');
            if (btns.length <= 0) {
                btns = $('.hidable-button.ui-selected');
            }
            if (btns) {
                if (btns.length <= 0) {
                    TrainingService.showWarningMessage('Selecte at least one button for this action.');
                } else if (btns.length > 1) {
                    TrainingService.showWarningMessage('You have selected more then button for this action.');
                } else {
                    // do your work here
                    const objClass = btns.attr('id') + '-show';
                    const selectedObject = $('.ui-selected');
                    if (selectedObject && selectedObject.length > 0) {
                        for (let i = 0; i < selectedObject.length; i++) {
                            if (!selectedObject[i].classList.contains('btn')) {
                                selectedObject[i].classList.add(objClass);
                            }
                        }
                    }
                }
            }
            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
    }

    hideObjectMenu(): void {
        $('#hide-menu').click((e) => {
            e.preventDefault();
            e.stopPropagation();
            let btns = $('.btn.ui-selected');
            if (btns.length <= 0) {
                btns = $('.hidable-button.ui-selected');
            }

            if (btns) {
                if (btns.length <= 0) {
                    TrainingService.showWarningMessage('Selecte at least one button for this action.');
                } else if (btns.length > 1) {
                    TrainingService.showWarningMessage('You have selected more then button for this action.');
                } else {
                    // do your work here
                    const objClass = btns.attr('id') + '-hide';
                    const selectedObject = $('.ui-selected');
                    if (selectedObject && selectedObject.length > 0) {
                        for (let i = 0; i < selectedObject.length; i++) {
                            if (!selectedObject[i].classList.contains('btn')) {
                                selectedObject[i].classList.add(objClass);
                            }
                        }
                    }
                }
            }
            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
    }

    deactivateButtonAction(): void {
        $('#none-menu').click((e) => {
            e.preventDefault();
            e.stopPropagation();
            const btns = $('.btn.ui-selected');
            if (btns) {
                if (btns.length <= 0) {
                    TrainingService.showWarningMessage('Selecte at least one button for this action.');
                } else {
                    // do your work here
                    const selectedObject = $('.ui-selected');
                    for (let i = 0; i < btns.length; i++) {
                        const id = btns[i].id;
                        const classShow = '.' + id + '-show';
                        const classHide = '.' + id + '-hide';
                        if (id) {
                            if ($(classShow).hasClass('btn') || $(classShow).hasClass('hidable-button')) {
                                $(classShow).css('display', 'flex');
                            } else {
                                $(classShow).css('display', 'block');
                            }

                            if ($(classHide).hasClass('btn') || $(classHide).hasClass('hidable-button')) {
                                $(classHide).css('display', 'flex');
                            } else {
                                $(classHide).css('display', 'block');
                            }

                            const hide = $(classHide);
                            if (hide && hide.length > 0) {
                                hide.each(function (j) {
                                    $(this).removeClass(classHide.replace('.', ''));
                                });
                            }

                            const show = $(classShow);
                            if (show && show.length > 0) {
                                show.each(function (j) {
                                    $(this).removeClass(classShow.replace('.', ''));
                                });
                            }
                        }
                    }
                }
            }
            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
    }

    setActiveBGColor(): void {
        $('#active-bg-menu').click((e) => {
            e.stopPropagation();
            const el = $('#bg-color-picker');
            el[0].click();
            let color = '';
            $('#bg-color-picker').change(function () {
                color = $(this).val();
                const btns = $('.btn.ui-selected');
                btns.each(function (i) {
                    $(this).attr('activeColor', color);
                });
            });
            $('#bg-color-picker').val('#ff0000');
            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
    }

    setDeactiveBGColor(): void {
        $('#inactive-bg-menu').click((e) => {
            e.stopPropagation();
            $('#ibg-color-picker')[0].click();
            let color = '';
            $('#ibg-color-picker').change(function () {
                color = $(this).val();
                const btns = $('.btn.ui-selected');
                btns.each(function (i) {
                    $(this).attr('inActiveColor', color);
                });
            });
            $('#ibg-color-picker').val('#ff0000');
            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
    }

    setHidableItem(): void {
        $('#hidable-item-menu').click((e) => {
            e.stopPropagation();
            const items = $('.ui-selected');
            items.each(function () {
                if (!$(this).hasClass('btn')) {
                    $(this).addClass('hidable-item');
                }
            });

            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
    }

    hideItem(): void {
        $('#hide-item-menu').click((e) => {
            e.stopPropagation();
            $('.ui-selected').each(function (i) {
                if (!$(this).hasClass('btn')) {
                    $(this).css('display', 'none');
                }
            });
            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
    }

    showHiddenItem(): void {
        $("#show-hidden-menu").click((e) => {
            e.stopPropagation();
            const child = $("#container").children();
            child.each(function (i) {
                if (child[i].classList.contains('btn') || child[i].classList.contains('hidable-button')) {
                    child[i].style.display = 'flex';
                } else {
                    child[i].style.display = 'block';
                }
            });

            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
    }

    showHiddenItemAdmin(): void {
        $("#show-hidden-menu").click((e) => {
            e.stopPropagation();
            const child = $(ControlName.outerContainerId).children();
            child.each(function (i) {
                if (child[i].classList.contains('btn') || child[i].classList.contains('hidable-button')) {
                    child[i].style.display = 'flex';
                } else {
                    child[i].style.display = 'block';
                }
            });

            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
    }

    setAsButton(): void {
        $('#button-menu').click((e) => {
            if ($('.ui-selected').hasClass('hidable-button')) {
                $('.ui-selected').removeClass('hidable-button');
            }
            $('.ui-selected').addClass('btn');
            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
    }

    setAsHidableButton(): void {
        $('#hidable-button-menu').click((e) => {
            if ($('.ui-selected').hasClass('btn')) {
                $('.ui-selected').removeClass('btn');
            }
            $('.ui-selected').addClass('hidable-button');
            // remove all .btn from .hidable-button
            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
    }

    sendBack(zindex): number {
        $('#back-menu').click((e) => {
            e.preventDefault();
            e.stopPropagation();
            const item = $('.ui-selected');
            if (item) {
                if (item.length <= 0) {
                    TrainingService.showWarningMessage('No item is selected for this action.');
                } else if (item.length > 1) {
                    TrainingService.showWarningMessage('More then one item selection is not allowed for this action.');
                } else {
                    // do your work here
                    const prevIndex = item[0].style['z-index'];
                    let index = 0;
                    if (prevIndex) {
                        index = parseInt(prevIndex, 0);
                        index = index - 1;
                    } else {
                        zindex = zindex - 1;
                        index = zindex;
                    }
                    if (index < 0) {
                        index = 0;
                        zindex = 1;
                    }
                    item[0].style['z-index'] = index;
                }
            }
            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
        return zindex;
    }

    bringFront(zindex): number {
        $('#front-menu').click((e) => {
            e.preventDefault();
            e.stopPropagation();
            const item = $('.ui-selected');
            if (item) {
                if (item.length <= 0) {
                    TrainingService.showWarningMessage('No item is selected for this action.');
                } else if (item.length > 1) {
                    TrainingService.showWarningMessage('More then one item selection is not allowed for this action.');
                } else {
                    // do your work here
                    const prevIndex = item[0].style['z-index'];
                    let index = 0;
                    if (prevIndex) {
                        index = parseInt(prevIndex, 0);
                        index = index + 1;
                        zindex = index;
                    } else {
                        zindex = zindex + 1;
                        index = zindex;
                    }
                    item[0].style['z-index'] = index;
                }
            }
            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
        return zindex;
    }

    unselectAll(): void {
        $(document).on('click', '#inner-container', (e) => {
            e.preventDefault();
            e.stopPropagation();
            const selected = $('.ui-selected');
            if (selected && selected.length > 0) {
                selected.each(function (i) {
                    selected[i].classList.remove('ui-selected');
                });
            }
            if (this.menuVisible) {
                this.ShowHideMenu('hide');
            }
        });
    }

    actionforSuccessDrop() {
        $('#actionforSuccessDrop').click((e) => {
            let countDroppableItem = 0;
            let droppableItemId: string;

            //to check the items
            $(ControlName.selectedClass).each((i, ele) => {
                console.log('selected item:', ele);
                console.log($(ele).hasClass(ControlName.dynamicDroppable));
                if ($(ele).hasClass(ControlName.dynamicDroppable)) {
                    countDroppableItem = countDroppableItem + 1;
                    droppableItemId = $(ele).attr('id');
                }
            });

            if (countDroppableItem == 0) {
                TrainingService.showWarningMessage('Please select droppable item and apply droppable property from property section and try again.');
            } else if (countDroppableItem > 1) {
                TrainingService.showWarningMessage('More then one droppable item you have selected. Please try again.');
            } else {
                //to apply class for success
                $(ControlName.selectedClass).each((i, ele) => {
                    console.log($(ele).hasClass(ControlName.dynamicDroppable));
                    if (!$(ele).hasClass(ControlName.dynamicDroppable)) {
                        $(ele).addClass(droppableItemId + '-success display-hidden');
                    }
                });
            }
            this.ShowHideMenu('hide');
        });

    }

    actionAfterUnsuccessfulDrop() {
        $('#actionAfterUnsuccessfulDrop').click((e) => {
            let countDroppableItem = 0;
            let droppableItemId: string;

            //to check the items
            $(ControlName.selectedClass).each((i, ele) => {
                console.log('selected item:', ele);
                console.log($(ele).hasClass(ControlName.dynamicDroppable));
                if ($(ele).hasClass(ControlName.dynamicDroppable)) {
                    countDroppableItem = countDroppableItem + 1;
                    droppableItemId = $(ele).attr('id');
                }
            });

            if (countDroppableItem == 0) {
                TrainingService.showWarningMessage('Please select droppable item and apply droppable property from property section and try again.');
            } else if (countDroppableItem > 1) {
                TrainingService.showWarningMessage('More then one droppable item you have selected. Please try again.');
            } else {
                //to apply class for success
                $(ControlName.selectedClass).each((i, ele) => {
                    if (!$(ele).hasClass(ControlName.dynamicDroppable)) {
                        $(ele).removeClass(droppableItemId + '-success');
                        $(ele).addClass(droppableItemId + '-unsuccess display-hidden');
                    }
                });
            }
            this.ShowHideMenu('hide');
        });

    }

    makeSuccessSequenceAfterDrop() {
        $('#makeSuccessSequesnce').click((e) => {
            let countDroppableItem = 0;
            let countDrDraggableItem = 0;
            let droppableItemId: string;
            let draggableItemId: string;

            //to check the items
            $(ControlName.selectedClass).each((i, ele) => {
                console.log('selected item:', ele);
                console.log($(ele).hasClass(ControlName.dynamicDroppable));
                if ($(ele).hasClass(ControlName.dynamicDroppable)) {
                    countDroppableItem = countDroppableItem + 1;
                    droppableItemId = $(ele).attr('id');
                }
                else if ($(ele).hasClass(ControlName.dynamicDraggable)) {
                    countDrDraggableItem = countDrDraggableItem + 1;
                    draggableItemId = $(ele).attr('id');
                }
            });

            if (countDroppableItem < 1 || countDrDraggableItem < 1) {
                TrainingService.showWarningMessage('Please select droppable and draggable item and apply again.');
            } else {
                //to apply class for success
                $(ControlName.selectedClass).each((i, ele) => {
                    if ($(ele).attr('id') == draggableItemId || $(ele).attr('id') == droppableItemId) {
                        console.log('item id:', $(ele).attr('id'));
                        $(ele).attr('success-sequesnce', droppableItemId + '-result-success');
                    }
                });
            }
            this.ShowHideMenu('hide');
        });

    }

}
