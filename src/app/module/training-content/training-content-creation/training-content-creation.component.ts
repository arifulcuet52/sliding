import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ObjectDesign } from './../../../entity/training/objectDesign';
import { TrainingService } from './training.service';
import { TrainingContentService } from '../../../shared/service/remote/training-content/training-content.service';
import { presentation, slides, Property } from '../../../entity/trainingContent/Presentation';
import { ContextMenuService } from '../context-menu.service';

declare var $: any;
declare var BalloonEditor: any;
declare var CKEDITOR: any;

@Component({
    selector: 'app-training-content-creation',
    templateUrl: './training-content-creation.component.html',
    providers: [ContextMenuService],
    styleUrls: ['./training-content-creation.component.scss']
})
export class TrainingContentCreationComponent implements OnInit, AfterViewInit {
    public editorValue = '';
    title = 'drag-drop-jquery';
    slideName = '';
    public ExistingData = [{ Title: 'Training Batch', Id: 1 }];
    public badge = 'badge-secondary';
    public Mode = 'Save';
    public bg = 'bg-success';
    public avatar = 'O3ENJ59Avatar page 1.JPG';
    public textColor = 'text-white';
    public button = 'Button';
    public paragraph = 'Paragraph';
    public objectDesign = new ObjectDesign();
    public listOfTheme: any[] = [];
    public listOfAvatar: any[] = [];
    public listOfIcon: any[] = [];
    public listOfVideos: any[] = [];
    public listOfImages: any[] = [];
    public listOfAudio: any[] = [];
    public listOfSlides: slides[] = [];
    public listOfDisplaySlide: slides[] = [];
    public listOfPresentation: presentation[] = [];
    public presentation: presentation = new presentation();
    public slide: slides = new slides();
    public order: number = 1;
    public animation: Animation = new Animation();
    public isPresentationChanged = false;
    public buttonText: string;
    public audioCollection: any[] = [];
    public timerSubcribe: any;
    public contextMenu: ContextMenuService;
    public property:Property=new Property();

    constructor(private trainingContentService: TrainingContentService, private menu: ContextMenuService) {
        this.contextMenu = menu;
     }
    /**
    ************* IMPORTANT NOTICE **********************
    * Jquery UI is edited to protect inner-container selection and follwing line is added
    * if (that.selectees[0].id != 'inner-container') {
    *      ... jquery ui code to add class .ui-selectee
    *      ... if condition will be closed after following loop
    *      that.selectees.each( function() {}
    *  }
    */
    async ngOnInit() {
        await this.fnGetImage('Themes');
        await this.fnGetImage('Icons');
        await this.fnGetImage('Avatars');
        await this.fnGetImage('Images');
        await this.fnGetImage('Videos');
        await this.fnGetImage('Audio');
        await this.addJquery();
        this.getPresentation();

       
    }

    getContainer() {
        // you'll get your through 'elements' below code
        localStorage.setItem('DragDrop', $('#container').html());
    }
    ngAfterViewInit() {
       
    }

    changePresentation(presentationId: number): void {
        this.isPresentationChanged = true
        let presentation = this.listOfPresentation.filter((x: presentation) => { if (x.id == presentationId) { return true } else { return false } })[0];
        this.presentation.id = presentation.id;
        this.presentation.title = presentation.title;
        this.slideName = this.presentation.title;
        //get all slides of specific presentation 
        this.trainingContentService.getAllSlidesByPresentationId(presentationId).subscribe(
            (data: any) => {
                console.log( data.data);
                this.listOfDisplaySlide = data.data.slideDetails || [];
            },
            (error) => {
                TrainingService.showErrorMessage(JSON.parse(JSON.stringify(error)).error.message);
            }
        )
    }

    getPresentation(): void {
        this.trainingContentService.getAllPresentation().subscribe(
            (success: any) => {
                this.listOfPresentation = success.data || [];
            },
            (error: string) => {
                TrainingService.showErrorMessage(JSON.parse(JSON.stringify(error)).error.message);
            },
        );
    }

    fnGenerateId(fileUrl: any) {
        //console.log(fileUrl);
        var urlSplitArray = fileUrl.split('/');
        var id = urlSplitArray[urlSplitArray.length - 1];
        return id;
    }

    fnCollectAudioFile() {
        TrainingService.fnCollectAudioFile();
    }

    fnPuaseAllAudio() {
        TrainingService.fnPuaseAllAudio();
    }

    fnRenderSlide(slide: slides) {
        this.slide.id = slide.id;
        $('#container').html(slide.slideContent);
        TrainingService.fnRerenderJquery();
    }

    fnDeleteSlide(slide: slides) {
        if (confirm("Do you want to delete slide?")) {
            this.trainingContentService.deleteSlideById(slide.presentationId, slide.id).subscribe(
                (res: any) => {
                    if (res.msgCode == 'Ok') {
                        TrainingService.showSuccessMessage(res.msg);
                        this.listOfDisplaySlide = this.listOfDisplaySlide.filter((s: slides) => s.id != slide.id);
                    } else {
                        TrainingService.showErrorMessage(res.msg);
                    }
                },
                (err) => {
                    TrainingService.showErrorMessage(JSON.parse(JSON.stringify(err)).error.message);
                }
            )
        }
    }

    savePresentation(): void {
        const desData = $('#card-body').html();

        this.slide.order = this.order;
        this.slide.slideContent = desData;
        this.presentation.slideDetails.push(this.slide);
        this.trainingContentService.savePresentation(this.presentation).subscribe(
            (success: any) => {
                TrainingService.showSuccessMessage(success.msg);
                this.listOfSlides.push(this.slide);
                this.order = this.slide.order + 1;
            },
            (error: string) => {
                TrainingService.showErrorMessage(JSON.parse(JSON.stringify(error)).error.message);
            },
        );
    }

    fnResetSlide() {
        this.slide.id = 0;
        $('#container').html('<div _ngcontent-c3="" id="inner-container" style="height: 100vh; max-height: 100%; width: 100%;"></div>');
    }

    fnApplyColor() {
        var colorCode = $('.favcolor').val();
        TrainingService.fnApplyColor(colorCode);
    }

    fnApplyBackgroudTheme(imageUrl: string) {
        TrainingService.fnApplyTheme(imageUrl);
    }

    fnApplyBackgroundOpacity(opacityValue: number) {
        TrainingService.fnApplyOpacity(opacityValue);
    }

    fnApplyButtonText() {
        $(".ui-selected").each((i, e) => {
            if ($(e).hasClass('btn')) {
                let id = $(e).attr('id');
                $('#' + id + ' p').text(this.buttonText);
            }
        });
    }

    public async fnGetImage(type: string) {
        await this.trainingContentService.getAllImage(type).then((rec: any) => {
            if (rec.msgCode == "Ok") {
                if (type == "Avatars") {
                    this.listOfAvatar = rec.data || [];
                } else if (type == "Icons") {
                    this.listOfIcon = rec.data || [];
                } else if (type == "Themes") {
                    this.listOfTheme = rec.data || [];
                } else if (type == "Images") {
                    this.listOfImages = rec.data || [];
                } else if (type == 'Videos') {
                    this.listOfVideos = rec.data || [];
                } else if (type == 'Audio') {
                    this.listOfAudio = rec.data || [];
                }
            }
        },
            () => {
            });
    }


    fnUploadImage(event, type) {
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let formData: FormData = new FormData();
            formData.append('uploadFile', file, file.name);
            this.trainingContentService.addImage(formData, type).subscribe(
                (rec: any) => {
                    if (rec.msgCode == "Ok") {
                        if (type == "Avatars") {
                            this.listOfAvatar.push(rec.data);
                        } else if (type == "Themes") {
                            this.listOfTheme.push(rec.data);
                        } else if (type == "Icons") {
                            this.listOfIcon.push(rec.data);
                        } else if (type == "Images") {
                            this.listOfImages.push(rec.data);
                        } else if (type == 'Videos') {
                            this.listOfVideos.push(rec.data);
                        } else if (type == 'Audio') {
                            this.listOfAudio.push(rec.data);
                        }
                    }
                    //this.addJquery();
                    setTimeout(() => {
                        TrainingService.fnRenderDraggableObject($('.draggable-object'));
                    }, 1000)
                },
                () => {
                    alert("Failed");
                }
            );

        }
    }

    public selectedTab: string;
    fnSelectedTab(tabName) {
        if (this.selectedTab == tabName) {
            this.selectedTab = "None";
        } else {
            this.selectedTab = tabName;
        }
    }

    addJquery(): void {

        $(document).ready(() => {
            let zindex = 1;
            $.fn.tagName = function () {
                return this.prop('tagName');
            };
            $.fn.removeClassStartingWith = function (filter) {
                $(this).removeClass(function (index, className) {
                    return (className.match(new RegExp('\\S*' + filter + '\\S*', 'g')) || []).join(' ');
                });
                return this;
            };

            $(document).on('click', '.btn', function (e) {
                const id = $(this).attr('id');
                $('.hidable-item').css('display', 'none')
                const show = '.' + id + '-show',
                    hide = '.' + id + '-hide';
                if ($(show).hasClass('hidable-button')) {
                    $(show).css('display', 'flex');
                } else {
                    $(show).css('display', 'block');
                }

                $(hide).css('display', 'none');

                const btns = $('.btn');
                let color = '';
                let tag = '';
                btns.each(function (i) {
                    const btnId = btns[i].id;
                    if (btnId) {
                        color = $('#' + btnId).attr('inActiveColor');
                        if (color) {
                            const first_child = $('#' + btnId).children()[0];
                            if (first_child) {
                                const tagName = first_child.nodeName
                                if (tagName.toLowerCase() === 'div') {
                                    first_child.style.background = color;
                                } else {
                                    $('#' + btnId).css('background', color);
                                }
                            }
                            color = undefined;
                        } else {
                            const first_child = $('#' + btnId).children()[0];
                            if (first_child) {
                                const tagName = first_child.nodeName
                                if (tagName.toLowerCase() === 'div') {
                                    first_child.style.background = 'transparent';
                                } else {
                                    $('#' + btnId).css('background', 'transparent');
                                }
                            }
                        }
                    }
                });
                color = $('#' + id).attr('activeColor');
                if (color) {
                    const first_child = $('#' + id).children()[0];
                    if (first_child) {
                        const tagName = first_child.nodeName
                        if (tagName.toLowerCase() === 'div') {
                            first_child.style.background = color;
                        } else {
                            $('#' + id).css('background', color);
                        }
                    }
                }
            });

            $(document).on('click', '.hidable-button', function (e) {
                const id = $(this).attr('id');
                const show = '.' + id + '-show',
                    hide = '.' + id + '-hide';
                if ($(show).hasClass('hidable-button')) {
                    $(show).css('display', 'flex');
                } else {
                    $(show).css('display', 'block');
                }

                $(hide).css('display', 'none');

                $(this).css('display', 'none');
            });

           
            this.contextMenu.showContextMenu(); 
            this.contextMenu.hideMenu();         
            this.contextMenu.showObjectMenu();
            this.contextMenu.hideObjectMenu();
            this.contextMenu.deactivateButtonAction();
            this.contextMenu.setActiveBGColor();
            this.contextMenu.setDeactiveBGColor();
            this.contextMenu.setHidableItem();
            this.contextMenu.hideItem();
            this.contextMenu.showHiddenItem();
            this.contextMenu.setAsButton();
            this.contextMenu.setAsHidableButton();
            zindex = this.contextMenu.sendBack(zindex);
            zindex = this.contextMenu.bringFront(zindex);
            this.contextMenu.unselectAll();


            TrainingService.fnRenderDraggableObject($('.realobject, .btnObj, .draggable-object, .text-editor-draggable'));
            TrainingService.fnRenderDropableObject($('#container'));
           
            $(document).on('click', '.realobject,.draggable-object, .text-editor-draggable, .btnObj', (ele) => {
                let elementId = $(ele.currentTarget).attr('id');
                let evt = ele.currentTarget;
                let property:any=$(evt).attr('property')||null;
                if ($(evt).hasClass('ui-selected')) {
                    $(evt).removeClass('ui-selected');                   
                } else {
                    $(evt).addClass('ui-selected'); 
                    //set the property
                    this.property=JSON.parse(property)||new Property();
                }
            });

            $(document).keyup(function (e) {
                if (e.keyCode === 46) {
                    $('#container > div').remove('.ui-selected');
                }
            });

          
        });  

    }

    preview() {
 
        $('.animated').each((i, ele) => {
            let type = $(ele).attr('data-animation-type') || 0;
            let delay = $(ele).attr('transition-delay') || 0;
            let duration = $(ele).attr('transition-duration') || 0;
            let width = $(ele).width();
            $(ele).css('width', 0)

            if (type == 'slide-down') {
                setTimeout(() => {
                    $(ele).slideDown(duration);
                }, delay);
            } else if (type == 'slide-right') {
                setTimeout(() => {
                    $(ele).animate({
                        width: width
                    });
                }, delay);
            }
        });
    }

    applyAnimation() {
        let animationType = this.animation.animationType;
        let animationDelay = this.animation.animationDelay || 0;
        let animationDuration = this.animation.animationDuration || 0;
        $('.ui-selected').addClass('animated');
        $('.ui-selected').attr('data-animation-type', animationType);
        $('.ui-selected').attr('transition-delay', animationDelay);
        $('.ui-selected').attr('transition-duration', animationDuration);
    }

}

export class Animation {
    animationType: string;
    animationDelay: number;
    animationDuration: number;
}
