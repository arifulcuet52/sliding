import { timer } from 'rxjs/observable/timer';
import { TrainingContentService } from '../../../shared/service/remote/training-content/training-content.service';
import { presentation, slides, Property } from '../../../entity/trainingContent/Presentation';
import { ControlName } from './../training-controls-name';
declare var $: any;
declare var BalloonEditor: any;
declare var CKEDITOR: any;
import swal from 'sweetalert2';
import { Subject } from 'rxjs/Subject';
export class TrainingService {
  constructor(private trainingContentService: TrainingContentService) { }
  public static mainContainerId = '#container';
  public static innerContainerId = '#inner-container';
  public static selectedItemClass = '.ui-selected';
  public static zIndex = 1;
  public static audioCollection: any[] = [];
  public static propertyCollection: any[] = [];
  public static timerSubcribe: any;
  public static propertySet:Subject<any>=new Subject<any>();;

  public static fnApplyTheme(imageUrl: string) {
    $(this.innerContainerId).css({
      'background-image': 'url(' + imageUrl + ')',
      'background-repeat': 'no-repeat',
      'background-size': 'cover'
    });
  }

  public static fnApplyColor(colorCode: string) {
    $('#container .ui-selected').css('background-color', colorCode);
  }

  public static fnApplyOpacity(opacityValue: number) {
    const selected = $(this.selectedItemClass);
    if (selected) {
      console.log(selected);
    }
    $(this.innerContainerId).css('opacity', opacityValue);
  }

  //    public static fnImageUpload(event,type:string) {
  //         if (event.target.files && event.target.files[0]) {
  //           const reader = new FileReader();
  //           //this.filePicture = event.target.files[0];
  //           reader.readAsDataURL(event.target.files[0]); // read file as data url

  //           reader.onload = (e: any) => { // called once readAsDataURL is completed
  //             this.collectionAdminPicUrl = e.target.result;
  //             this.model.profilePicSrc = this.collectionAdminPicUrl;
  //             console.log(e.target);
  //             this.picture_name = this.filePicture.name;
  //           };
  //           if (this.formData.has('profilePicture')) { this.formData.delete('profilePicture'); }
  //           this.formData.append('profilePicture', this.filePicture, this.filePicture.name);
  //           if (this.model.id) {
  //             this.userInfoService.upload_picture(this.model.id, this.formData).subscribe(res => { });
  //           }
  //         }
  //       }

  public static showSuccessMessage(message: string) {
    swal({
      title: 'Success!',
      text: message,
      type: 'success',
      confirmButtonText: 'Close'
    });
  }

  public static showWarningMessage(message: string) {
    swal({
      title: 'Warning!',
      text: message,
      type: 'warning',
      confirmButtonText: 'Close'
    });
  }

  public static showInformationMessage(message: string) {
    swal({
      title: 'Information!',
      text: message,
      type: 'info',
      confirmButtonText: 'Close'
    });
  }

  public static showErrorMessage(message: string) {
    swal({
      title: 'Error!',
      text: message,
      type: 'error',
      confirmButtonText: 'Close'
    });
  }

  public static bindCkEditor(
    $canvasElement: any,
    id: any,
    reRender: boolean = false
  ) {
    $canvasElement.draggable();
    $('.text-container', $canvasElement)
      .mousedown(function (ev) {
        $canvasElement.draggable('disable');
        //console.log('result:', document.getElementById(id).getElementsByClassName("ck-content"));
        $('#' + id).removeClass('ui-selected');
        $('#' + id + ' .text-container').attr('contenteditable', true);
        if (
          document.getElementById(id).getElementsByClassName('cke_editable')
            .length == 0 ||
          reRender
        ) {
          // // BalloonEditor.extraPlugins = 'colorbutton';
          // BalloonEditor.create(document.getElementById(id).getElementsByClassName("text-container")[0])
          //   .catch(error => {
          //     console.error(error);
          //   });
          //console.log("mousedown...");
          CKEDITOR.disableAutoInline = true;
          CKEDITOR.inline(
            document
              .getElementById(id)
              .getElementsByClassName('text-container')[0],
            {
              extraAllowedContent: 'a(documentation);abbr[title];code',
              removePlugins: 'stylescombo',
              extraPlugins: 'justify, colorbutton, font',
              toolbarLocation: 'top',
              // Show toolbar on startup (optional).
              startupFocus: false
            }
          );
        }
      })
      .mouseup(function (ev) {
        // $('#' + id+' .text-container').attr("contenteditable", false);
        //console.log("mouse up..");
        $canvasElement.draggable('enable');
      });

    //$canvasElement.draggable();
    $canvasElement.resizable({
      handles: 'e, w, n, s',
      resize: function () {
        $(this).css('height', 'width');
      }
    });

  }

  public static fnRenderCkEditor($canvasElement, id) {
    //console.log('id', document.querySelector('.ck-editor1'))
    // BalloonEditor.create(document.getElementById(id)).then(() => { console.log('start editor') })
    //   .catch(error => {
    //     console.error(error);
    //   });
    // BalloonEditor.create(document.querySelector('.ck-editor1'))
    //   .catch(error => {
    //     console.error(error);
    //   });
  }

  public static fnGenerateGuid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return (
      s4() +
      s4() +
      '-' +
      s4() +
      '-' +
      s4() +
      '-' +
      s4() +
      '-' +
      s4() +
      s4() +
      s4()
    );
  }

  public static fnRenderDraggableObject($object) {
    $object.draggable({
      helper: 'clone',
      cursor: 'move',
      revert: true,
      start: function () {
        $(this).addClass('ui-draggable-dragging');
      },
      stop: function () {
        $(this).removeClass('ui-draggable-dragging');
      }
    });
  }

  public static selectedObjs:any;
  // public static fnRenderDraggableObject($object) {
  //   $object.draggable({
  //     // helper: 'clone',
  //     // cursor: 'move',
  //     // revert: true,
  //     start: function(event, ui) {
  //       console.log('start....');
  //       //get all selected...
  //       if (ui.helper.hasClass('ui-selected')) this.selectedObjs = $('div.ui-selected');
  //       else {
  //           this.selectedObjs = $(ui.helper);
  //           $('div.ui-selected').removeClass('ui-selected')
  //       }

  //       console.log('selectred :', this.selectedObjs);
  //   },
  //   drag: function(event, ui) {
  //     console.log('start 2....');
  //       var currentLoc = $(this).position();
  //       var prevLoc = $(this).data('prevLoc');
  //       if (!prevLoc) {
  //           prevLoc = ui.originalPosition;
  //       }

  //       var offsetLeft = currentLoc.left-prevLoc.left;
  //       var offsetTop = currentLoc.top-prevLoc.top;

  //       //this.moveSelected(offsetLeft, offsetTop);
  //       console.log('selectred 2:', this.selectedObjs);
  //       this.selectedObjs.each(function(){
  //         let $this =$(this);
  //         var p = $this.position();
  //         var l = p.left;
  //         var t = p.top;
  //         console.log({id: $this.attr('id'), l: l, t: t});
  
  
  //         $this.css('left', l+offsetLeft);
  //         $this.css('top', t+offsetTop);
  //     })

  //       $(this).data('prevLoc', currentLoc);
  //   },
  //   stop:function(){
  //     alert('stop...');
  //   }
  //   });
  // }

  public static moveSelected(ol, ot){
    console.log("moving to: " + ol + ":" + ot);
    this.selectedObjs.each(function(){
        let $this =$(this);
        var p = $this.position();
        var l = p.left;
        var t = p.top;
        console.log({id: $this.attr('id'), l: l, t: t});


        $this.css('left', l+ol);
        $this.css('top', t+ot);
    })
}


  public static fnRenderDropableObjectBack($object) {
    $object.droppable({
      drop: function (event, ui) {
        const id = TrainingService.fnGenerateGuid();
        const $canvas = $(this);
        if (!ui.draggable.hasClass('canvas-element')) {
          const $canvasElement = ui.draggable.clone();
          //remove all selectd class
          $('.ui-selected').removeClass('ui-selected');
          //to assign Id
          $canvasElement.attr('id', id);
          $canvasElement.addClass('canvas-element');
          $canvasElement.removeClass('ui-draggable-dragging');
          if (!ui.draggable.hasClass('text-editor-draggable')) {
            $canvasElement.draggable({
              containment: this.mainContainerId,
              cursor: 'move'
            });
          }

          // if (!ui.draggable.hasClass('btnObj')) {
          $canvasElement.resizable({
            handles: 'e, w, n, s',
            resize: function () {
              $(this).css('height', 'width');
            }
          });
          // }

          $canvas.append($canvasElement);
          $canvasElement.css({
            left: ui.offset.left - $(this).offset().left,
            top: ui.offset.top - $(this).offset().top,
            position: 'absolute',
            'z-index': ui.draggable.hasClass('realobject') ? 0 : this.zIndex,
            margin: '0 auto'
          });
          // if ($canvasElement.hasClass('trigger')) {
          //   $canvasElement.attr(
          //     'data-bind',
          //     Math.random()
          //       .toString(36)
          //       .substring(7)
          //   );
          // }

          if (ui.draggable.hasClass('text-editor-draggable')) {
            //TrainingService.bindTextEditor($canvasElement);
            TrainingService.bindCkEditor($canvasElement, id);
          }
        }
      }
    });
  }

  public static fnRenderDropableObject($object) {
    $object.droppable({
      drop: function (event, ui) {
        const id = TrainingService.fnGenerateGuid();
        const $canvas = $(this);
        if (!ui.draggable.hasClass('canvas-element')) {
          const $canvasElement = ui.draggable.clone();
          //remove all selectd class
          $('.ui-selected').removeClass('ui-selected');
          //to assign Id
          $canvasElement.attr('id', id);
          $canvasElement.addClass('canvas-element');
          $canvasElement.removeClass('ui-draggable-dragging');
          if (!ui.draggable.hasClass('text-editor-draggable')) {
            $canvasElement.draggable({
              containment: this.mainContainerId,
              cursor: 'move'
            });
          }

          // if (!ui.draggable.hasClass('btnObj')) {
          $canvasElement.resizable({
            handles: 'e, w, n, s',
            resize: function () {
              $(this).css('height', 'width');
            }
          });
          // }

          $canvas.append($canvasElement);
          $canvasElement.css({
            left: ui.offset.left - $(this).offset().left,
            top: ui.offset.top - $(this).offset().top,
            position: 'absolute',
            'z-index': ui.draggable.hasClass('realobject') ? 0 : this.zIndex,
            margin: '0 auto'
          });
          if ($canvasElement.hasClass('trigger')) {
            $canvasElement.attr(
              'data-bind',
              Math.random()
                .toString(36)
                .substring(7)
            );
          }

          if (ui.draggable.hasClass('text-editor-draggable')) {
            //TrainingService.bindTextEditor($canvasElement);
            TrainingService.bindCkEditor($canvasElement, id);
          }
        }
      }
    });
  }

  public static fnRerenderJquery() {
    $(document).on('click', '.btn', function (e) {
      const id = $(this).attr('id');
      $('.' + id + '-show').css('display', 'block');
      $('.' + id + '-hide').css('display', 'none');

      const btns = $('.btn');
      let color = '';
      const tag = '';
      btns.each(function (i) {
        const btnId = btns[i].id;
        if (btnId) {
          color = $('#' + btnId).attr('inActiveColor');
          $('#' + btnId).css('background', color);
        }
      });
      color = $('#' + id).attr('activeColor');
      $('#' + id).css('background', color);
    });

    setTimeout(() => {
      $('#container .draggable-object, #container .text-editor-draggable').each((i, e) => {
        // console.log(e);
        console.log($(e).attr('id'));
        const domId = $(e).attr('id');
        console.log('domId', domId);
        //TrainingService.fnRenderDraggableObject($('#' + domId));
        $('#' + domId).draggable({
          containment: '#container',
          cursor: 'move'
        });

        $('#' + domId).resizable({ distroy: true });
        $('#' + domId).resizable({
          handles: 'e, w, n, s',
          resize: function () {
            $(this).css('height', 'width');
          }
        });
      });
      //TrainingService.fnRenderDraggableObject($('#' + domId));

      // TrainingService.fnRenderDraggableObject($('.draggable-object'));
    }, 100);
    // bindCkEditor()

    //TrainingService.fnRenderDropableObject($('#container'));
    $('#container .text-editor-draggable').each((i, e) => {
      const editorId = $(e).attr('id');
      TrainingService.bindCkEditor($('#' + editorId), editorId, true);
    });
  }

  public static fnShowSlidePreview() {    
    $('.display-hidden').css('display', 'none');
    this.propertyCollection = [];
    //console.log($(ControlName.mainContainerId).data('property'));
    $('.enable-data-property').each((i, e) => {
      let id = $(e).attr('id');
      let type = '';
      let property = $(e).attr('property') || null;
      console.log(property);
      if (property != null) {
        if ($(e).hasClass('avatar-shape')) {
          type = 'image';
        } else if ($(e).hasClass('text-editor-draggable')) {
          type = 'text';
        } else if ($(e).hasClass('video-shape')) {
          type = 'video';
        } else if ($(e).hasClass('audio-shape')) {
          type = 'audio';
        }
        property = JSON.parse(property);
        property.id = id;
        property.type = type;
        this.propertyCollection.push(property);
      }
    });
    console.log(this.propertyCollection);
    const source = timer(1000, 2000);
    this.timerSubcribe = source.subscribe(val => { console.log('timer:...', val); this.fnDynamicPreview(val) });
  }

  public static fnDynamicPreview(timer: number) {
    console.log('collection ', this.propertyCollection);
    this.propertyCollection.forEach((rec: any) => {
      if (timer == rec.startTime || timer == rec.endTime || timer == rec.delay) {
        if (rec.type == 'image' || rec.type == 'text') {
          if (timer == rec.delay) {
            $('#' + rec.id).css('display', 'block');
          }
        } else if (rec.type = 'audio') {
          let audio: HTMLVideoElement = <HTMLVideoElement>document.getElementById(rec.id);
          if (timer == rec.startTime) {
            if (audio) {
              console.log('play audio');
              audio.play();
            }
          } else if (timer == rec.endTime) {
            audio.pause();
          }
        }

      }
    });

    this.propertyCollection = this.propertyCollection.filter((r: any) => r.startTime > timer || r.endTime > timer || Number(r.delay) > timer);
    console.log('collection ', this.propertyCollection);
    if (this.propertyCollection.length == 0) {
      this.timerSubcribe.unsubscribe();
    }
  }

  public static fnCollectAudioFile() {
    $(ControlName.mainContainerId + ' audio').each((i, e) => {
      let id = $(e).attr('id');
      let type = '';
      console.log('audio id:', id);
      let startTime = $(e).attr('start-time');
      let endTime = $(e).attr('end-time') || -1;
      console.log($(e).attr('property'));
      let property = $(e).attr('property') || null;
      if (property != null) {
        if ($(e).hasClass('avatar-shape')) {
          console.log('image...');
        } else if ($(e).hasClass('text-editor-draggable')) {
          console.log('edit...');
        } else if ($(e).hasClass('video-shape')) {
          console.log('video...');
        } else if ($(e).hasClass('video-shape')) {
          console.log('video...');
        }
        property = JSON.parse(property);
        property.id = id;
        this.audioCollection.push(property);
      }
    });

    const source = timer(1000, 2000);
    this.timerSubcribe = source.subscribe(val => { console.log('timer:...', val); this.fnPlayAudio(val) });
  }

  public static fnPlayAudio(timer: number) {
    this.audioCollection.forEach((rec: any) => {
      if (timer == rec.startTime || timer == rec.endTime) {
        let audio: HTMLVideoElement = <HTMLVideoElement>document.getElementById(rec.id);
        if (timer == rec.startTime) {
          if (audio) {
            console.log('play audio');
            audio.play();
          }
        } else if (timer == rec.endTime) {
          audio.pause();
        }
      }
    });

    this.audioCollection = this.audioCollection.filter((r: any) => r.startTime > timer || r.endTime > timer);
    if (this.audioCollection.length == 0) {
      this.timerSubcribe.unsubscribe();
    }
  }

  public static fnPuaseAllAudio() {
    this.audioCollection.forEach((rec: any) => {
      if (timer == rec.startTime || timer == rec.endTime) {
        let audio: HTMLVideoElement = <HTMLVideoElement>document.getElementById(rec.id);
        audio.pause();
      }
    });
  }

} 
