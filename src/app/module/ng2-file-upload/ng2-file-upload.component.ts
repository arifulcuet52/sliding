import { Component, OnInit, Directive } from '@angular/core';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-ng2-file-upload',
  templateUrl: './ng2-file-upload.component.html',
  styleUrls: ['./ng2-file-upload.component.scss']
})
export class Ng2FileUploadComponent implements OnInit {
  constructor(public bsModalRef: BsModalRef) {}
  public uploader: FileUploader;
  public hasBaseDropZoneOver = false;
  public hasAnotherDropZoneOver = false;
  public uploadStatus: string;
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  ngOnInit() {
    this.uploadStatus = 'Uploading.....';
    this.uploader.uploadAll();
    this.uploader.onCompleteAll = () => {
      console.log('complete');
      this.uploadStatus = 'Uploaded';
      // this.bsModalRef.hide();
    };
    this.uploader.onSuccessItem = (
      item: any,
      response: string,
      status: number,
      headers: any
    ): any => {
      if (response) {
        console.log('response' + JSON.stringify(response));
      }
      console.log('success');
      //   this.bsModalRef.hide();
    };
    this.uploader.onErrorItem = (
      item: any,
      response: string,
      status: number,
      headers: any
    ): any => {
      console.log(response);
    };
  }
}
