import { Component, OnInit, ViewChild } from '@angular/core';
import { Area } from '../../entity/area';
import { CountryInfo } from '../../entity/country';
import { District } from '../../entity/district';
import { CountryService } from '../../shared/service/remote/country/country.service';
import { DistrictService } from '../../shared/service/remote/district/district.service';
import { AreaService } from '../../shared/service/remote/area/area.service';
import { MatDialog, MatDialogConfig, MatTableDataSource, MatSort } from '@angular/material';
import swal from 'sweetalert2';
import { DialogAreaComponent } from '../../dialog/dialog-area/dialog-area.component';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.css']
})
export class AreaComponent implements OnInit {

  displayedColumns: string[] = ['sl', 'name', 'districtName', 'areaName', 'distances', 'action'];
  dataSource: any;
  @ViewChild(MatSort) sort: MatSort;
  errorMessage = '';
  area: Area;
  areas: Area[] = [];
  countries: CountryInfo[] = [];
  districts: District[] = [];
  countryId = 0;
  constructor(
    private dialog: MatDialog,
    private areaService: AreaService) { }

  ngOnInit() {
    this.getArears();
  }
  getArears() {
    this.areaService.getAll().subscribe(
      (success: any) => {
        this.dataSource = new MatTableDataSource(success);
        this.dataSource.sort = this.sort;
        console.log(this.dataSource);
      }
    );
  }

  open_dialog(model) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { model: model };
    // dialogConfig.width = '600px';
    // dialogConfig.height = '450px';
    return this.dialog.open(DialogAreaComponent, dialogConfig);
  }

  add() {
    this.area = new Area(0, 0, '');
    this.area.district = new District(0, 0, '');
    this.area.district.countryInfo = new CountryInfo();
    this.open_dialog(this.area).afterClosed().subscribe(data => {
      if (data !== undefined) {
        this.getArears();
      }
    });
  }

  save() {
    if (this.area.areaId > 0) {
      this.areaService.update(this.area).subscribe(
        (success) => {
          this.errorMessage = '';
          this.area.areaName = '';
          this.area.distances = 0;
          this.getArears();
        },
        (error) => {
          console.log(error);
          this.errorMessage = JSON.parse(JSON.stringify(error)).error;
        }
      );
    }
    else {
      this.areaService.add(this.area).subscribe(
        (success) => {
          this.errorMessage = '';
          this.area.areaName = '';
          this.area.distances = 0;
          this.getArears();
        },
        (error) => {
          console.log(error);
          this.errorMessage = JSON.parse(JSON.stringify(error)).error;
        }
      );
    }
  }
  cancel() {
    this.area.distances = 0;
    this.area.areaName = '';

  }
  edit(areaId) {
    this.areaService.get(areaId).subscribe(
      (success: Area) => {
        this.open_dialog(success).afterClosed().subscribe(data => {
          if (data !== undefined) {
            this.getArears();
          }
          // this.dataSource[index] = data;
          // this.refresh();
        });
      },
      (error) => {

      }
    );
  }
  delete(areaId) {

    swal({
      title: 'Warning!',
      text: 'Are you sure you want to delete this?',
      type: 'warning',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
    }).then(res => {
      if (res.value){
        this.areaService.delete(areaId).subscribe(
          (success) => {
            this.getArears();
          },
          (error) => {
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error,
              type: 'error',
              confirmButtonText: 'Close'
            });
          }
        );
      }
    });
  }

}
