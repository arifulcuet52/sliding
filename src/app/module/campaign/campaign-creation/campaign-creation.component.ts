import { Component, OnInit } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Campaign } from '../../../entity/campaign';
import { CampaignService } from '../../../shared/service/remote/campaign/campaign.service';
import swal from 'sweetalert2';
import { AuthService } from '../../../shared/service/auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UrlSettingService } from '../../../shared/service/remote/url-setting/url-setting.service';
import { UrlSettingEnum } from '../../../entity/enums/enum';
import { DataService } from '../../../shared/service/data/data.service';

@Component({
  selector: 'app-campaign-creation',
  templateUrl: './campaign-creation.component.html',
  styleUrls: ['./campaign-creation.component.scss']
})
export class CampaignCreationComponent implements OnInit {
  public currentDate: Date = new Date();
  public datePickerConfig: Partial<BsDatepickerConfig>;
  public model: Campaign = new Campaign();
  public durationString: string = " day(s)";
  public validAdminVerification: boolean = true;
  public showCampaignLink: boolean = false;
  public clipboardEvent: any;

  constructor(
    private campaignService: CampaignService,
    private router: Router,
    private route: ActivatedRoute,
    private urlSetting: UrlSettingService,
    private dataService: DataService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params.id > 0) {
        this.model.id = params.id;
        this.getById(params.id);
      }
    });
    // this.getCampaignLink();
    this.datePickerConfig = Object.assign({}, { dateInputFormat: 'DD/MM/YYYY', showWeekNumbers: false });
  }

  getCampaignLink() {
    if (this.model.id > 0) {
      this.generateCampaignLink();
    }
  }

  checkAdminVerification() {
    if (this.model.campaignStartDate != null && this.model.campaignEndDate != null && this.model.adminVerificationEndDate != null) {
      if (this.model.campaignStartDate <= this.model.adminVerificationEndDate && this.model.adminVerificationEndDate <= this.model.campaignEndDate) {
        this.validAdminVerification = true;
      }
      else {
        this.validAdminVerification = false;
      }
    }
  }

  checkAdminVerificationTime(){
    if (this.model.campaignStartDate != null && this.model.campaignEndDate != null && this.model.adminVerificationEndDate != null){
      if (this.model.campaignStartTime != null && this.model.campaignEndTime != null && this.model.adminVerificationTime != null){
        this.model.campaignStartDate = new Date(
          this.model.campaignStartDate.getFullYear(),
          this.model.campaignStartDate.getMonth(),
          this.model.campaignStartDate.getDate(),
          this.model.campaignStartTime.getHours(),
          this.model.campaignStartTime.getMinutes()
        );

        this.model.campaignEndDate = new Date(
          this.model.campaignEndDate.getFullYear(),
          this.model.campaignEndDate.getMonth(),
          this.model.campaignEndDate.getDate(),
          this.model.campaignEndTime.getHours(),
          this.model.campaignEndTime.getMinutes()
        );

        this.model.adminVerificationEndDate = new Date(
          this.model.adminVerificationEndDate.getFullYear(),
          this.model.adminVerificationEndDate.getMonth(),
          this.model.adminVerificationEndDate.getDate(),
          this.model.adminVerificationTime.getHours(),
          this.model.adminVerificationTime.getMinutes()
        );

        if (this.model.campaignStartDate <= this.model.adminVerificationEndDate && this.model.adminVerificationEndDate <= this.model.campaignEndDate) {
          this.validAdminVerification = true;
        }
        else {
          this.validAdminVerification = false;
        }
      }
  
    }

  }

  getDuration() {
    if (this.model.campaignStartDate != null && this.model.campaignEndDate != null) {
      if (this.model.campaignEndDate < this.model.campaignStartDate) {
        this.model.campaignDuration = 0;
      }
      else {
        var duration = Math.ceil(Math.abs(this.model.campaignEndDate.getTime() - this.model.campaignStartDate.getTime()) / 86400000) + 1;
        this.durationString = duration + " day(s)"
        this.model.campaignDuration = duration;
      }
      this.durationString = this.model.campaignDuration + " day(s)";
      // this.checkAdminVerification();
      this.checkAdminVerificationTime();
    }
  }

  getById(id: number) {
    this.campaignService.getById(id).subscribe(
      (succ) => {
        this.model = succ.data;
        this.model.campaignStartDate = this.dataService.utc_to_local_time(succ.data.campaignStartDate);
        this.model.campaignStartTime = this.dataService.utc_to_local_time(succ.data.campaignStartTime);

        this.model.campaignEndDate = this.dataService.utc_to_local_time(succ.data.campaignEndDate);
        this.model.campaignEndTime = this.dataService.utc_to_local_time(succ.data.campaignEndTime);

        this.model.adminVerificationEndDate = this.dataService.utc_to_local_time(succ.data.adminVerificationEndDate);
        this.model.adminVerificationTime = this.dataService.utc_to_local_time(succ.data.adminVerificationTime);

        this.durationString = this.model.campaignDuration + " day(s)";

        this.generateCampaignLink();

      },
      (err) => {
        console.log(err);
      }
    );
  }

  policyPrivilegedChanged() {
    this.model.minPolicyRequired = null;
  }

  generateCampaignLink() {
    // this.copyToClipBoard();
    if (this.model.campaignCode && this.model.campaignCode.length > 0) {
      this.campaignService.getByCode(this.model.campaignCode).subscribe(
        (succ) => {
          console.log(succ.data);
          if (succ.data) {
            //get route prefix
            this.urlSetting.getByCode(UrlSettingEnum.cmpCode).subscribe(
              (succ) => {
                console.log(succ.data);
                this.model.campaignLink = succ.data.url + this.model.campaignCode;
                this.showCampaignLink = true;
              },
              (error) => {
                console.log(error);
              }
            );
          }
          else {
            this.showCampaignLink = false;
            swal({
              title: "Campaign code does not match!",
              text: "Sorry, no such campaign is created with the given code.",
              type: 'warning',
              confirmButtonText: 'Close'
            });
          }

        },
        (error) => {
          console.log(error);
        }
      );
    }
    else {
      this.showCampaignLink = false;
      swal({
        text: "Please, a provide campaign code first.",
        type: 'warning',
        confirmButtonText: 'Close'
      });
    }
  }

  copyToClipBoard() {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.height = '0';
    selBox.value = this.model.campaignLink;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    try {
      var successful = document.execCommand("copy");
      var msg = successful ? 'successful' : 'unsuccessful';
      console.log('Copying text command was ' + msg);
      document.body.removeChild(selBox);
      if (successful) {
        swal({
          text: 'Campaign link copied to clipboard',
          type: 'success',
          confirmButtonText: 'Ok'
        });
      }
    } catch (err) {
      console.log('Oops, unable to copy', err);
    }
  }

  save() {
    if (this.model.id == 0) {
      this.campaignService.create(this.model).subscribe(
        (succ) => {
          this.model = succ.data;
          this.model.campaignStartDate = this.dataService.utc_to_local_time(succ.data.campaignStartDate);
          this.model.campaignStartTime = this.dataService.utc_to_local_time(succ.data.campaignStartTime);

          this.model.campaignEndDate = this.dataService.utc_to_local_time(succ.data.campaignEndDate);
          this.model.campaignEndTime = this.dataService.utc_to_local_time(succ.data.campaignEndTime);

          this.model.adminVerificationEndDate = this.dataService.utc_to_local_time(succ.data.adminVerificationEndDate);
          this.model.adminVerificationTime = this.dataService.utc_to_local_time(succ.data.adminVerificationTime);

          console.log(this.model);

          swal({
            title: 'Success',
            text: 'Successfully created',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Unable to create!',
            text: JSON.parse(JSON.stringify(error)).error.exceptionMessage,
            type: 'warning',
            confirmButtonText: 'Close'
          });
        }
      );
    }
    else {
      this.campaignService.edit(this.model).subscribe(
        (succ) => {
          this.model = succ.results;
          this.model.campaignStartDate = this.dataService.utc_to_local_time(succ.results.campaignStartDate);
          this.model.campaignStartTime = this.dataService.utc_to_local_time(succ.results.campaignStartTime);

          this.model.campaignEndDate = this.dataService.utc_to_local_time(succ.results.campaignEndDate);
          this.model.campaignEndTime = this.dataService.utc_to_local_time(succ.results.campaignEndTime);

          this.model.adminVerificationEndDate = this.dataService.utc_to_local_time(succ.results.adminVerificationEndDate);
          this.model.adminVerificationTime = this.dataService.utc_to_local_time(succ.results.adminVerificationTime);

          console.log(this.model);

          this.generateCampaignLink();

          swal({
            title: 'Success',
            text: 'Successfully updated',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Unable to update!',
            text: JSON.parse(JSON.stringify(error)).error.exceptionMessage,
            type: 'warning',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }
}
