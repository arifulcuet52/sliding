import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { Campaign } from '../../../entity/campaign';
import { CampaignService } from '../../../shared/service/remote/campaign/campaign.service';
import { PagingModel } from '../../../entity/PagingModel';
import { DataService } from '../../../shared/service/data/data.service';
import swal from 'sweetalert2';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-campaign-list',
  templateUrl: './campaign-list.component.html',
  styleUrls: ['./campaign-list.component.scss']
})
export class CampaignListComponent implements OnInit {

  constructor(
    private cmpService: CampaignService,
    private dataService: DataService,
    private router: Router
  ) { }
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = [
    'sl', 
    'cmpName', 
    'cmpCode', 
    'startDate', 
    'endDate', 
    'minPolicyReq', 
    'trainingFeeExempted', 
    'noOfCandidateRegiWithCode', 
    'action'];
  dataSource: any;
  model: Campaign;
  swalWithBootstrapButtons: any;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  searchText = '';
  pageSize = 10;
  pageIndex = 1;
  durationStr: string;

  ngOnInit() {
    this.searchText = '';
    this.model = new Campaign();
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }

  getAll(searchText: string, pageSize: number, pageIndex: number) {
    this.cmpService.getBySearch(searchText, pageSize, pageIndex).subscribe(
      (response: PagingModel<Campaign>) => {
        console.log(response.data);
        this.dataSource = new MatTableDataSource(response.data);
        this.dataSource.sort = this.sort;
        this.length = response.totalItem;
      },
      (error) => {
        console.log('error');
      }
    );
  }

  pageChange($event) {
    console.log($event);
    window.scrollTo(0, 0);

    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
    console.log($event);
  }

  filter(searchText: string) {
    this.searchText = searchText;
    this.pageIndex = 1;
    this.getAll(this.searchText, this.pageSize, this.pageIndex);

  }

  add() {
    this.router.navigate(['/ap/campaign']);
  }

  edit(index) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: this.dataSource.data[index].id
      }
  }
  this.router.navigate(['/ap/campaign'], navigationExtras);
  }

  delete(id) {
    const swalWithBootstrapButtons = swal.mixin({
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger mr-3',
      buttonsStyling: false,
    });

    swalWithBootstrapButtons({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.cmpService.delete(id).subscribe(
          (success) => {
            console.log('deleted');
            console.log(success);
            this.getAll(this.searchText, this.pageSize, this.pageIndex);
          },
          (error) => {
            console.log('can\'t delete');
            console.log(error);
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error.exceptionMessage,
              type: 'error',
              confirmButtonText: 'Close'
            });
          }
        );
      } else if (
        result.dismiss === swal.DismissReason.cancel
      ) {
        /* swalWithBootstrapButtons(
          'Cancelled',
          'Nothing was deletd :)',
          'error'
        ) */
      }
    });
  }

}
