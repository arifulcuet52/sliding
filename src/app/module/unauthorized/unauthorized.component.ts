import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../shared/service/remote/authentication/account.service';

@Component({
  selector: 'app-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.css']
})
export class UnauthorizedComponent implements OnInit {

  constructor(private accountService: AccountService) { }
  message: string;
  ngOnInit() {
    this.message = this.accountService.getUnauthrize();
  }

}
