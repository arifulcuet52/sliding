import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-document-viewer',
  templateUrl: './document-viewer.component.html',
  styleUrls: ['./document-viewer.component.scss']
})
export class DocumentViewerComponent implements OnInit {
  filePath: any;
  fileType: string;
  fileExt: string;
  iframe: SafeResourceUrl;
  header: string;
  type: string;

  constructor(private sanitizer: DomSanitizer, public bsModalRef: BsModalRef) {}

  ngOnInit() {

    if (this.type === 'saferesourceurl'){
       this.iframe = this.filePath;
       return;
    }

    if (this.filePath) {
      this.fileExt = this.filePath.substring(
        this.filePath.lastIndexOf('.', this.filePath.length)
      );

      if (this.fileExt.toLowerCase() === '.pdf') {
        this.fileType = 'pdf';
        this.iframe = this.sanitizer.bypassSecurityTrustResourceUrl(
          this.filePath
        );
      } else {
         this.fileType = 'image';
      }
    }
  }
}
