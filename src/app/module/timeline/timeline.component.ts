import { CandidateProfileInfo } from './../../entity/candidate-profile-info';
import { DataService } from './../../shared/service/data/data.service';
import { Component, OnInit } from '@angular/core';
import * as feather from 'feather-icons';
import { UtilityService } from 'src/app/shared/service/utility/utility.service';
import { CandidateInfoService } from 'src/app/shared/service/remote/candidate-info/candidate-info.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../shared/service/auth/auth.service';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {
  logs: any = [];
  activities: any = [];
  public candidateProfile = new CandidateProfileInfo();
  apiUrl: string;

  constructor(public ds: DataService, private utilityService: UtilityService, private activatedRoute: ActivatedRoute,
    private candidateInfoService: CandidateInfoService, private authService: AuthService) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(x => {
      const userId = x['id'];
      this.candidateInfoService.getCandidateInfoByCandidateId(userId).subscribe(res => {
        this.candidateProfile = res.data;
        console.log(this.candidateProfile);
      });
      this.utilityService.get_timeline_logs(userId).subscribe(res => {
        this.generate_logs(res['data']);
        setTimeout(function() {
          feather.replace();
        }, 1000);
      });
    });
    this.apiUrl = this.authService.APPSERVER;
  }

  generate_logs(data) {
    this.logs = [];
    const currentDate = new Date();
    const todayslog = [];
    const previouslog = [];
    data.forEach(element => {
      element['createDate'] = this.ds.utc_to_local_time(element['createDate']);
      if (this.ds.IsDateEqual(element['createDate'], currentDate)) {
        todayslog.push(element);
      } else {
        previouslog.push(element);
      }
    });

    if (todayslog.length) {
      this.logs.push({ activities: todayslog, name: 'Today' });
    }
    if (previouslog.length) {
      this.logs.push({ activities: previouslog, name: 'History' });
    }
  }
}
