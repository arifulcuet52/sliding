import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateTagRequestComponent } from './candidate-tag-request.component';

describe('CandidateTagRequestComponent', () => {
  let component: CandidateTagRequestComponent;
  let fixture: ComponentFixture<CandidateTagRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateTagRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateTagRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
