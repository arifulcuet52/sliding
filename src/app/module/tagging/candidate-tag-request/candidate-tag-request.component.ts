import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { CandidateTaggingService } from '../../../shared/service/remote/candidate-tagging/candidate-tagging.service';
import { TagRequestVm } from '../../../entity/tag-result.vm';
import { PagingModel } from '../../../entity/PagingModel';
import { StatusEnum } from '../../../entity/status.enum';
import swal from 'sweetalert2';
import { DataService } from 'src/app/shared/service/data/data.service';
@Component({
  selector: 'app-candidate-tag-request',
  templateUrl: './candidate-tag-request.component.html',
  styleUrls: ['./candidate-tag-request.component.css']
})
export class CandidateTagRequestComponent implements OnInit {
  displayedColumns: string[] = [
    'candidateUserId',
    'fullName',
    'age',
    'educationName',
    'requestDateTime',
    'reponseDateTime',
    'statusName',
    'action'
  ];
  dataSource: any;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageSize = 10;
  pageIndex = 1;
  searchText: string;
  status: any = StatusEnum;
  @ViewChild(MatSort)
  sort: MatSort;
  constructor(
    private dialog: MatDialog,
    private candidateTaggingService: CandidateTaggingService,
    private dataService: DataService
  ) {}
  ngOnInit() {
    this.getAllRequest(this.searchText, this.pageIndex, this.pageSize);
  }

  getAllRequest(searchText: string, pageIndex: number, pageSize: number) {
    if (!searchText) {
      searchText = ' ';
    }
    this.candidateTaggingService
      .getAllTagRequest(searchText, pageIndex, pageSize)
      .subscribe(
        (success: PagingModel<TagRequestVm>) => {
          // console.log(success);
          success.data['requestDateTime'] = this.dataService.utc_to_local_time(
            success.data['requestDateTime']
          );
          success.data['reponseDateTime'] = this.dataService.utc_to_local_time(
            success.data['reponseDateTime']
          );
          this.dataSource = new MatTableDataSource(success.data);
          this.length = success.totalItem;
        },
        error => {
          console.log(error);
        }
      );
  }
  changeStatus(id: number, status: number) {
    let statusEnum: StatusEnum;
    let message = 'You want to ';
    if (status === 1) {
      statusEnum = StatusEnum.Accepted;
      message = message + 'accept the request.';
    } else {
      statusEnum = StatusEnum.Rejected;
      message = message + 'reject the request.';
    }

    swal({
      title: 'Are you sure?',
      text: message,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Submit'
    }).then(result => {
      if (result.value) {
        this.candidateTaggingService.changeTagStatus(statusEnum, id).subscribe(
          success => {
            this.getAllRequest(this.searchText, this.pageIndex, this.pageSize);
          },
          error => {
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error.message,
              type: 'error',
              confirmButtonText: 'Close'
            });
          }
        );
      }
    });
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.getAllRequest(this.searchText, this.pageIndex, this.pageSize);
  }
  filter(searchText: string) {
    this.searchText = searchText;
    this.pageIndex = 1;
    this.getAllRequest(this.searchText, this.pageIndex, this.pageSize);
  }
}
