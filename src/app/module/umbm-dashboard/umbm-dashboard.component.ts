import {
  Component,
  OnInit,
  AfterViewInit,
  AfterViewChecked
} from '@angular/core';
import * as feather from 'feather-icons';
import { AccountService } from 'src/app/shared/service/remote/authentication/account.service';
import { Router } from '@angular/router';
import '../../entity/extension/array.extension';
import { DashboardService } from 'src/app/shared/service/remote/dashboard/dashboard.service';
@Component({
  selector: 'app-umbm-dashboard',
  templateUrl: './umbm-dashboard.component.html',
  styleUrls: ['./umbm-dashboard.component.scss']
})
export class UmbmDashboardComponent implements OnInit, AfterViewChecked {
  headerGroup: HeaderGroup[] = [];
  childGroup: ChildGroup[] = [];
  tchildGroup: ChildGroup[] = [];
  constructor(
    public accountService: AccountService,
    private router: Router,
    private dashboard: DashboardService
  ) {}

  ngOnInit() {
    feather.replace();
    this.getumbmDashboard();
  }

  getumbmDashboard() {
    this.dashboard.getUMBMDashboard().subscribe(
      success => {
        this.headerGroup = success['data']['headerGroup'];
        this.childGroup = success['data']['childGroup'];
        this.headerGroup = this.headerGroup.orderByPropertyNames(
          'sortOrder',
          'title'
        );
        this.childGroup = this.childGroup.orderByPropertyNames(
          'groupKey',
          'sortOrder',
          'key'
        );
      },
      error => {
        console.log(error);
      }
    );
  }

  getDetailsElement(parentKey) {
    return this.childGroup.filter(x => x.groupKey === parentKey);
    //  return this.tchildGroup;
  }

  ngAfterViewChecked(): void {
    // console.log('asas');
    feather.replace();
  }
  gotourl(routeLink) {
    if (routeLink) {
      if (routeLink.includes('?')) {
        const params = {};
        const allQuery = routeLink.split('&');
        for (let index = 0; index < allQuery.length; index++) {
          const element = allQuery[index];

          let key = '';
          let value = '';
          if (index === 0) {
            const firstElement = allQuery[0].substring(
              allQuery[0].indexOf('?') + 1
            );
            key = firstElement.substring(0, firstElement.indexOf('='));
            value = firstElement.substring(firstElement.indexOf('=') + 1);
          } else {
            key = element.substring(0, element.indexOf('='));
            value = element.substring(element.indexOf('=') + 1);
          }
          params[key] = value;
        }
        routeLink = routeLink.substring(0, routeLink.indexOf('?'));
        this.router.navigate([routeLink], {
          queryParams: params
        });
      } else {
        this.router.navigate([routeLink]);
      }
    }
  }
}

interface HeaderGroup {
  groupKey: string;
  keyName: string;
  title: string;
  sortOrder: number;
  imageUrl: string;
}
interface ChildGroup {
  key: string;
  groupKey: string;
  value: string;
  url: string;
  sortOrder: number;
}
