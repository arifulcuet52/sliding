import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmbmDashboardComponent } from './umbm-dashboard.component';

describe('UmbmDashboardComponent', () => {
  let component: UmbmDashboardComponent;
  let fixture: ComponentFixture<UmbmDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmbmDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmbmDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
