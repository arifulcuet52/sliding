import { DataService } from './../../../shared/service/data/data.service';
import { Component, OnInit, ViewChild, Pipe } from '@angular/core';
import { BehaviorSubject, Observable, of as observableOf } from 'rxjs';
import {
  MatSort,
  MatDialog,
  MatDialogConfig,
  MatTableDataSource
} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import swal from 'sweetalert2';
import { BookingService } from '../../../shared/service/remote/exam/booking/booking.service';
import * as pdfmake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { formatDate } from '@angular/common';

pdfmake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-exam-booking',
  templateUrl: './exam-booking.component.html',
  styleUrls: ['./exam-booking.component.scss']
})
export class ExamBookingComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  searchText = '';
  pageSize = 10;
  pageIndex = 1;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  dataSource: any;
  displayedColumns: string[] = [
    'sl',
    'examDate',
    'fullName',
    'bookingDate',
    'startTime',
    'endTime',
    'venueLocation'
  ];
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private bookingService: BookingService,
    public dataService: DataService
  ) {}

  ngOnInit() {
    this.search();
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.search();
  }

  search() {
    this.bookingService
      .getall(this.searchText, this.pageSize, this.pageIndex)
      .subscribe(res => {
        this.dataSource = new MatTableDataSource(res['data'].data);
        this.dataSource.sort = this.sort;
        this.length = res['data'].totalItem;
      });
  }

  filter(searchText: string) {
    this.searchText = searchText;
    this.pageIndex = 1;
    this.search();
  }

  print() {
    console.log(this.dataSource.data);
    this.makePdfContent(this.dataSource.data);
  }

  makePdfContent(list) {
    const content = [
      {
        table: {
          headerRows: 1,
          margin: [0, 10, 0, 0],
          widths: [30, 75, '*', 120, 60, 60, 'auto'],
          body: [
            [
              { text: 'SL#', alignment: 'center', bold: true },
              { text: 'Exam Date', alignment: 'left', bold: true },
              { text: 'Full Name', alignment: 'left', bold: true },
              { text: 'Booking Date', alignment: 'left', bold: true },
              { text: 'Start Time', alignment: 'left', bold: true },
              { text: 'End Time', alignment: 'left', bold: true },
              { text: 'Venu', alignment: 'left', bold: true }
            ]
          ]
        },
        fontSize: 9
      }
    ];

    for (let i = 0; i < list.length; i++) {
      const obj = [
        { text: (i + 1).toString(), alignment: 'center', bold: false },
        {
          text: this.dataService.utc_to_local_time(
            list[i]['examSchedule']['examDate'],
            this.dataService.DATEFORMAT
          ),
          alignment: 'left',
          bold: false
        },
        { text: list[i]['fullName'], alignment: 'left', bold: false },
        {
          text: this.dataService.utc_to_local_time(
            list[i]['bookingDate'],
            this.dataService.DATETIMEFORMAT
          ),
          alignment: 'left',
          bold: false
        },
        {
          text: this.dataService.utc_to_local_time(
            list[i]['examSchedule']['startTime'],
            this.dataService.TIMEFORMAT
          ),
          alignment: 'left',
          bold: false
        },
        {
          text: this.dataService.utc_to_local_time(
            list[i]['examSchedule']['endTime'],
            this.dataService.TIMEFORMAT
          ),
          alignment: 'left',
          bold: false
        },
        {
          text: list[i]['examSchedule']['venueLocation'],
          alignment: 'left',
          bold: false
        }
      ];
      content[0].table.body.push(obj);
    }
    this.generatePdf(content);
  }

  generatePdf(bodyContent) {
    const docDefinition = {
      pageSize: 'A4',
      pageOrientation: 'portrait', // portrait, landscape
      pageMargins: [20, 70, 20, 30],
      header: [
        {
          image: this.dataService.companyImageBase64Encoding,
          width: 120,
          height: 25,
          margin: [20, 15, 0, 0]
        },
        {
          text: 'MetLife Bangladesh',
          alignment: 'center',
          fontSize: 15,
          margin: [0, -30, 0, 0]
        },
        { text: 'Dhaka, Bangladesh', alignment: 'center', fontSize: 10 },
        {
          text: 'Exam Booking List',
          alignment: 'center',
          fontSize: 12,
          margin: [0, 12, 0, 0]
        }
      ],
      content: bodyContent,
      footer: function(currentPage, pageCount) {
        return {
          columns: [
            '',
            {
              text: 'Developed by BrainStation-23 Limited',
              alignment: 'center'
            },
            {
              text: currentPage.toString() + ' of ' + pageCount.toString(),
              alignment: 'right'
            }
          ],
          margin: [10, 10, 10, 10],
          fontSize: 9
        };
      }
    };
    // open the PDF in a new window
    pdfmake.createPdf(docDefinition).open();

    // download the PDF
    pdfmake.createPdf(docDefinition).download('exambooking.pdf');
  }
}
