import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ExamSetting } from '../../../../entity/exam/exam-setting';
import swal from 'sweetalert2';
import { ExamSettingService } from '../../../../shared/service/remote/exam/exam-setting.service';
import { QuestionPerModule } from '../../../../entity/exam/question-per-module';
import { TrainingModuleService } from '../../../../shared/service/remote/exam/training-module.service';
import { TrainingModule } from '../../../../entity/training-module';
import { ResponseMessage } from '../../../../entity/response-message';
import {
  FormBuilder,
  FormGroup,
  Validators,
  ValidatorFn,
  AbstractControl,
  FormArray,
  FormControl
} from '@angular/forms';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-exam-setting-dialog',
  templateUrl: './exam-setting-dialog.component.html',
  styleUrls: ['./exam-setting-dialog.component.css']
})
export class ExamSettingDialogComponent implements OnInit {
  swal: any;
  model: ExamSetting;
  trainingModule: TrainingModule[];
  temptrainingModule: TrainingModule[][] = [];
  errorMessage: string;
  errorMessageArray = [];
  questionPerModulevalid = true;
  public totalScore = 0;
  examForm: FormGroup;
  constrol: AbstractControl;
  moduleCtrl: AbstractControl[];
  tempid: number[] = [];
  formErrors = {
    totalNoOfQuestion: '',
    questionPerPage: '',
    totalScore: '',
    passScore: '',
  };
  validationMessages = {
    totalNoOfQuestion: {
      required: 'Total number of Question is required',
      min: 'Total number of Question must be greater than 0.',

    },
    questionPerPage: {
      required:
        'Question per page is required.',
      min: 'Question Per Page must be greater than 0.',
      max: 'Question Per Page cannot be gretter then total number of question.'
    },
    totalScore: {
      required: 'Total Score is required',
      min: 'Total Score must be greater than 0.'
    },
    passScore: {
      required: 'Pass Score is required',
      min: 'Pass Score must be greater than 0.',
      max: 'Pass Score cannot be greater than total score.'
    },
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ExamSettingDialogComponent>,
    public examSettingService: ExamSettingService,
    private trainingModuleService: TrainingModuleService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.model = this.data;

    this.formBuilder();
    if (this.model.id) {
      this.editForm(this.model);
    }
    this.getModule();
  }

  editForm(value: ExamSetting) {
    this.examForm.patchValue({
      questionPerPageGroup: {
        totalNoOfQuestion: value.totalNoOfQuestion,
        questionPerPage: value.questionPerPage,
      },
      passScoreGroup: {
        totalScore: value.totalScore,
        passScore: value.passScore
      },
      status: value.status
    });

    (<FormArray>this.examForm.get('questionPerPageGroup').get('modules')).removeAt(0);

    for (let index = 0; index < value.questionPerModules.length; index++) {
      (<FormArray>this.examForm.get('questionPerPageGroup').get('modules'))
        .push(this.addModuleFormGroup());

      (<FormArray>this.examForm.get('questionPerPageGroup').get('modules'))
        .controls[index].get('moduleId').setValue(value.questionPerModules[index].moduleId);
      (<FormArray>this.examForm.get('questionPerPageGroup').get('modules'))
        .controls[index].get('numberOfQuestion').setValue(value.questionPerModules[index].numberOfQuestion);
    }

    // (<FormGroup>this.examForm.get('questionPerPageGroup'))
    //   .setControl('modules',
    //     this.setExistingSkills(value.questionPerModules));

    const questionPerPageGroup = <FormGroup>this.examForm.controls['questionPerPageGroup'];
    this.moduleCtrl = questionPerPageGroup.get('modules')['controls'];
    const selectedModuleId = this.moduleCtrl.map(x => x.get('moduleId').value);
    this.tempid = selectedModuleId;
    this.initializeSetting();
    // this.setAllModuleValue(true);


    // this.examForm.get('questionPerPageGroup').setControl(
    //   'modules',
    //   this.setExistingSkills(value.questionPerModules)
    // );
  }

  setExistingSkills(modules: QuestionPerModule[]): FormArray {
    const formArray = new FormArray([]);
    modules.forEach(s => {
      formArray.push(
        this.fb.group({
          moduleId: s.moduleId,
          numberOfQuestion: s.numberOfQuestion
        })
      );
    });
    return formArray;
  }

  formBuilder() {
    this.examForm = this.fb.group({
      questionPerPageGroup: this.fb.group(
        {
          totalNoOfQuestion: ['', [Validators.required, Validators.min(1)
          ]],
          questionPerPage: ['', [Validators.required, Validators.min(1)]],
          modules: this.fb.array([this.addModuleFormGroup()])
        },
        {
          validator: [this.maxQuestionPerPage.bind(this),
          this.compareTotalQWithNumOfQ.bind(this)
          ]
        }
      ),
      passScoreGroup: this.fb.group(
        {
          totalScore: ['', [Validators.required, Validators.min(1)]],
          passScore: ['', [Validators.required, Validators.min(1)]]
        },
        {
          validator: this.maxPassScore.bind(this)
        }
      ),
      status: [false],

    });
    this.initializeSetting();


  }


  initializeSetting() {
    const questionPerPageGroup = <FormGroup>this.examForm.controls['questionPerPageGroup'];
    this.constrol = questionPerPageGroup.controls['totalNoOfQuestion'];
    this.moduleCtrl = questionPerPageGroup.get('modules')['controls'];
    this.examForm.valueChanges.subscribe(data => {
      //this.compareTotalQWithNumOfQ.bind(null);
      this.logValidationErrors(this.examForm);

    });

    this.examForm.get('questionPerPageGroup').get('modules').valueChanges.subscribe(
      data => {
        this.tempid = data.map(x => x['moduleId']);
        this.setAllModuleValue();
      }
    );
  }


  addModuleFormGroup(): FormGroup {
    return this.fb.group(
      {
        moduleId: ['', Validators.required],
        numberOfQuestion: ['', [Validators.required, Validators.min(1)]]
      },
      {
        validator: [
          this.maxQuestionModuleWise.bind(this),

        ]
      }
    );
  }

  logValidationErrors(group: FormGroup = this.examForm): void {
    Object.keys(group.controls).forEach((key: string) => {
      const abstractControl = group.get(key);
      if (abstractControl instanceof FormGroup) {
        this.logValidationErrors(abstractControl);
      } else {
        this.formErrors[key] = '';
        if (abstractControl && !abstractControl.valid
          && (abstractControl.touched || abstractControl.dirty)) {
          const messages = this.validationMessages[key];
          for (const errorKey in abstractControl.errors) {

            if (errorKey) {
              if (errorKey == 'totalNumQ') {
                // this.formErrors['totalNoOfQuestion'] += this.validationMessages['totalNoOfQuestion']['totalNumQ'] + ' ';
              } else {
                this.formErrors[key] += messages[errorKey] + ' ';
              }

            }

          }
        } else {

        }
      }
    });
  }

  getModule() {
    this.trainingModuleService.getDropdown().subscribe(
      (response: TrainingModule[]) => {
        this.trainingModule = response;
        if (this.model.id) {
          for (let index = 0; index < this.moduleCtrl.length; index++) {
            this.temptrainingModule[index] = this.trainingModule;
          }
          this.setAllModuleValue(true);
        } else {
          this.temptrainingModule[0] = this.trainingModule;
        }
        // this.modelChange();
      },
      error => { }
    );
  }

  mapFormToModel() {
    this.model.totalNoOfQuestion = this.examForm.value.questionPerPageGroup.totalNoOfQuestion;
    this.model.questionPerPage = this.examForm.value.questionPerPageGroup.questionPerPage;
    this.model.totalScore = this.examForm.value.passScoreGroup.totalScore;
    this.model.passScore = this.examForm.value.passScoreGroup.passScore;
    this.model.status = this.examForm.value.status;
    this.model.questionPerModules = this.examForm.value.questionPerPageGroup.modules;

  }



  save() {
    this.mapFormToModel();

    if (this.model.id) {
      this.examSettingService.update(this.model).subscribe(
        (response: ResponseMessage<ExamSetting>) => {
          this.dialogRef.close(response);
          if (response.msgCode === 'error') {
            swal({
              title: 'Failed!',
              text: response.msg,
              type: 'error',
              confirmButtonText: 'Close'
            });
          } else {
            swal({
              title: '',
              text: 'success',
              type: 'success',
              confirmButtonText: 'Ok'
            });
          }
        },
        error => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error.message,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    } else {
      this.examSettingService.add(this.model).subscribe(
        (response: ResponseMessage<ExamSetting>) => {
          this.dialogRef.close(response);
          if (response.msgCode === 'error') {
            swal({
              title: 'Failed!',
              text: response.msg,
              type: 'error',
              confirmButtonText: 'Close'
            });
          } else {
            swal({
              title: '',
              text: 'success',
              type: 'success',
              confirmButtonText: 'Ok'
            });
          }
        },
        error => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error.message,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }

  close() {
    this.dialogRef.close();
  }
  setAllModuleValue(isEdit = false) {

    if (!this.trainingModule) {
      return;
    }


    for (let index = 0; index < this.moduleCtrl.length; index++) {
      // this.temptrainingModule[index] = this.trainingModule.filter(x =>
      //   !this.tempid.includes(x.id));
      const moduleId = this.moduleCtrl[index].get('moduleId').value;
      const filterId = this.tempid.filter(x => x != moduleId);
      // console.log(filterId);
      if (isEdit && index == 0) {
        this.temptrainingModule[index] = this.trainingModule.filter(x =>
          !filterId.includes(x.id));
      }
      else if (isEdit) {
        // console.log(this.tempid);
        const id = this.tempid.filter(x => x == moduleId);
        if (id.length > 1) {

          this.temptrainingModule[index] = this.trainingModule.filter(x =>
            !this.tempid.includes(x.id));
          (<FormArray>this.examForm.get('questionPerPageGroup').get('modules')).controls[index].get('moduleId').setValue('');

          //(<FormArray>this.examForm.get('questionPerPageGroup').get('modules')).controls[index].get('moduleId').reset();

          (<FormArray>this.examForm.get('questionPerPageGroup').get('modules')).controls[index].get('moduleId').setErrors(Validators.required);

          // console.log(
          //   (<FormArray>this.examForm.get('questionPerPageGroup').get('modules')).controls[index].get('moduleId'));

        } else {
          this.temptrainingModule[index] = this.trainingModule.filter(x =>
            !filterId.includes(x.id));
        }

      }
      else {
        this.temptrainingModule[index] = this.trainingModule.filter(x =>
          !filterId.includes(x.id));
      }



      //  this.temptrainingModule[index] = this.trainingModule;


      // console.log(this.temptrainingModule[index]);
    }

    if (isEdit) {
      // console.log('aaaa');
      //  this.examForm.updateValueAndValidity();
    }



    // this.temptrainingModule.forEach(element => {
    //   console.log(element);
    //   element = this.trainingModule.filter(x =>
    //     !this.tempid.includes(x.id)
    //   );
    // });
    //this.cdr.detectChanges();
  }
  addOption(index: number = null) {
    if (index == null) {
      //this.moduleCtrl.push(this.addModuleFormGroup());
      (<FormArray>this.examForm.get('questionPerPageGroup').get('modules'))
        .push(this.addModuleFormGroup());
      this.temptrainingModule[this.temptrainingModule.length] =
        this.trainingModule.filter(x =>
          !this.tempid.includes(x.id)
        );
      //(<FormArray>this.examForm.get('modules')).push(this.addModuleFormGroup());
    } else {
      (<FormArray>this.examForm.get('questionPerPageGroup').get('modules')).removeAt(index);
      this.temptrainingModule.splice(index, 1);

      // this.moduleCtrl.splice(index, 1);
      // (<FormArray>this.examForm.get('modules')).removeAt(index);
      this.setAllModuleValue();
      this.compareTotalQWithNumOfQ(null);
      this.logValidationErrors();

    }
  }

  getModuleQuestionCount(modulId) {
    if (modulId) {
      const tra = this.trainingModule.filter(x => x.id == modulId);
      if (tra.length > 0) {
        return tra[0].totalQuestions;
      } else {
        return 0;
      }
    }
  }
  getModuleName(modulId) {
    if (modulId) {
      const tra = this.trainingModule.filter(x => x.id == modulId);
      return tra[0].moduleName;
    }
  }
  maxQuesThenModuleQuesError(c: AbstractControl) {
    const numberOfQuestion = c.get('numberOfQuestion');
    const moduleId = c.get('moduleId');
    if (moduleId.status != 'INVALID' && numberOfQuestion.value) {
      const value = moduleId.value;
      const tra = this.trainingModule.filter(x => x.id == value);
      if (tra.length > 0) {
        if (tra[0].totalQuestions < numberOfQuestion.value) {
          return 'Maximum number of questions can be set '
            +
            tra[0].totalQuestions +
            ' under ' +
            this.getModuleName(value);
        }
      }
    }
    return '';
  }




  maxQuestionPerPage(c: AbstractControl) {
    if (this.examForm) {

      const questionPerPage = c.get('questionPerPage');
      const totalNoOfQuestion = c.get('totalNoOfQuestion');
      //console.log(questionPerPage);
      if (questionPerPage.hasError('max')) {
        questionPerPage.setErrors(null);
        // delete questionPerPage.errors['max'];
        //  questionPerPage.updateValueAndValidity();

      }
      if (totalNoOfQuestion.value && questionPerPage.value && (+totalNoOfQuestion.value < +questionPerPage.value)) {

        questionPerPage.setErrors({
          max: true
        });
        return { max: true };
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
  maxPassScore(c: AbstractControl) {
    if (this.examForm) {
      const passScore = c.get('passScore');
      const totalScore = c.get('totalScore');

      if (passScore.hasError('max')) {
        passScore.updateValueAndValidity();
      }
      if (totalScore.value && (+totalScore.value < +passScore.value)) {
        passScore.setErrors({
          max: true
        });
        passScore.markAsTouched();
        return { max: true };
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  maxQuestionModuleWise(c: AbstractControl) {
    if (this.examForm) {
      const numberOfQuestion = c.get('numberOfQuestion');
      const moduleId = c.get('moduleId');
      if (numberOfQuestion.hasError('maxQuesThenModuleQues')) {
        delete numberOfQuestion.errors['maxQuesThenModuleQues'];
        numberOfQuestion.updateValueAndValidity();
      }
      if (moduleId.status != 'INVALID' && numberOfQuestion.value) {
        const value = moduleId.value;
        let tra = [];
        if (this.trainingModule) {
          tra = this.trainingModule.filter(x => x.id == value);
        }
        if (tra.length > 0) {
          if (tra[0].totalQuestions < numberOfQuestion.value) {

            numberOfQuestion.markAsTouched();

            numberOfQuestion.setErrors({
              maxQuesThenModuleQues: true
            });
            return { maxQuesThenModuleQues: true };
          } else {
            return null;

          }
        } else {
          return null;
        }
      }
      return null;
    } else {
      return null;
    }
  }

  getTotal() {
    const ctrl = this.moduleCtrl;// <FormArray>this.examForm.controls['modules'];
    let total = 0;
    ctrl.forEach(x => {

      const parsed = +x.get('numberOfQuestion').value;
      total = total + parsed;
    });
    return total;
  }
  compareTotalQWithNumOfQ(c: AbstractControl) {
    if (this.examForm != null) {
      const totalNoOfQuestion = this.constrol;
      const total = this.getTotal();


      if (this.constrol.hasError('totalNumQ')) {
        delete this.constrol.errors['totalNumQ'];
        this.constrol.updateValueAndValidity();
        // (<FormArray>this.examForm.controls['modules']).updateValueAndValidity();
      }
      if (totalNoOfQuestion.value && (+totalNoOfQuestion.value != total)) {

        this.constrol.setErrors({
          totalNumQ: true
        });
        return { totalNumQ: true };
      } else {
        return null;
      }


    }
    return null;

  }
  public findInvalidControls(group = this.examForm) {
    const invalid = [];
    const controls = this.examForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }

    return invalid;
  }

}
