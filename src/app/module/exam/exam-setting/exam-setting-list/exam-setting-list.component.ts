import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatDialog, MatTableDataSource, MatDialogConfig } from '@angular/material';
import { ExamSetting } from '../../../../entity/exam/exam-setting';
import { ExamSettingService } from '../../../../shared/service/remote/exam/exam-setting.service';
import { DataService } from '../../../../Shared/Service/data/data.service';
import { PagingModel } from '../../../../entity/PagingModel';
import { ResponseMessage } from '../../../../entity/response-message';
import { ExamSettingDialogComponent } from '../exam-setting-dialog/exam-setting-dialog.component';
import swal from 'sweetalert2';

@Component({
  selector: 'app-exam-setting-list',
  templateUrl: './exam-setting-list.component.html',
  styleUrls: ['./exam-setting-list.component.css']
})
export class ExamSettingListComponent implements OnInit {

  displayedColumns: string[] = ['sl', 'totalNoOfQuestion', 'questionPerPage', 'status', 'createdOn', 'action'];
  dataSource: any;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  @ViewChild(MatSort) sort: MatSort;

  examSettings: ExamSetting[] = [];
  examSetting: ExamSetting;
  errorMessage = '';
  successMessage = '';
  searchText = '';
  pageSize = 10;
  pageIndex = 1;

  constructor(
    private dialog: MatDialog,
    private examSettingService: ExamSettingService, public dataService: DataService) { }

  ngOnInit() {
    this.initiation();
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }

  getAll(searchText: string, pageSize: number, pageIndex: number) {
    this.examSettingService.get(searchText, pageSize, pageIndex).subscribe(
      (response: ResponseMessage<PagingModel<ExamSetting>>) => {
        this.dataSource = new MatTableDataSource(response.data.data);
        this.dataSource.sort = this.sort;
        this.length = response.data.totalItem;
      },
      (error) => { }
    );
  }

  initiation() {
    this.searchText = '';
    this.examSetting = new ExamSetting();
    this.successMessage = '';
    this.errorMessage = '';
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }

  filter(searchText: string) {
    this.searchText = searchText;
    this.pageIndex = 1;
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }

  cancel() {
    this.initiation();
  }

  delete(examSettingId: number) {
    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.examSettingService.delete(examSettingId).subscribe(
          (response: string) => {
            this.getAll(this.searchText, this.pageSize, this.pageIndex);
            this.initiation();
          },
          (error) => {
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error.message,
              type: 'error',
              confirmButtonText: 'Close'
            });
          }
        );
      }
    });
  }

  edit(examSettingId: number) {
    this.examSettingService.getById(examSettingId).subscribe(
      (response: ResponseMessage<ExamSetting>) => {
        this.examSetting = response.data;
        this.open_dialog(this.examSetting).afterClosed().subscribe(data => {
          if (data !== undefined) {
            this.getAll(this.searchText, this.pageSize, this.pageIndex);
          }
        });
      },
      (error) => {
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).message,
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    );
  }

  add() {
    this.examSetting = new ExamSetting();
    this.open_dialog(this.examSetting).afterClosed().subscribe(data => {
      if (data !== undefined) {
        this.getAll(this.searchText, this.pageSize, this.pageIndex);
      }
    });
  }

  open_dialog(model) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '600px';
    dialogConfig.data = model;
    return this.dialog.open(ExamSettingDialogComponent, dialogConfig);
  }

}
