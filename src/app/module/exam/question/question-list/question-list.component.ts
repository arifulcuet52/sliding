import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatSort, MatTableDataSource } from '@angular/material';
import swal from 'sweetalert2';

import { Question } from '../../../../entity/exam/question';
import { PagingModel } from '../../../../entity/PagingModel';
import { DataService } from '../../../../Shared/Service/data/data.service';
import { QuestionService } from '../../../../shared/service/remote/exam/question.service';
import { QuestionDialogComponent } from '../question-dialog/question-dialog.component';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.css']
})
export class QuestionListComponent implements OnInit {

  displayedColumns: string[] = ['sl', 'moduleName', 'title', 'questionTypeName', 'score', 'isActive', 'createDateTime', 'action'];
  dataSource: any;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  @ViewChild(MatSort) sort: MatSort;

  questions: Question[] = [];
  question: Question;
  errorMessage = '';
  successMessage = '';
  searchText = '';
  pageSize = 10;
  pageIndex = 1;

  constructor(
    private dialog: MatDialog,
    private questionService: QuestionService,
    public dataService: DataService) { }

  ngOnInit() {
    this.initiation();
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }

  getAll(searchText: string, pageSize: number, pageIndex: number) {
    this.questionService.get(searchText, pageSize, pageIndex).subscribe(
      (response: PagingModel<Question>) => {
        this.dataSource = new MatTableDataSource(response.data);
        this.dataSource.sort = this.sort;
        this.length = response.totalItem;
      },
      (error) => { }
    );
  }

  initiation() {
    this.searchText = '';
    this.question = new Question('');
    this.successMessage = '';
    this.errorMessage = '';
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }

  filter(searchText: string) {
    this.searchText = searchText;
    this.pageIndex = 1;
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }

  cancel() {
    this.initiation();
  }

  delete(questionId: number) {
    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.questionService.delete(questionId).subscribe(
          (response: string) => {
            this.getAll(this.searchText, this.pageSize, this.pageIndex);
            this.initiation();
          },
          (error) => {
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error.message,
              type: 'error',
              confirmButtonText: 'Close'
            });
          }
        );
      }
    });
  }

  edit(questionId: number) {
    this.questionService.getById(questionId).subscribe(
      (response: Question) => {
        this.question = response;
        this.openDialog(this.question).afterClosed().subscribe(data => {
          if (data !== undefined) {
            this.getAll(this.searchText, this.pageSize, this.pageIndex);
          }
        });
      },
      (error) => {
        swal({
          title: 'Error!',
          text: JSON.parse(JSON.stringify(error)).message,
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    );
  }

  add() {
    this.question = new Question('');
    this.openDialog(this.question).afterClosed().subscribe(data => {
      if (data !== undefined) {
        this.getAll(this.searchText, this.pageSize, this.pageIndex);
      }
    });
  }

  openDialog(model) {
    const dialogConfig = new MatDialogConfig();
    // dialogConfig.width = '600px';
    dialogConfig.data = model;
    return this.dialog.open(QuestionDialogComponent, dialogConfig);
  }

}
