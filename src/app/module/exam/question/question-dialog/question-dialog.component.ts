import { Component, OnInit, Inject } from '@angular/core';
import swal from 'sweetalert2';
import { Question } from '../../../../entity/exam/question';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { QuestionService } from '../../../../shared/service/remote/exam/question.service';
import { TrainingModuleService } from '../../../../shared/service/remote/exam/training-module.service';
import { QuestionOption } from '../../../../entity/exam/question-option';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { TrainingModule } from '../../../../entity/training-module';
import { QuestionType } from '../../../../entity/exam/question-type';
import { QuestionTypeService } from '../../../../shared/service/remote/exam/question-type.service';
import { debug } from 'util';

@Component({
  selector: 'app-question-dialog',
  templateUrl: './question-dialog.component.html',
  styleUrls: ['./question-dialog.component.scss']
})
export class QuestionDialogComponent implements OnInit {

  trainingModule: TrainingModule[];
  questionType: QuestionType[];
  swal: any;
  model: Question;
  public questionForm: FormGroup;
  public totalScore = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<QuestionDialogComponent>,
    public questionService: QuestionService,
    public trainingModuleService: TrainingModuleService,
    public questionTypeService: QuestionTypeService,
    public formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.model = this.data;
    if (this.model.questionOptions.length === 0) {
      this.model.questionOptions.push(new QuestionOption());
    }
    this.getModule();
    this.getQuestionType();
    this.checkTotalScore();
    // this.questionFormValidation();
  }

  getModule() {
    this.trainingModuleService.getDropdown().subscribe(
      (response: TrainingModule[]) => {
        this.trainingModule = response;
      },
      (error) => { }
    );
  }

  getQuestionType() {
    this.questionTypeService.getDropdown().subscribe(
      (response: QuestionType[]) => {
        this.questionType = response;
      },
      (error) => { }
    );
  }

  save() {
    console.log(this.model);
    if (this.model.id) {
      this.questionService.update(this.model).subscribe(
        (response: string) => {
          this.dialogRef.close(response);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error.message,
            type: 'error',
            confirmButtonText: 'Close'
          });
        });
    } else {
      this.questionService.add(this.model).subscribe(
        (response: string) => {
          this.dialogRef.close(response);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error.message,
            type: 'error',
            confirmButtonText: 'Close'
          });
        });
    }
  }

  close() {
    this.dialogRef.close();
  }

  addOption(index: number = null) {
    if (index != null) {
      this.model.questionOptions.splice(index, 1);
    } else {
      this.model.questionOptions.push(new QuestionOption());
    }

    this.autoCalculateScore();
  }

  // public questionFormValidation() {
  //   // this.questionForm = new FormGroup({
  //   //   'score': new FormControl(this.model.score, [
  //   //     Validators.required,
  //   //     Validators.min(0)
  //   //   ]),
  //   // });
  //   this.questionForm = this.formBuilder.group({
  //     'score': ['', Validators.required, Validators.min(0)]
  //   });
  // }

  checkTotalScore() {
    this.totalScore = this.model.questionOptions.filter(qo => qo.isAnswer).map(qo => qo.score).reduce((sum, current) => sum + current, 0);
  }

  autoCalculateScore() {
    let totalAns = 0;
    let optionScore = 0;

    totalAns = this.model.questionOptions.filter(qo => qo.isAnswer).length;

    if (totalAns && totalAns <= 0) {
      totalAns = 1;
    }

    optionScore = parseFloat((this.model.score / totalAns).toFixed(2));

    this.model.questionOptions.filter(qo => qo.isAnswer).map(qo => qo.score = optionScore);
    this.model.questionOptions.filter(qo => !qo.isAnswer).map(qo => qo.score = 0);
    if (this.model.questionOptions) {
      this.totalScore = this.model.questionOptions.filter(qo => qo.isAnswer).map(qo => qo.score).reduce((sum, current) => sum + current, 0);
    }
  }
}


