import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatDialog, MatDialogConfig, MatTableDataSource } from '@angular/material';
import { ExamScheduleService } from '../../../shared/service/remote/exam/exam-schedule/exam-schedule.service';
import { ExamSchedule } from '../../../entity/exam-schedule';
import { DialogExamScheduleComponent } from '../../../dialog/exam/dialog-exam-schedule/dialog-exam-schedule.component';
import swal from 'sweetalert2';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { DataService } from '../../../shared/service/data/data.service';
import { PagingModel } from '../../../entity/PagingModel';

@Component({
  selector: 'app-exam-schedule',
  templateUrl: './exam-schedule.component.html',
  styleUrls: ['./exam-schedule.component.scss']
})
export class ExamScheduleComponent implements OnInit {

  constructor(
    private examScheduleService: ExamScheduleService,
    private dialog: MatDialog,
    private dataService: DataService
  ) { }

  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['sl', 'examDate', 'startTime', 'endTime', 'durationStr', 'district', 'area', 'venueLocation', 'venueSeatingCapacity', 'availableSeats', 'action'];
  dataSource: any;
  model: ExamSchedule;
  swalWithBootstrapButtons: any;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  searchText = '';
  pageSize = 10;
  pageIndex = 1;
  durationStr: string;

  ngOnInit() {
    this.initiation();
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }

  getAll(searchText: string, pageSize: number, pageIndex: number) {
    this.examScheduleService.getBySearch(searchText, pageSize, pageIndex).subscribe(
      (response: PagingModel<ExamSchedule>) => {
        let hh: any;
        let mm: any;
        for (const model of response.data) {
          // model.examDuration = parseFloat(Math.round(model.examDuration * 100) / 100).toFixed(2);
          hh = Math.trunc(model.examDuration / 60);
          mm = model.examDuration % 60;
          model.durationStr = hh + ':' + mm;
        }
        this.dataSource = new MatTableDataSource(response.data);
        this.dataSource.sort = this.sort;
        this.length = response.totalItem;
      },
      (error) => {
        console.log('error');
      }
    );
  }

  initiation() {
    this.searchText = '';
    this.model = new ExamSchedule('');
  }

  open_dialog(model) {
    window.scrollTo(0, 0);

    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { model: model };

    return this.dialog.open(DialogExamScheduleComponent, dialogConfig);
  }

  add() {
    this.model = new ExamSchedule('');
    this.open_dialog(this.model).afterClosed().subscribe(data => {
      this.getAll(this.searchText, this.pageSize, this.pageIndex);
    });
  }

  edit(index) {
    // prepare data for edit
    this.dataSource.data[index].startTime = this.dataService.utc_to_local_time(this.dataSource.data[index].startTime);
    this.dataSource.data[index].endTime = this.dataService.utc_to_local_time(this.dataSource.data[index].endTime);
    this.dataSource.data[index].examDate = this.dataService.utc_to_local_time(this.dataSource.data[index].examDate);
    if (this.dataSource.data[index].areaId === null) {
      this.dataSource.data[index].areaId = 0;
    }

    this.dataSource.data[index].examDate = moment(new Date(this.dataSource.data[index].examDate)).toDate()
    this.open_dialog(this.dataSource.data[index]).afterClosed().subscribe(data => {
      this.getAll(this.searchText, this.pageSize, this.pageIndex);
    });
  }

  pageChange($event) {
    console.log($event);
    window.scrollTo(0, 0);

    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
    console.log($event);
  }

  filter(searchText: string) {
    this.searchText = searchText;
    this.pageIndex = 1;
    this.getAll(this.searchText, this.pageSize, this.pageIndex);

  }

  delete(id) {
    const swalWithBootstrapButtons = swal.mixin({
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
    });

    swalWithBootstrapButtons({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.examScheduleService.delete(id).subscribe(
          (success) => {
            console.log('deleted');
            console.log(success);
            this.getAll(this.searchText, this.pageSize, this.pageIndex);
          },
          (error) => {
            console.log('can\'t delete');
            console.log(error);
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error,
              type: 'error',
              confirmButtonText: 'Close'
            });
          }
        );

        /* swalWithBootstrapButtons(
          'Deleted!',
          'Selected schedule has been deleted.',
          'success'
        ) */

      } else if (
        result.dismiss === swal.DismissReason.cancel
      ) {
        /* swalWithBootstrapButtons(
          'Cancelled',
          'Nothing was deletd :)',
          'error'
        ) */
      }
    });
  }
}
