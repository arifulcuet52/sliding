import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { DataService } from '../../../../Shared/Service/data/data.service';
import { ResponseMessage } from '../../../../entity/response-message';
import { PagingModel } from '../../../../entity/PagingModel';
import { QuestionPerModule } from '../../../../entity/exam/question-per-module';
import { QuestionPerModuleService } from '../../../../shared/service/remote/exam/question-per-module.service';

@Component({
  selector: 'app-question-per-module-list',
  templateUrl: './question-per-module-list.component.html',
  styleUrls: ['./question-per-module-list.component.css']
})
export class QuestionPerModuleListComponent implements OnInit {

  displayedColumns: string[] = ['sl', 'examSettingId', 'moduleName', 'numberOfQuestion', 'status', 'createdOn', 'action'];
  dataSource: any;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  @ViewChild(MatSort) sort: MatSort;

  QuestionPerModules: QuestionPerModule[] = [];
  QuestionPerModule: QuestionPerModule;
  errorMessage = '';
  successMessage = '';
  searchText = '';
  pageSize = 10;
  pageIndex = 1;

  constructor(
    private dialog: MatDialog,
    private questionPerModuleService: QuestionPerModuleService, public dataService: DataService) { }

  ngOnInit() {
    this.initiation();
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }

  getAll(searchText: string, pageSize: number, pageIndex: number) {
    this.questionPerModuleService.get(searchText, pageSize, pageIndex).subscribe(
      (response: ResponseMessage<PagingModel<QuestionPerModule>>) => {
        this.dataSource = new MatTableDataSource(response.data.data);
        this.dataSource.sort = this.sort;
        this.length = response.data.totalItem;
      },
      (error) => { }
    );
  }

  initiation() {
    this.searchText = '';
    this.QuestionPerModule = new QuestionPerModule();
    this.successMessage = '';
    this.errorMessage = '';
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }

  filter(searchText: string) {
    this.searchText = searchText;
    this.pageIndex = 1;
    this.getAll(this.searchText, this.pageSize, this.pageIndex);
  }

  cancel() {
    this.initiation();
  }

  delete(questionId: number) {

  }

  edit(questionId: number) {

  }

  add() {

  }

}
