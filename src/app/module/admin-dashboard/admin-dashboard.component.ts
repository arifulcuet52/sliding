import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AccountService } from '../../shared/service/remote/authentication/account.service';
import { DashboardService } from '../../shared/service/remote/dashboard/dashboard.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  tagRequest: number;

  constructor(private router: Router,
    public accountService: AccountService,
    private dashbord: DashboardService) { }

  ngOnInit() {
    this.getUMBMPendingTagRequestCount();
  }

  role_setup() {
    this.router.navigate(['/ap/role']);
  }

  user_permission() {
    this.router.navigate(['/ap/user-permission']);
  }

  menu_setup() {
    this.router.navigate(['/ap/menu']);
  }

  role_permission() {
    this.router.navigate(['/ap/role-permission']);
  }

  getUMBMPendingTagRequestCount() {
    this.dashbord.getUMBMPendingTagRequest().subscribe(
      (success: number) => { this.tagRequest = success; },
      (error) => { },
    );
  }

  goToCertificateFee() {
    this.router.navigate(['/ap/certificate-list']);
  }
}
