import { Component, OnInit, OnDestroy } from '@angular/core';
import * as feather from 'feather-icons';
import { CandidateInfoService } from '../../shared/service/remote/candidate-info/candidate-info.service';
import { ToastrMessageService } from '../../shared/utils/toastr-message.service';
import { ActivatedRoute } from '@angular/router';
import { StatusEnum } from '../../entity/status.enum';
import { CandidateTaggingService } from '../../shared/service/remote/candidate-tagging/candidate-tagging.service';
import { TagRequest } from '../../entity/tag-request';
import swal from 'sweetalert2';
@Component({
  selector: 'app-candidateprofileview',
  templateUrl: './candidateprofileview.component.html',
  styleUrls: ['./candidateprofileview.component.scss']
})
export class CandidateprofileviewComponent implements OnInit, OnDestroy {
  public candidateProfile: any = null;
  private sub: any;
  public id: any;
  tagId: number;
  tagRequest: TagRequest;
  status: any = StatusEnum;
  profilePic = 'images/avatar.png';
  constructor(
    private candidateInfoService: CandidateInfoService,
    private toastr: ToastrMessageService,
    private route: ActivatedRoute,
    private candidateTaggingService: CandidateTaggingService
  ) {}

  ngOnInit() {
    feather.replace();
    this.tagRequest = new TagRequest();
    this.tagRequest.statusId = StatusEnum.Rejected;
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.tagId = params['tagId'];
      this.getTag(this.tagId);
      this.getCandidateInfo();
    });
  }

  getCandidateInfo() {
    this.candidateInfoService.getCandidateInfoByCandidateId(this.id).subscribe(
      res => {
        this.candidateProfile = res;
        this.profilePic = this.candidateProfile.data.profilePictureUrl;
      },
      error => {
        this.toastr.showError();
      }
    );
  }

  back() {
    history.back();
  }

  getTag(tagId: number) {
    this.candidateTaggingService.getTag(tagId).subscribe(
      (success: TagRequest) => {
        this.tagRequest = success;
      },
      error => {}
    );
  }
  changeStatus(status: StatusEnum) {
    let statusEnum: StatusEnum;
    let message = 'You want to ';
    if (status === StatusEnum.Accepted) {
      statusEnum = StatusEnum.Accepted;
      message = message + 'accept the request.';
    } else {
      statusEnum = StatusEnum.Rejected;
      message = message + 'reject the request.';
    }

    swal({
      title: 'Are you sure?',
      text: message,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Submit'
    }).then(result => {
      if (result.value) {
        this.candidateTaggingService
          .changeTagStatus(statusEnum, this.tagId)
          .subscribe(
            success => {
              this.back();
            },
            error => {
              swal({
                title: 'Error!',
                text: JSON.parse(JSON.stringify(error)).error.message,
                type: 'error',
                confirmButtonText: 'Close'
              });
            }
          );
      }
    });
  }

  ngOnDestroy() {}
}
