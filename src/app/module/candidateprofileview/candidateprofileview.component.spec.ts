import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateprofileviewComponent } from './candidateprofileview.component';

describe('CandidateprofileviewComponent', () => {
  let component: CandidateprofileviewComponent;
  let fixture: ComponentFixture<CandidateprofileviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateprofileviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateprofileviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
