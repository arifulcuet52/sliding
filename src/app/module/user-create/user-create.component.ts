import { Component, OnInit } from '@angular/core';
import { UserCreationVm } from '../../entity/UserCreationVm';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { UserPermissionService } from '../../shared/service/remote/authentication/user-permission.service';
import { UserPermission } from '../../entity/user-permission';
import {
  ActivatedRoute,
  Router
} from '../../../../node_modules/@angular/router';
import { UserService } from '../../shared/service/remote/user/user.service';
import { ThrowStmt } from '../../../../node_modules/@angular/compiler';
import { StatusService } from '../../shared/service/remote/status/status.service';
import { Status } from '../../entity/status';
import { DesignationService } from '../../shared/service/remote/designation/designation.service';
import { Designation } from '../../entity/designation';
import * as moment from 'moment';
import { Department } from '../../entity/department';
import { DepartmentService } from '../../shared/service/remote/department/department.service';
import { DataService } from '../../Shared/Service/data/data.service';
import swal from 'sweetalert2';
import {
  TypeHeadSettings,
  TypeHeadResponseType,
  TypeHeadRequestType
} from 'src/app/module/typehead/entity/typehead.entity';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  constructor(
    private userPer: UserPermissionService,
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router,
    private statusService: StatusService,
    private designationService: DesignationService,
    private departmentService: DepartmentService,
    private dataService: DataService,
    private authService: AuthService
  ) {
    this.setting.showPropertyKey = 'userCode';
    this.setting.listTextFormate = '<p >{{fullName}} ({{userCode}})</p>';
    this.setting.returnPropertyKey = 'userCode';
    this.setting.placeHolderText = 'BM Code';
    this.setting.responseType = TypeHeadResponseType.complex;
    this.setting.endpointUrl =
      this.authService.APPSERVER + 'user/GetUmBMCode?userType=BM';
    this.setting.querystring = 'userCode';
    this.setting.requestType = TypeHeadRequestType.GET;
  }
  setting: TypeHeadSettings = new TypeHeadSettings();
  model: UserCreationVm;
  rolesid: string[] = [];
  Statuses: Status[] = [];
  designations: Designation[] = [];
  departments: Department[] = [];
  datePickerConfig: Partial<BsDatepickerConfig>;
  btnText = 'Submit';
  pageTitle = 'Create User';
  public formData: FormData = new FormData();
  public filePicture: File = null;
  public collectionAdminPicUrl = null;
  public picture_name: string;
  public tempImage = 'images/avatar.png';
  isRoleSelect: boolean;
  currentDate: Date = new Date();
  minDate: Date = new Date(1918, 1, 11);
  showUserList = false;
  isAE = false;
  aETextErrorMsg = '';
  umbmId = [];
  ngOnInit() {
    this.datePickerConfig = Object.assign(
      {},
      {
        dateInputFormat: 'DD/MM/YYYY',
        showWeekNumbers: false
      }
    );

    this.model = new UserCreationVm();
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.btnText = 'Update';
      this.pageTitle = 'Update User';
    }
    this.getUser(id);
    this.getAllStatus();
    this.getDeisgnation();
    this.getDepartmentList();
  }
  getDepartmentList() {
    this.departmentService.getAll().subscribe(
      (success: Department[]) => {
        this.departments = success;
        //console.table(success);
      },
      error => {
        console.log(error);
      }
    );
  }

  save() {
    const m = Object.assign({}, this.model);
    m.roles = m.roles.filter(x => x.userExists === true);

    if (this.model.id && this.model.id.length > 0) {
      this.userService.update(m).subscribe(
        (success: string) => {
          this.showSuccessMessage(success.toString());
          this.router.navigate(['/ap/user-list']);
        },
        error => {
          this.showErrorMessage(
            JSON.parse(JSON.stringify(error)).error.message
          );
        }
      );
    } else {
      if (this.formData.has('model')) {
        this.formData.delete('model');
      }
      this.formData.append('model', JSON.stringify(this.model));
      if (this.formData.has('roles')) {
        this.formData.delete('roles');
      }
      this.formData.append('roles', JSON.stringify(m.roles));
      this.userService.saveCollectionAdmin(this.formData).subscribe(
        (success: string) => {
          this.showSuccessMessage(success.toString());
          this.router.navigate(['/ap/user-list']);
        },
        (error: string) => {
          this.showErrorMessage(
            JSON.parse(JSON.stringify(error)).error.message
          );
        }
      );

      // this.userService.save(m).subscribe(
      //   (success: string) => {
      //     console.log(success),
      //       this.successMessage = success;
      //   },
      //   (error) => { this.errorMessage = JSON.parse(JSON.stringify(error)).error.message; }
      // );
    }
  }

  getRole() {
    this.userPer.Get(this.model.id, 'all', 'internalUserCreate').subscribe(
      (success: UserPermission[]) => {
        this.model.roles = success;
        this.onChange();
      },
      error => {
        console.log(error);
      }
    );
  }
  roleSelect(roleid: string) {
    if (!this.rolesid.includes(roleid)) {
      this.rolesid.push(roleid);
    } else {
      this.rolesid.splice(this.rolesid.findIndex(i => i === roleid), 1);
    }
  }
  getUser(userId: string) {
    this.userService.get(userId).subscribe(
      (success: UserCreationVm) => {
        console.log(success);
        this.model = success;
        // this.model.tagUserList.push('');
        this.designationChange();
        this.collectionAdminPicUrl = this.model.profilePicUrl;
        if (this.model.dob) {
          this.model.dob = moment(new Date(this.model.dob)).toDate();
          this.model.dob = this.dataService.utc_to_local_time(this.model.dob);
        }

        this.getRole();
      },
      error => {
        this.getRole();
      }
    );
  }

  getAllStatus() {
    this.statusService.getAll().subscribe(
      (success: Status[]) => {
        this.Statuses = success;
      },
      error => {
        this.getRole();
      }
    );
  }
  statusOnChange(statusId: number) {
    this.model.statusId = statusId;
  }
  resetPassword() {
    this.userService.resetPassword(this.model.id).subscribe(
      (success: string) => {
        this.showSuccessMessage(success);
      },
      error => {
        this.showErrorMessage(JSON.parse(JSON.stringify(error)).error.message);
      }
    );
  }
  getDeisgnation() {
    this.designationService.getAll('internalUserCreate').subscribe(
      (success: Designation[]) => {
        this.designations = success;
        this.designationChange();
      },
      error => {
        console.log(error);
      }
    );
  }
  onPictureChange(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      this.filePicture = event.target.files[0];
      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (e: any) => {
        // called once readAsDataURL is completed
        this.collectionAdminPicUrl = e.target.result;
        this.model.profilePicSrc = this.collectionAdminPicUrl;
        this.model.profilePicUrl = this.collectionAdminPicUrl;
        this.picture_name = this.filePicture.name;
      };
      if (this.formData.has('profilePicture')) {
        this.formData.delete('profilePicture');
      }
      this.formData.append(
        'profilePicture',
        this.filePicture,
        this.filePicture.name
      );
      if (this.model.id) {
        this.userService
          .uploadPicture(this.model.userId, this.formData)
          .subscribe(res => {});
      }
    }
  }

  onChange() {
    if (this.model.roles) {
      // console.log(this.model.roles.filter(x => x.userExists === true));
      this.isRoleSelect =
        this.model.roles.filter(x => x.userExists).length === 0;
      // console.log(this.isRoleSelect);
    } else {
      this.isRoleSelect = false;
    }
  }
  showSuccessMessage(message: string) {
    swal({
      title: 'Success!',
      text: message,
      type: 'success',
      confirmButtonText: 'Close'
    });
  }
  showErrorMessage(message: string) {
    swal({
      title: 'Error!',
      text: message,
      type: 'error',
      confirmButtonText: 'Close'
    });
  }
  designationChange() {
    const tempDes = this.designations.filter(
      x => x.id == this.model.designationId
    )[0];

    this.showUserList = false;
    this.isAE = false;
    if (tempDes) {
      if (tempDes.shortCode === 'E') {
        this.showUserList = true;
        this.isAE = true;
      }
    }
  }

  itemChange(e) {
    this.model.tagUserList = e;
  }
}
