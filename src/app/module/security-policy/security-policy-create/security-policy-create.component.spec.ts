import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityPolicyCreateComponent } from './security-policy-create.component';

describe('SecurityPolicyCreateComponent', () => {
  let component: SecurityPolicyCreateComponent;
  let fixture: ComponentFixture<SecurityPolicyCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecurityPolicyCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecurityPolicyCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
