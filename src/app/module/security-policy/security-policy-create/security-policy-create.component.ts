import { Component, OnInit } from '@angular/core';
import { SecurityPoliciesService } from '../../../shared/service/remote/security-policies/security-policies.service';
import { SecurityPolicies } from '../../../entity/security-policies';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-security-policy-create',
  templateUrl: './security-policy-create.component.html',
  styleUrls: ['./security-policy-create.component.css']
})
export class SecurityPolicyCreateComponent implements OnInit {

  constructor(private securityService: SecurityPoliciesService,
    private route: ActivatedRoute,
    private router: Router, ) { }

  model: SecurityPolicies;
  errorMessage = '';
  successMessage = '';
  btnText = 'Save';
    ngOnInit() {
    this.model = new SecurityPolicies();
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.btnText = 'Update';
      this.get(id);
    }
  }

  get(id){
    console.log(id);
    this.securityService.get(id)
    .subscribe(
      (success) => {
        this.model = success.data;
        console.log(success.data);
      },
          (error) => { this.errorMessage = JSON.parse(JSON.stringify(error)).error.message; }
    );
  }

  save(id)
  {
    const m = Object.assign({}, this.model);
    if (id){
      this.securityService.edit(id, m).subscribe(
        (success: string) => { this.router.navigate(['/ap/security-policy']); },
        (error) => {
          this.errorMessage = JSON.parse(JSON.stringify(error)).error.message; }
      );
    }
    else{
      // tslint:disable-next-line:no-debugger
      this.securityService.save(m).subscribe(
        (success) => {
          if (success.data == null)
          {
            console.log('Policy already exist.');
            alert('Policy already exist. Cannont create a new policy.');
          }
          // tslint:disable-next-line:no-debugger
          this.router.navigate(['/ap/security-policy']);
        },
        (error) => { this.errorMessage = JSON.parse(JSON.stringify(error)).error.message; }
      );
    }
  }
}
