import { Component, OnInit } from '@angular/core';
import { SecurityPoliciesService } from '../../../shared/service/remote/security-policies/security-policies.service';
import { SecurityPolicies } from '../../../entity/security-policies';
import { Router } from '@angular/router';

@Component({
  selector: 'app-security-policies',
  templateUrl: './security-policies.component.html',
  styleUrls: ['./security-policies.component.css']
})
export class SecurityPoliciesComponent implements OnInit {

  constructor(private securityPoliciesService: SecurityPoliciesService,
              private router: Router) { }
  models: SecurityPolicies[] = [];
  model: SecurityPolicies;
  errorMessage = '';
  successMessage = '';

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.securityPoliciesService.getAll().subscribe(
      (success) => {      
        this.models = success.data;  
        console.log(success.data);   
      },
      (error) => { console.log("error")},
    );
  }

  delete(id, index)
  {
    var isConfirmed = confirm("Are you sure deleting this policy?");
    if(isConfirmed){
      this.securityPoliciesService.delete(id).subscribe(
        (success: string) => { 
            this.models.splice(index,1);
         },
        (error) => { this.errorMessage = JSON.parse(JSON.stringify(error)).error.message; }
      );
    }
  }
}
