export class TypeHeadSettings {
  endpointUrl: string;
  placeHolderText: string;
  listTextFormate: string;
  showPropertyKey: string;
  returnPropertyKey: string;
  responseType: TypeHeadResponseType;
  requestType: TypeHeadRequestType;
  querystring: string;
  selectionType: TypeHeadSelectionType;
}
export enum TypeHeadResponseType {
  complex = 1,
  primitive = 2
}
export enum TypeHeadRequestType {
  GET = 1,
  POST = 2
}
export enum TypeHeadSelectionType {
  Single = 1,
  Multiple = 2
}
