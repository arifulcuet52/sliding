import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypeheadComponent } from './component/typehead.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  imports: [
    CommonModule,
    MatChipsModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatFormFieldModule
  ],
  declarations: [TypeheadComponent],
  exports: [TypeheadComponent]
})
export class TypeheadModule {}
