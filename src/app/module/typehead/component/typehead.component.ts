import { COMMA, ENTER } from '@angular/cdk/keycodes';
import {
  Component,
  ElementRef,
  ViewChild,
  OnInit,
  Input,
  EventEmitter,
  Output,
  ChangeDetectionStrategy
} from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  MatAutocompleteSelectedEvent,
  MatAutocomplete
} from '@angular/material';
import { Observable } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import {
  TypeHeadSettings,
  TypeHeadResponseType,
  TypeHeadSelectionType
} from 'src/app/module/typehead/entity/typehead.entity';
import { TypeheadService } from '../service/typehead-service.service';
import { fromEvent } from 'rxjs';
export class FilterObject {
  id: string;
  value: string;
  showfield: string;
}
export class FilterSelectObject {
  id: string;
  value: string;
}
@Component({
  selector: 'app-typehead',
  templateUrl: './typehead.component.html',
  styleUrls: ['./typehead.component.scss']
})
export class TypeheadComponent implements OnInit {
  @Input() Setting: TypeHeadSettings;
  @Input() preSelectedItem: any[];
  @Output() onitemchange = new EventEmitter();
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  type = TypeHeadResponseType;
  filteredItems: Observable<FilterObject[]>;
  selectedItem: FilterSelectObject[] = [];
  inputCtrl = new FormControl();
  allItems: any[] = [];
  @ViewChild('input') input: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  constructor(private typeheadService: TypeheadService) {}
  ngOnInit(): void {
    if (!this.Setting) {
      throw new Error('Typehead Setting Property Is Undefined');
    } else {
      if (this.preSelectedItem) {
        this.preSelectedItem.forEach(element => {
          const obj = new FilterSelectObject();
          obj.id = element;
          obj.value = element;
          this.selectedItem.push(obj);
        });
      }
      this.showList();
    }
  }
  showList() {
    const input = document.getElementById('typeheadtext');
    const obs = fromEvent(input, 'input');
    obs
      .pipe(
        map(evenet => evenet.target['value']),
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe(s => {
        this._filter(s);
        //  console.log(typeof s);
      });
  }
  getValue(value: string): Observable<FilterObject[]> {
    let que;
    if (this.Setting.querystring) {
      que = this.Setting.querystring + '=' + value;
    }
    const a = this.typeheadService.get(this.Setting.endpointUrl, que).pipe(
      map(x => {
        if (this.Setting.responseType == this.type.complex) {
          return this._prepareComplexValue(x);
        } else {
          return this._preparePrimitiveValue(x);
        }
      })
    );
    return a;
  }
  onSelectedItemChanged() {
    this.onitemchange.next(this.selectedItem.map(x => x.id));
  }
  // add(event: MatChipInputEvent): void {
  //   // Add fruit only when MatAutocomplete is not open
  //   // To make sure this does not conflict with OptionSelected Event
  //   if (!this.matAutocomplete.isOpen) {
  //     const input = event.input;
  //     const value = event.value;

  //     // Add our fruit
  //     if ((value || '').trim()) {
  //       this.selectedItem.push(value.trim());
  //     }

  //     // Reset the input value
  //     if (input) {
  //       input.value = '';
  //     }

  //     this.fruitCtrl.setValue(null);
  //   }
  // }

  remove(item: FilterSelectObject): void {
    const index = this.selectedItem.findIndex(x => x.id === item.id);

    if (index >= 0) {
      this.selectedItem.splice(index, 1);
      this.onSelectedItemChanged();
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    // substring
    const value = event.option.value.substring(
      0,
      event.option.value.lastIndexOf(',')
    );
    const id = event.option.value.substring(
      event.option.value.lastIndexOf(',') + 1
    );

    const obj = new FilterSelectObject();
    obj.id = id;
    obj.value = value;

    if (
      this.Setting.selectionType === TypeHeadSelectionType.Single &&
      this.selectedItem.length > 0
    ) {
      this.selectedItem[0] = obj;
    } else {
      this.selectedItem.push(obj);
    }

    this.input.nativeElement.value = '';
    this.inputCtrl.setValue(null);
    this.onSelectedItemChanged();
  }

  private _prepareComplexValue(values): FilterObject[] {
    const temp = values.filter(
      item =>
        !this.selectedItem.filter(
          y => y.id == item[this.Setting.returnPropertyKey]
        ).length
    );

    const tempArray: FilterObject[] = [];
    for (let index = 0; index < temp.length; index++) {
      let filterTextFormate = this.Setting.listTextFormate;
      let dynamicProperty = filterTextFormate.split('{{');
      if (dynamicProperty.length > 0) {
        dynamicProperty.splice(0, 1);
      }
      dynamicProperty = dynamicProperty.map(x =>
        x.substring(0, x.lastIndexOf('}}'))
      );

      dynamicProperty.forEach(element => {
        filterTextFormate = filterTextFormate.replace(
          '{{' + element + '}}',
          temp[index][element]
        );
      });
      const obj = new FilterObject();
      obj.value = filterTextFormate;
      obj.id = temp[index][this.Setting.returnPropertyKey];
      obj.showfield = temp[index][this.Setting.showPropertyKey];
      tempArray.push(obj);
    }
    return tempArray;
  }

  private _preparePrimitiveValue(values): FilterObject[] {
    let temp = [];
    values.forEach(x => temp.push({ id: x, value: x, showfield: x }));
    temp = temp.filter(
      item => !this.selectedItem.filter(y => y.id == item['id']).length
    );
    return temp;
  }

  private _filter(value: string) {
    if (!value) {
      value = ' ';
    }
    const filterValue = value.toLowerCase();
    this.filteredItems = this.getValue(filterValue);
  }
  displayFn(state) {
    console.log(state, 'state');
  }
}
