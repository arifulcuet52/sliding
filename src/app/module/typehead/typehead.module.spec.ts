import { TypeheadModule } from './typehead.module';

describe('TypeheadModule', () => {
  let typeheadModule: TypeheadModule;

  beforeEach(() => {
    typeheadModule = new TypeheadModule();
  });

  it('should create an instance', () => {
    expect(typeheadModule).toBeTruthy();
  });
});
