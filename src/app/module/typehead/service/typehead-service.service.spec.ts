import { TestBed, inject } from '@angular/core/testing';

import { TypeheadService } from './typehead-service.service';

describe('TypeheadServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TypeheadService]
    });
  });

  it('should be created', inject(
    [TypeheadService],
    (service: TypeheadService) => {
      expect(service).toBeTruthy();
    }
  ));
});
