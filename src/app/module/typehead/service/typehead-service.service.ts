import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TypeheadService {
  constructor(private http: HttpClient) {}
  get(url: string, query: string) {
    if (query) {
      if (url.includes('?')) {
        url = url + '&' + query;
      } else {
        url = url + '?' + query;
      }
    }
    return this.http.get(url);
  }
}
