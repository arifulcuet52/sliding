import { Component, OnInit } from '@angular/core';
import { UserMenuService } from '../../shared/service/remote/user-menu/user-menu.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-navbar',
  templateUrl: './side-navbar.component.html',
  styleUrls: ['./side-navbar.component.scss']
})
export class SideNavbarComponent implements OnInit {

  constructor(private userMenuService: UserMenuService, private router: Router) { 
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }
  isexpend =false;
  public reportMenus: any[] = [];
  ngOnInit() {
    this.fnGetReportMenusByUser();
  }

  fnGetReportMenusByUser() {
    this.userMenuService.getReportMenusByUserId().subscribe(
      (rec: any) => {
        this.reportMenus = rec.data;
      },
      (error: any) => {
        console.log("error");
      }
    );
  }

  fnChangeReport(menu: any) {
    this.router.navigateByUrl( menu.path);
  }
  test() {
    console.log('aaaa');
    this.isexpend = !this.isexpend;
  }
}
