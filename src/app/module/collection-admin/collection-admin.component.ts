import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-collection-admin',
  templateUrl: './collection-admin.component.html',
  styleUrls: ['./collection-admin.component.scss']
})
export class CollectionAdminComponent implements OnInit {
  profilePic = 'images/avatar.png';
  constructor() {}
  ngOnInit() {}
}
