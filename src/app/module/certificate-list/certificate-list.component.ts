import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource, MatDialog, MatTable } from '@angular/material';
import { CertificateService } from '../../shared/service/remote/certificate/certificate.service';
import { StatusFeeEnum } from '../../entity/status.enum';
import swal from 'sweetalert2';
import * as feather from 'feather-icons';
import * as pdfmake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { DataService } from 'src/app/shared/service/data/data.service';
import { AccountService } from 'src/app/shared/service/remote/authentication/account.service';

pdfmake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-certificate-list',
  templateUrl: './certificate-list.component.html',
  styleUrls: ['./certificate-list.component.scss']
})
export class CertificateListComponent implements OnInit {

  public searchText = '';
  public dataSource: any = null;
  public certificateRejection: any;
  public candidateId: string;
  public reasonToReject = '';
  public status: any = StatusFeeEnum;
  public element: any;
  public selectedRecord: any;
  public dataSourceArray: any;
  public userEmail: string;

  public displayedColumns: string[] = ['sl', 'candidateId', 'candidateName', 'receiptNumber', 'certificateFeeSubmissionDate',
    'amountPaid', 'paymentDate', 'depositPoint', 'status', 'verificationDate', 'action'];

  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild('openModalButton') openModalButton: ElementRef;
  @ViewChild('modalCloseButton') modalCloseButton: ElementRef;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, public certificateService: CertificateService
    , private dialog: MatDialog, private dataService: DataService, private accService: AccountService) {
    this.userEmail = this.accService.getUserEmail();
    this.filter('');
  }

  ngOnInit() {
    feather.replace();
  }

  getNext() {
    this.search();
  }

  search() {
    this.certificateService.getNextCertificate()
      .subscribe(res => {
        this.element = res.data;
        console.log(this.element);
        if (res.data) {
          this.candidateId = res.candidateId;
          this.dataSourceArray = res.data;
         // this.dataSourceArray.push(res.data);
          this.dataSource = new MatTableDataSource(res.data);
          console.log(this.dataSourceArray);
          if (res.isQueueEmpty){
            swal({
              title: '',
              text: 'Queue is empty!',
              type: 'warning',
              confirmButtonText: 'Close'
            });
          }
        } else {
          alert('hi');
          swal({
            title: '',
            text: 'Queue is empty!',
            type: 'warning',
            confirmButtonText: 'Close'
          });
        }
        this.table.renderRows();
      }, error => {

      });
  }

  filter(searchText: string) {
    this.searchText = searchText;
    this.certificateService.getFilteredCertificate(this.searchText)
      .subscribe(res => {
        this.element = res;
        this.dataSourceArray = this.element;
        this.dataSource = new MatTableDataSource(this.dataSourceArray);
        this.table.renderRows();
      }, error => {

      });
  }

  accept(element: any) {
    this.selectedRecord = Object.assign({}, element);
    this.selectedRecord.statusId = this.status.Accepted;
    swal({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then(result => {
      if (result.value) {
        this.certificateService.updateStatus(this.selectedRecord)
        .subscribe(res => {
          swal({
            title: 'Success!',
            text: 'Save successfully',
            type: 'success',
            confirmButtonText: 'Close'
          });
          for (let idx = 0; idx < this.dataSourceArray.length; idx++) {
            console.log(this.dataSourceArray);
            if (this.dataSourceArray[idx].certificateFeeId === this.selectedRecord.certificateFeeId) {
              // this.dataSourceArray.splice(idx, 1);
              this.dataSourceArray[idx].statusId = this.status.Accepted;
              this.dataSource = new MatTableDataSource(this.dataSourceArray);
              break;
            }
          }
        }, error => {
          swal({
            title: 'Warning!',
            text: error.error,
            type: 'warning',
            confirmButtonText: 'Close'
          });
        });
      }
    });



  }

  closePopUp() {
    this.reasonToReject = '';
  }

  openModal(element: any) {
    this.selectedRecord = Object.assign({}, element);
    // tslint:disable-next-line:no-unused-expression
    this.openModalButton && this.openModalButton.nativeElement.click();
  }

  reject() {
    const cetrificateRejection: CetrificateRejection = this.selectedRecord;
    cetrificateRejection.remarks = this.reasonToReject;
    cetrificateRejection.statusId = this.status.Rejected;
    // tslint:disable-next-line:no-unused-expression
    this.modalCloseButton && this.modalCloseButton.nativeElement.click();
    this.certificateService.updateStatus(cetrificateRejection)
      .subscribe(res => {
        swal({
          title: 'Success!',
          text: 'Save successfully',
          type: 'success',
          confirmButtonText: 'Close'
        });
        this.reasonToReject = '';

        for (let idx = 0; idx < this.dataSourceArray.length; idx++) {
          if (this.dataSourceArray[idx].certificateFeeId === this.selectedRecord.certificateFeeId) {
            this.dataSourceArray[idx].statusId = this.status.Rejected;
            this.dataSource = new MatTableDataSource(this.dataSourceArray);
            break;
          }
        }
      }, error => {
        swal({
          title: 'Error!',
          text: error.error,
          type: 'error',
          confirmButtonText: 'Close'
        });
      });
  }

  // Make PDF functions

  print() {
    // console.log(this.dataSource.data);
    if (this.dataSourceArray){
      this.makePdfContent(this.dataSourceArray);
    }
  }

  makePdfContent(list) {
    const content = [{
      table: {
        headerRows: 1,
        margin: [0, 10, 0, 0],
        widths: [30, 75, '*', 120, 60, 60, 60, 60, 60, 'auto'],
        body: [
          [
            { text: 'SL#', alignment: 'center', bold: true },
            { text: 'Candidate Id', alignment: 'left', bold: true },
            { text: 'Candidate Name', alignment: 'left', bold: true },
            { text: 'Receipt Number', alignment: 'left', bold: true },
            { text: 'Certificate Fee Submission Date', alignment: 'left', bold: true },
            { text: 'Amount Paid', alignment: 'left', bold: true },
            { text: 'Payment Date', alignment: 'left', bold: true },
            { text: 'Deposit Point', alignment: 'left', bold: true },
            { text: 'Status', alignment: 'left', bold: true },
            { text: 'Verification Date', alignment: 'left', bold: true },
            // { text: 'Action', alignment: 'left', bold: true }
          ]
        ]
      },
      fontSize: 9
    }];


    for (let i = 0; i < list.length; i++) {
      if (list[i].statusId === this.status.Accepted){
        status = 'Accepted';
      } else if (list[i].statusId === this.status.Pending){
        status = 'Pending';
      } else if (list[i].statusId === this.status.Rejected){
        status = 'Rejected';
      }

      const obj = [
        { text: (i + 1).toString(), alignment: 'center', bold: false },
        { text: list[i]['candidateId'], alignment: 'left', bold: false },
        { text: list[i]['candidateName'], alignment: 'left', bold: false },
        { text: list[i]['receiptNumber'], alignment: 'left', bold: false },
        { text: this.dataService.utc_to_local_time(list[i]['createDateTime'], this.dataService.DATEFORMAT), alignment: 'left', bold: false },
        { text: list[i]['amountPaid'], alignment: 'left', bold: false },
        { text: this.dataService.utc_to_local_time(list[i]['paymentDate'], this.dataService.DATEFORMAT), alignment: 'left', bold: false },
        { text: list[i]['depositPointName'], alignment: 'left', bold: false },
        { text: status, alignment: 'left', bold: false }, //
        { text: this.dataService.utc_to_local_time(list[i]['verificationDate'], this.dataService.DATEFORMAT), alignment: 'left', bold: false },
      ];
      content[0].table.body.push(obj);
    }
    this.generatePdf(content);
  }

  generatePdf(bodyContent) {
    const docDefinition = {
      pageSize: 'A3',
      pageOrientation: 'portrait', // portrait, landscape
      pageMargins: [20, 70, 20, 30],
      header: [
        {
          image: this.dataService.companyImageBase64Encoding,
          width: 120,
          height: 25,
          margin: [20, 15, 0, 0]
        },
        { text: 'MetLife Bangladesh', alignment: 'center', fontSize: 15, margin: [0, -30, 0, 0] },
        { text: 'Dhaka, Bangladesh', alignment: 'center', fontSize: 10 },
        { text: 'List of Certificate Fee Submission', alignment: 'center', fontSize: 12, margin: [0, 12, 0, 0] },
      ],
      content: bodyContent,
      footer: function (currentPage, pageCount) {
        return {
          columns: [
            '',
            { text: 'Developed by BrainStation-23 Limited', alignment: 'center' },
            { text: currentPage.toString() + ' of ' + pageCount.toString(), alignment: 'right' }
          ],
          margin: [10, 10, 10, 10],
          fontSize: 9
        };
      }

    };
    // open the PDF in a new window
    // pdfmake.createPdf(docDefinition).open();

    // download the PDF
    pdfmake.createPdf(docDefinition).download('certificatefeelist.pdf');
  }
  //
}

export class CetrificateRejection {
  remarks: string;
  CertificateFeeId: number;
  CandidateId: number;
  ReceiptNumber: string;
  AmountPaid: number;
  PaymentDate: Date;
  AreaId: number;
  FACode: string;
  statusId: number;
}
