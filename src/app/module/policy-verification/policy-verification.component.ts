import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatSort, MatTableDataSource } from '@angular/material';
import { PolicyVerificationService } from 'src/app/shared/service/remote/policy-verification/policy-verification.service';
import { PolicyVerificationVm } from 'src/app/entity/policy-verification-vm';
import { DataService } from 'src/app/shared/service/data/data.service';

@Component({
  selector: 'app-policy-verification',
  templateUrl: './policy-verification.component.html',
  styleUrls: ['./policy-verification.component.scss']
})
export class PolicyVerificationComponent implements OnInit {

  @ViewChild(MatSort)
  sort: MatSort;
  searchText = '';
  pageSize = 10;
  pageIndex = 1;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  dataSource: any;
  tempData: any;
  displayedColumns: string[] = ['sl', 'userId', 'fullName', 'faCode', 'trainingCompletionDate',
   'trainingExamPassingDate', 'dateOfFirstPolicySubmission', 'dateOfFinalPolicySubmission',
  'policyPendingDetail', 'policyApprovedDetail', 'policyRejectDetail' ];

  model = new PolicyVerificationVm();
  constructor(
    private router: Router,
    private dialog: MatDialog,
    private pvs: PolicyVerificationService,
    public ds: DataService
  ) {}

  ngOnInit() {
    this.search();
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.search();
  }

  search() {
    this.pvs
      .get_policy_verifications(this.searchText, this.pageSize, this.pageIndex)
      .subscribe(res => {
        this.dataSource = new MatTableDataSource(res['data'].data);
        this.dataSource.sort = this.sort;
        this.length = res['data'].totalItem;
        console.log(res['data']);
      });
  }

  filter(searchText: string) {
    this.searchText = searchText;
    this.pageIndex = 1;
    this.search();
  }

  view(userId, index){
    this.router.navigate(['/ap/candidate-profile']);
  }

}
