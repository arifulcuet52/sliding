import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy
} from '@angular/core';
import * as feather from 'feather-icons';
import { UserInfoService } from '../../shared/service/remote/user-info/user-info.service';
import { ToastrMessageService } from '../../shared/utils/toastr-message.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-profileview',
  templateUrl: './profileview.component.html',
  styleUrls: ['./profileview.component.css']
})
export class ProfileviewComponent implements OnInit, OnDestroy {
  public userProfile: any = null;
  public isEditView: boolean;

  @ViewChild('propic') propic: ElementRef;

  constructor(
    private userInfoService: UserInfoService,
    private toastr: ToastrMessageService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    feather.replace();
    this.activatedRoute.params.subscribe(x => {
      if (x['id']) {
        this.getUserInfo(x['id']);
      }
    });
  }

  getUserInfo(id) {
    this.userInfoService.getByAgentId(id).subscribe(
      res => {
        this.userProfile = res;
      },
      error => {
        this.toastr.showError();
      }
    );
  }

  changeUserProfile(editedProfile: any) {
    this.router.navigate([
      '/ap/admin-profile-creation/',
      this.userProfile.data.userCode
    ]);
    // this.userProfile = editedProfile;
    // this.isEditView = false;
  }

  ngOnDestroy() {}
}
