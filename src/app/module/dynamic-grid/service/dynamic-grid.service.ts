import { Injectable } from '@angular/core';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DynamicGridService {
  private url = this.app.APPSERVER + 'DynamicGrid/';
  constructor(private app: AuthService,
    private htttp: HttpClient
  ) { }
  getDataByLegendId(legendkey: string, pageIndex: number, pageSize: number,
    searchtext: string
  ): Observable<any> {
    return this.htttp.get(this.url + 'data/' +
      legendkey + '/' + pageIndex + '/' + pageSize + '/' + searchtext +
      '/'

    );
  }
}
