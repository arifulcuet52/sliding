import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { UserMenuCanActivateRouteGuardService } from 'src/app/shared/route-gurd/user-menu-can-activate-route-guard.service';
import { DynamicGridComponent } from './component/dynamic-grid/dynamic-grid.component';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule, MatFormFieldModule, MatSelectModule, MatInputModule } from '@angular/material';




const routes: Routes = [
  {
    path: '',
    component: DynamicGridComponent
  },
  {
    path: ':key',
    component: DynamicGridComponent
  }
  ,
  {
    path: ':id/:name',
    component: DynamicGridComponent
  }
];


@NgModule({
  imports: [
    CommonModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    RouterModule.forChild(routes),
  ],
  declarations: [DynamicGridComponent]
})
export class DynamicGridModule { }
