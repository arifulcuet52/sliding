import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { DynamicGridService } from '../../service/dynamic-grid.service';
import { ActivatedRoute, Router } from '@angular/router';
import { fromEvent } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
  selector: 'app-dynamic-grid',
  templateUrl: './dynamic-grid.component.html',
  styleUrls: ['./dynamic-grid.component.scss']
})
export class DynamicGridComponent implements OnInit {
  @ViewChild(MatSort)
  sort: MatSort;
  dataSource: any;
  legendkey: string;
  data: any[] = [];
  config: any[] = [];
  displayedColumns: string[] = [];
  pageSize = 10;
  pageIndex = 0;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 50, 100];
  legendTitle: string;
  searchText: string;
  headerRowStore: string[] = [];
  constructor(private dynamicGridService: DynamicGridService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.displayedColumns = [];
    this.legendkey = this.route.snapshot.paramMap.get('key');
    this.getDataByLegendId();
    this.filter();
  }
  getDataByLegendId() {
    let tempSearch = '';
    if (this.searchText) {
      tempSearch = this.searchText;
    }

    this.dynamicGridService.getDataByLegendId(this.legendkey, this.pageIndex, this.pageSize, tempSearch).subscribe(
      (x) => {

        if (x['data']) {
          console.log(x);
          this.data = x['data'];
          this.data = this.data.map(a =>
            this.makePropertyLowercase(a)
          );
          this.config = x['config'];
          this.legendTitle = this.config[0]['legendtitle'];
          if (this.displayedColumns.length === 0) {
            // this.displayedColumns = Object.keys(this.data[0]);
            // this.displayedColumns = this.displayedColumns.filter(a => a !== 'totalRow');
            this.headerRowStore = x['header'].map(a => a['name']);
            this.displayedColumns = x['header'].map(a => a['name'].toLowerCase());
            this.displayedColumns = this.displayedColumns.filter(a => a !== 'totalrow');
          }

          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.sort = this.sort;
          if (this.data.length > 0 && this.data[0]['totalrow']) {
            this.length = this.data[0]['totalrow'];
          } else {
            this.length = 0;
          }
        }
      }
    );
  }
  fistLetterCap(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
    this.getDataByLegendId();
  }
  isurl(columnName: string) {
    const filter = this.config.filter(x => x['columnName'] && x['columnName'].toLowerCase() === columnName.toLowerCase());

    if (filter.length > 0) {
      return true;
    } else {
      return false;
    }
  }
  onUrlClick(columnName: string, row) {
    const filter = this.config.filter(x => x['columnName'].toLowerCase() === columnName.toLowerCase())[0];
    const mainUrl: string = filter['url'];
    row = this.makePropertyLowercase(row);


    const url = mainUrl.substring(0, mainUrl.indexOf('{'));
    console.log(url);
    const routeConfig: any[] = [url];
    if (mainUrl.includes('{')) {
      let urlWithoutQuesry = mainUrl;
      if (mainUrl.includes('?')) {
        urlWithoutQuesry = mainUrl.substring(mainUrl.indexOf('{')
          , mainUrl.indexOf('?'));
      } else {
        urlWithoutQuesry = urlWithoutQuesry.substring(
          mainUrl.indexOf('{')
          , mainUrl.length);
      }

      let dynamicParamterList: string[] = urlWithoutQuesry.split('{');
      dynamicParamterList.splice(0, 1);
      dynamicParamterList = dynamicParamterList.map(x => x.replace('}', '').toLowerCase());
      dynamicParamterList = dynamicParamterList.map(x => x.replace('/', '').toLowerCase());
      console.log(dynamicParamterList);
      dynamicParamterList.forEach(element => {
        routeConfig.push(row[element]);
      });
    }

    const query = this.getAllQuery(mainUrl);
    if (query) {
      this.router.navigate(routeConfig, {
        queryParams: query
      });
    } else {
      this.router.navigate(routeConfig);
    }
  }

  makePropertyLowercase(obj) {
    let key;
    const keys = Object.keys(obj);
    let n = keys.length;
    const newobj = {};
    while (n--) {
      key = keys[n];
      newobj[key.toLowerCase()] = obj[key];
    }
    return newobj;
  }

  filter() {
    const input = document.getElementById('search');
    const obs = fromEvent(input, 'input');
    obs
      .pipe(
        map(evenet => evenet.target['value']),
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe(s => {
        this.searchText = s;
        this.getDataByLegendId();
      });
  }
  getAllQuery(routeLink) {
    const params = {};
    if (routeLink.includes('?')) {
      const allQuery = routeLink.split('&');
      for (let index = 0; index < allQuery.length; index++) {
        const element = allQuery[index];

        let key = '';
        let value = '';
        if (index === 0) {
          const firstElement = allQuery[0].substring(
            allQuery[0].indexOf('?') + 1
          );
          key = firstElement.substring(0, firstElement.indexOf('='));
          value = firstElement.substring(firstElement.indexOf('=') + 1);
        } else {
          key = element.substring(0, element.indexOf('='));
          value = element.substring(element.indexOf('=') + 1);
        }
        params[key] = value;
      }
    }
    return params;

  }
  getHeader(name: string) {
    // console.log(this.headerRowStore);
    const temp = this.headerRowStore.filter(x => x.toLowerCase() == name.toLowerCase())[0];
    return temp;
  }

}
