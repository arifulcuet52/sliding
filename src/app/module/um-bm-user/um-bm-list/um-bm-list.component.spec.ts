import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmBmListComponent } from './um-bm-list.component';

describe('UmBmListComponent', () => {
  let component: UmBmListComponent;
  let fixture: ComponentFixture<UmBmListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmBmListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmBmListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
