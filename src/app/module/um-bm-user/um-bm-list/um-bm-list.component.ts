import { Component, OnInit, ViewChild } from '@angular/core';
import { UserInfoService } from '../../../shared/service/remote/user-info/user-info.service';
import {
  MatSort,
  MatDialog,
  MatDialogConfig,
  MatTableDataSource
} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-um-bm-list',
  templateUrl: './um-bm-list.component.html',
  styleUrls: ['./um-bm-list.component.css']
})
export class UmBmListComponent implements OnInit {
  @ViewChild(MatSort)
  sort: MatSort;
  searchText = '';
  pageSize = 10;
  pageIndex = 1;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  dataSource: any;
  displayedColumns: string[] = [
    'sl',
    'firstName',
    'agencyName',
    'agentId',
    'mobileNumber',
    'teamMember',
    'quotaLimit',
    'quotaConsume',
    'totalScore',
    'action'
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public userInfoService: UserInfoService
  ) {}

  ngOnInit() {
    this.search();
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.search();
  }

  search() {
    this.userInfoService
      .getall(this.searchText, this.pageSize, this.pageIndex)
      .subscribe(res => {
        this.dataSource = new MatTableDataSource(res.data.data);
        this.dataSource.sort = this.sort;
        this.length = res.data.totalItem;
      });
  }

  edit(id: number, userCode: string, index: number) {
    this.router.navigate(['/ap/admin-profile-creation/', userCode]);
  }

  view(id: number, userCode: string, index: number) {
    this.router.navigate(['/ap/ProfileView/', userCode]);
  }

  add() {
    this.router.navigate(['/ap/admin-profile-creation']);
  }
  filter(searchText: string) {
    this.searchText = searchText.replace(/\./gi,"^");
    this.pageIndex = 1;
    this.search();
  }
}
