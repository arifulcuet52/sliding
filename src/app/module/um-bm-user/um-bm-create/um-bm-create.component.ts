import { AccountService } from './../../../shared/service/remote/authentication/account.service';
import swal from 'sweetalert2';
import { Component, OnInit } from '@angular/core';
import { DesignationService } from '../../../shared/service/remote/designation/designation.service';
import { Designation } from '../../../entity/designation';
import { DistrictService } from '../../../shared/service/remote/district/district.service';
import { District } from '../../../entity/district';
import { AreaService } from '../../../shared/service/remote/area/area.service';
import { Area } from '../../../entity/area';
import { UserInfoService } from '../../../shared/service/remote/user-info/user-info.service';
import { EligibilityCriteriaEducation } from '../../../entity/eligibility-criteria-education';
import { EducationEligibilityService } from '../../../shared/service/remote/eligibility/education-eligibility/education-eligibility.service';
import { Status } from '../../../entity/status';
import { StatusService } from '../../../shared/service/remote/status/status.service';
import { UmBmCreationVm } from '../../../entity';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserPermission } from '../../../entity/user-permission';
import { UserPermissionService } from '../../../shared/service/remote/authentication/user-permission.service';
import { DataService } from '../../../shared/service/data/data.service';
import {
  TypeHeadSettings,
  TypeHeadResponseType,
  TypeHeadRequestType,
  TypeHeadSelectionType
} from '../../typehead/entity/typehead.entity';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import { forkJoin } from 'rxjs';
// declare var tinymce: any;

@Component({
  selector: 'app-um-bm-create',
  templateUrl: './um-bm-create.component.html',
  styleUrls: ['./um-bm-create.component.css']
})
export class UmBmCreateComponent implements OnInit {
  // ngOnDestroy(): void {
  //   //tinymce.remove(this.editor);
  // }
  // ngAfterViewInit(): void {
  //   tinymce.init({
  //     selector: 'textarea',
  //     setup: editor => {
  //       this.editor = editor;
  //       editor.on('keyup', () => {
  //         const content = editor.getContent();
  //         this.onEditorKeyup.emit(content);
  //       })
  //     }
  //   });
  // }
  // @Input() tinymce: string;
  // ngOnDestroy(): void {
  //   tinymce.remove(this.editor);
  // }
  // ngAfterViewInit(): void {
  //   tinymce.init({
  //     selector:'textarea',
  //     setup: editor => {
  //       this.editor = editor;
  //       editor.on('keyup', () => {
  //         const content = editor.getContent();
  //         this.onEditorKeyup.emit(content);
  //       })
  //     }
  //   });
  // }
  // @Input() tinymce: string;
  @Input() userProfile: any;
  @Input() isFromUserView = false;
  @Output() onSaveHandler = new EventEmitter<any>();
  // @Output() onEditorKeyup = new EventEmitter<any>();

  public IsDisable = false;
  public agentId = '0';
  public editor: any;
  public pageTitle = 'Manager\'s Profile';
  public model: UmBmCreationVm = new UmBmCreationVm();
  public statusList: Status[] = [];
  public designations: Designation[] = [];
  public districts: District[] = [];
  public areas: Area[] = [];
  public educations: EligibilityCriteriaEducation[] = [];
  public picture_name = '';
  public filePicture: File = null;
  public formData: FormData = new FormData();
  public currentDate: Date = new Date();
  public minDate: Date = new Date(1918, 1, 11);
  public datePickerConfig: Partial<BsDatepickerConfig>;
  public errorMessage = '';
  public successMessage = '';
  public showTiny = false;

  public isStatusChanged: boolean;
  public isAreaChanged: boolean;
  public isDistrictChanged: boolean;
  public isDesignationChanged: boolean;
  public isEducationChanged: boolean;
  setting: TypeHeadSettings = new TypeHeadSettings();
  showTypeHead = false;
  constructor(
    private designationService: DesignationService,
    private districtService: DistrictService,
    private areaService: AreaService,
    private userinfoService: UserInfoService,
    private statusService: StatusService,
    private educationService: EducationEligibilityService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userPermission: UserPermissionService,
    private dataService: DataService,
    private authService: AuthService,
    private accountService: AccountService
  ) {
    this.setting.showPropertyKey = 'userCode';
    this.setting.listTextFormate = '<p >{{fullName}} ({{userCode}})</p>';
    this.setting.returnPropertyKey = 'userCode';
    this.setting.responseType = TypeHeadResponseType.complex;

    this.setting.querystring = 'userCode';
    this.setting.requestType = TypeHeadRequestType.GET;
    this.setting.selectionType = TypeHeadSelectionType.Single;
  }

  ngOnInit() {
    this.model.quotaConsume = 0;
    this.model.profilePicUrl = this.dataService.DEFAULTPICSRC;
    this.datePickerConfig = Object.assign(
      {},
      { dateInputFormat: 'DD/MM/YYYY' }
    );

    this.onload();

    if (this.userProfile) {
      this.model = this.userProfile.data;
      this.fetchAgent();
      // if (this.model.districtId != null) {
      //   this.getArea(this.model.districtId);
      // }
    }
    // this.activatedRoute.params.subscribe(x => {
    //   if (x['id']) {
    //     this.IsDisable = true;
    //     this.model.agentId = parseInt(x['id'], 10);
    //     this.fetchAgent();
    //   } else {
    //     this.showTiny = true;
    //   }
    // });
  }

  onload() {
    this.getTotalScore();

    this.districtService.getAll().subscribe(
      (success: District[]) => {
        this.districts = success;
      },
      error => {
        console.log(error);
      }
    );

    const designation = this.designationService.getAll('umbmCreatePage');
    const education = this.educationService.getAll();
    forkJoin(designation, education).subscribe(([des, edu]) => {
      this.designations = des as Designation[];
      this.educations = edu.data;

      this.activatedRoute.params.subscribe(x => {
        if (x['id']) {
          this.IsDisable = true;
          this.model.userCode = x['id']; //parseInt(x['id'], 10);
          this.fetchAgent();
        } else {
          this.showTiny = true;
        }
      });
    });

    // this.designationService.getAll('umbmCreatePage').subscribe(
    //   (success: Designation[]) => {
    //     this.designations = success;
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // );

    // this.educationService.getAll().subscribe(
    //   (success: any) => {
    //     console.log(success, 'success');
    //     this.educations = success.data || [];
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // );

    this.statusService.getAll().subscribe(
      (success: Status[]) => {
        this.statusList = success;
      },
      error => {
        console.log(error);
      }
    );
  }

  onPictureChange(event) {
    debugger;
    if (event.target.files.length > 0) {
      this.filePicture = event.target.files[0];

      const reader = new FileReader();
      reader.onload = (load: any) => {
        this.model.profilePicUrl = load.target.result;
        this.model.profilePicSrc = load.target.result;
      };
      reader.readAsDataURL(this.filePicture);

      this.picture_name = this.filePicture.name;

      if (this.formData.has('profilePicture')) {
        this.formData.delete('profilePicture');
      }
      this.formData.append(
        'profilePicture',
        this.filePicture,
        this.filePicture.name
      );
      if (this.model.id > 0) {
        this.userinfoService
          .upload_picture(this.model.id, this.formData)
          .subscribe(res => {
            this.accountService.set_profile_pic_url(res.data.profilePicUrl);
          });
      }
    }
  }

  changeDataPattern(data) {
    this.model = data;

    this.picture_name =
      this.model.profilePicSrc != null &&
        this.model.profilePicSrc.indexOf('/') > -1
        ? this.model.profilePicSrc.split('/')[
        this.model.profilePicSrc.split('/').length - 1
        ]
        : '';
  }



  fetchAgent() {
    this.showTiny = false;
    this.userinfoService.getByAgentId(this.model.userCode).subscribe(
      (success: any) => {
        this.model = success.data;
        // console.log(success.data.email);
        if (success.data.email) {
          this.IsDisable = true;
        }
        this.showTiny = true;
        this.agentId = this.model.userCode;
        this.getArea(this.model.districtId);
        this.getRoles();
        // prepare remaining quota value
        this.model.quotaRemaining =
          success.data.quotaLimit - success.data.quotaConsume;

        // prepare datet of birth
        this.dataParsing();
        //this.model.dateOfBirth = moment(new Date(this.model.dateOfBirth)).toDate();

        const selectedDesignation = this.designations.filter(
          (rec: Designation) => {
            if (
              rec.shortCode == null
                ? ''
                : rec.shortCode === success.data.designation.shortCode
            ) {
              return true;
            } else {
              return false;
            }
          }

        );
        // prepare designation dropdown
        this.model.designationId =
          selectedDesignation === null || selectedDesignation.length === 0
            ? 0
            : selectedDesignation[0].id;
        this.designationChange();

        // prepare education dropdown

        const filterEdu = this.educations.filter(
          (rec: EligibilityCriteriaEducation) => {
            if (
              rec.shortCode == null
                ? ''
                : rec.shortCode === success.data.education.shortCode
            ) {
              return true;
            } else {
              return false;
            }
          }
        );
        if (filterEdu && filterEdu.length >= 1) {
          this.model.educationId = filterEdu[0].id;
        } else {
          this.model.educationId = 0;
        }
        // this.model.educationId =
        //   this.educations.filter((rec: EligibilityCriteriaEducation) => {
        //     if (
        //       rec.shortCode == null
        //         ? ''
        //         : rec.shortCode === success.data.education.shortCode
        //     ) {
        //       return true;
        //     } else {
        //       return false;
        //     }
        //   })[0].id || 0;
      // tslint:disable-next-line:no-unused-expression
    }, (error) => {
        this.model = new UmBmCreationVm();
        this.model.profilePicUrl = this.dataService.DEFAULTPICSRC;
        swal({
          title: 'No such Agent!',
          text: 'Please enter a valid Agent ID',
          type: 'error',
          confirmButtonText: 'Close'
        });
      }
    );
  }

  getRoles() {
    let userId = 0;
    if (this.model.userId != null) {
      userId = this.model.userId;
    }
    this.userPermission.GetByUserId(userId, 'true', 'umbmCreate').subscribe(
      (success: UserPermission[]) => {
        this.model.roles = [];
        this.model.roles = success;
      },
      error => {
        console.log(error);
      }
    );
  }

  getTotalScore() {
    this.model.totalScore =
      (this.model.yearOfExp +
        this.model.requirementRanking +
        this.model.productionBasedRanking +
        this.model.activeManPower) /
      4;
  }

  getArea(id) {
    this.areaService.getAllByDistrict(id).subscribe(
      (success: Area[]) => {
        this.areas = success;
      },
      error => {
        console.log(error);
      }
    );
  }

  save() {
    let m = Object.assign({}, this.model);
    m.roles = m.roles.filter(x => x.userExists === true);
    if (this.formData.has('model')) {
      this.formData.delete('model');
    }
    this.formData.append('model', JSON.stringify(this.model));
    this.formData.append('email', JSON.stringify(this.model.email));
    this.formData.delete('roles');
    this.formData.append('roles', JSON.stringify(m.roles));
    this.model.quotaConsume = this.model.quotaLimit - this.model.quotaRemaining;

    if (this.model.id === 0) {
      if (this.isFromUserView) {
        this.userProfile.data = this.model;
      }

      this.userinfoService.create(this.formData).subscribe(
        success => {
          this.router.navigate(['/ap/ProfileView/', success.data.userCode]);
          swal({
            title: 'Success!',
            text: 'Save successfully',
            type: 'success',
            confirmButtonText: 'Close'
          });
        },
        error => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error['message'],
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    } else {
      // if (this.isFromUserView) {
      //   this.userProfile.data = this.model;
      // }
      m = Object.assign({}, this.model);
      m.roles = m.roles.filter(x => x.userExists === true);
      this.userinfoService.update(m).subscribe(
        success => {
          // if (this.isFromUserView) {
          //   this.onSaveHandler.emit(this.userProfile);
          // }
          swal({
            title: 'Success!',
            text: 'Save successfully',
            type: 'success',
            confirmButtonText: 'Ok'
          });
          this.router.navigate(['/ap/ProfileView/', success.data.userCode]);
        },
        error => {
          console.log(error);
          swal({
            text: JSON.parse(JSON.stringify(error)).error['message'],
            type: 'warning',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }

  dataParsing() {
    this.model.dateOfBirth = this.dataService.utc_to_local_time(
      this.model.dateOfBirth
    );
    this.model.yearOfExp =
      this.model.yearOfExp === 0 ? null : this.model.yearOfExp;
    this.model.requirementRanking =
      this.model.requirementRanking === 0
        ? null
        : this.model.requirementRanking;
    this.model.activeManPower =
      this.model.activeManPower === 0 ? null : this.model.activeManPower;
    this.model.productionBasedRanking =
      this.model.productionBasedRanking === 0
        ? null
        : this.model.productionBasedRanking;
    this.model.totalScore =
      this.model.totalScore === 0 ? null : this.model.totalScore;
    this.model.quotaLimit =
      this.model.quotaLimit === 0 ? null : this.model.quotaLimit;
    this.model.quotaRemaining =
      this.model.quotaRemaining === 0 ? null : this.model.quotaRemaining;
    this.model.teamMember =
      this.model.teamMember === 0 ? null : this.model.teamMember;
    this.model.achievementNo =
      this.model.achievementNo === 0 ? null : this.model.achievementNo;
  }
  keyupHandler($event) {
    // this.model.achievements = $event;
  }
  onRecruitmentQuotaChnage($event) {
    this.model.quotaRemaining = this.model.quotaLimit - this.model.quotaConsume;
  }
  itemChange($event) {
    // this.model.tagUserList = $event;
  }
  designationChange() {
    //   const tempDes = this.designations.filter(
    //     x => x.id == this.model.designationId
    //   )[0];
    //   if (tempDes) {
    //     if (tempDes.shortCode === 'U') {
    //       // UM
    //       this.setting.endpointUrl =
    //         this.authService.APPSERVER + 'user/GetUmBMCode?userType=BM';
    //       this.showTypeHead = true;
    //       this.setting.selectionType = TypeHeadSelectionType.Multiple;
    //       this.setting.placeHolderText = 'BM Code';
    //     } else if (tempDes.shortCode === 'A') {
    //       // BM
    //       this.setting.endpointUrl =
    //         this.authService.APPSERVER + 'user/GetUmBMCode?userType=UM';
    //       this.showTypeHead = true;
    //       this.setting.selectionType = TypeHeadSelectionType.Single;
    //       this.setting.placeHolderText = 'UM Code';
    //     }
    //   }
  }
}
