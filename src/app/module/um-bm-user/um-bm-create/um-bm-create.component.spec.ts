import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmBmCreateComponent } from './um-bm-create.component';

describe('UmBmCreateComponent', () => {
  let component: UmBmCreateComponent;
  let fixture: ComponentFixture<UmBmCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmBmCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmBmCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
