import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatesEligibleListComponent } from './candidates-eligible-list.component';

describe('CandidatesEligibleListComponent', () => {
  let component: CandidatesEligibleListComponent;
  let fixture: ComponentFixture<CandidatesEligibleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatesEligibleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatesEligibleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
