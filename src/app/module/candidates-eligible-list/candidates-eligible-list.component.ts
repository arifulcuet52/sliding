import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/shared/service/data/data.service';
import { MatSort, MatTableDataSource } from '@angular/material';
import { UtilityService } from 'src/app/shared/service/utility/utility.service';
import { EligibleCandidateForContractVm } from 'src/app/entity/eligible.candidate.for.contract.vm';
import swal from 'sweetalert2';
import { Subject, fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
@Component({
  selector: 'app-candidates-eligible-list',
  templateUrl: './candidates-eligible-list.component.html',
  styleUrls: ['./candidates-eligible-list.component.scss']
})
export class CandidatesEligibleListComponent implements OnInit {
  search = '';
  dataSource: any;
  @ViewChild(MatSort)
  sort: MatSort;
  searchTextChanged = new Subject<string>();
  displayedColumns: string[];
  pageSize = 10;
  pageIndex = 0;
  length: number;
  pageSizeOptions: number[] = [1, 5, 10, 25, 50, 75, 100];
  dropDown = 'C';
  constructor(public ds: DataService, private utility: UtilityService) { }

  ngOnInit() {
    this.filter();
    this.getList();
    this.showHideCloumn();
  }

  showHideCloumn() {
    if (this.dropDown === 'C') {
      this.displayedColumns = [
        'candidateId',
        'candidateName',
        'faCode',
        'taggedUmBmName',
        'campaignCode',
        'campaignEndDate',
        'contractDate',
        'action',
        'dateOfAckowledgement'
      ];
    } else {
      this.displayedColumns = [
        'candidateId',
        'candidateName',
        'faCode',
        'taggedUmBmName',
        'contractDate',
        'action',
        'dateOfAckowledgement'
      ];
    }
  }
  filter() {
    const input = document.getElementById('search');
    const obs = fromEvent(input, 'input');
    obs
      .pipe(
        map(evenet => evenet.target['value']),
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe(s => {
        this.search = s;
        this.getList();
        //  console.log(typeof s);
      });
    // this.getList();
  }

  getList() {
    let tempSearch = '';
    if (this.search) {
      tempSearch = this.search;
    }
    this.utility
      .getCandidatesEligibleList(this.pageIndex, this.pageSize, tempSearch)
      .subscribe(
        (success: EligibleCandidateForContractVm[]) => {
          // console.log(success);
          this.dataSource = new MatTableDataSource(success);
          this.dataSource.sort = this.sort;
          if (success.length > 0) {
            this.length = success[0].totalRow;
          } else {
            this.length = 0;
          }
        },
        error => {
          this.showErrorMessage();
          console.log(error);
        }
      );
  }
  pageChange($event) {
    this.pageIndex = +$event['pageIndex'];
    this.pageSize = +$event['pageSize'];
    this.getList();
  }
  dropDownValueChange($event) {
    // console.log($event);
    this.showHideCloumn();
  }
  onContractSigned(candidateId: number) {
    this.showConfirmation(candidateId);
  }
  showErrorMessage() {
    swal({
      type: 'warning',
      text: 'An error occurred.'
    });
  }
  showConfirmation(candidateId: number) {
    swal({
      title: 'Are you sure?',
      text: 'Confirm this contract as signed?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then(result => {
      if (result.value) {
        this.utility.candiateSignin(candidateId).subscribe(
          sucess => {
            console.log(sucess);
            this.getList();
          },
          error => {
            this.showErrorMessage();
            console.log(error);
          }
        );
      }
    });
  }
}
