import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms'
import { DynamicReportService } from '../../../shared/service/report/dynamic-report.service';
import { DynamicForm } from '../../../entity/dynamicReport'
import {ActivatedRoute} from '@angular/router'

@Component({
  selector: 'app-generic-report',
  templateUrl: './generic-report.component.html',
  styleUrls: ['./generic-report.component.scss']
})
export class GenericReportComponent implements OnInit {
  public form: FormGroup;
  public reportId = 2;
  public reportTitle = 'Dynamic Report Generation Forms';
  unsubcribe: any
  constructor(private dynamicReportService: DynamicReportService, private route:ActivatedRoute) {
    this.form = new FormGroup({
      fields: new FormControl(JSON.stringify(this.fields))
    });
    this.unsubcribe = this.form.valueChanges.subscribe((update) => {
      console.log(update);
      this.fields = JSON.parse(update.fields);
    });
  }

  ngOnInit() {
    this.reportId=this.route.snapshot.params['reportId']||1;
    this.dynamicReportService.fnGetReportParams(this.reportId).subscribe((data: any) => {
      this.fields = data.data || [];
      console.log(data.data);
    });

    this.dynamicReportService.fnGetReportTitle(this.reportId).subscribe(
      (success: any) => {
          console.log('report title');
          console.log(success);
          this.reportTitle = success.reportTitle;
      },
      (error) => {
        console.log(error);
      }
    );
  }


  fnChangeDropDown(event: any) {
    console.log("success: ", event);
    let param: DynamicForm = event.data || new DynamicForm();
    if (param.onChangeDependency) {
      let fieldId = param.childDropdownId || 0;
      let paramId = param.id;
      this.dynamicReportService.fnGenerateOptions(paramId, event.dataId || 0).subscribe((data: any) => {
        let options = data || [];
        console.log("options record: ", options);
        this.fields.forEach((rec: any) => {
          if (rec.id == +fieldId) {
            rec.options = options;
          }
        });

      });
    }

  }

  fnGetSubmitValues(event: any) {
    console.log(event);
    this.dynamicReportService.fnGetReportData(event, this.reportId).subscribe((rec: any) => {
      console.log(rec.result);
      this.dynamicReportTableSetting.tableColDef = [];
      this.dynamicReportData = rec.result.data || [];
      if (this.dynamicReportData.length > 0) {
        Object.keys(this.dynamicReportData[0]).forEach(x => {
          let title=x.replace(/_/g,' ');
          //let title = x.split(/(?=[A-Z])/).join(" ");
          title = title.charAt(0).toUpperCase() + title.slice(1);
          this.dynamicReportTableSetting.tableColDef.push({ headerName: title, width: '10%', internalName: x, sort: true, type: "" });
        });
      }

    },
      (error: any) => {
        console.log(error);
        alert(error);
      }
    );

    this.dynamicReportTableSetting.tableName=this.reportTitle;

  }

  public fields: any[] = [];
  public dynamicReportData: any[] = [];
  public dynamicReportTableSetting = {
    tableID: "report-table",
    tableClass: "table table-border ",
    tableName: this.reportTitle,
    tableRowIDInternalName: "ID",
    tableColDef: [],
    enabledSearch: true,
    enabledSerialNo: true,
    pageSize: 15,
    enabledPagination: true,
    enabledEditBtn: false,
    enabledCellClick: true,
    enabledColumnFilter: true,
    enabledPdfDownload:true,
    enabledPrint:true,
    enabledXLS:true,
  };
}
