import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericReportComponent } from './generic-report/generic-report.component';
import { DynamicFormBuilderModule} from './dynamic-form-builder/dynamic-form-builder.module';
import { Route, ActivatedRoute, Routes, RouterModule} from '@angular/router';
import { PTableModule } from './../../shared/modules';
import { ReportPanelComponent } from './report-panel/report-panel.component';
import { SideNavbarComponent } from '../side-navbar/side-navbar.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {path: '', component: ReportPanelComponent},
      {path: ':reportId', component: GenericReportComponent},
      {path: 'report-panel', component: ReportPanelComponent}
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, DynamicFormBuilderModule, RouterModule.forChild(routes), PTableModule,
  ],
  declarations: [GenericReportComponent, ReportPanelComponent, SideNavbarComponent]
})
export class DynamicReportModule { }
