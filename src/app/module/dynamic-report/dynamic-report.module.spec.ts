import { DynamicReportModule } from './dynamic-report.module';

describe('DynamicReportModule', () => {
  let dynamicReportModule: DynamicReportModule;

  beforeEach(() => {
    dynamicReportModule = new DynamicReportModule();
  });

  it('should create an instance', () => {
    expect(dynamicReportModule).toBeTruthy();
  });
});
