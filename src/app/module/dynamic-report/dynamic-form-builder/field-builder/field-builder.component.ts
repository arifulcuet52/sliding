import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'field-builder',
  template: `
  <div class="form-group" [formGroup]="form">   
    <div class="" [ngSwitch]="field.type">
      <textbox class="" *ngSwitchCase="'text'" [field]="field" [form]="form"></textbox>
      <textarea  class="" *ngSwitchCase="'textarea'" [field]="field" [form]="form"></textarea>
      <dropdown class="" *ngSwitchCase="'dropdown'" [field]="field" [form]="form" (dropdownChange)="fnOnChange($event)"></dropdown>
      <checkbox class="" *ngSwitchCase="'checkbox'" [field]="field" [form]="form"></checkbox>
      <radio *ngSwitchCase="'radio'" [field]="field" [form]="form"></radio>
      <file *ngSwitchCase="'file'" [field]="field" [form]="form"></file>
      <date-picker *ngSwitchCase="'datetime'" [field]="field" [form]="form"></date-picker>
      <div class="alert alert-danger my-1 p-2 fadeInDown animated" *ngIf="!isValid && isDirty">{{field.label}} is required</div>
    </div>
  </div>
  `


  // <div class="form-group row" [formGroup]="form">
  //   <label class="col-md-3 form-control-label" [attr.for]="field.label">
  //     {{field.label}}
  //     <strong class="text-danger" *ngIf="field.required">*</strong>
  //   </label>
  //   <div class="col-md-9 mt-9" [ngSwitch]="field.type">
  //     <textbox *ngSwitchCase="'text'" [field]="field" [form]="form"></textbox>
  //     <textarea *ngSwitchCase="'textarea'" [field]="field" [form]="form"></textarea>
  //     <dropdown *ngSwitchCase="'dropdown'" [field]="field" [form]="form" (change)="fnOnChange($event,field)"></dropdown>
  //     <checkbox *ngSwitchCase="'checkbox'" [field]="field" [form]="form"></checkbox>
  //     <radio *ngSwitchCase="'radio'" [field]="field" [form]="form"></radio>
  //     <file *ngSwitchCase="'file'" [field]="field" [form]="form"></file>
  //     <date-picker *ngSwitchCase="'datetime'" [field]="field" [form]="form"></date-picker>
  //     <div class="alert alert-danger my-1 p-2 fadeInDown animated" *ngIf="!isValid && isDirty">{{field.label}} is required</div>
  //   </div>
  // </div>
})
export class FieldBuilderComponent implements OnInit {
  @Input() field: any;
  @Input() form: any;
  @Output() fnOnChangeDropDown = new EventEmitter() || null;

  get isValid() { return this.form.controls[this.field.name].valid; }
  get isDirty() { return this.form.controls[this.field.name].dirty; }

  constructor() { }

  ngOnInit() {
  }

  fnOnChange(event:any) {
    //this.fnOnChangeDropDown.emit({ dataId: event.target.value, data: data });
    this.fnOnChangeDropDown.emit(event);
  }

}
