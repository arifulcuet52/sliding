import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'dropdown',
  template: `
    <div [formGroup]="form">
    <mat-form-field class="column-full-width col s12">
      <mat-select
      [placeholder]="field.label"
      [id]="field.name" [formControlName]="field.name" (selectionChange)="fnChange($event.value,field)">
      <mat-option  value="#">
         Select All
        </mat-option>
        <mat-option *ngFor="let opt of field.options" [value]="opt.key">
          {{opt.label}}
        </mat-option>
      </mat-select>
  </mat-form-field>       
  </div>       
    `

  // <div [formGroup]="form">
  //     <select class="form-control" [id]="field.name" [formControlName]="field.name">
  //       <option *ngFor="let opt of field.options" [value]="opt.key">{{opt.label}}</option>
  //     </select>
  //   </div> 
})
export class DropDownComponent {
  @Input() field: any = {};
  @Input() form: FormGroup;
  @Output() dropdownChange=new EventEmitter()||null;

  constructor() {

  }

  fnChange(value, field) {
   this.dropdownChange.emit({ dataId: value, data: field });
  }
}