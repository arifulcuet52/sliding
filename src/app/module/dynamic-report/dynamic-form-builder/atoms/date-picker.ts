import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { MAT_DATE_FORMATS, DateAdapter } from '@angular/material/core';
import { AppDateAdapter } from './date.adapter';


export const APP_DATE_FORMATS =
{
  parse: {
    dateInput: { month: 'short', year: 'numeric', day: 'numeric' }
  },
  display: {
    // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
    dateInput: 'input',
    // monthYearLabel: { month: 'short', year: 'numeric', day: 'numeric' },
    monthYearLabel: 'inputMonth',
    dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
    monthYearA11yLabel: { year: 'numeric', month: 'long' },
  }
}
// text,email,tel,textarea,password, 
@Component({
  selector: 'date-picker',
  template: `
  <div [formGroup]="form">
  <mat-form-field class="column-full-width col s12">
  <input matInput [matDatepicker]="picker" [placeholder]="field.label"  [formControlName]="field.name" [id]="field.name" [name]="field.name" autocomplete="off" (ngModelChange)="fnDateChange(picker)">
  <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
  <mat-datepicker #picker  (ngModelChange)="fnDateChange(picker)"></mat-datepicker>
</mat-form-field>
  </div>
    `,
  providers: [
    {
      provide: DateAdapter, useClass: AppDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }]
})
export class DatePickerComponent implements OnInit {
  @Input() field: any = {};
  @Input() form: FormGroup;
  public datePicker = new FormControl(new Date());
  public datePickerConfig: Partial<BsDatepickerConfig>;
  get isValid() { return this.form.controls[this.field.name].valid; }
  get isDirty() { return this.form.controls[this.field.name].dirty; }

  constructor() {

  }
  ngOnInit() {
    this.datePickerConfig = Object.assign(
      {},
      {
        dateInputFormat: 'DD/MM/YYYY',
        showWeekNumbers: false
      }
    );
  }

  fnDateChange(picker) {

    console.log("date picker : ", this.field.name, picker);
  }
}


