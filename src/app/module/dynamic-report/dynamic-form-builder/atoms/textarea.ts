import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

// text,email,tel,textarea,password, 
@Component({
    selector: 'textarea',
    template: `
      <div [formGroup]="form">
          <mat-form-field class="column-full-width col s12">
              <textarea matInput [formControlName]="field.name" [id]="field.name"
              rows="9" class="form-control" [placeholder]="field.label"></textarea>
          </mat-form-field>
      </div> 
    `

  //   <div [formGroup]="form">
  //   <textarea [class.is-invalid]="isDirty && !isValid" [formControlName]="field.name" [id]="field.name"
  //   rows="9" class="form-control" [placeholder]="field.placeholder"></textarea>
  // </div> 
    
})
export class TextAreaComponent {
    @Input() field:any = {};
    @Input() form:FormGroup;
    get isValid() { return this.form.controls[this.field.name].valid; }
    get isDirty() { return this.form.controls[this.field.name].dirty; }
  
    constructor() {

    }
}