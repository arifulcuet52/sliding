import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'dynamic-form-builder',
  template: `
    <form (ngSubmit)="onFormSubmit()" [formGroup]="form" class="form-horizontal">
     
      <div   class="form-row">
      <div *ngFor="let field of fields" class="col-md-6">
          <field-builder [field]="field" [form]="form" (fnOnChangeDropDown)="fnOnChangeDropDown($event)"></field-builder>
      </div>

      </div>
      <div class="form-group">
        <div class="col-md-12 text-right">
          <button type="submit" class="btn btn-primary">Generate Report</button>         
        </div>
      </div>
    </form>
  `,
  //[disabled]="!form.valid"
})
export class DynamicFormBuilderComponent implements OnInit {
  @Output() onSubmit = new EventEmitter();
  @Input() fields: any[] = [];
  @Output() fnChangeDropDown = new EventEmitter() || null;
  form: FormGroup;
  public datetimeControlCollection: any[] = [];
  constructor() { }


  ngOnInit() {
    console.log("fields: ", this.fields);
    this.datetimeControlCollection = [];
    let fieldsCtrls = {};
    for (let f of this.fields) {
      if (f.type.toLowerCase() == 'datetime') {
        console.log("datetime controll name.....", f.type);
        fieldsCtrls[f.name] = new FormControl(f.value || new Date());
        //fieldsCtrls[f.value] = new FormControl(new Date());
        this.datetimeControlCollection.push(f.name);
      }
      else if (f.type != 'checkbox') {
        fieldsCtrls[f.name] = new FormControl(f.value || '', Validators.required);
      } else {
        let opts = {};
        for (let opt of f.options) {
          opts[opt.key] = new FormControl(opt.value);
        }
        fieldsCtrls[f.name] = new FormGroup(opts)
      }
    }

    console.log("fieldsCtrls", fieldsCtrls);
    this.form = new FormGroup(fieldsCtrls);

    //this.form.get('from_date').setValue(new Date());
  }

  fnOnChangeDropDown(event: any) {
    console.log("from form builder: ", event);
    this.fnChangeDropDown.emit(event);
  }

  onFormSubmit() {
    let newForm: FormGroup = this.form;
    this.datetimeControlCollection.forEach((x:any)=>{
      newForm.get(x).setValue(this.convertDate(newForm.get(x).value));
    });

    this.onSubmit.emit(newForm.value)
  }

  convertDate(str) {
    if (str == null || str == "") {
      return null;
    }
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }
}


