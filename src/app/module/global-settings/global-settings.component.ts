import { Component, OnInit } from '@angular/core';
import { GeneralSettingsService } from '../../shared/service/remote/general-settings/general-settings.service';
import { GeneralSetting } from '../../entity/general-setting';
import * as feather from 'feather-icons';
import swal from 'sweetalert2';

@Component({
  selector: 'app-global-settings',
  templateUrl: './global-settings.component.html',
  styleUrls: ['./global-settings.component.css']
})
export class GlobalSettingsComponent implements OnInit {

  constructor(private generalSettingService: GeneralSettingsService) { }

  model: GeneralSetting;
  backupModel: GeneralSetting;
  errorMessage = '';
  successMessage = '';
  updatedFields: any = [];

  ngOnInit() {
    //feather.replace();
    this.model = new GeneralSetting();
    this.backupModel = new GeneralSetting();
    this.getSettings();
  }

  getSettings() {
    this.generalSettingService.get()
      .subscribe(
        (success) => {
          this.model = success.data;
          this.backupModel = Object.assign({}, this.model);

          console.log(success.data);
        },
        (error) => { this.errorMessage = JSON.parse(JSON.stringify(error)).error.message; }
      );
  }

  update(id) {
    const m = Object.assign({}, this.model);

    this.generalSettingService.edit(id, m).subscribe(
      (success) => {
        swal({
          title: 'Success!', type: 'success', confirmButtonText: 'Close',
          text: 'Save Successfully'
        });
      },
      (error) => {
        swal({
          title: 'Warning!', type: 'warning', confirmButtonText: 'Close',
          text: JSON.parse(JSON.stringify(error)).error.message
        });
      }
    );
  }

  // public update() {
  //   //const m = Object.assign({}, this.model);

  //   // for(let i=0; i< this.updatedFields.length; i++){
  //   //   let fieldName = this.updatedFields[i];
  //   //   this.backupModel[fieldName] = this.model[fieldName];
  //   // }

  //   // this.generalSettingService.edit(this.backupModel.id, this.backupModel).subscribe(
  //   //   (success) => {
  //   //     console.log(success.results); 
  //   //   },
  //   //   (error) => {
  //   //     this.errorMessage = JSON.parse(JSON.stringify(error)).error.message;
  //   //   }
  //   // );

  //   console.log(this.model);
  //   this.generalSettingService.edit(this.model.id, this.model).subscribe(
  //     (success) => {
  //       console.log(success.results);
  //     },
  //     (error) => {
  //       this.errorMessage = JSON.parse(JSON.stringify(error)).error.message;
  //     }
  //   );
  // }

  modelChanged(fieldName) {
    this.updatedFields.push(fieldName);
    console.log("---", fieldName);
  }

  toggleClicked() {
    if(this.updatedFields.length > 0){
      this.model = Object.assign({}, this.backupModel);
    }
    this.updatedFields = [];
  }
}
