import { Component, OnInit, ViewChild } from '@angular/core';
import { StatusFeeEnum } from 'src/app/entity/status.enum';
import { MatDialog, MatTableDataSource, MatDialogConfig, MatSort } from '@angular/material';
import { CertificateService } from 'src/app/shared/service/remote/certificate/certificate.service';
import { CertificateFeeReassignDialogComponent } from '../certificate-fee-reassign-dialog/certificate-fee-reassign-dialog.component';
import { StatusService } from '../../../shared/service/remote/status/status.service';
import { PagingModel } from 'src/app/entity/PagingModel';
import { CertificateFeeCa } from 'src/app/entity/certificate-fee-ca';

@Component({
  selector: 'app-certificate-fee-list',
  templateUrl: './certificate-fee-list.component.html',
  styleUrls: ['./certificate-fee-list.component.scss']
})
export class CertificateFeeListComponent implements OnInit {

  public filterStatus = '';
  public searchText = '';
  public completeStatus = false;
  public inProgressStatus = false;
  public filterStatuses: string[] = ['Pending', 'In Progress', 'Completed'];
  public certificateFeeCa: CertificateFeeCa;
  public status: any = StatusFeeEnum;
  errorMessage = '';
  successMessage = '';

  pageSize = 10;
  pageIndex = 1;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  public dataSource: any = null;
  public displayedColumns: string[];

  @ViewChild(MatSort) sort: MatSort;

  constructor(private dialog: MatDialog,
    public certificateFeeService: CertificateService,
    public statusService: StatusService) {
  }

  ngOnInit() {
    this.filterStatus = this.filterStatuses[0];
    this.statusChange();
    this.getAllCa();
  }

  filter() {
    this.statusChange();
    this.getAllCa(this.pageIndex, this.pageSize);
  }

  getAllCa(pageIndex: number = 1, pageSize: number = 10) {
    this.certificateFeeService.getAllCa(this.filterStatus, this.searchText, pageIndex, pageSize)
      .subscribe(
        (response: PagingModel<CertificateFeeCa>) => {
          this.dataSource = new MatTableDataSource(response.data);
          this.dataSource.sort = this.sort;
          this.length = response.totalItem;
        }
        , () => { });
  }

  statusChange() {
    this.completeStatus = this.filterStatus === 'Completed';
    this.inProgressStatus = this.filterStatus === 'In Progress';

    if (this.completeStatus) {
      this.displayedColumns = ['candidateId', 'candidateName', 'faCode', 'receiptNumber', 'createDateTime',
        'amountPaid', 'paymentDate', 'depositPoint', 'authorizedByName', 'lockedDate', 'status', 'authorizedDate'];
    }
    else {
      this.displayedColumns = ['candidateId', 'candidateName', 'faCode', 'receiptNumber', 'createDateTime',
        'amountPaid', 'paymentDate', 'depositPoint', 'lockedByName', 'lockedDate'];
    }
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.getAllCa(this.pageIndex, this.pageSize);
  }

  reassign(certificateFeeCa) {
    this.openDialog(certificateFeeCa).afterClosed().subscribe(data => {
      if (data !== undefined) {
        this.getAllCa(this.pageIndex, this.pageSize);
      }
    });
  }

  openDialog(model: CertificateFeeCa) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = model;
    return this.dialog.open(CertificateFeeReassignDialogComponent, dialogConfig);
  }
}
