import { Component, OnInit, Inject } from '@angular/core';
import { CertificateFee } from 'src/app/entity/certificate-fee';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import swal from 'sweetalert2';
import { CertificateService } from 'src/app/shared/service/remote/certificate/certificate.service';
import { Status } from 'src/app/entity/status';
import { StatusService } from 'src/app/shared/service/remote/status/status.service';
import { UserService } from 'src/app/shared/service/remote/user/user.service';
import { ResponseMessage } from '../../../entity/response-message';
import { User } from '../../../entity/user';
import { UserInfo } from '../../../entity/userInfo';
import { CertificateFeeCa } from 'src/app/entity/certificate-fee-ca';

@Component({
  selector: 'app-certificate-fee-reassign-dialog',
  templateUrl: './certificate-fee-reassign-dialog.component.html',
  styleUrls: ['./certificate-fee-reassign-dialog.component.scss']
})
export class CertificateFeeReassignDialogComponent implements OnInit {
  model: CertificateFeeCa;
  users: UserInfo[];
  roleName = 'Collections Admin';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<CertificateFeeReassignDialogComponent>,
    public certificateFeeService: CertificateService,
    public userService: UserService
  ) { }

  ngOnInit() {
    this.model = this.data;
    this.getCallCenterAdmin();
  }

  getCallCenterAdmin() {
    this.userService.getByRole(this.roleName).subscribe(
      (response: ResponseMessage<UserInfo[]>) => {
        this.users = response.data;
      },
      () => { }
    );
  }

  reassign() {
    console.log(this.model);
    swal({
      title: 'Confirm',
      text: 'Are you sure to reassign?',
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        this.certificateFeeService.reassign(this.model).subscribe(
          (response: any) => {
            if (response.msgCode === 'ok') {
              swal({
                title: 'Success',
                text: response.msg,
                type: 'success',
                confirmButtonText: 'Ok'
              });
              this.dialogRef.close(response);
              return;
            } else {
              swal({
                title: 'Error!',
                text: response.msg,
                type: 'error',
                confirmButtonText: 'Close'
              });
              return;
            }
          },
          (error) => {
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error.message,
              type: 'error',
              confirmButtonText: 'Close'
            });
          }
        );
      }
    });
  }

  close() {
    this.dialogRef.close();
  }
}
