import { Component, OnInit, ViewChild } from '@angular/core';
import { District } from '../../entity/district';
import { DistrictService } from '../../shared/service/remote/district/district.service';
import { CountryService } from '../../shared/service/remote/country/country.service';
import { CountryInfo } from '../../entity/country';
import { MatSort, MatTableDataSource, MatDialogConfig, MatDialog } from '@angular/material';
import { DialogDistrictComponent } from '../../dialog/dialog-district/dialog-district.component';
import swal from 'sweetalert2';
@Component({
  selector: 'app-district',
  templateUrl: './district.component.html',
  styleUrls: ['./district.component.css']
})
export class DistrictComponent implements OnInit {
  displayedColumns: string[] = ['sl', 'name', 'districtName', 'action'];
  dataSource: any;
  @ViewChild(MatSort) sort: MatSort;
  district: District;
  districts: District[] = [];
  countries: CountryInfo[] = [];
  errorMessage = '';
  constructor(
    private dialog: MatDialog,
    private districtService: DistrictService, private countryService: CountryService) { }

  ngOnInit() {
    this.district = new District(0, 0, '');
    this.getCountries();
  }

  save() {
    if (this.district.districtId > 0) {
      this.districtService.update(this.district).subscribe(
        (success) => {
          this.district.districtName = '';
          this.getdistricts();
        },
        (error) => {
          console.log(error);
          this.errorMessage = JSON.parse(JSON.stringify(error)).error;
        }
      );
    } else {
      this.districtService.add(this.district).subscribe(
        (success) => {
          this.district.districtName = '';
          this.getdistricts();
        },
        (error) => {
          console.log(error);
          this.errorMessage = JSON.parse(JSON.stringify(error)).error;
        }
      );
    }
  }
  cancel() {
    this.district.districtName = '';
  }
  getdistricts() {
    this.districtService.getAll().subscribe(
      (success: District[]) => {
        this.dataSource = new MatTableDataSource(success);
        this.dataSource.sort = this.sort;
      }
    );
  }
  getCountries() {
    this.countryService.getAll().subscribe(
      (success) => {
        this.countries = success['data'] as CountryInfo[];
        this.getdistricts();
      }
    );
  }
  countryChange() {
    this.getdistricts();
  }
  edit(districtId: number) {
    this.districtService.get(districtId).subscribe(
      (success: District) => {
        this.open_dialog(success).afterClosed().subscribe(data => {
          if (data !== undefined) {
            this.getdistricts();
          }
          // this.dataSource[index] = data;
          // this.refresh();
        });
      },
      (error) => {

      }
    );
  }
  delete(districtId: number) {
    swal({
      title: 'Warning!',
      text: 'Are you sure you want to delete this?',
      type: 'warning',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
    }).then(res => {
      if (res.value){
        this.districtService.delete(districtId).subscribe(
          (success) => {
            this.getdistricts();
          },
          (error) => {
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error,
              type: 'error',
              confirmButtonText: 'Close'
            });
          }
        );
      }
    }); 
  }
  add() {
    this.district = new District(0, 0, '');
    this.open_dialog(this.district).afterClosed().subscribe(data => {
      if (data !== undefined) {
        this.getdistricts();
      }
    });
  }
  open_dialog(model) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { model: model };
    return this.dialog.open(DialogDistrictComponent, dialogConfig);
  }

}
