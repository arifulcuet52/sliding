import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import {
  MatSort,
  MatDialog,
  MatTableDataSource,
  MatDialogConfig
} from '@angular/material';
import { DataService } from 'src/app/shared/service/data/data.service';
import { PagingModel } from 'src/app/entity/PagingModel';
import { QuestionDialogComponent } from '../exam/question/question-dialog/question-dialog.component';
import { TrainingCertificate } from 'src/app/entity/exam/training-certificate';
import { TrainingCertificateService } from 'src/app/shared/service/remote/exam/training-certificate.service';
import { SelectionModel } from '@angular/cdk/collections';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { FileUploader } from 'ng2-file-upload';
import { AccountService } from 'src/app/shared/service/remote/authentication/account.service';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import * as $ from 'jquery';
import { BsModalService } from 'ngx-bootstrap';
import { Ng2FileUploadComponent } from '../ng2-file-upload/ng2-file-upload.component';
import swal from 'sweetalert2';
import { DocumentViewerComponent } from '../document-viewer/document-viewer.component';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import * as moment from 'moment';
import { SelectedCandidateVm } from 'src/app/entity/selected-candidate.vm';
import { map } from 'rxjs/internal/operators/map';
import { from } from 'rxjs/internal/observable/from';
import { mapTo } from 'rxjs/internal/operators/mapTo';

@Component({
  selector: 'app-training-certificate',
  templateUrl: './training-certificate.component.html',
  styleUrls: ['./training-certificate.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TrainingCertificateComponent implements OnInit {
  // #region variables
  displayedColumns: string[] = [
    'select',
    'sl',
    'candidateId',
    'candidateName',
    'faCode',
    'trainingExamDate',
    'documentSubmissionDate',
    'certificateUploadDate',
    'action'
  ];
  dataSource: any;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  selection = new SelectionModel<TrainingCertificate>(true, []);
  selectedCandidates: SelectedCandidateVm[];
  @ViewChild(MatSort)
  sort: MatSort;
  datePickerConfig: Partial<BsDatepickerConfig>;
  // documentViewerService: BsModalService;

  trainingCertificates: TrainingCertificate[] = [];
  trainingCertificate: TrainingCertificate;
  errorMessage = '';
  successMessage = '';
  searchText = '';
  fromDate: Date = null;
  toDate: Date = null;
  pageSize = 10;
  pageIndex = 1;
  uploader: FileUploader;
  path: SafeResourceUrl;
  documentAllowExtention = '.jpg,.bmp,.jpeg,.pdf,.png';
  // #endregion

  //#region init
  constructor(
    private dialog: MatDialog,
    private trainingCertificateService: TrainingCertificateService,
    public dataService: DataService,
    private accountService: AccountService,
    private authService: AuthService,
    private modalService: BsModalService,
    private documentViewerService: BsModalService,
    private domSanitizer: DomSanitizer
  ) {
    this.uploader = new FileUploader({
      url: this.authService.APPSERVER + 'training/certificate/upload',
      authTokenHeader: 'Authorization',
      authToken: 'Bearer ' + this.accountService.getToken()
    });
  }

  ngOnInit() {
    this.datePickerConfig = Object.assign(
      {},
      {
        dateInputFormat: 'DD/MM/YYYY',
        showWeekNumbers: false
      }
    );
    this.initiation();
    this.reload();
  }
  //#endregion

  // #region get data
  getAll(pageSize: number, pageIndex: number, searchText: string, fromDate: Date, toDate: Date) {
    this.trainingCertificateService
      .get(pageSize, pageIndex, searchText, fromDate, toDate)
      .subscribe((response: PagingModel<TrainingCertificate>) => {
        this.dataSource = new MatTableDataSource(response.data);
        this.dataSource.sort = this.sort;
        this.length = response.totalItem;
      }, () => { });
  }
  // #endregion

  // #region grid functions
  initiation() {
    this.pageIndex = 1;
    this.searchText = '';
    this.fromDate = null;
    this.toDate = null;
    this.trainingCertificate = new TrainingCertificate();
    this.successMessage = '';
    this.errorMessage = '';
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.reload();
  }

  filter() {
    this.pageIndex = 1;
    this.reload();
  }

  clear() {
    this.initiation();
    this.reload();
  }

  reload() {
    this.getAll(
      this.pageSize,
      this.pageIndex,
      this.searchText,
      this.fromDate,
      this.toDate
    );
  }

  open_dialog(model) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = model;
    return this.dialog.open(QuestionDialogComponent, dialogConfig);
  }
  // #endregion

  // #region zip, grid multi select
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  zip(candidateId: number, faCode: string) {
    const date = moment(new Date()).format('DDMMYYYY');
    const filename = `${candidateId}_${faCode}_${date}.zip`;
    this.trainingCertificateService
      .zip(candidateId)
      .subscribe(response => {
        this.downloadFile(response, filename);
      });
  }

  zipSelected() {
    this.selectedCandidates = this.selection.selected.map(e => {
      const d = new SelectedCandidateVm();
      d.candidateId = e.candidateId;
      d.faCode = e.faCode;
      return d;
    });
    const filename = `All_${moment(new Date()).format('DDMMYYYY')}.zip`;
    this.trainingCertificateService
      .zipSelected(this.selectedCandidates)
      .subscribe(response => {
        this.downloadFile(response, filename);
      });
  }

  downloadRediret(response: any) {
    const blob = new Blob([response], {
      type: 'application/zip'
    });
    const url = window.URL.createObjectURL(blob);
    window.open(url);
  }

  downloadFile(response: any, filename: string) {
    const linkElement = document.createElement('a');
    const blob = new Blob([response], { 'type': 'application/zip' });
    linkElement.setAttribute('href', window.URL.createObjectURL(blob));
    linkElement.setAttribute('download', filename);

    const clickEvent = new MouseEvent('click', {
      'view': window,
      'bubbles': true,
      'cancelable': false
    });
    linkElement.dispatchEvent(clickEvent);
  }

  //#endregion

  // #region file upload, view, print pdf
  uploadFile(candidateId: number, candidateExamId: number) {
    this.trainingCertificate.candidateId = candidateId;
    this.trainingCertificate.candidateExamId = candidateExamId;
    document.getElementById('certificateUpload').click();
  }

  upload(event) {
    if (!this.documentValidation(event.target.files)) {
      this.uploader.clearQueue();
      this.reload();
      return;
    }

    this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
      form.append('candidateId', this.trainingCertificate.candidateId);
      form.append('candidateExamId', this.trainingCertificate.candidateExamId);
    };
    this.modalService.show(Ng2FileUploadComponent, {
      initialState: {
        uploader: this.uploader
      },
      ignoreBackdropClick: true
    });
    this.modalService.onHide.subscribe(x => {
      this.uploader.clearQueue();
      this.reload();
    });
  }

  viewCertificate(path: string, title: string) {
    const rootPath = this.authService.APPSERVER.replace('/api/', '');
    if (path) {
      this.documentViewerService.show(DocumentViewerComponent, {
        initialState: {
          filePath: rootPath + path,
          header: `Training Certificate ${title}`
        },
        ignoreBackdropClick: true,
        class: 'custome-modal'
      });
    } else {
      swal({
        title: 'Warning!',
        type: 'warning',
        confirmButtonText: 'Close',
        text: 'File not uploaded.'
      });
    }
  }

  trainingCertificatePrint(): void {
    this.trainingCertificateService
      .trainingCertificatePrint(this.fromDate, this.toDate, this.searchText)
      .subscribe(response => {
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          // IE workaround
          this.iePdf(response);
          return;
        }

        const pdfData = 'data:application/pdf;base64,' + response;
        this.path = this.domSanitizer.bypassSecurityTrustResourceUrl(pdfData);

        if (this.path) {
          this.documentViewerService.show(DocumentViewerComponent, {
            initialState: {
              filePath: this.path,
              header: 'Training Certificate List',
              type: 'saferesourceurl'
            },
            ignoreBackdropClick: true,
            class: 'custome-modal'
          });
        } else {
          swal({
            title: 'Warning!',
            type: 'warning',
            confirmButtonText: 'Close',
            text: 'File not uploaded.'
          });
        }
      });
  }
  //#endregion

  // #region utilities
  documentValidation(files: FileList) {
    let valid = false;
    let file;
    let extention = '';
    let fileSize = 0;

    if (files.length > 0) {
      file = files[0];
    } else {
      return;
    }

    extention = this.fileExtention(file.name);
    fileSize = this.fileToMB(file.size);

    if (
      this.documentAllowExtention &&
      this.documentAllowExtention.includes(extention)
    ) {
      valid = true;
    } else {
      swal({
        title: 'Warning!',
        type: 'warning',
        confirmButtonText: 'Close',
        text:
          'only these types(' +
          this.documentAllowExtention +
          ') of images are supported'
      });
    }

    return valid;
  }

  fileExtention(filename: string) {
    return (
      filename.substring(filename.lastIndexOf('.') + 1, filename.length) ||
      filename
    );
  }

  fileToMB(size: number) {
    if (size <= 0) {
      return 0;
    }

    return Math.ceil(size / (1024 * 1024));
  }

  iePdf(response: any, filename?: string) {
    const byteCharacters = atob(response);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: 'application/pdf' });
    filename = filename != null ? filename : 'report.pdf';
    window.navigator.msSaveOrOpenBlob(blob, filename);
  }
  // #endregion
}
