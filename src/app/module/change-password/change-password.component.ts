import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../shared/service/remote/authentication/account.service';
import {
  ActivatedRoute,
  Router
} from '../../../../node_modules/@angular/router';
import { PasswordResetAttemptOptsEnum } from '../../entity/password-reset-attempt-opt-enum';
import { OtpService } from '../../shared/service/remote/otp/otp.service';
import { interval, timer, Subject, Subscription } from 'rxjs';
import { takeUntil, takeWhile } from 'rxjs/operators';
import { OTPVm } from '../../entity/otpVm';
import { OTPDeliveryMethodEnum } from '../../entity/otp-delivery-method-enum';
import { OTPIssueReasonEnum } from '../../entity/otp-issue-reason-enum';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  constructor(
    private accountService: AccountService,
    private router: Router,
    private otp: OtpService,
    private activatedRoute: ActivatedRoute
  ) {}

  email = '';
  currentPassword = '';
  newPassword = '';
  confirmPassword = '';
  islogIn: boolean;
  errorMessage = '';
  title = '';
  otpCode = '';
  successMessage = '';
  changePassword = false;
  coundDown = 0;
  counDownDate: number;
  // c = new Date().getTime();
  disableOTPResend = true;
  showCountDown = false;
  ngOnInit() {
    const tempId = this.activatedRoute.snapshot.paramMap.get('id');
    if (
      !this.accountService.isLogIn() &&
      (!this.hasQueryString() && !tempId) &&
      !this.defaultPasswordChange()
    ) {
      this.accountService.setUnauthrize(this.router.url);
      this.router.navigate(['/ap/unauthorized']);
    }

    this.getQueryString();
    this.successMessage = '';
    this.email = '';
    this.currentPassword = '';
    this.newPassword = '';
    this.confirmPassword = '';
    this.errorMessage = '';
    this.title = '';
    this.otpCode = '';
    this.changePassword = false;
    this.disableOTPResend = true;
    this.islogIn = this.accountService.isLogIn();
    this.errorMessage = this.accountService.getMessage();
    this.getUserName();

    if (
      this.router.url.includes('/ap/change-password') &&
      !this.defaultPasswordChange()
    ) {
      this.title = 'Change Password';
      this.changePassword = true;
      this.accountService.passwordChangeReason(
        PasswordResetAttemptOptsEnum.ChangePassword
      );
    } else if (this.router.url.includes('/ap/forget-passwor')) {
      this.title = 'Forget Password';
      this.email = this.otp.getOTPMail();
      this.successMessage = this.otp.getOTP().message;
      this.otpValidation();
      this.otpGenerate();
      this.accountService.passwordChangeReason(
        PasswordResetAttemptOptsEnum.ForgotPassword
      );
    } else if (this.router.url.includes('/ap/unlock-account')) {
      this.title = 'Unlock Account';
      this.accountService.passwordChangeReason(
        PasswordResetAttemptOptsEnum.UnlockAccount
      );
    } else if (this.defaultPasswordChange()) {
      this.changePassword = true;
      this.accountService.passwordChangeReason(
        PasswordResetAttemptOptsEnum.DefaultPasswordChange
      );
    }
  }

  hasQueryString() {
    if (this.activatedRoute.snapshot.queryParamMap.get('code')) {
      return true;
    }
    return false;
  }

  defaultPasswordChange() {
    if (this.activatedRoute.snapshot.queryParamMap.get('chanage-password')) {
      return true;
    }
    return false;
  }

  getQueryString() {
    return this.activatedRoute.snapshot.queryParamMap.get('code');
  }
  getUserName() {
    if (this.hasQueryString()) {
      this.email = this.accountService.getTempEmail();
    } else {
      this.email = this.accountService.getUserEmail();
    }
  }

  reset() {
    this.successMessage = '';
    this.errorMessage = '';
    this.newPassword = '';
    this.confirmPassword = '';
    this.currentPassword = '';
    this.otpCode = '';
    // if (!this.islogIn) {
    //   this.email = '';
    // }
  }
  change() {
    this.successMessage = '';
    this.errorMessage = '';

    // console.log(unescape(this.getQueryString()));
    if (this.hasQueryString()) {
      this.accountService
        .changePassowrdByURL(
          this.email,
          this.newPassword,
          this.confirmPassword,
          unescape(this.getQueryString())
        )
        .subscribe(
          success => {
            //  this.accountService.logout('Password Change');
            this.accountService.setMessage(success.toString());
            this.router.navigate(['/ap/signin']);
            this.errorMessage = '';
          },
          error => {
            this.errorMessage = error.error.message;
          }
        );
    } else if (
      this.accountService.getPasswordChangeReason() ===
        PasswordResetAttemptOptsEnum.ForgotPassword ||
      this.accountService.getPasswordChangeReason() ===
        PasswordResetAttemptOptsEnum.UnlockAccount
    ) {
      this.accountService
        .forgetPassowrd(
          this.email,
          this.currentPassword,
          this.newPassword,
          this.confirmPassword,
          this.otpCode
        )
        .subscribe(
          success => {
            this.accountService.setMessage(success.toString());
            this.router.navigate(['/ap/signin']);
            this.errorMessage = '';
          },
          error => {
            this.errorMessage = error.error.message;
          }
        );
    } else {
      console.log('fire');
      this.accountService
        .changePassowrd(
          this.email,
          this.currentPassword,
          this.newPassword,
          this.confirmPassword
        )
        .subscribe(
          success => {
            this.accountService.logout('Password Change');
            this.accountService.setMessage(success.toString());
            this.router.navigate(['/ap/signin']);
            this.errorMessage = '';
          },
          error => {
            this.errorMessage = error.error.message;
          }
        );
    }
  }
  resendOTP() {
    console.log('aaa');
    this.successMessage = '';
    this.errorMessage = '';
    this.otp
      .regenerateOTP(
        this.otp.getOTPDeliveryMethod(),
        OTPIssueReasonEnum.ForgetPassword,
        this.email
      )
      .subscribe(
        (succces: any) => {
          console.log(succces);
          this.successMessage = succces.message;
          this.otp.setOtp({
            path: succces.path,
            allowAfter: succces.allowAfter,
            message: succces.message
          });
          this.otp.setOTPMail(this.email);
          this.otpGenerate();
        },
        error => {
          console.log(error);
          if (
            JSON.parse(error.error.message).type &&
            JSON.parse(error.error.message).type === 'disableResendButton'
          ) {
            this.errorMessage = JSON.parse(error.error.message).message;
            this.coundDown = 1;
            this.maxOTPExcedDisable(JSON.parse(error.error.message).unlockTime);
            console.log(JSON.parse(error.error.message).unlockTime as Date);
          } else {
            this.errorMessage = error.error.message;
          }
          this.otp.setOtp(new OTPVm());
        }
      );
  }

  otpValidation() {
    if (this.accountService.isLogIn()) {
      this.router.navigate(['/ap/change-password']);
    }
    const otpvm = this.otp.getOTP();
    this.activatedRoute.params.subscribe(x => {
      if (!x['id'] || !otpvm || x['id'] != otpvm.path) {
        this.router.navigate(['/ap/signin']);
      }
    });
  }

  maxOTPExcedDisable(time: string) {
    const tempDate = new Date(time + 'Z');
    const numbers = timer(0, 1000);
    let ngUnsubscribe = new Subscription();
    const allowAfter = (tempDate.getTime() - new Date().getTime()) / 1000;
    console.log(allowAfter, 'allowAfter');
    ngUnsubscribe = numbers.subscribe(x => {
      //console.log(x);
      if (allowAfter <= x) {
        this.coundDown = 0;
        ngUnsubscribe.unsubscribe();
      }
    });
  }

  otpGenerate() {
    this.showCountDown = true;
    const otpvm = this.otp.getOTP();

    this.coundDown = otpvm.allowAfter;
    let ngUnsubscribe = new Subscription();
    const numbers = timer(0, 1000);
    const today1 = new Date();
    const myToday = new Date(
      today1.getFullYear(),
      today1.getMonth(),
      today1.getDate(),
      0,
      0,
      otpvm.allowAfter
    );
    ngUnsubscribe = numbers.subscribe(x => {
      const temp = new OTPVm();
      temp.allowAfter = otpvm.allowAfter - x;
      temp.path = otpvm.path;
      this.coundDown = this.coundDown - 1;
      const tempD = new Date(myToday).getTime() - x * 1000;
      this.counDownDate = tempD;
      this.otp.setOtp(temp);
      if (otpvm.allowAfter <= x) {
        this.showCountDown = false;
        ngUnsubscribe.unsubscribe();
      }
    });
  }
}
