import { DashboardComponent } from './../dashboard/dashboard.component';
import { DialogEventCalenderComponent } from './../../dialog/dialog-event-calender/dialog-event-calender.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatTableDataSource } from '@angular/material';
import { EventCalender } from 'src/app/entity/event-calender';
import { EventCalenderService } from 'src/app/shared/service/remote/event-calender/event-calender.service';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { DataService } from 'src/app/shared/service/data/data.service';
import swal from 'sweetalert2';


@Component({
  selector: 'app-event-calender',
  templateUrl: './event-calender.component.html',
  styleUrls: ['./event-calender.component.scss']
})
export class EventCalenderComponent implements OnInit {

  displayColumns = ['eventTitle', 'startDate', 'startTime', 'endDate', 'endTime', 'status', 'district', 'area', 'capacity', 'availableCapacity', 'action'];
  datasource: any;
  eventCalenders: EventCalender[] = [];
  eventCalender: EventCalender;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  searchText = '';
  pageSize = 10;
  pageIndex = 1;

  constructor(private dialog: MatDialog,
              private eventCalenderService: EventCalenderService,
              private dataService: DataService) { }

  ngOnInit() {
    this.getAll();
  }

  getAll(){
    this.eventCalenderService.getAll(this.searchText, this.pageSize, this.pageIndex).subscribe(
      (success: any) => { // console.log(success);
                   for (const item of success.data){
                      item.startDateString = this.formatDate(item.startDate);
                      item.endDateString = this.formatDate(item.endDate);
                      item.startTimeString = this.formatTime(item.sTime);
                      item.endTimeString = this.formatTime(item.eTime);

                   }
                   this.datasource = new MatTableDataSource(success.data);
                   console.log(this.datasource);
                   this.length = success.totalItem;
              },
      (error) => {}
    );
  }

  formatDate(date: string){
     return moment(new Date(date)).toDate().getDate()
     + '/' + (moment(new Date(date)).toDate().getMonth() + 1)
     + '/' + moment(new Date(date)).toDate().getFullYear();
  }

  formatTime(time: Date){
    return moment.utc(time, 'HH:mm').local().format('hh:mm A');
  }

  add(){
    this.eventCalender = new EventCalender();
    this.openDialog(this.eventCalender).afterClosed()
    .subscribe(data => {
        this.getAll();
    });
  }


  edit(index){
   // console.log(this.datasource.data[index]);
    const localStartTime = moment.utc(this.datasource.data[index].sTime, 'HH:mm').local().format('hh:mm A');
    const localEndTime = moment.utc(this.datasource.data[index].eTime, 'HH:mm').local().format('hh:mm A');

    this.datasource.data[index].startDate = this.dataService.utc_to_local_time(this.datasource.data[index].startDate);
    this.datasource.data[index].endDate = this.dataService.utc_to_local_time(this.datasource.data[index].endDate);
    this.datasource.data[index].startDate = moment(new Date(this.datasource.data[index].startDate)).toDate();
    this.datasource.data[index].endDate = moment(new Date(this.datasource.data[index].endDate)).toDate();

    // console.log(this.datasource.data[index].startDate.toDateString() + ' ' + this.datasource.data[index].sTime);

    this.datasource.data[index].startTime = moment(new Date(this.datasource.data[index].startDate.toDateString() + ' ' + localStartTime)).toDate();
    this.datasource.data[index].endTime = moment(new Date(this.datasource.data[index].endDate.toDateString() + ' ' + localEndTime)).toDate();

    this.openDialog(this.datasource.data[index]).afterClosed()
     .subscribe(data => {
       if (data !== undefined) {
         this.getAll();
       }
     });
  }

  filter(searchText: string) {
    this.searchText = searchText;
    this.pageIndex = 1;
    this.getAll();

  }

  delete(id){
    const swalWithBootstrapButtons = swal.mixin({
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
    });

    swalWithBootstrapButtons({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.eventCalenderService.delete(id).subscribe(
          (success) => {
            console.log('deleted');
            console.log(success);
            this.getAll();
          },
          (error) => {
            console.log('can\'t delete');
            console.log(error);
            swal({
              title: 'Error!',
              text: JSON.parse(JSON.stringify(error)).error,
              type: 'error',
              confirmButtonText: 'Close'
            });
          }
        );

        /* swalWithBootstrapButtons(
          'Deleted!',
          'Selected schedule has been deleted.',
          'success'
        ) */

      } else if (
        result.dismiss === swal.DismissReason.cancel
      ) {
        /* swalWithBootstrapButtons(
          'Cancelled',
          'Nothing was deletd :)',
          'error'
        ) */
      }
    });
  }

  openDialog(model){
    const clientHeight = window.innerHeight;
    console.log(clientHeight);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {model: model};
   // dialogConfig.height = '800px';
   // dialogConfig.minHeight = clientHeight - 150;
    return this.dialog.open(DialogEventCalenderComponent, dialogConfig);
  }

  pageChange($event) {
    console.log($event);
    window.scrollTo(0, 0);

    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex + 1;
    this.getAll();
    console.log($event);
  }

}
