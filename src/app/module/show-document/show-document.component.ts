import {
  Component,
  OnInit,
  EventEmitter,
  Input,
  Injector,
  Output
} from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { BsModalRef } from 'ngx-bootstrap';
import * as feather from 'feather-icons';
import swal from 'sweetalert2';
import { UtilityService } from 'src/app/shared/service/utility/utility.service';

@Component({
  selector: 'app-show-document',
  templateUrl: './show-document.component.html',
  styleUrls: ['./show-document.component.scss']
})
export class ShowDocumentComponent implements OnInit {
  acceptMethod: Function;
  rejectMethod: Function;
  filePath: string;
  fileType: string;
  fileExt: string;
  iframe: SafeResourceUrl;
  header: string;
  fileName: string;
  accepReject: boolean;
  showAcceptbtn?: boolean = null;
  showRejectbtn?: boolean = null;
  constructor(
    private sanitizer: DomSanitizer,
    public bsModalRef: BsModalRef,
    private service: UtilityService
  ) { }

  ngOnInit() {
    feather.replace();
    if (this.filePath) {
      this.fileExt = this.filePath.substring(
        this.filePath.lastIndexOf('.'),
        this.filePath.length
      );
      this.fileName = this.filePath.substring(
        this.filePath.lastIndexOf('/') + 1,
        this.filePath.length
      );
      //console.log(this.fileExt);
      if (this.fileExt.toLowerCase() === '.pdf') {
        this.fileType = 'pdf';
        this.iframe = this.sanitizer.bypassSecurityTrustResourceUrl(
          this.filePath
        );
      } else {
        this.fileType = 'image';
      }
    }
  }

  onAccept() {
    this.service.sendAcceptNotification();
    this.bsModalRef.hide();
  }
  onReject() {
    swal({
      title: 'Reason',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Submit',
      showLoaderOnConfirm: true,
      allowOutsideClick: () => !swal.isLoading()
    }).then(result => {
      if (result.value) {
        this.service.sendRejectNotification(result.value);
        this.bsModalRef.hide();
      }
    });
  }
  downloadFile() {
    console.log(this.filePath);
    this.service.getFile(this.filePath).then((x: boolean) => {
      // if (x) {
      //   this.utility.sendFileDownloadNotification();
      // }
    });
  }
  print() {
    this.service.printDom('print');
  }
}
