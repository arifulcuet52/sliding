import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/shared/service/remote/authentication/account.service';
import { UserService } from 'src/app/shared/service/remote/user/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/entity/user';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  constructor(
    public accountService: AccountService,
    private userService: UserService,
    public router: Router
  ) {}
  model: User;
  dashboardContent: RoleWiseDashboard[] = [];
  roles: string[] = [];
  profilePic = 'images/avatar.png';
  ngOnInit() {
    this.roles = this.accountService.getUserRoles();
    this.getUserInfo(this.accountService.getUserEmail());
    this.getDashboardContent();
  }

  getUserInfo(email: string) {
    this.userService.getByEmail(email).subscribe((success: User) => {
      this.model = success;
    });
  }
  checkRole(roleName: string): boolean {
    return (
      this.roles.filter(
        x => x.toLocaleLowerCase() === roleName.toLocaleLowerCase()
      ).length > 0
    );
  }
  getDashboardContent() {
    const tempDashboard = this.accountService.getDashboardContent();
    if (tempDashboard.length) {
      this.dashboardContent = tempDashboard;
    } else {
      this.userService.getUserDashboard().subscribe(
        list => {
          this.dashboardContent = list;
          this.accountService.setDashboardContent(list);
        },
        error => {
          console.log(error);
        }
      );
    }
  }
  onClick(routeLink: string) {
    if (routeLink) {
      if (routeLink.includes('?')) {
        const params = {};
        const allQuery = routeLink.split('&');
        for (let index = 0; index < allQuery.length; index++) {
          const element = allQuery[index];

          let key = '';
          let value = '';
          if (index === 0) {
            const firstElement = allQuery[0].substring(
              allQuery[0].indexOf('?') + 1
            );
            key = firstElement.substring(0, firstElement.indexOf('='));
            value = firstElement.substring(firstElement.indexOf('=') + 1);
          } else {
            key = element.substring(0, element.indexOf('='));
            value = element.substring(element.indexOf('=') + 1);
          }
          params[key] = value;
        }
        routeLink = routeLink.substring(0, routeLink.indexOf('?'));
        this.router.navigate([routeLink], {
          queryParams: params
        });
      } else {
        this.router.navigate([routeLink]);
      }
    }
  }
  routeTo(url: string) {
    const hasQuery = url.includes('?') ? true : false;
    if (hasQuery) {
      const withoutQuery = url.substring(0, url.indexOf('?'));
      const queryKey = url.substring(url.indexOf('?') + 1, url.indexOf('='));
      const queryString = url.substring(url.indexOf('=') + 1, url.length);
      this.router.navigate([withoutQuery], {
        queryParams: { [queryKey]: queryString }
      });
    } else {
      this.router.navigate([url]);
    }
  }
}
