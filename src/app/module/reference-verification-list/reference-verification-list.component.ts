import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { CandidateReferenceService } from 'src/app/shared/service/remote/candidate-reference/candidate-reference.service';
import swal from 'sweetalert2';
import { RefrenceVerificationVm } from 'src/app/entity/refrence.verification.vm';
import { Pager } from 'src/app/entity/pager';
import { DataService } from 'src/app/shared/service/data/data.service';

@Component({
  selector: 'app-reference-verification-list',
  templateUrl: './reference-verification-list.component.html',
  styleUrls: ['./reference-verification-list.component.scss']
})
export class ReferenceVerificationListComponent implements OnInit {
  search = '';
  dataSource: any;
  @ViewChild(MatSort)
  sort: MatSort;
  displayedColumns: string[] = [
    'warning',
    'candidateId',
    'name',
    'faCode',
    'dateofFinalrefsubmission',
    'pendingcount',
    'approvecount',
    'rejectcount'
  ];
  pageSize = 10;
  pageIndex = 0;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  constructor(
    private candidateReferenceService: CandidateReferenceService,
    public ds: DataService
  ) { }

  ngOnInit() {
    this.getListOfDate(true);
  }

  getListOfDate(isFirstTimeLoad: boolean) {
    let tempSearch = '';
    if (this.search) {
      tempSearch = this.search;
    }
    this.candidateReferenceService
      .getRefernceVerificationLIst(
        isFirstTimeLoad,
        tempSearch,
        this.pageIndex,
        this.pageSize
      )
      .subscribe(
        (success: Pager<RefrenceVerificationVm>) => {
          //console.log(success);
          if (success.data.length === 0 && !isFirstTimeLoad) {
            swal({
              text: 'Queue is empty!',
              type: 'warning',
              confirmButtonText: 'Close'
            });
          }
          this.dataSource = new MatTableDataSource(success.data);
          this.dataSource.sort = this.sort;
          this.length = success.totalItem;
        },
        error => {
          console.log(error);
          swal({
            text: error.error.message,
            type: 'warning',
            confirmButtonText: 'Close'
          });
        }
      );
  }

  filter(value) {
    this.search = value;
    this.getListOfDate(true);
  }
  getNext() {
    //this.pageIndex = 1;
    this.search = '';
    this.getListOfDate(false);
  }
  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
    this.getListOfDate(true);
  }
}
