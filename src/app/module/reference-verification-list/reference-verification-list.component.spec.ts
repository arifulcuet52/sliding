import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenceVerificationListComponent } from './reference-verification-list.component';

describe('ReferenceVerificationListComponent', () => {
  let component: ReferenceVerificationListComponent;
  let fixture: ComponentFixture<ReferenceVerificationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferenceVerificationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenceVerificationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
