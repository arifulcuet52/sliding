import { Component, OnInit, ViewChild } from '@angular/core';
import {
  MatSort,
  MatTableDataSource,
  MatDialog,
  MatDialogConfig
} from '@angular/material';
import { CandidateReferenceService } from 'src/app/shared/service/remote/candidate-reference/candidate-reference.service';
import swal from 'sweetalert2';
import { RefrenceVerificationVm } from 'src/app/entity/refrence.verification.vm';
import { Pager } from 'src/app/entity/pager';
import { DataService } from 'src/app/shared/service/data/data.service';
import { UserService } from '../../shared/service/remote/user/user.service';
import { RoleEnum } from '../../entity/enums/enum';
import { Observable } from 'rxjs';
import { DialogReassignCcAdminComponent } from '../../dialog/dialog-reassign-cc-admin/dialog-reassign-cc-admin.component';

@Component({
  selector: 'app-reference-verification-list',
  templateUrl: './reference-verification-list-by.component.html',
  styleUrls: ['./reference-verification-list-by.component.scss']
})
export class ReferenceVerificationListByComponent implements OnInit {
  search = '';
  dataSource: any;
  @ViewChild(MatSort)
  sort: MatSort;
  displayedColumns: string[] = [
    'candidateId',
    'name',
    'faCode',
    'dateofFinalrefsubmission'
  ];
  public filterStatuses: string[] = ['Pending', 'In-Progress', 'Completed'];
  public filterStatus = '';
  public completeStatus = false;
  public inProgressStatus = false;
  public pendingStatus = false;
  pageSize = 10;
  pageIndex = 0;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  constructor(
    private candidateReferenceService: CandidateReferenceService,
    public ds: DataService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.filterStatus = this.filterStatuses[0];
    this.getListOfData();
  }

  getListOfData() {
    let tempSearch = ' ';
    if (this.search) {
      tempSearch = this.search;
    }
    this.candidateReferenceService
      .getRefVerificListBy(
        this.filterStatus,
        tempSearch,
        this.pageIndex,
        this.pageSize
      )
      .subscribe(
        (success: Pager<RefrenceVerificationVm>) => {
          console.log(success.data);
          /* if (success.data.length === 0) {
            swal({
              text: 'Queue is empty!',
              type: 'warning',
              confirmButtonText: 'Close'
            });
          } */
          this.dataSource = new MatTableDataSource(success.data);
          this.dataSource.sort = this.sort;
          this.length = success.totalItem;
        },
        error => {
          console.log(error);
          swal({
            text: error.error.message,
            type: 'warning',
            confirmButtonText: 'Close'
          });
        }
      );
  }

  filter() {
    this.statusChange();
    this.getListOfData();
  }

  statusChange() {
    this.completeStatus = this.filterStatus === 'Completed';
    this.inProgressStatus = this.filterStatus === 'In-Progress';
    this.pendingStatus = this.filterStatus === 'Pending';

    if (this.completeStatus) {
      this.displayedColumns = [
        'candidateId',
        'name',
        'faCode',
        'dateofFinalrefsubmission',
        'assignedCcAdmin',
        'assignedDate',
        'completedDate'
      ];
    } else if (this.inProgressStatus) {
      this.displayedColumns = [
        'candidateId',
        'name',
        'faCode',
        'dateofFinalrefsubmission',
        'pendingcount',
        'approvecount',
        'rejectcount',
        'assignedCcAdmin',
        'assignedDate'
      ];
    } else if (this.pendingStatus) {
      this.displayedColumns = [
        'candidateId',
        'name',
        'faCode',
        'dateofFinalrefsubmission'
      ];
    }
  }

  reAssign(model, i) {
    console.log(model);
    this.open_dialog(model)
      .afterClosed()
      .subscribe(success => {
        console.log(success);
        if (success) {
          this.getListOfData();
          // this.dataSource[i]=success;
        }
      });
  }

  open_dialog(data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { model: data };
    dialogConfig.width = '400px';
    return this.dialog.open(DialogReassignCcAdminComponent, dialogConfig);
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageIndex = $event.pageIndex;
    this.getListOfData();
  }
}
