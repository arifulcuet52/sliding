import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferenceVerificationListByComponent } from './reference-verification-list-by.component';

describe('ReferenceVerificationListByComponent', () => {
  let component: ReferenceVerificationListByComponent;
  let fixture: ComponentFixture<ReferenceVerificationListByComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferenceVerificationListByComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferenceVerificationListByComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
