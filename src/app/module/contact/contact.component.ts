import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { BsDatepickerConfig, BsModalService } from 'ngx-bootstrap';
import { TrainingCertificate } from 'src/app/entity/exam/training-certificate';
import { FileUploader } from 'ng2-file-upload';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { TrainingCertificateService } from 'src/app/shared/service/remote/exam/training-certificate.service';
import { DataService } from 'src/app/shared/service/data/data.service';
import { AccountService } from 'src/app/shared/service/remote/authentication/account.service';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import { PagingModel } from 'src/app/entity/PagingModel';
import { ContactService } from 'src/app/shared/service/remote/contact/contact.service';
import { toDate } from '@angular/common/src/i18n/format_date';
import { Contact } from 'src/app/entity/contact';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  displayedColumns: string[] = [
    'name',
    'mobile',
    'email',
    'commnet'
  ];
  dataSource: any;
  length: number;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  @ViewChild(MatSort)
  sort: MatSort;
  errorMessage = '';
  successMessage = '';
  searchText = '';
  pageSize = 10;
  pageNumber = 1;

  constructor(public dataService: DataService,
    private contactService: ContactService) { }

  ngOnInit() {
    this.initiation();
    this.reload();
  }


  getAll(searchText: string, pageNumber: number, pageSize: number) {
    this.contactService
      .getAll(searchText, pageNumber, pageSize)
      .subscribe((response: PagingModel<Contact>) => {
        this.dataSource = new MatTableDataSource(response.data);
        this.dataSource.sort = this.sort;
        this.length = response.totalItem;
      }, () => { });
  }

  initiation() {
    this.pageNumber = 1;
    this.searchText = '';
    this.successMessage = '';
    this.errorMessage = '';
  }

  pageChange($event) {
    this.pageSize = $event.pageSize;
    this.pageNumber = $event.pageIndex + 1;
    this.reload();
  }

  filter() {
    this.pageNumber = 1;
    this.reload();
  }

  clear() {
    this.initiation();
    this.reload();
  }

  reload() {
    this.getAll(this.searchText, this.pageNumber, this.pageSize,
    );
  }
}
