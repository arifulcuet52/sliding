import { Component, OnInit, ViewChild } from '@angular/core';
import { ProfileDocumentService } from 'src/app/shared/service/remote/profile-document/profile-document.service';
import { DocumentVerificationListVm } from 'src/app/entity/document.verification.list.vm';
import { MatTableDataSource, MatSort } from '@angular/material';
import { DataService } from 'src/app/shared/service/data/data.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-document-verification-list',
  templateUrl: './document-verification-list.component.html',
  styleUrls: ['./document-verification-list.component.scss']
})
export class DocumentVerificationListComponent implements OnInit {
  search = '';
  dataSource: any;
  @ViewChild(MatSort)
  sort: MatSort;
  displayedColumns: string[] = [
    'warning',
    'candidateId',
    'candidateName',
    'faCode',
    'trainingCompletionDate',
    'trainingExamPassingDate',
    'dateOfFinalDocumentSubmission',
    'verificationPendingCount',
    'approveDocCount',
    'rejectDocCount'
  ];
  constructor(
    private profileDocumentService: ProfileDocumentService,
    public ds: DataService
  ) { }

  ngOnInit() {
    this.getListOfDate(true);
  }
  getListOfDate(isFirstTimeLoad: boolean) {
    let tempSearch = '';
    if (this.search) {
      tempSearch = this.search;
    }
    this.profileDocumentService
      .getDocumentVerificationLIst(tempSearch, isFirstTimeLoad)
      .subscribe(
        (success: DocumentVerificationListVm[]) => {
          // console.log(success);
          if (success.length === 0 && !isFirstTimeLoad) {
            swal({
              text: 'Queue is empty!',
              type: 'warning',
              confirmButtonText: 'Close'
            });
          }

          this.dataSource = new MatTableDataSource(success);
          this.dataSource.sort = this.sort;
          //console.table(success);
        },
        error => {
          console.table(error);
        }
      );
  }
  view(id) { }
  filter(value) {
    this.search = value;
    this.getListOfDate(true);
  }
  getNext() {
    this.getListOfDate(false);
  }
}
