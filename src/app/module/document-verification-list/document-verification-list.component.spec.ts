import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentVerificationListComponent } from './document-verification-list.component';

describe('DocumentVerificationListComponent', () => {
  let component: DocumentVerificationListComponent;
  let fixture: ComponentFixture<DocumentVerificationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentVerificationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentVerificationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
