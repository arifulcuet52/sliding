import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  HostListener,
  OnInit,
  Inject
} from '@angular/core';
import {
  NavigationCancel,
  NavigationEnd,
  NavigationStart,
  Router
} from '@angular/router';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
import * as feather from 'feather-icons';
import * as $ from 'jquery';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subscription, interval } from 'rxjs';

import { PasswordResetAttemptOptsEnum } from './entity/password-reset-attempt-opt-enum';
import { UserMenuVm } from './entity/user-menu';
import { IdelModalComponent } from './module/idel-modal/idel-modal.component';
import { AuthService } from './shared/service/auth/auth.service';
import { LoadingService } from './shared/service/loading/loading.service';
import { AccountService } from './shared/service/remote/authentication/account.service';
import { UserMenuService } from './shared/service/remote/user-menu/user-menu.service';
import { ApplicationUserService } from './shared/service/remote/application-user/application-user.service';
import { DesignationEnum, DesignationDescription } from './entity/enums/enum';
import { UserInfoService } from './shared/service/remote/user-info/user-info.service';
import { UserIconVm } from './entity/user-icon-vm';
import { NotificationService } from './shared/service/remote/notification/notification.service';

export let browserRefresh = false;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewChecked {
  showSpinner = false;
  title = 'app';
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
  isLogIn = false;
  list: UserMenuVm[] = [];
  menuPopulate: UserMenuVm[] = [];
  searchText = '';
  menuSize = 5;
  currentMenuPosition = 0;
  refresh_prepare = 1;
  subscription: Subscription;
  isUserIconExpanded = false;
  userIconVm: UserIconVm = new UserIconVm();
  pendingNotificationCount = 0;
  notificationTimer: any;
  showNotification = false;
  showLoader = false;
  constructor(
    private router: Router,
    private idle: Idle,
    private keepalive: Keepalive,
    public accountService: AccountService,
    private userMenuService: UserMenuService,
    public loadingService: LoadingService,
    private modalService: BsModalService,
    private authService: AuthService,
    private cdr: ChangeDetectorRef,
    private userInfo: UserInfoService,
    private userService: ApplicationUserService,
    private userNotificationService: NotificationService
  ) {

    this.router.events.subscribe((routerEvent: any) => {
      if (routerEvent instanceof NavigationStart) {
        // this.alertService.fnLoading(true);
      }
      if (routerEvent instanceof NavigationEnd) {
        window.scrollTo(0, 0);
        // this.alertService.fnLoading(false);
      }
      if (routerEvent instanceof NavigationCancel) {
        // this.alertService.fnLoading(false);
      }
    });
  }

  setValueFromSession() {
    const sessionValue = sessionStorage.getItem('AD2');
    if (sessionValue) {
      if (!localStorage.getItem('AD')) {
        localStorage.setItem('AD', sessionValue);
      }
      sessionStorage.removeItem('AD2');
      sessionStorage.removeItem('refresh');
    }
    this.accountService.incrementTabCount();
  }

  ngOnInit(): void {
    this.getUserIconVmInfo();
    const access = this;
    this.onload();
    this.beforeUnload();
    feather.replace();
    this.logOutAllTab();
    this.list = [];
    this.list = this.accountService
      .getUserMenu()
      .filter(x => x.portalName === 'Admin' && x.showInMenu); // .slice(1, 4);
    this.getUserMenu();
    this.isLogIn = this.accountService.isLogIn();
    if (this.isLogIn) {
      this.change_header();
      this.idleMetod();
    }

    this.accountService.logInEmitter.subscribe(res => {
      this.isLogIn = res.isLogedin;
      if (this.isLogIn) {
        this.change_header();
        this.idleMetod();
        this.getUserIconVmInfo();
        this.getUserAllPendingNotificationCount();
        this.timeForGetNotficationCount();
      }
      if (!this.isLogIn) {
        this.idle.stop();
        this.keepalive.stop();
      }
    });

    this.userNotificationService.clickORScrollInformEmitter.subscribe(res => {
      console.log('emiter');
      this.getUserAllPendingNotificationCount();
    });
    // $('[data-toggle="tooltip"]').tooltip();

    // loading spinner
    // this.loadingService.loadingEventEmiter.subscribe(x => {
    //   this.showSpinner = x;
    // });
  }

  timeForGetNotficationCount() {
    this.notificationTimer = interval(20000).subscribe(val => {
      if (this.isLogIn) {
        this.getUserAllPendingNotificationCount();
      }
    });
  }

  getUserAllPendingNotificationCount() {
    if (this.isLogIn) {
      // console.log("getUserAllPendingNotificationCount");
      this.userNotificationService
        .getUserAllPendingNotification()
        .subscribe(success => {
          console.log(success);
          this.pendingNotificationCount = +success;
        });
    }
  }

  notificationClick() {
    this.isUserIconExpanded = false;
    this.showNotification = !this.showNotification;
  }

  closeNotification() {
    this.showNotification = false;
  }
  /* notify() {
    const con = (<any>window).$;
    con.signalR.ajaxDefaults.headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.accService.getToken()
    };
    const options: IConnectionOptions = {
      qs: { Authorization: "Bearer " + this.accService.getToken() }
    };
    this.connection = this.signalR.createConnection(options);
    this.connection.start();
  
    const listener = new BroadcastEventListener<String>("Send");
    this.connection.listen(listener);
    listener.subscribe(x => {
      console.log(x);
    });
  } */

  getUserIconVmInfo() {
    this.userService.getUserProfileView().subscribe(
      success => {
        if (success.data) {
          this.userIconVm = success.data;
          if (this.userIconVm.designation === DesignationEnum.bm) {
            this.userIconVm.designation = DesignationDescription.bm;
          } else if (this.userIconVm.designation === DesignationEnum.um) {
            this.userIconVm.designation = DesignationDescription.um;
          } else if (this.userIconVm.designation === DesignationEnum.ae) {
            this.userIconVm.designation = DesignationDescription.ae;
          } else if (this.userIconVm.designation === DesignationEnum.fa) {
            this.userIconVm.designation = DesignationDescription.fa;
          }
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  redirectToProfile() {
    const test = this.accountService.getUserRoles();
    console.log(test);

    if (
      (this.userIconVm.designation === DesignationDescription.bm ||
        this.userIconVm.designation === DesignationDescription.um) &&
      this.userIconVm.agentId
    ) {
      this.router.navigate(['/ap/ProfileView/', this.userIconVm.agentId]);
    }
    /* this.userService.getUserProfileView().subscribe(success => {
      if (success.data) {
        console.log(success.data);
  
        if (success.data.designation === DesignationEnum.bm || success.data.designation === DesignationEnum.um) {
          this.router.navigate(['/ap/ProfileView/', success.data.agentId]);
        }
      }
  
    },
      (error => {
        console.log(error);
      })); */
  }

  toggleUserIcon() {
    this.showNotification = false;
    this.isUserIconExpanded = !this.isUserIconExpanded;
  }

  closeUserIcon() {
    this.isUserIconExpanded = false;
  }

  reload() {
    this.router.navigate([this.accountService.getDashboardPath()]);
  }
  changePassword() {
    this.accountService.passwordChangeReason(
      PasswordResetAttemptOptsEnum.ChangePassword
    );
    this.router.navigate(['/ap/change-password']);
  }

  idleMetod() {
    // 60 * 60 * 10
    this.idle.setIdle(this.accountService.getIdleTime() * 60);
    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    this.idle.setTimeout(1);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => (this.idleState = 'No longer idle.'));
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      this.modalService.show(IdelModalComponent, {
        initialState: {
          msg: 'Idle time expired.'
        }
      });
      this.logout('Idle User');
    });
    this.idle.onIdleStart.subscribe(
      () => (this.idleState = 'You\'ve gone idle!')
    );
    this.idle.onTimeoutWarning.subscribe(
      countdown =>
        (this.idleState = 'You will time out in ' + countdown + ' seconds!')
    );

    // sets the ping interval to 15 seconds
    this.keepalive.interval(1);
    this.keepalive.onPing.subscribe(() => {
      const token = JSON.parse(localStorage.getItem('AD'));
      if (
        token &&
        new Date(new Date().toUTCString()) > new Date(token.expires)
      ) {
        // this.modalService.show(IdelModalComponent, {
        //   initialState: {
        //     msg: 'Session expired'
        //   }
        // });
        // this.logout('Session expired');
      } else {
      }
    });
    this.reset();
  }

  logOutAllTab() {
    window.addEventListener(
      'storage',
      event => {
        if (event.storageArea === localStorage) {
          const token = localStorage.getItem('AD');
          const tab = localStorage.getItem('tab');

          if (!token && +tab > 1) {
            // you can update this as per your key
            // DO LOGOUT FROM THIS TAB AS WELL
            this.idle.stop();
            this.keepalive.stop();
            this.list = [];
            this.isLogIn = false;
            this.change_header();
            this.currentMenuPosition = 0;
            this.router.navigate(['/ap/signin']); // If you are using router
            // OR
            // window.location.href = '<home page URL>';
          }
        }
      },
      false
    );
  }
  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  login() {
    this.router.navigate(['/ap/signin']);
  }

  search() {
    this.router.navigate(['/ap/search', this.searchText]);
    this.searchText = '';
  }
  change_header() {
    if (this.isLogIn) {
      $('body').removeClass('layout__1');
      $('body').addClass('layout__2');
    } else {
      $('body').removeClass('layout__2');
      $('body').addClass('layout__1');
    }
  }
  focusSearch() {
    $('.home-search').removeClass('d-none');
    $('.home-search').addClass('home-search--200');
  }
  focusOutSearch() {
    console.log('aaaa');
    $('.home-search').removeClass('home-search--200');
    $('.home-search').addClass('d-none');
  }

  logout(logOutType: string) {
    this.accountService.logout(logOutType);
    this.idle.stop();
    this.keepalive.stop();
    this.list = [];
    this.router.navigate(['/ap/signin']);
    this.change_header();
    this.currentMenuPosition = 0;
  }
  signup() {
    this.router.navigate(['/ap/candidate-registration']);
  }

  get_login_status() {
    return localStorage.getItem('AD') === null ? false : true;
  }
  getUserMenu() {
    this.accountService.menuEmitter.subscribe((success: UserMenuVm[]) => {
      this.list = success.filter(x => x.portalName === 'Admin' && x.showInMenu);
    });
  }

  onload() {
    if (sessionStorage.getItem('AD') && sessionStorage.getItem('AD') != 'null') {
      localStorage.setItem('AD', sessionStorage.getItem('AD'));
    }
    this.incTabCount();
    sessionStorage.setItem('process', 'false');
  }
  incTabCount() {
    let tab = localStorage.getItem('tab');
    if (tab) {
      tab = (+tab + 1).toString();
    } else {
      tab = '1';
    }
    localStorage.setItem('tab', tab);
  }
  beforeUnload() {
    const context = this;
    window.onbeforeunload = function (e) {
      window.onunload = function () {
        let tab = localStorage.getItem('tab');
        const isProcess = sessionStorage.getItem('process');
        if (tab) {
          if (!isProcess) {
            sessionStorage.setItem('process', 'true');
          } else {
            console.log(isProcess);
            if (isProcess == 'true') {
              // return undefined;
            } else {

            }
          }
          if ((+tab) != 0) {
            if (!isProcess || isProcess !== 'true') {

              sessionStorage.setItem('AD', localStorage.getItem('AD'));
              tab = ((+tab) - 1).toString();
              if (+tab === 0) {
                context.logoutLog();
                localStorage.removeItem('AD');
              }
              sessionStorage.setItem('process', 'true');
            }
          }

        } else {
          tab = '0';
        }
        if (tab == '0') {
          localStorage.removeItem('tab');
        } else {
          localStorage.setItem('tab', tab);
        }
      };
      return undefined;
    };
  }
  logoutLog() {
    if (this.isLogIn) {
      const email = this.accountService.getUserEmail();
      // localStorage.removeItem('AD');
      $.ajax({
        async: false,
        type: 'POST',
        url:
          this.authService.APPSERVER +
          'Account/Logout?userId=' +
          email +
          '&logoutMethod=Browser Closed',
        dataType: 'json',
        timeout: 60000,
        success: function (response) {
          const r = response;
        },
        error: function (jq, status, message) {
          const e = message;
        }
      });
    }
  }


  // @HostListener('window:unload', ['$event'])
  // unloadHandler(event) {
  //   if (!sessionStorage.getItem('aaaa')) {
  //     if (this.isLogIn) {
  //       const email = this.accountService.getUserEmail();
  //       // localStorage.removeItem('AD');
  //       $.ajax({
  //         async: false,
  //         type: 'POST',
  //         url:
  //           this.authService.APPSERVER +
  //           'Account/Logout?userId=' +
  //           email +
  //           '&logoutMethod=Browser Closed',
  //         dataType: 'json',
  //         timeout: 60000,
  //         success: function (response) {
  //           const r = response;
  //         },
  //         error: function (jq, status, message) {
  //           const e = message;
  //         }
  //       });
  //     }
  //   }
  // }
  onClick() {
    $('a').mousedown(function (event) {
      switch (event.which) {
        case 1:
          alert('Left mouse button pressed');
          // $(this).attr('target','_self');
          break;
        case 2:
          alert('Middle mouse button pressed');
          // $(this).attr('target','_blank');
          break;
        case 3:
          alert('Right mouse button pressed');
          // $(this).attr('target','_blank');
          break;
        default:
          alert('You have a strange mouse');
        // $(this).attr('target','_self"');
      }
    });
  }
  // beforeunload() {
  //   const tabCount = this.accountService.tabCount();
  //   const refresh = sessionStorage.getItem('refresh');
  //   if (!refresh) {
  //     this.accountService.decremntTabCount();
  //   }
  //   const local = localStorage.getItem('AD');

  //   if (tabCount <= 1 && !refresh) {
  //     if (local) {
  //       sessionStorage.setItem('refresh', 'true');
  //       sessionStorage.setItem('AD2', local);
  //       localStorage.removeItem('AD');
  //     }
  //   }
  // }

  // @HostListener('window:beforeunload', ['$event'])
  // beforeUnloadHander($event) {
  //   // if (performance.navigation.type == 1) {
  //   //   console.log('close');
  //   // } else {
  //   //   console.log('refresh');
  //   // }
  //   const tabCount = this.accountService.tabCount();
  //   this.accountService.decremntTabCount();
  //   if (tabCount <= 1) {
  //     const local = localStorage.getItem('AD');
  //     sessionStorage.setItem('temp', local);
  //     localStorage.removeItem('AD');
  //   }

  //   // if (window.opener.title == undefined) {
  //   //   localStorage.setItem('item', 'refresh');
  //   //   //  document.write('Window was refreshed!');
  //   // } else {
  //   //   localStorage.setItem('item', 'close');
  //   // }
  //   // console.log(window.event,'window.event');
  //   // const e = $event || window.event;
  //   // const y = e.pageY || e.clientY;

  //   // if (y < 0) {
  //   //   console.log('Do You really Want to Close the window ?');
  //   // } else {
  //   //   console.log('Refreshing this page can result in data loss.');
  //   // }
  //   // if (this.isLogIn) {
  //   //   // this.checkRefresh();
  //   //   $event.preventDefault();
  //   //   $event.returnValue = 'Are you sure?';
  //   //   console.log($event);
  //   //  // return false;
  //   // }

  //   // this.prepareForRefresh();
  // }
  // @HostListener('window:onLoad', ['$event'])
  // onloadHander($event) {
  //   const sess = sessionStorage.getItem('temp');
  //   if (sess) {
  //     localStorage.setItem('AD', sess);
  //     //   sessionStorage.removeItem('temp');
  //     //console.log(sessionStorage.getItem('time'));
  //   }
  // }
  // checkRefresh() {
  //   // Get the time now and convert to UTC seconds
  //   const today = new Date();
  //   const now = today.getUTCSeconds();
  //   let cookieTime;
  //   let cookieName;
  //   // Get the cookie
  //   const cookie = document.cookie;
  //   const cookieArray = cookie.split('; ');

  //   // Parse the cookies: get the stored time
  //   for (let loop = 0; loop < cookieArray.length; loop++) {
  //     const nameValue = cookieArray[loop].split('=');
  //     // Get the cookie time stamp
  //     if (nameValue[0].toString() == 'SHTS') {
  //       cookieTime = parseInt(nameValue[1]);
  //     }
  //     // Get the cookie page
  //     else if (nameValue[0].toString() == 'SHTSP') {
  //       cookieName = nameValue[1];
  //     }
  //   }
  //   console.log(cookieName, 'cookieName');
  //   console.log(cookieTime, 'cookieTime');
  //   console.log(now, 'now');
  //   if (
  //     cookieName &&
  //     cookieTime &&
  //     cookieName == escape(location.href) &&
  //     Math.abs(now - cookieTime) < 5
  //   ) {
  //     console.log('true');
  //     this.refresh_prepare = 0;
  //     return true;
  //     //  alert('refresh detect');
  //     // Refresh detected
  //     // Insert code here representing what to do on
  //     // a refresh
  //     // If you would like to toggle so this refresh code
  //     // is executed on every OTHER refresh, then
  //     // uncomment the following line
  //   } else {
  //     console.log('false');
  //     return false;
  //   }

  //   // You may want to add code in an else here special
  //   // for fresh page loads
  // }
  prepareForRefresh() {
    if (this.refresh_prepare > 0) {
      // Turn refresh detection on so that if this
      // page gets quickly loaded, we know it's a refresh
      const today = new Date();
      const now = today.getUTCSeconds();
      document.cookie = 'SHTS=' + now + ';';
      document.cookie = 'SHTSP=' + escape(location.href) + ';';
    } else {
      // Refresh detection has been disabled
      document.cookie = 'SHTS=;';
      document.cookie = 'SHTSP=;';
    }
  }
  // @HostListener('mousemove', ['$event'])
  // onMousemove(event: MouseEvent) {
  //   // console.log(event.clientY, 'pageY');
  //   // console.log(event.pageX, 'pageX');
  // }
  // @HostListener('mouseout', ['$event'])
  // onMouseout() {
  //   // console.log('mouse out');
  //   // console.log(event.pageX, 'pageX');
  // }
  // @HostListener('document:keydown', ['$event'])
  // handleKeyboardEvent(event: KeyboardEvent) {
  //   // console.log(event.key);
  // }

  ngAfterViewChecked(): void {
    const show = this.loadingService.loader();
    if (show !== this.showLoader) {
      // check if it change, tell CD update view
      this.showLoader = show;
      this.cdr.detectChanges();
    }
  }
}
