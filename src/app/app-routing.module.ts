import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserMenuCanActivateRouteGuardService } from './shared/route-gurd/user-menu-can-activate-route-guard.service';

import { HomeComponent } from './module/home/home.component';
import { MenuComponent } from './module/authentication/menu/menu.component';
import { SigninComponent } from './module/authentication/signin/signin.component';
import { RoleComponent } from './module/authentication/role/role.component';
import { RolePermissionComponent } from './module/authentication/role-permission/role-permission.component';
import { UserPermissionComponent } from './module/authentication/user-permission/user-permission.component';
import { AdminDashboardComponent } from './module/admin-dashboard/admin-dashboard.component';
import { UserCreateComponent } from './module/user-create/user-create.component';
import { UserListComponent } from './module/user-list/user-list.component';
import { SecurityPoliciesComponent } from './module/security-policy/security-policies/security-policies.component';
import { SecurityPolicyCreateComponent } from './module/security-policy/security-policy-create/security-policy-create.component';
import { UnauthorizedComponent } from './module/unauthorized/unauthorized.component';
import { SearchComponent } from './module/search/search.component';
import { NotFoundComponent } from './module/not-found/not-found.component';
import { ChangePasswordComponent } from './module/change-password/change-password.component';
import { GlobalSettingsComponent } from './module/global-settings/global-settings.component';
import { AccountRecoveryComponent } from './module/account-recovery/account-recovery.component';
import { LoadingTestComponent } from './module/loading-test/loading-test.component';
import { DistrictComponent } from './module/district/district.component';
import { AreaComponent } from './module/area/area.component';
import { EducationEligibilityComponent } from './module/eligibility-criteria-configuration/education-eligibility/education-eligibility.component';
import { AgeEligibilityComponent } from './module/eligibility-criteria-configuration/age-eligibility/age-eligibility.component';
import { EligibilityCriteriaComponent } from './module/eligibility-criteria/eligibility-criteria.component';
import { MaritalStatusEligibilityComponent } from './module/eligibility-criteria-configuration/marital-status-eligibility/marital-status-eligibility.component';
import { JobExperienceEligibilityComponent } from './module/eligibility-criteria-configuration/job-experience-eligibility/job-experience-eligibility.component';
import { UmBmCreateComponent } from './module/um-bm-user/um-bm-create/um-bm-create.component';
import { AccountActivationComponent } from './module/authentication/account-activation/account-activation.component';
import { ProfileviewComponent } from './module/profileview/profileview.component';
import { CandidateprofileviewComponent } from './module/candidateprofileview/candidateprofileview.component';
import { UmBmListComponent } from './module/um-bm-user/um-bm-list/um-bm-list.component';
import { CandidateTagRequestComponent } from './module/tagging/candidate-tag-request/candidate-tag-request.component';
import { QuestionListComponent } from './module/exam/question/question-list/question-list.component';
import { ExamSettingListComponent } from './module/exam/exam-setting/exam-setting-list/exam-setting-list.component';
import { QuestionPerModuleListComponent } from './module/exam/question-per-module/question-per-module-list/question-per-module-list.component';
import { ExamScheduleComponent } from './module/exam/exam-schedule/exam-schedule.component';
import { CertificateListComponent } from './module/certificate-list/certificate-list.component';
import { CertificateFeeListComponent } from './module/certificate/certificate-fee-list/certificate-fee-list.component';
import { UntaggedleadListComponent } from './module/lead/untaggedlead-list/untaggedlead-list.component';
import { TaggedleadListComponent } from './module/lead/taggedlead-list/taggedlead-list.component';
import { ExamBookingComponent } from './module/exam/exam-booking/exam-booking.component';
import { TrainingCertificateComponent } from './module/training-certificate/training-certificate.component';
import { InternalUserCreationComponent } from './module/internal-user-creation/internal-user-creation.component';
import { TrainingModuleComponent } from './module/training-module/training-module.component';
import { DocumentVerificationListComponent } from './module/document-verification-list/document-verification-list.component';
import { DocumentVerificationByComponent } from './module/document-verification/document-verification-by/document-verification-by.component';
import { PolicyVerificationComponent } from './module/policy-verification/policy-verification.component';
import { CandidateProfileComponent } from './module/candidate-profile/profile/candidate-profile.component';
import { TimelineComponent } from './module/timeline/timeline.component';
import { SalesPolicyFileUploadComponent } from './module/sales-policy-file-upload/sales-policy-file-upload.component';
import { ReferenceVerificationListComponent } from './module/reference-verification-list/reference-verification-list.component';
import { ReferenceVerificationListByComponent } from './module/reference-verification-list-by/reference-verification-list-by.component';
import { VivaResultComponent } from './module/viva-result/viva-result.component';
import { DashboardComponent } from './module/dashboard/dashboard.component';
import { CampaignCreationComponent } from './module/campaign/campaign-creation/campaign-creation.component';
import { CandidatesEligibleListComponent } from './module/candidates-eligible-list/candidates-eligible-list.component';
import { TrainingCertificateListViewComponent } from './module/training-certificate-list-view/training-certificate-list-view.component';
import { EventCalenderComponent } from './module/event-calender/event-calender.component';
import { FaqComponent } from './module/faq/faq.component';
import { AuthComponent } from './auth/auth.component';
import { ContactComponent } from './module/contact/contact.component';
import { CampaignListComponent } from './module/campaign/campaign-list/campaign-list.component';
import { UmbmDashboardComponent } from './module/umbm-dashboard/umbm-dashboard.component';
import { LinkedTrainingModuleComponent } from './module/linked-training-module/linked-training-module.component';
import { LocationStrategy, PathLocationStrategy, HashLocationStrategy, APP_BASE_HREF } from '@angular/common';
import { getBaseLocation } from './shared/common-functions.util';

const routes: Routes = [
  { path: '', redirectTo: '/ap/home', pathMatch: 'full' },
  { path: 'ap/home', component: HomeComponent },
  {
    path: 'ap/menu',
    component: MenuComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/signin',
    component: SigninComponent
  },
  {
    path: 'ap/role',
    component: RoleComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/role-permission',
    component: RolePermissionComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/user-permission',
    component: UserPermissionComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/admin-dashboard',
    component: AdminDashboardComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/user-create',
    component: UserCreateComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/user-create/:id',
    component: UserCreateComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/user-list',
    component: UserListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/security-policy',
    component: SecurityPoliciesComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/security-policy-create',
    component: SecurityPolicyCreateComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/security-policy-create/:id',
    component: SecurityPolicyCreateComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/user-create',
    component: UserCreateComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/user-create/:id',
    component: UserCreateComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/user-list',
    component: UserListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  { path: 'ap/unauthorized', component: UnauthorizedComponent },
  { path: 'ap/search', component: SearchComponent },
  { path: 'ap/search/:text', component: SearchComponent },
  { path: 'ap/404', component: NotFoundComponent },
  { path: 'ap/change-password', component: ChangePasswordComponent },
  { path: 'ap/forget-password', component: ChangePasswordComponent },
  { path: 'ap/forget-password/:id', component: ChangePasswordComponent },
  {
    path: 'ap/global-settings',
    component: GlobalSettingsComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/unlock-account',
    component: ChangePasswordComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  { path: 'ap/account-recovery', component: AccountRecoveryComponent },
  { path: 'ap/loading', component: LoadingTestComponent },
  {
    path: 'ap/district',
    component: DistrictComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/area',
    component: AreaComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/education-eligibility',
    component: EducationEligibilityComponent
  },
  { path: 'ap/age-eligibility', component: AgeEligibilityComponent },
  { path: 'ap/eligibility-criteria', component: EligibilityCriteriaComponent },
  {
    path: 'ap/marital-eligibility',
    component: MaritalStatusEligibilityComponent
  },
  { path: 'ap/job-eligibility', component: JobExperienceEligibilityComponent },
  {
    path: 'ap/admin-profile-creation',
    component: UmBmCreateComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  { path: 'account/activation', component: AccountActivationComponent },
  {
    path: 'multilingual',
    loadChildren:
      '../app/module/multilingual/multilingual.module#MultilingualModule'
  },
  {
    path: 'ap/ProfileView/:id',
    component: ProfileviewComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/candidateprofileview',
    component: CandidateprofileviewComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/candidateprofileview/:id/:tagId',
    component: CandidateprofileviewComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/admin-profile-creation',
    component: UmBmCreateComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/admin-profile-creation/:id',
    component: UmBmCreateComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  { path: 'ap/um-bm-list', component: UmBmListComponent },
  {
    path: 'ap/tagging/candidate-tag-request',
    component: CandidateTagRequestComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/question/bank',
    component: QuestionListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/exam/setting',
    component: ExamSettingListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/exam/question-per-module',
    component: QuestionPerModuleListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/exam-schedule',
    component: ExamScheduleComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/certificate-list',
    component: CertificateListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/certificate-fee-list',
    component: CertificateFeeListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/lead/untaggedlead-list',
    component: UntaggedleadListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/lead/taggedlead-list',
    component: TaggedleadListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/exam-booking-list',
    component: ExamBookingComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },

  {
    path: 'ap/training/certificate',
    component: TrainingCertificateComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/internal-user-creation',
    component: InternalUserCreationComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  { path: 'ap/training-module', component: TrainingModuleComponent },
  {
    path: 'training-content',
    loadChildren:
      '../app/module/training-content/training.module#TrainingModule'
  },
  {
    path: 'ap/document-verification-list',
    component: DocumentVerificationListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/document-verification-by',
    component: DocumentVerificationByComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/policy-verification',
    component: PolicyVerificationComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/candidate-profile/:id',
    component: CandidateProfileComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/timeline/:id',
    component: TimelineComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/sales/policy',
    component: SalesPolicyFileUploadComponent
  },
  {
    path: 'ap/reference-verification-list',
    component: ReferenceVerificationListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/reference-verification-list-by',
    component: ReferenceVerificationListByComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/viva-result',
    component: VivaResultComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/dashboard',
    component: DashboardComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/campaign',
    component: CampaignCreationComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/campaign-list',
    component: CampaignListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/candidates-eligible-list',
    component: CandidatesEligibleListComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/training/certificate/list/view',
    component: TrainingCertificateListViewComponent
  },
  {
    path: 'ap/event/calender',
    component: EventCalenderComponent
  },
  {
    path: 'ap/contact',
    component: ContactComponent,
    canActivate: [UserMenuCanActivateRouteGuardService]
  },
  {
    path: 'ap/faq',
    component: FaqComponent
  },
  {
    path: 'report',
    loadChildren:
      '../app/module/dynamic-report/dynamic-report.module#DynamicReportModule'
  },
  {
    path: 'ap/um-bm-dashboard',
    component: UmbmDashboardComponent
  },

  {
    path: 'um-bm-legend-details',
    loadChildren:
      '../app/module/dynamic-grid/dynamic-grid.module#DynamicGridModule'
  },
  {
    path: 'link-training-module',
    component: LinkedTrainingModuleComponent
  },
  { path: 'ap/auth', component: AuthComponent },
  { path: '**', redirectTo: 'ap/404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
  declarations: [],
  // providers: [
  //   { provide: LocationStrategy, useClass: HashLocationStrategy }
  // ]
})
export class AppRoutingModule { }
