import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, mixinDisabled } from '@angular/material';
import { ExamScheduleService } from '../../../shared/service/remote/exam/exam-schedule/exam-schedule.service';
import { ExamSchedule } from '../../../entity/exam-schedule';
import swal from 'sweetalert2';
import * as moment from 'moment';
import { District } from '../../../entity/district';
import { Area } from '../../../entity/area';
import { DistrictService } from '../../../shared/service/remote/district/district.service';
import { AreaService } from '../../../shared/service/remote/area/area.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-dialog-exam-schedule',
  templateUrl: './dialog-exam-schedule.component.html',
  styleUrls: ['./dialog-exam-schedule.component.css']
})
export class DialogExamScheduleComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogExamScheduleComponent>,
    private examScheduleService: ExamScheduleService,
    private districtService: DistrictService,
    private areaService: AreaService
  ) { }

  public model: ExamSchedule = new ExamSchedule('');
  public swal: any;
  public currentDate: Date = new Date();
  public duration: any;
  public durationString = '00:00';
  public districts: District[] = [];
  public areas: Area[] = [];
  public datePickerConfig: Partial<BsDatepickerConfig>;
  public invalidTodaysTime = false;
  public currentAvailableSeats = 0;
  public currentSeatingCapacity = 0;
  public buttonClicked = false;

  ngOnInit() {
    this.model = this.data.model;

    if (this.model.availableSeats != null) this.currentAvailableSeats = this.model.availableSeats;
    if (this.model.venueSeatingCapacity != null) this.currentSeatingCapacity = this.model.venueSeatingCapacity;

    this.datePickerConfig = Object.assign({}, { dateInputFormat: 'DD/MM/YYYY', showWeekNumbers: false });
    this.getArea(this.model.districtId);
    this.getDistrict();
  }

  getDistrict() {
    this.districtService.getAll().subscribe(
      (success: District[]) => {
        this.districts = success;
      },
      (error) => { console.log(error); }
    );
  }

  getArea(id) {
    this.areaService.getAllByDistrict(id).subscribe(
      (success: Area[]) => {
        this.areas = success;
      },
      (error) => { console.log(error); }
    );
  }

  close() {
    console.log(this.model);
    this.dialogRef.close();
  }

  getDuration() {

    if (this.model.startTime != null && this.model.endTime != null) {
      /* var today = new Date();
      const st = new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.model.startTime.getHours(), this.model.startTime.getMinutes());
      const et = new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.model.endTime.getHours(), this.model.endTime.getMinutes());
      if ((et <= st)) {
        this.durationString = '00:00';
      } */
      if (this.model.endTime <= this.model.startTime) {
        this.durationString = '00:00';
      }
      else {
        const start = moment(this.model.startTime);
        const end = moment(this.model.endTime);
        this.duration = Math.round(moment.duration(end.diff(start)).asMinutes());

        const hh = Math.trunc(this.duration / 60);
        const mm = this.duration % 60;
        this.durationString = hh + ':' + mm;

        this.model.examDuration = this.duration;
      }

    }

    this.onDateChange();
  }

  timeCheck() {
    var today = new Date();
    if (this.model.startTime != null && this.model.endTime != null) {
      /* const et = new Date(this.model.endTime);
    const st = new Date(this.model.startTime); */

      const st = new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.model.startTime.getHours(), this.model.startTime.getMinutes());
      const et = new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.model.endTime.getHours(), this.model.endTime.getMinutes());

      // debugger;
      /* console.log(et < st);
     console.log(et.toTimeString() == st.toTimeString());
     console.log(et.toTimeString(), st.toTimeString()); */

      return ((et < st) || (et.toTimeString() == st.toTimeString())) ? true : false;
    }
    else {
      return false;
    }

  }

  onDateChange() {
    this.invalidTodaysTime = false;
    if (this.model.examDate != null && this.model.startTime != null && this.model.endTime != null) {
      var today = new Date();

      var todayOnlyDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
      var xmDate = new Date(this.model.examDate.getFullYear(), this.model.examDate.getMonth(), this.model.examDate.getDate());

      if (xmDate.toDateString() == todayOnlyDate.toDateString()) {
        var st = new Date(today.getFullYear(), today.getMonth(), today.getDate(), this.model.startTime.getHours(), this.model.startTime.getMinutes());
        var todayWithTime = new Date(today.getFullYear(), today.getMonth(), today.getDate(), today.getHours(), today.getMinutes());
        
        if (todayWithTime > st || (todayWithTime.toTimeString() == st.toTimeString())) {
          this.invalidTodaysTime = true;
        }
      }
    }
  }

  checkTodaysTime() {
    return this.invalidTodaysTime;
  }

  save() {
    this.buttonClicked = true;

    // calculate exam duration
    // this.model.examDate.setHours(0, 0, 0, 0);
    this.getDuration();

    if (this.model.id <= 0) {
      this.examScheduleService.save(this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          this.dialogRef.close(error);
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        },
        () => { this.buttonClicked = false; }
      );
    } else {
      if (this.model.areaId === 0) {
        this.model.areaId = null;
      }
      this.examScheduleService.edit(this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          this.dialogRef.close(error);
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        },
        () => { this.buttonClicked = false; }
      );
    }
  }

  changeSeatingCapacity() {
    let diff: number;
    diff = this.currentSeatingCapacity - this.currentAvailableSeats;
    this.model.availableSeats = this.model.venueSeatingCapacity - diff;
  }

  changeTimepicker() {
    if (this.model.startTime != null && this.model.endTime != null) {
      if (this.model.startTime > this.model.endTime) {
        this.model.startTime = null;
        this.model.endTime = null;
      }
    }
  }

  isDistrictSelected() {
    if (this.model.districtId === 0) {
      return true;
    }
    else {
      return false;
    }
  }
}
