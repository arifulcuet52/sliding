import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogExamScheduleComponent } from './dialog-exam-schedule.component';

describe('DialogExamScheduleComponent', () => {
  let component: DialogExamScheduleComponent;
  let fixture: ComponentFixture<DialogExamScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogExamScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogExamScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
