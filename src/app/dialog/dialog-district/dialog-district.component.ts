import { Component, OnInit, Inject } from '@angular/core';
import { District } from '../../entity/district';
import { CountryInfo } from '../../entity/country';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CountryService } from '../../shared/service/remote/country/country.service';
import { DistrictService } from '../../shared/service/remote/district/district.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-dialog-district',
  templateUrl: './dialog-district.component.html',
  styleUrls: ['./dialog-district.component.css']
})
export class DialogDistrictComponent implements OnInit {

  swal: any;
  model: District;
  countries: CountryInfo[] = [];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogDistrictComponent>,
    private countryService: CountryService,
    private districtService: DistrictService,
  ) { }

  ngOnInit() {
    this.model = this.data.model;
    this.getCountries();
  }
  getCountries() {
    this.countryService.getAll().subscribe(
      (success) => {
        this.countries = success['data'] as CountryInfo[];
      }
    );
  }
  close() {
    this.dialogRef.close();
  }
  save() {
    if (this.model.districtId > 0) {
      this.districtService.update(this.model).subscribe(
        (success) => {
          this.dialogRef.close(success);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Warning!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'warning',
            confirmButtonText: 'Close'
          });
          // this.errorMessage = JSON.parse(JSON.stringify(error)).error;
        }
      );
    } else {
      this.districtService.add(this.model).subscribe(
        (success) => {
          this.dialogRef.close(success);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Warning!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'warning',
            confirmButtonText: 'Close'
          });
          // this.errorMessage = JSON.parse(JSON.stringify(error)).error;
        }
      );
    }
  }
}
