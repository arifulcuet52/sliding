import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDistrictComponent } from './dialog-district.component';

describe('DialogDistrictComponent', () => {
  let component: DialogDistrictComponent;
  let fixture: ComponentFixture<DialogDistrictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogDistrictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogDistrictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
