import { EventCalenderService } from './../../shared/service/remote/event-calender/event-calender.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EventCalender } from 'src/app/entity/event-calender';
import { District } from 'src/app/entity/district';
import { Area } from 'src/app/entity/area';
import { DistrictService } from 'src/app/shared/service/remote/district/district.service';
import { AreaService } from 'src/app/shared/service/remote/area/area.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import swal from 'sweetalert2';
import * as moment from 'moment';
import { StatusService } from 'src/app/shared/service/remote/status/status.service';
import { UtilityService } from 'src/app/shared/service/utility/utility.service';
import { UrlSettingService } from 'src/app/shared/service/remote/url-setting/url-setting.service';
import { UrlSettingEnum } from 'src/app/entity/enums/enum';
@Component({
  selector: 'app-dialog-event-calender',
  templateUrl: './dialog-event-calender.component.html',
  styleUrls: ['./dialog-event-calender.component.scss']
})
export class DialogEventCalenderComponent implements OnInit {
  model: EventCalender;
  districts: District[] = [];
  areas: Area[] = [];
  status: any = [];
  datePickerConfig: Partial<BsDatepickerConfig>;
  fileType = 'png,jpg';
  fileObject: File = null;
  fileName: string;
  fileSetting: any;
  formData: FormData = new FormData();
  submitBtnText = 'Save';
  urlPrefix = '';
  diff: number;
  minDate: Date;
  dateOk = true;
  constructor(
    private dialogRef: MatDialogRef<DialogEventCalenderComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private districtService: DistrictService,
    private areaService: AreaService,
    private eventCalenderService: EventCalenderService,
    private statusService: StatusService,
    private utilityService: UtilityService,
    private urlSetting: UrlSettingService
  ) {}
  modelChange() {
    if (
      this.model.startDate &&
      this.model.startTime &&
      this.model.endDate &&
      this.model.endTime
    ) {
      const startTime = new Date(this.model.startTime.toString());
      const endTime = new Date(this.model.endTime.toString());
      const statrHour = startTime.getHours();
      const statrMin = startTime.getMinutes();
      const statrSec = startTime.getSeconds();

      const endHour = endTime.getHours();
      const endMin = endTime.getMinutes();
      const endSec = endTime.getSeconds();

      const startDateTime = new Date(
        this.model.startDate.toDateString() +
          ' ' +
          statrHour +
          ':' +
          statrMin +
          ':' +
          statrSec
      );
      const endDateTime = new Date(
        this.model.endDate.toDateString() +
          ' ' +
          endHour +
          ':' +
          endMin +
          ':' +
          endSec
      );

      console.log(startDateTime);
      console.log(endDateTime);

      if (endDateTime < startDateTime) {
        this.dateOk = false;
      } else {
        this.dateOk = true;
      }
    }
  }
  ngOnInit() {
    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate());
    this.model = this.data.model;
    this.diff = this.model.capacity - this.model.availableCapacity;
    if (this.model.eventImageUrl != null) {
      const path = this.model.eventImageUrl.split('/');
      this.fileName = path[path.length - 1];
      this.submitBtnText = 'Update';
    }
    console.log(this.model);
    this.datePickerConfig = Object.assign(
      {},
      { dateInputFormat: 'DD/MM/YYYY', showWeekNumbers: false }
    );
    setTimeout(() => {
      this.getDistrict();
      this.getArea(this.model.districtId);
      this.getStatus();
      this.getFileSetting();
      this.getUrlPrefix();
    });

    this.modelChange();
  }

  getUrlPrefix() {
    this.urlSetting.getByCode(UrlSettingEnum.eventUrlPrefix).subscribe(
      success => {
        this.urlPrefix = success.data.url;
        console.log(this.urlPrefix);
        if (this.model.eventCalenderId > 0) {
          this.model.eventUrl = this.urlPrefix + this.model.eventCalenderId;
        }
      },
      error => {}
    );
  }

  getFileSetting() {
    this.utilityService.get_image_settings().subscribe(res => {
      this.fileSetting = res.data;
      this.fileType = this.fileSetting.imageType;
      console.log(this.fileSetting);
    });
  }
  getStatus() {
    console.log('call');
    this.statusService.getStatusByType('Event').subscribe(
      success => {
        console.log(success);
        this.status = success;
        console.log(this.status);
      },
      error => {}
    );
  }
  getDistrict() {
    this.districtService.getAll().subscribe(
      (success: District[]) => {
        this.districts = success;
      },
      error => {
        console.log(error);
      }
    );
  }

  getArea(id) {
    this.areaService.getAllByDistrict(id).subscribe(
      (success: Area[]) => {
        this.areas = success;
      },
      error => {
        console.log(error);
      }
    );
  }

  save() {
    if (this.model.eventCalenderId <= 0) {
      if (this.formData.has('model')) {
        this.formData.delete('model');
      }
      this.formData.append('model', JSON.stringify(this.model));
      console.log(this.model);
      this.eventCalenderService.save(this.formData).subscribe(
        success => {
          console.log(success);
          // this.dialogRef.close(success);
          this.model.eventUrl = this.urlPrefix + success.data.eventCalenderId;
          this.model.eventCalenderId = success.data.eventCalenderId;
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error: any) => {
          console.log('e');
          console.log(error);
          // this.dialogRef.close(error);
          swal({
            title: 'Error!',
            text: error.error.exceptionMessage,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    } else {
      if (this.formData.has('model')) {
        this.formData.delete('model');
      }
      this.formData.append('model', JSON.stringify(this.model));
      this.eventCalenderService.update(this.formData).subscribe(
        success => {
          console.log(success);
          this.dialogRef.close(success);
          swal({
            title: '',
            text: 'updated',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        error => {
          // this.dialogRef.close(error);
          swal({
            title: 'Error!',
            text: error.error.exceptionMessage,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }

  close() {
    this.dialogRef.close();
  }

  copyToClipBoard() {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.height = '0';
    selBox.value = this.model.eventUrl;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    try {
      const successful = document.execCommand('copy');
      const msg = successful ? 'successful' : 'unsuccessful';
      console.log('Copying text command was ' + msg);
      document.body.removeChild(selBox);
      if (successful) {
        swal({
          text: 'Campaign link copied to clipboard',
          type: 'success',
          confirmButtonText: 'Ok'
        });
      }
    } catch (err) {
      console.log('Oops, unable to copy', err);
    }
  }

  changeCapacity() {
    this.model.availableCapacity = this.model.capacity - this.diff;
  }

  onFileChange(event) {
    if (
      event.target.files.length > 0 &&
      this.fileValidate(
        event.target.files[0],
        this.fileType,
        this.fileSetting.imageSize
      )
    ) {
      this.fileObject = event.target.files[0];
      if (this.formData.has('fileId')) {
        this.formData.delete('fileId');
      }
      this.formData.append('fileId', this.fileObject, this.fileObject['name']);
      this.fileName = this.fileObject.name;
    }
  }

  fileValidate(file: File, allowFileType?: string, allowMaxSize?: number) {
    let extention = '';
    let size = 0;
    let valid = false;

    if (file.name) {
      extention = this.fileExtention(file.name);
    }

    if (file.size) {
      size = this.fileToMB(file.size);
      console.log('size');
      console.log(size);
    }

    if (!allowFileType || (extention && allowFileType.includes(extention))) {
      if (!allowMaxSize || (size && size <= allowMaxSize)) {
        valid = true;
        console.log('hello');
      } else {
        swal({
          title: 'Warning!',
          type: 'warning',
          confirmButtonText: 'Close',
          text: `File type should be ${allowFileType} & max ${allowMaxSize} MB size of file are supported`
        });
      }
    } else {
      console.log('valid');
      swal({
        title: 'Warning!',
        type: 'warning',
        confirmButtonText: 'Close',
        text: `File type should be ${allowFileType} & max ${allowMaxSize} MB size of file are supported`
      });
    }
    console.log(valid);
    return valid;
  }

  fileExtention(filename: string) {
    return (
      filename.substring(filename.lastIndexOf('.') + 1, filename.length) ||
      filename
    );
  }

  fileToMB(size: number) {
    if (size <= 0) {
      return 0;
    }

    return Math.ceil(size / (1024 * 1024));
  }
}
