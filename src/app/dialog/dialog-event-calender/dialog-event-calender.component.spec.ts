import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEventCalenderComponent } from './dialog-event-calender.component';

describe('DialogEventCalenderComponent', () => {
  let component: DialogEventCalenderComponent;
  let fixture: ComponentFixture<DialogEventCalenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEventCalenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEventCalenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
