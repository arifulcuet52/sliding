import { FaqCategory } from './../../entity/faq-category';
import { FaqService } from './../../shared/service/remote/fac/fac.service';
import { VivaResultService } from './../../shared/service/remote/viva-result/viva-result.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import swal from 'sweetalert2';
import { commonEnum } from 'src/app/entity/enums/enum';

@Component({
  selector: 'app-dialog-faq',
  templateUrl: './dialog-faq.component.html',
  styleUrls: ['./dialog-faq.component.scss']
})
export class DialogFaqComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA)public data: any,
  public dialogRef: MatDialogRef<DialogFaqComponent>, private facService: FaqService) { }

  model: FaqCategory = new FaqCategory();
  swal: any;
  ngOnInit() {
    this.model = this.data.model;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    if (this.model.id <= 0){
      this.facService.save_category(this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'Save successfully.',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error.message,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }else{
      this.facService.edit_category(this.model.id, this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'Update sucessfully.',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error.message,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }

}

