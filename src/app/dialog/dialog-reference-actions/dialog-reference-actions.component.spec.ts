import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogReferenceActionsComponent } from './dialog-reference-actions.component';

describe('DialogReferenceActionsComponent', () => {
  let component: DialogReferenceActionsComponent;
  let fixture: ComponentFixture<DialogReferenceActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogReferenceActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogReferenceActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
