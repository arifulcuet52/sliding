import { Component, OnInit, Inject } from '@angular/core';
import { ReferenceActivity } from '../../entity/reference-activity';
import { LeadService } from '../../shared/service/remote/lead/lead.service';
import { LeadAction, ActionTypeEum } from '../../entity/enums/lead-action.enum';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CandidateReferenceService } from '../../shared/service/remote/candidate-reference/candidate-reference.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-reference-actions',
  templateUrl: './dialog-reference-actions.component.html',
  styleUrls: ['./dialog-reference-actions.component.scss']
})
export class DialogReferenceActionsComponent implements OnInit {

  public model: ReferenceActivity = new ReferenceActivity();
  public actionTypes: any;
  public subActionstatusTypes: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogReferenceActionsComponent>,
    private actionStatusService: LeadService,
    private referenceService: CandidateReferenceService) { }

  ngOnInit() {
    console.log(this.data)
    this.model.referenceInfoId = this.data.model.id;
    this.loadData();
  }

  loadData() {
    this.getActions();
    this.getSubAction();
  }

  getActions() {
    this.actionStatusService.getLeadActionStatusByType(LeadAction.ActionType).subscribe(
      (success) => {
        // console.log(success);
        this.actionTypes = success;
      }
    );
  }

  isDisabled(action: any): boolean {
    return (action.leadActionId != ActionTypeEum.call);
  }

  getSubAction() {
    this.actionStatusService.getSubActionsByActionId(ActionTypeEum.call).subscribe(
      (success) => {
        console.log(success);
        this.subActionstatusTypes = success;
      }
    );
  }

  save() {
    console.log(this.model);
    if (this.model.activityId == 0) {
      this.referenceService.submitReferenceAction(this.model).subscribe(
        (succ) => {
          console.log(succ);
          this.dialogRef.close(true);
          this.showSuccess();
        },
        (err) => {
          console.log(err);
          this.showError(err, "Unable to aubmit");
        }
      );
    }
  }

  showError(error, title) {
    swal({
      title: title,
      text: JSON.parse(JSON.stringify(error)).error['exceptionMessage'] ? 
            JSON.parse(JSON.stringify(error)).error['exceptionMessage'] : JSON.parse(JSON.stringify(error)).error['message'],
      type: 'warning',
      confirmButtonText: 'Close'
    });
  }

  showSuccess() {
    swal({
      title: 'Success',
      text: 'Action successfully submitted',
      type: 'success',
      confirmButtonText: 'Close'
    });
  }

  check() {
    console.log(this.model);
  }

  close() {
    this.dialogRef.close();
  }
}
