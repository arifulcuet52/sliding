import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogMenuCreateComponent } from './dialog-menu-create.component';

describe('DialogMenuCreateComponent', () => {
  let component: DialogMenuCreateComponent;
  let fixture: ComponentFixture<DialogMenuCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogMenuCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogMenuCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
