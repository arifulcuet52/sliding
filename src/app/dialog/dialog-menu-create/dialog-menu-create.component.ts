import { Component, OnInit, Inject } from '@angular/core';
import { Menu } from '../../entity/menu';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MenuService } from '../../shared/service/remote/authentication/menu.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-dialog-menu-create',
  templateUrl: './dialog-menu-create.component.html',
  styleUrls: ['./dialog-menu-create.component.css']
})
export class DialogMenuCreateComponent implements OnInit {

  menus: Menu[];
  swal: any;
  model: Menu;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogMenuCreateComponent>,
    private menuService: MenuService
  ) { }

  ngOnInit() {
    this.model = this.data.model;
    if (!this.model.parentId) {
      this.model.parentId = null;
    }
    if (!this.model.portalName) {
      this.model.portalName = 'Admin';
    }
    this.getAll();
  }

  getAll() {
    this.menuService.GetDropdownList().subscribe(
      (response: Menu[]) => {
        this.menus = response;
      },
      (error) => { }
    );
  }
  save() {
    if (this.model.menuId) {
      this.menuService.update(this.model).subscribe(
        (response: string) => {
          this.dialogRef.close(response);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error.message,
            type: 'error',
            confirmButtonText: 'Close'
          });
        });
    } else {
      this.menuService.insert(this.model).subscribe(
        (response: string) => {
          this.dialogRef.close(response);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error.message,
            type: 'error',
            confirmButtonText: 'Close'
          });
        });
    }

  }
  close() {
    this.dialogRef.close();
  }



}
