import { Component, OnInit, Inject } from '@angular/core';
import { EducationEligibilityService } from '../../shared/service/remote/eligibility/education-eligibility/education-eligibility.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { EligibilityCriteriaEducation } from '../../entity/eligibility-criteria-education';
import swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-education-eligibility',
  templateUrl: './dialog-education-eligibility.component.html',
  styleUrls: ['./dialog-education-eligibility.component.css']
})
export class DialogEducationEligibilityComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA)public data: any,
  public dialogRef: MatDialogRef<DialogEducationEligibilityComponent>, private eduEligibilityService: EducationEligibilityService) { }

  model: EligibilityCriteriaEducation = new EligibilityCriteriaEducation();
  swal: any;

  ngOnInit() {
    this.model = this.data.model;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    if (this.model.id <= 0){
      this.eduEligibilityService.save(this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }else{
      this.eduEligibilityService.edit(this.model.id, this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }

}
