import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEducationEligibilityComponent } from './dialog-education-eligibility.component';

describe('DialogEducationEligibilityComponent', () => {
  let component: DialogEducationEligibilityComponent;
  let fixture: ComponentFixture<DialogEducationEligibilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEducationEligibilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEducationEligibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
