import { Validators } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { TrainingModuleService } from '../../shared/service/remote/exam/training-module.service';
import { TrainingModule } from '../../entity/training-module';
import swal from 'sweetalert2';
import { TrainingModuleEnum } from 'src/app/entity/enums/enum';

@Component({
  selector: 'app-dialog-training-module',
  templateUrl: './dialog-training-module.component.html',
  styleUrls: ['./dialog-training-module.component.css']
})
export class DialogTrainingModuleComponent implements OnInit {
  type = TrainingModuleEnum;
  model = new TrainingModule();
  types = trainingModuleTypes;

  /* dummy objects */
  public titleModels: TrainingModule[];
  public moduleModels: TrainingModule[] = [];
  public subModuleModels: TrainingModule[] = [];
  public typeName: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogTrainingModuleComponent>,
    private tms: TrainingModuleService
  ) {}

  ngOnInit() {
    this.typeName = '';
    this.model = this.data.model;
    this.onChangeType();
  }

  onChangeType() {
    // console.log(this.model);
    // if (this.model.moduleType !== enumTrainingModule.module) {
    //   this.SetUnderSubModule();
    // }
    if (this.model.moduleType === 0 || this.model.moduleType === TrainingModuleEnum.title )
    {
      this.typeName = 'Title';
    }
    else if (this.model.moduleType === TrainingModuleEnum.module) {
      this.typeName = 'Module';
      this.tms.getByModuleType(TrainingModuleEnum.title).subscribe(res => {
        this.titleModels = res.data;
      });
    }
    else if (this.model.moduleType === TrainingModuleEnum.section) {
      this.typeName = 'Section';
      this.tms.getByModuleType(TrainingModuleEnum.module).subscribe(res => {
        this.moduleModels = res.data;
      });
      this.tms.getByModuleType(TrainingModuleEnum.section).subscribe(res => {
        this.subModuleModels = res.data;
      });
    }
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    if (this.model.id <= 0) {
      this.tms.save(this.model).subscribe(
        succ => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        error => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    } else {
      this.tms.edit(this.model.id, this.model).subscribe(
        succ => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        error => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }

  SetUnderSubModule() {
    this.model.parentId = 0;
  }
}


const trainingModuleTypes = [
  { id: TrainingModuleEnum.title, name: 'Title' },
  { id: TrainingModuleEnum.module, name: 'Module' },
  { id: TrainingModuleEnum.section, name: 'Section' }
];
