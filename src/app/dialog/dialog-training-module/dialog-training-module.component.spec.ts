import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogTrainingModuleComponent } from './dialog-training-module.component';

describe('DialogTrainingModuleComponent', () => {
  let component: DialogTrainingModuleComponent;
  let fixture: ComponentFixture<DialogTrainingModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogTrainingModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogTrainingModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
