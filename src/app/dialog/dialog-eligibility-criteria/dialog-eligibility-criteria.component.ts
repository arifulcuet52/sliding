import { EligibilityCriteria } from './../../entity/eligibility-criteria';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EligibilityService } from '../../shared/service/remote/eligibility/eligibility.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-eligibility-criteria',
  templateUrl: './dialog-eligibility-criteria.component.html',
  styleUrls: ['./dialog-eligibility-criteria.component.css']
})
export class DialogEligibilityCriteriaComponent implements OnInit {

  model: EligibilityCriteria = new EligibilityCriteria();
  eduList: any;
  ageList: any;
  swal: any;
  constructor(@Inject(MAT_DIALOG_DATA)public data: any,
  public dialogRef: MatDialogRef<DialogEligibilityCriteriaComponent>, private eligibilityService: EligibilityService) { }

  ngOnInit() {
    this.model = this.data.model;
    this.eduList = this.data.eduList;
    this.ageList = this.data.ageList;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    if (this.model.eligibilityCriteriaId <= 0){
      this.eligibilityService.save(this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }else{
      this.eligibilityService.edit(this.model.eligibilityCriteriaId, this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }

}
