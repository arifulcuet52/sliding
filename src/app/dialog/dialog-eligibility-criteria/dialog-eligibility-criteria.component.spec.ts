import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEligibilityCriteriaComponent } from './dialog-eligibility-criteria.component';

describe('DialogEligibilityCriteriaComponent', () => {
  let component: DialogEligibilityCriteriaComponent;
  let fixture: ComponentFixture<DialogEligibilityCriteriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEligibilityCriteriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEligibilityCriteriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
