import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEligibilityMaritalStatusComponent } from './dialog-eligibility-marital-status.component';

describe('DialogEligibilityMaritalStatusComponent', () => {
  let component: DialogEligibilityMaritalStatusComponent;
  let fixture: ComponentFixture<DialogEligibilityMaritalStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEligibilityMaritalStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEligibilityMaritalStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
