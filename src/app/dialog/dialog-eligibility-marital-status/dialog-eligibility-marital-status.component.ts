import { EligibilityCriteriaMarital } from './../../entity/eligibility-criteria-marital';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MaritalEligibilityService } from '../../shared/service/remote/eligibility/marital-eligibility/marital-eligibility.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-eligibility-marital-status',
  templateUrl: './dialog-eligibility-marital-status.component.html',
  styleUrls: ['./dialog-eligibility-marital-status.component.css']
})
export class DialogEligibilityMaritalStatusComponent implements OnInit {

  model: EligibilityCriteriaMarital = new EligibilityCriteriaMarital();
  eduList: any;
  swal: any;
  constructor(@Inject(MAT_DIALOG_DATA)public data: any,
  public dialogRef: MatDialogRef<DialogEligibilityMaritalStatusComponent>, private maritalEligibilityService: MaritalEligibilityService) { }

  ngOnInit() {
    this.model = this.data.model;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    if (this.model.id <= 0){
      this.maritalEligibilityService.save(this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }else{
      this.maritalEligibilityService.edit(this.model.id, this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }

}
