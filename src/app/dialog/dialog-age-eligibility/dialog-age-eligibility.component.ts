import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AgeEligibilityService } from '../../shared/service/remote/eligibility/age-eligibility/age-eligibility.service';
import { EligibilityCriteriaAge } from '../../entity/eligibility-criteria-age';
import swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-age-eligibility',
  templateUrl: './dialog-age-eligibility.component.html',
  styleUrls: ['./dialog-age-eligibility.component.css']
})
export class DialogAgeEligibilityComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA)public data: any,
  public dialogRef: MatDialogRef<DialogAgeEligibilityComponent>, private ageEligibilityService: AgeEligibilityService) { }

  model: EligibilityCriteriaAge = new EligibilityCriteriaAge();
  swal: any;

  ngOnInit() {
    this.model = this.data.model;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    console.log(this.model);
    if (this.model.id <= 0){
      this.ageEligibilityService.save(this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }else{
      this.ageEligibilityService.edit(this.model.id, this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }
}
