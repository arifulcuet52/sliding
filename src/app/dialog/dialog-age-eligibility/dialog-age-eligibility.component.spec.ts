import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAgeEligibilityComponent } from './dialog-age-eligibility.component';

describe('DialogAgeEligibilityComponent', () => {
  let component: DialogAgeEligibilityComponent;
  let fixture: ComponentFixture<DialogAgeEligibilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAgeEligibilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAgeEligibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
