import { VivaResultService } from './../../shared/service/remote/viva-result/viva-result.service';
import { Component, OnInit, Inject } from '@angular/core';
import { VivaResult } from 'src/app/entity/viva-result';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import swal from 'sweetalert2';
import { commonEnum } from 'src/app/entity/enums/enum';

@Component({
  selector: 'app-dialog-viva-result',
  templateUrl: './dialog-viva-result.component.html',
  styleUrls: ['./dialog-viva-result.component.scss']
})
export class DialogVivaResultComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA)public data: any,
  public dialogRef: MatDialogRef<DialogVivaResultComponent>, private vrs: VivaResultService) { }

  model: VivaResult = new VivaResult();
  swal: any;
  ngOnInit() {
    this.model = this.data.model;
    this.model.statusId = commonEnum.fail;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    if (this.model.id <= 0){
      this.vrs.save(this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }else{
      this.vrs.edit(this.model.id, this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }

}
