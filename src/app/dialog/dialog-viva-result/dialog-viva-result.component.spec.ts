import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogVivaResultComponent } from './dialog-viva-result.component';

describe('DialogVivaResultComponent', () => {
  let component: DialogVivaResultComponent;
  let fixture: ComponentFixture<DialogVivaResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogVivaResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogVivaResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
