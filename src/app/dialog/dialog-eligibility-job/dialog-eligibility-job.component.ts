import { EligibilityCriteriaJob } from './../../entity/eligibility-criteria-job';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { JobEligibilityService } from '../../shared/service/remote/eligibility/job-eligibility/job-eligibility.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-eligibility-job',
  templateUrl: './dialog-eligibility-job.component.html',
  styleUrls: ['./dialog-eligibility-job.component.css']
})
export class DialogEligibilityJobComponent implements OnInit {

  model: EligibilityCriteriaJob = new EligibilityCriteriaJob();
  eduList: any;
  swal: any;
  constructor(@Inject(MAT_DIALOG_DATA)public data: any,
  public dialogRef: MatDialogRef<DialogEligibilityJobComponent>, private jobEligibilityService: JobEligibilityService) { }

  ngOnInit() {
    this.model = this.data.model;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    if (this.model.id <= 0){
      this.jobEligibilityService.save(this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }else{
      this.jobEligibilityService.edit(this.model.id, this.model).subscribe(
        (succ) => {
          this.dialogRef.close(succ.data);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });
        }
      );
    }
  }

}
