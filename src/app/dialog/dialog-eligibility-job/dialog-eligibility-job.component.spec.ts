import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEligibilityJobComponent } from './dialog-eligibility-job.component';

describe('DialogEligibilityJobComponent', () => {
  let component: DialogEligibilityJobComponent;
  let fixture: ComponentFixture<DialogEligibilityJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEligibilityJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEligibilityJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
