import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogReassignCcAdminComponent } from './dialog-reassign-cc-admin.component';

describe('DialogReassignCcAdminComponent', () => {
  let component: DialogReassignCcAdminComponent;
  let fixture: ComponentFixture<DialogReassignCcAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogReassignCcAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogReassignCcAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
