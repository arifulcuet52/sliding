import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserService } from '../../shared/service/remote/user/user.service';
import { RoleEnum } from '../../entity/enums/enum';
import { UserRoleVm } from '../../entity/user-role-vm';
import { RefVerificationQueue } from '../../entity/ref-verification-queue';
import { CandidateReferenceService } from '../../shared/service/remote/candidate-reference/candidate-reference.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-dialog-reassign-cc-admin',
  templateUrl: './dialog-reassign-cc-admin.component.html',
  styleUrls: ['./dialog-reassign-cc-admin.component.scss']
})
export class DialogReassignCcAdminComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogReassignCcAdminComponent>,
    private userService: UserService,
    private referenceService: CandidateReferenceService) { }

  public ccUsers: UserRoleVm[] = [];
  public model: RefVerificationQueue = new RefVerificationQueue();
  ngOnInit() {
    this.model = this.data.model;
    this.getCcAdminList();
  }

  getCcAdminList() {
    this.userService.getUserByRoleId(RoleEnum.CallCenterAdmin).subscribe(
      (success) => {
        console.log(success.data);
        this.ccUsers = success.data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    const swalWithButtons = swal.mixin({
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
    });

    swalWithButtons({
      title: 'Are you sure?',
      text: "Are you sure to reassign to a new admin?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.referenceService.updateRefVerificationQueue(this.model).subscribe(
          (success) => {
            this.model = success.data;
            console.log(success.data);
          },
          (error) => {
            console.log(error);
          }
        );
        this.dialogRef.close(this.model);
      }
      else if (result.dismiss === swal.DismissReason.cancel) { }
    });

  }
}
