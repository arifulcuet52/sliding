import { Component, OnInit, Inject } from '@angular/core';
import { Area } from '../../entity/area';
import { CountryInfo } from '../../entity/country';
import { CountryService } from '../../shared/service/remote/country/country.service';
import { DistrictService } from '../../shared/service/remote/district/district.service';
import { District } from '../../entity/district';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AreaService } from '../../shared/service/remote/area/area.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-dialog-area',
  templateUrl: './dialog-area.component.html',
  styleUrls: ['./dialog-area.component.css']
})
export class DialogAreaComponent implements OnInit {

  swal: any;
  model: Area;
  countries: CountryInfo[] = [];
  districts: District[] = [];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogAreaComponent>,
    private countryService: CountryService,
    private districtService: DistrictService,
    private areaService: AreaService
  ) { }

  ngOnInit() {
    this.model = this.data.model;
    this.getCountries();
  }

  getCountries() {
    this.countryService.getAll().subscribe(
      (success) => {
        this.countries = success['data'] as CountryInfo[];
        this.getdistricts();
      }
    );
  }
  getdistricts() {
    console.log(this.model);
    this.districtService.getAllByCountryCode(this.model.district.countryId).subscribe(
      (success: District[]) => {
        this.districts = success;
      }
    );
  }

  changeCountry() {
    this.getdistricts();
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    if (this.model.areaId > 0) {
      const temp = Object.assign({}, this.model);
      temp.district = null;
      this.areaService.update(temp).subscribe(
        (success) => {
          this.dialogRef.close(success);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Error!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'error',
            confirmButtonText: 'Close'
          });

          //  JSON.parse(JSON.stringify(error)).error;
        }
      );
    }
    else {
      const temp = Object.assign({}, this.model);
      temp.district = null;
      this.areaService.add(temp).subscribe(
        (success) => {
          this.dialogRef.close(success);
          swal({
            title: '',
            text: 'success',
            type: 'success',
            confirmButtonText: 'Ok'
          });
        },
        (error) => {
          swal({
            title: 'Warning!',
            text: JSON.parse(JSON.stringify(error)).error,
            type: 'warning',
            confirmButtonText: 'Close'
          });

          // this.errorMessage = JSON.parse(JSON.stringify(error)).error;
        }
      );
    }
  }

}
