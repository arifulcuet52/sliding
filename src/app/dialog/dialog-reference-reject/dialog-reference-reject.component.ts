import { Component, OnInit, Inject } from '@angular/core';
import { CandidateReferenceService } from '../../shared/service/remote/candidate-reference/candidate-reference.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Reference } from '../../entity/reference';

@Component({
  selector: 'app-dialog-reference-reject',
  templateUrl: './dialog-reference-reject.component.html',
  styleUrls: ['./dialog-reference-reject.component.scss']
})
export class DialogReferenceRejectComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA)public data: any,
    public dialogRef: MatDialogRef<DialogReferenceRejectComponent>
) { }

  model: Reference = new Reference();
  remarks: string = "";
  ngOnInit() {
    this.remarks = this.data.model.remarks;
  }

  close() {
    this.remarks = null;
    this.dialogRef.close();
  }

  save(){
    this.remarks = this.model.remarks;
    this.dialogRef.close(this.remarks);
  }
}
