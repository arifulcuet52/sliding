import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogReferenceRejectComponent } from './dialog-reference-reject.component';

describe('DialogReferenceRejectComponent', () => {
  let component: DialogReferenceRejectComponent;
  let fixture: ComponentFixture<DialogReferenceRejectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogReferenceRejectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogReferenceRejectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
