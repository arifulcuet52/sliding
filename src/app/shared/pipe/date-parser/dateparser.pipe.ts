import { Pipe, PipeTransform } from '@angular/core';
import { FormatService } from 'src/app/shared/service/formatter/format.service';
import * as moment from 'moment';
import { DataService } from 'src/app/Shared/Service/data/data.service';

@Pipe({
  name: 'dateparser'
})
export class DateparserPipe implements PipeTransform {

  constructor(public formatService: FormatService){
  }
  transform(value: Date, args?: any): any {
    return this.time_formation(value);
  }

  time_formation(date_time): string {
    let formatted_time = '';
    if ( date_time !== '' && date_time != null){
      formatted_time = moment(new Date(date_time)).format(this.formatService.date_format);
    }
    return formatted_time;
  }

}
