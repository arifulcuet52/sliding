import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByProperty',
  pure: false
})
export class FilterByPropertyPipe implements PipeTransform {

  transform(value: any[], args?: any): any {
    // console.log(value, 'value');
    const objKey = Object.keys(args)[0];
    const objValue = args[objKey];
    const v = value.filter(x => x[objKey] === objValue);
   // console.log(v);
    return v;
  }

}
