import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterByList',
    pure: false
})
export class FilterPipe implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toLowerCase();
        return items.filter(it => {
            let index1 =it.lastIndexOf('~')+1;
            let index2 = it.lastIndexOf('-')+1;
           it= it.substring(Math.max(index1,index2),it.lastIndexOf('.'));
            return it.toLowerCase().includes(searchText);
        });
    }
}
