import { TestBed, inject } from '@angular/core/testing';

import { UserMenuCanActivateRouteGuardService } from './user-menu-can-activate-route-guard.service';

describe('UserMenuCanActivateRouteGurdService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserMenuCanActivateRouteGuardService]
    });
  });

  it('should be created', inject([UserMenuCanActivateRouteGuardService], (service: UserMenuCanActivateRouteGuardService) => {
    expect(service).toBeTruthy();
  }));
});
