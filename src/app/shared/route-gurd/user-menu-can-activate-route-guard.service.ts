import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  ActivatedRoute
} from '@angular/router';
import { AccountService } from '../service/remote/authentication/account.service';
import { UserMenuVm } from '../../entity/user-menu';

@Injectable({
  providedIn: 'root'
})
export class UserMenuCanActivateRouteGuardService implements CanActivate {
  alluserMenu: UserMenuVm[] = [];
  constructor(
    private route: Router,
    private accountSertvice: AccountService,
    private active: ActivatedRoute
  ) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    let url = state.url;
    console.log(

      decodeURI(url)
    );
    const allMenu = this.accountSertvice.getUserMenu();
    this.alluserMenu = [];

    const params = route.paramMap.keys;
    params.forEach(element => {
      url = url.replace('/' + route.paramMap.get(element), '');
    });
    if (url.includes('?')) {
      url = url.substring(0, url.lastIndexOf('?'));
    }

    this.recursive(allMenu);
    const result = this.alluserMenu.filter(x => x.path == url);

    if (result.length > 0) {
      return true;
    }
    this.accountSertvice.setUnauthrize(state.url);
    this.route.navigate(['/ap/unauthorized']);
    return false;
  }

  recursive(node: UserMenuVm[]) {
    node.forEach(element => {
      if (element.list.length === 0) {
        this.alluserMenu.push(element);
      } else if (element.list.length === 1) {
        const temp = Object.assign({}, element);
        temp.list = [];
        this.alluserMenu.push(temp);
      }
      this.recursive(element.list);
    });
  }
}
