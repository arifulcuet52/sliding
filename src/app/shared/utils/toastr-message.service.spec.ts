import { TestBed, inject } from '@angular/core/testing';

import { ToastrMessageService } from './toastr-message.service';

describe('ToastrMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToastrMessageService]
    });
  });

  it('should be created', inject([ToastrMessageService], (service: ToastrMessageService) => {
    expect(service).toBeTruthy();
  }));
});
