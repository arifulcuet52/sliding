import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})

export class ToastrMessageService {

  constructor(private toastr: ToastrService) { }

  showSuccess(message: string = 'Information Saved.', title: string = 'Success!') {
    this.toastr.success(message, title);
  }

  showError(message: string = 'Please try again later.', title: string = 'Error!') {
    this.toastr.error(message, title);
  }

  showWarning(message: string = '', title: string = 'Alert!') {
    this.toastr.warning(message, title);
  }

  showInfo(message: string = "Notice") {
    this.toastr.info(message);
  }

  clearToastrMessage(){
    this.toastr.clear();
  }
}