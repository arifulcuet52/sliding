import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent
} from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { throwError } from 'rxjs/internal/observable/throwError';
import { LoadingService } from '../service/loading/loading.service';
import { finalize, catchError, switchMap } from 'rxjs/operators';
import { AccountService } from '../service/remote/authentication/account.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private loadingService: LoadingService,
    private injector: Injector
  ) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!req.url.toLocaleLowerCase().includes('logout') || !req.url.toLocaleLowerCase().includes('getUserAllPendingNotification')) {
      this.loadingService.start();
    }

    const token = JSON.parse(localStorage.getItem('AD'));
    const account = this.injector.get(AccountService);
    if (!req.url.includes('token')) {
      if (token) {
        const clone = req.clone({
          headers: req.headers.set('Authorization', 'Bearer ' + token.token)
        });
        return next.handle(clone).pipe(
          catchError(error => {
            if (error.status === 401) {
              // hit for refresh token
              return account.refreshToken().pipe(
                switchMap((newToken: string) => {
                  const authReqRepeat = req.clone({
                    headers: req.headers.set(
                      'Authorization',
                      'Bearer ' + newToken
                    )
                  });
                  return next.handle(authReqRepeat);
                })
              );
            } else {
              return throwError(error);
            }
          }),

          // catchError(error => {
          //   if (error.status === 401) {
          //     // account.refreshToken().pipe(
          //     //   switchMap((newToken: string) => {
          //     //     // unset request inflight
          //     //     console.log(newToken);
          //     //     account.setRefreshTokenValue(newToken);
          //     //     // use the newly returned token
          //     //     // const authReqRepeat = req.clone({
          //     //     //   headers: req.headers.set(
          //     //     //     'Authorization',
          //     //     //     'Bearer ' + account.getToken()
          //     //     //   )
          //     //     // });
          //     //     return next.handle(req.clone());
          //     //   })
          //     // );

          //     return account.refreshToken().subscribe(
          //       success => {
          //         account.setRefreshTokenValue(success);
          //         const authReqRepeat = req.clone({
          //           headers: req.headers.set(
          //             'Authorization',
          //             'Bearer ' + account.getToken()
          //           )
          //         });
          //         console.log(authReqRepeat);
          //         return next.handle(authReqRepeat);
          //       },
          //       err => {
          //         return throwError(error);
          //       }
          //     );
          //     // return next.handle(req);
          //   } else {
          //     return throwError(error);
          //   }
          // })
          finalize(() => {
            this.loadingService.stop();
          })
        );
      } else {
        return next.handle(req).pipe(
          finalize(() => {
            this.loadingService.stop();
          })
        );
      }
    } else {
      return next.handle(req).pipe(
        finalize(() => {
          this.loadingService.stop();
        })
      ); //.pipe(finally(r=>console.log('asas')));

      //.finally(res => this.loadingService.stop());
    }
  }
}
