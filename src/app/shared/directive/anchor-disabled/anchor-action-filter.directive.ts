import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector : '[href, button]'
})
export class AnchorActionFilterDirective {
  @Input() href: string;

  @HostListener('click', ['$event'])
  noop(event: MouseEvent) {
      console.log('event :', event.target);
    //alert("successfully disabled");
    //event.preventDefault();
    //return false
    // if(this.href.length === 0 || this.href === '#') {
    //   event.preventDefault();
    // }
  }
}
