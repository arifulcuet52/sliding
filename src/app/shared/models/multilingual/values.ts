
export class Values{
    id :number;
    keyId :number;
    keyName:string;
    values :string;
    language:string;
    key:Keys;
}

export class Keys{
    keyId:number;
    keyName:string
}