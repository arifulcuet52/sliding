import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserMenuService {

  url = this.auth.APPSERVER+"UserMenu";
  constructor(private auth: AuthService, private http: HttpClient) { }
  public getUserMenu() {
    return this.http.get(this.url + "/getusermenu");
  }

  public getReportMenusByUserId() {
    return this.http.get( this.url+'/ReportMenus');
  }
}
