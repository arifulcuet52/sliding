import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '../../../../../../node_modules/@angular/common/http';
import { AuthService } from '../../auth/auth.service';
import { ObjectDesign } from '../../../../entity/training/objectDesign';
import { Observable } from 'rxjs';
import { presentation, slides } from '../../../../entity/trainingContent/Presentation';

@Injectable({
  providedIn: 'root'
})
export class TrainingContentService {

  url = this.auth.APPSERVER + 'training';
  constructor(private http: HttpClient, private auth: AuthService) { }
  
  async getAllImage(type:string){   
    console.log("Call to service..");
    return this.http.get<any>(this.url + '/getImage?type='+type).toPromise();    
  }

  get(id: number) {
    return this.http.get(this.url + '/get', { params: new HttpParams().set('id', id.toString()) });
  }

  addImage(formData:FormData,type:string){
    let url=this.url + '/addImage?type='+type;
    return this.http.post(url,formData);
  }
  addSlideCoverImage(formData:FormData,type:string,slideId:number){
    let url=this.url + '/addSlideCoverImage?type='+type+'&slideId='+slideId;
    return this.http.post(url,formData);
  }


  savePresentation(presentation: presentation): Observable<any>  {
    console.log('presentation in services:',presentation);
    return this.http.post(this.url + '/SavePresentation',  presentation);
  }

  getAllPresentation() {
    return this.http.get(this.url + '/GetAllPresentation');
  }
  getAllSlidesByPresentationId(presentationId:number) {
    //return this.http.get(this.url + '/GetAllSlidesByPresentationId?presentationId='+presentationId);
    return this.http.get(this.url + '/GetSlidesById/'+presentationId);
  }

  deleteSlideById(presentationId:number, slideId:number){
    return this.http.get(this.url + '/DeleteSlideById?presentationId='+presentationId+'&slideId='+slideId);
  }

  fnChangeSlideOrder(slides: slides[], presentationId:number): Observable<any>  {
    return this.http.post(this.url + '/ChangeSlideOrder?presentationId='+presentationId,  slides);
  }
}
