import { Injectable, EventEmitter } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TrainingModule } from '../../../../entity/training-module';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PolicyVerificationService {
  expandEmitter: EventEmitter<any> = new EventEmitter<any>();
  url = this.auth.APPSERVER + 'policy/';
  constructor(private auth: AuthService, private http: HttpClient) {}

  get_policy_verifications(
    searchText,
    pageSize: number,
    pageNumber: number
  ): Observable<any> {
    const url =
      this.url +
      'get-policy-verifications/' +
      pageSize +
      '/' +
      pageNumber +
      '/' +
      searchText;
    return this.http.get(url);
  }

  getbyuser(userId: number): Observable<any> {
    return this.http.get(this.url + 'getbyuser/' + userId);
  }

  approve(id: number, obj: any): Observable<any> {
    return this.http.put(this.url + 'approve/' + id, obj);
  }

  reject(id: number, obj: any): Observable<any> {
    return this.http.put(this.url + 'reject/' + id, obj);
  }
  expanPolicy() {
    this.expandEmitter.next(true);
  }
}
