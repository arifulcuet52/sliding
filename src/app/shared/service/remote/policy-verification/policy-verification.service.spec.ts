import { TestBed, inject } from '@angular/core/testing';

import { PolicyVerificationService } from './policy-verification.service';

describe('PolicyVerificationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PolicyVerificationService]
    });
  });

  it('should be created', inject([PolicyVerificationService], (service: PolicyVerificationService) => {
    expect(service).toBeTruthy();
  }));
});
