import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';
import { UserCreationVm } from '../../../../entity/UserCreationVm';

@Injectable({
  providedIn: 'root'
})
export class UserCreateService {
  url = this.auth.APPSERVER + 'user';
  constructor(private http: HttpClient, private auth: AuthService) {}

  save(model: UserCreationVm) {
    return this.http.put(this.url + '/create', model);
  }
}
