import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeneralSettingsService {

  constructor(private authService: AuthService, private http: HttpClient) { }

  api = this.authService.APPSERVER + 'generalsetting/';

  edit(id: number, obj: any): Observable<any>{
    return this.http.put(this.api + 'edit/' + id, obj);
  }

  get(): Observable<any>{
    return this.http.get(this.api + 'get/');
  }
}
