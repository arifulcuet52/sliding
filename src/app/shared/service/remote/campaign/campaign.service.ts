import { Injectable } from '@angular/core';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {

  constructor(private authService: AuthService, private http: HttpClient) { }

  private api = this.authService.APPSERVER + 'campaign/settings/';

  create(obj: any): Observable<any>{
    return this.http.post(this.api + 'create/', obj);
  } 

  edit(obj: any): Observable<any>{
    return this.http.put(this.api + 'edit/', obj);
  }

  delete(id): Observable<any>{
    return this.http.delete(this.api + 'delete/' + id);
  }

  getByCode(code: string): Observable<any>{
    code = code.replace(/\./gi,"^");
    return this.http.get(this.api + 'getByCode/' + code);
  }

  getById(id: number): Observable<any>{
    return this.http.get(this.api + 'getById/' + id);
  }

  public getBySearch(searchText: string, pageSize: number, pageNumber: number) {
    return this.http.get(this.api + 'getBySearch', {
      params: new HttpParams()
        .set('searchText', searchText)
        .set('pageSize', pageSize.toString())
        .set('pageNumber', pageNumber.toString())
    });
  }
}
