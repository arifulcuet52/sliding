import { TestBed, inject } from '@angular/core/testing';

import { FacService } from './fac.service';

describe('FacService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FacService]
    });
  });

  it('should be created', inject([FacService], (service: FacService) => {
    expect(service).toBeTruthy();
  }));
});
