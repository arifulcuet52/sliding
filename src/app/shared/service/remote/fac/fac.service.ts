import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FaqService {

  api = this.authService.APPSERVER + 'faq/';
  constructor(private authService: AuthService, private http: HttpClient) { }

  get(id: number): Observable<any>{
    return this.http.get(this.api + 'get/' + id);
  }

  get_by_category(id: number): Observable<any>{
    return this.http.get(this.api + 'get-by-category/' + id);
  }

  save(obj: any): Observable<any>{
    return this.http.post(this.api + 'add', obj);
  }
  edit(id: number, obj: any): Observable<any>{
    return this.http.put(this.api + 'edit/' + id, obj);
  }
  delete(id): Observable<any>{
    return this.http.delete(this.api + 'delete/' + id);
  }

  // FAQ Categories

  get_categories(): Observable<any>{
    return this.http.get(this.api + 'category/all');
  }

  get_category(id: number): Observable<any>{
    return this.http.get(this.api + 'category/get/' + id);
  }

  save_category(obj: any): Observable<any>{
    return this.http.post(this.api + 'category/add', obj);
  }
  edit_category(id: number, obj: any): Observable<any>{
    return this.http.put(this.api + 'category/edit/' + id, obj);
  }
  delete_category(id): Observable<any>{
    return this.http.delete(this.api + 'category/delete/' + id);
  }

}

