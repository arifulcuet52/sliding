import { TestBed, inject } from '@angular/core/testing';

import { UrlSettingService } from './url-setting.service';

describe('UrlSettingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UrlSettingService]
    });
  });

  it('should be created', inject([UrlSettingService], (service: UrlSettingService) => {
    expect(service).toBeTruthy();
  }));
});
