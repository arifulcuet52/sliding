import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UrlSettingService {

  constructor(private authService: AuthService, private http: HttpClient) { }
  
  api = this.authService.APPSERVER + 'url/settings/';

  getByCode(code: string): Observable<any>{
    return this.http.get(this.api + 'getByCode/' + code);
  }
}
