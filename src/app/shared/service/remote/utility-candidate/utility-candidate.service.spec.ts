import { TestBed, inject } from '@angular/core/testing';

import { UtilityCandidateService } from './utility-candidate.service';

describe('UtilityCandidateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UtilityCandidateService]
    });
  });

  it('should be created', inject([UtilityCandidateService], (service: UtilityCandidateService) => {
    expect(service).toBeTruthy();
  }));
});
