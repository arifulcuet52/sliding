import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilityCandidateService {

  private api = this.authService.APPSERVER + 'candidate/';
  private api_edu = this.authService.APPSERVER + 'eligibility/edu/';
  private api_board = this.authService.APPSERVER + 'candidate/board/';
  constructor(private authService: AuthService, private http: HttpClient) {}

  get_cadidate_profile_info(candidateId: number): Observable<any> {
    return this.http.get(
      this.api + 'profileByCandidateId/' + candidateId
    );
  }

  get_qualific_by_candidateId(candidateId: number): Observable<any> {
    return this.http.get(
      this.api + 'academicQualific/getByCandidate/' + candidateId
    );
  }

  get_bank_info_by_candidateId(candidateId: number): Observable<any> {
    return this.http.get(this.api + 'bankInfo/getByCandidate/' + candidateId);
  }

  // Education Criteria
  get_criteria_edu(): Observable<any> {
    return this.http.get(this.api_edu + 'gets');
  }
  // Boards
  get_all_boards(): Observable<any> {
    return this.http.get(this.api_board + 'getAll');
  }

  // bank branch
  get_all_banks(): Observable<any>{
    return this.http.get(this.authService.APPSERVER + 'bank/getAll');
  }
}
