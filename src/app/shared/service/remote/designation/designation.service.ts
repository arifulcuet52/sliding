import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams
} from '../../../../../../node_modules/@angular/common/http';
import { AuthService } from '../../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class DesignationService {
  url = this.auth.APPSERVER + 'designation';
  constructor(private http: HttpClient, private auth: AuthService) {}
  getAll(pageName: string) {
    return this.http.get(this.url + '/getall/' + pageName + '/true/false');
  }
  get(id: number) {
    return this.http.get(this.url + '/get', {
      params: new HttpParams().set('id', id.toString())
    });
  }
}
