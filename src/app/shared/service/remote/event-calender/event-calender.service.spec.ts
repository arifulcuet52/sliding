import { TestBed, inject } from '@angular/core/testing';

import { EventCalenderService } from './event-calender.service';

describe('EventCalenderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EventCalenderService]
    });
  });

  it('should be created', inject([EventCalenderService], (service: EventCalenderService) => {
    expect(service).toBeTruthy();
  }));
});
