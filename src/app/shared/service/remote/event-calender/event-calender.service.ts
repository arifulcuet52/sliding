import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { EventCalender } from 'src/app/entity/event-calender';

@Injectable({
  providedIn: 'root'
})
export class EventCalenderService {

  url = this.auth.APPSERVER + 'event/calender/';
  constructor(private auth: AuthService, private http: HttpClient) { }

  public getAll(searchText: string, pageSize: number, pageNumber: number) {
    return this.http.get(this.url + 'getAll', {
      params: new HttpParams()
        .set('searchText', searchText)
        .set('pageSize', pageSize.toString())
        .set('pageNumber', pageNumber.toString())
    });
  }

  public save(model: FormData){
    return this.http.post<any>(this.url + 'add' , model);
  }

  public update(model: FormData){
    return this.http.post<any>(this.url + 'update' , model);
  }
  public delete(id){
    return this.http.delete<any>(this.url + 'delete/' + id);
  }
}
