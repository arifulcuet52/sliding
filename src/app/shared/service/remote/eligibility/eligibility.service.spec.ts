import { TestBed, inject } from '@angular/core/testing';

import { EligibilityService } from './eligibility.service';

describe('EligibilityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EligibilityService]
    });
  });

  it('should be created', inject([EligibilityService], (service: EligibilityService) => {
    expect(service).toBeTruthy();
  }));
});
