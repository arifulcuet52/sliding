import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JobEligibilityService {
  // hello
  api = this.authService.APPSERVER + 'eligibility/jobexp/';
  constructor(private authService: AuthService, private http: HttpClient) { }

  get(id: number): Observable<any>{
    return this.http.get(this.api + 'get/' + id);
  }

  getall(): Observable<any>{
    return this.http.get(this.api + 'all');
  }

  save(obj: any): Observable<any>{
    return this.http.post(this.api + 'add', obj);
  }
  edit(id: number, obj: any): Observable<any>{
    return this.http.put(this.api + 'edit/' + id, obj);
  }
  delete(id): Observable<any>{
    return this.http.delete(this.api + 'delete/' + id);
  }
}
