import { TestBed, inject } from '@angular/core/testing';

import { JobEligibilityService } from './job-eligibility.service';

describe('JobEligibilityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JobEligibilityService]
    });
  });

  it('should be created', inject([JobEligibilityService], (service: JobEligibilityService) => {
    expect(service).toBeTruthy();
  }));
});
