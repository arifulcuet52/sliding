import { TestBed, inject } from '@angular/core/testing';

import { MaritalEligibilityService } from './marital-eligibility.service';

describe('MaritalEligibilityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MaritalEligibilityService]
    });
  });

  it('should be created', inject([MaritalEligibilityService], (service: MaritalEligibilityService) => {
    expect(service).toBeTruthy();
  }));
});
