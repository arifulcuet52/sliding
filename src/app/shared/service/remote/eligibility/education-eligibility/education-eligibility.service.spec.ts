import { TestBed, inject } from '@angular/core/testing';

import { EducationEligibilityService } from './education-eligibility.service';

describe('EducationEligibilityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EducationEligibilityService]
    });
  });

  it('should be created', inject([EducationEligibilityService], (service: EducationEligibilityService) => {
    expect(service).toBeTruthy();
  }));
});
