import { TestBed, inject } from '@angular/core/testing';

import { AgeEligibilityService } from './age-eligibility.service';

describe('AgeEligibilityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AgeEligibilityService]
    });
  });

  it('should be created', inject([AgeEligibilityService], (service: AgeEligibilityService) => {
    expect(service).toBeTruthy();
  }));
});
