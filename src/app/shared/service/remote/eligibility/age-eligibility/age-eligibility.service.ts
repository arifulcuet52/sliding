import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AgeEligibilityService {
  api = this.authService.APPSERVER + 'eligibility/age/';
  constructor(private authService: AuthService, private http: HttpClient) { }



  // Age Criteria api
  gets_active(): Observable<any> {
    return this.http.get(this.api + 'gets');
  }
  getAll(): Observable<any>{
    return this.http.get(this.api + 'getAll/');
  }

  get(id): Observable<any>{
    return this.http.get(this.api + 'get/' + id);
  }

  edit(id: number, obj: any): Observable<any>{
    return this.http.put(this.api + 'edit/' + id, obj);
  }

  delete(id): Observable<any>{
    return this.http.delete(this.api + 'delete/' + id);
  }

  save(obj: any): Observable<any>{
    return this.http.post(this.api + 'add/', obj);
  }
}