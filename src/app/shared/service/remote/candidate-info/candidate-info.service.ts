import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CandidateInfoService {
  public api = this.auth.APPSERVER;

  constructor(private http: HttpClient, private auth: AuthService) {
  }

  getCandidateInfoById(id: number): Observable<any>{
    return this.http.get(this.api + 'candidate/profile/' + id);
  }
  getCandidateInfoByCandidateId(id: number): Observable<any>{
    return this.http.get(this.api + 'candidate/profileByCandidateId/' + id);
  }
}
