import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';
import { UserCreationVm } from '../../../../entity/UserCreationVm';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = this.auth.APPSERVER + 'user';
  userCurl = this.auth.APPSERVER + 'user';
  constructor(private http: HttpClient, private auth: AuthService) {}
  get(userId: string) {
    return this.http.get(this.url + '/GetUser', {
      params: new HttpParams().set('userId', userId)
    });
  }
  save(model: UserCreationVm) {
    return this.http.put(this.url + '/create', model);
  }
  update(model: UserCreationVm) {
    return this.http.post(this.url + '/UpdateUser', model);
  }
  getAll(userName: string) {
    return this.http.get(this.url + '/GetAll', {
      params: new HttpParams().set('userName', userName)
    });
  }

  getByRole(roleName: string) {
    const params = new HttpParams().set('roleName', roleName);
    return this.http.get(this.url + '/role', { params: params });
  }

  getUserByRoleId(roleId): Observable<any>{
    return this.http.get(this.url + '/getUserByRoleId/' + roleId);
  }

  resetPassword(userId: string) {
    return this.http.get(this.url + '/ResetPassword', {
      params: new HttpParams().set('userId', userId)
    });
  }
  saveCollectionAdmin(obj: any) {
    return this.http.put(this.url + '/create', obj);
  }
  uploadPicture(modelId: Number, formdata: FormData): Observable<any> {
    if (formdata.has('modelId')) {
      formdata.delete('modelId');
    }
    formdata.append('modelId', modelId.toString());
    return this.http.post(this.url + '/uploadpic', formdata);
  }

  getByEmail(email: string) {
    return this.http.get(this.url + '/getUserInfoByEmail', {
      params: new HttpParams().set('email', email)
    });
  }
  getUserDashboard() {
    return this.http.get<RoleWiseDashboard[]>(this.url + '/GetUserDashboard');
  }
}
