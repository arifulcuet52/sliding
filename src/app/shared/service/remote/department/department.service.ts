import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  url = this.auth.APPSERVER + 'department/';
  constructor(private auth: AuthService, private http: HttpClient) { }
  getAll() {
    return this.http.get(this.url + 'all');
  }
}
