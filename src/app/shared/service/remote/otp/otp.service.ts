import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { OTPDeliveryMethodEnum } from '../../../../entity/otp-delivery-method-enum';
import { OTPVm } from '../../../../entity/otpVm';
import { OTPIssueReasonEnum } from '../../../../entity/otp-issue-reason-enum';

@Injectable({
  providedIn: 'root'
})
export class OtpService {
  otpVm: OTPVm = new OTPVm();
  otpEmail: string;
  url = this.auth.APPSERVER + 'OTP/';
  otpDeliveryMethod: OTPDeliveryMethodEnum;
  constructor(private auth: AuthService, private http: HttpClient) { }

  generateOTP(deliveryMethod: OTPDeliveryMethodEnum, reason: OTPIssueReasonEnum, email: string) {
    return this.http.get(this.url + 'Getotp',
      {
        params: new HttpParams()
          .set('deliveryMethod', deliveryMethod.toString())
          .set('reason', reason.toString())
          .set('email', email)
      });
  }
  regenerateOTP(deliveryMethod: OTPDeliveryMethodEnum, reason: OTPIssueReasonEnum, email: string) {
    return this.http.get(this.url + 'ResendOTP',
      {
        params: new HttpParams()
          .set('deliveryMethod', deliveryMethod.toString())
          .set('reason', reason.toString())
          .set('email', email)
      });
  }
  setOTPDeliveryMethod(deliveryMethod: OTPDeliveryMethodEnum) {
    this.otpDeliveryMethod = deliveryMethod;
  }
  getOTPDeliveryMethod(): OTPDeliveryMethodEnum {
    return this.otpDeliveryMethod;
  }
  setOtp(otpVm: OTPVm) {
    this.otpVm = otpVm;
  }
  getOTP() {
    //  this.otpVm = new OTPVm();
    // this.otpVm.allowAfter=10;
    // this.otpVm.path= (1).toString();
    return this.otpVm;
  }
  setOTPMail(email: string) {
    this.otpEmail = email;
  }
  getOTPMail() {
    return this.otpEmail;// ="mustafizur@bs-23.net";
  }
}
