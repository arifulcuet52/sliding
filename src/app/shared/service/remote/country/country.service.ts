import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CountryService {
  url = this.auth.APPSERVER + 'country/';
  constructor(private auth: AuthService, private http: HttpClient) { }
  public getAll() {
    return this.http.get(this.url + 'all');
  }
}
