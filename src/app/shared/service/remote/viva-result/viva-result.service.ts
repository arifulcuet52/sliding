import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VivaResultService {
  api = this.auth.APPSERVER + 'viva/';
  constructor(private auth: AuthService, private http: HttpClient) {}

  get_candidate_viva_result(searchText, pageSize: number, pageNumber: number, filterText: string): Observable<any> {
    const url =
      this.api +
      'candidate-viva-results/' +
      pageSize +
      '/' +
      pageNumber +
      '/' +
      searchText;
    return this.http.get(url, { params: new HttpParams().set('filterText', filterText)});
  }

  get(id): Observable<any>{
    return this.http.get(this.api + 'get/' + id);
  }

  save(obj: any): Observable<any>{
    return this.http.post(this.api + 'add/', obj);
  }

  edit(id: number, obj: any): Observable<any>{
    return this.http.put(this.api + 'edit/' + id, obj);
  }

  delete(id): Observable<any>{
    return this.http.delete(this.api + 'delete/' + id);
  }
}
