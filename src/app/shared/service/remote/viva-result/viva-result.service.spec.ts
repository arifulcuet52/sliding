import { TestBed, inject } from '@angular/core/testing';

import { VivaResultService } from './viva-result.service';

describe('VivaResultService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VivaResultService]
    });
  });

  it('should be created', inject([VivaResultService], (service: VivaResultService) => {
    expect(service).toBeTruthy();
  }));
});
