import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApplicationUserService {
  url = this.auth.APPSERVER + 'User/';

  constructor(private auth: AuthService, private http: HttpClient) { }

  getUserProfileView(): Observable<any>{
    return this.http.get(this.url + 'getUserProfileView');
  }
}
