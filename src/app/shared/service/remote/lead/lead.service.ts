import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LeadService {

  public api = this.authService.APPSERVER ;

  constructor(private authService: AuthService, private httpClient: HttpClient) { }

  getAlllead(pageSize: number, pageNumber: number, leadGroup: string,  leadType?: number, searchText?: string): Observable<any>{

    return this.httpClient.get(this.api + 'lead/all', {
      params: new HttpParams()
        .set('pageSize', pageSize.toString())
        .set('pageNumber', pageNumber.toString())
        .set('leadGroup', leadGroup)
        .set('leadType', leadType.toString())
        .set('searchText', searchText)
    });
  }

  getDropdownLeadStatus(): Observable<any>{
    return this.httpClient.get(this.api + 'leadaction/all');
  }

  updateStatus(body: any = null): Observable<any>{
    return this.httpClient.put(this.api + 'lead/updatemaster/', body);
  }

  SelfAssign(body: any = null): Observable<any>{
    return this.httpClient.put(this.api + 'lead/selfassign/', body);
  }

  getFilteredLeads(searchText: string){
    const url = this.api + 'lead/search?searchText=' + searchText;
    return this.httpClient.get(url);
  }

  getCurrentUserInfo(email: string){
    const url = this.api + 'User/getUserInfoByEmail?email=' + email;
    return this.httpClient.get(url);
  }

  getDesignation(): Observable<any>{
    return this.httpClient.get(this.api + 'account/getdesignation');
  }

  //reference activity dialog
  getLeadActionStatusByType(type: string): Observable<any>{
    return this.httpClient.get(this.api + 'leadaction/getByType/' + type);
  }

  getSubActionsByActionId(actionId: number): Observable<any>{
    return this.httpClient.get(this.api + 'leadaction/subAction/getByAction/' + actionId);
  }
}
