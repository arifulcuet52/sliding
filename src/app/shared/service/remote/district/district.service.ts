import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpParams, HttpClient } from '@angular/common/http';
import { District } from '../../../../entity/district';

@Injectable({
  providedIn: 'root'
})
export class DistrictService {

  url = this.auth.APPSERVER + 'district/';
  constructor(private auth: AuthService, private http: HttpClient) { }

  public getAllByCountryCode(countryCode: number) {
    // return this.http.get(this.url + 'get/' + countryCode);
    return this.http.get(this.url + 'all', { params: new HttpParams().set('countryCode', countryCode.toString()) });
  }
  public getAll() {
    return this.http.get(this.url + 'all');
  }
  public get(districtId: number) {
    // return this.http.get(this.url + 'get/' + countryCode);
    return this.http.get(this.url + districtId);
  }
  public delete(districtId: number) {
    // return this.http.get(this.url + 'get/' + countryCode);
    return this.http.delete(this.url + 'delete/' + districtId);
  }
  public add(district: District) {
    // return this.http.get(this.url + 'get/' + countryCode);
    return this.http.post(this.url + 'add', district);
  }
  public update(district: District) {
    // return this.http.get(this.url + 'get/' + countryCode);
    return this.http.put(this.url + 'edit/' + district.districtId, district);
  }

}
