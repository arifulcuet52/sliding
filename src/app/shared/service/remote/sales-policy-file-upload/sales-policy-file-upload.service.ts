import { AuthService } from './../../auth/auth.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class SalesPolicyFileUploadService {
  url = this.auth.APPSERVER;

  constructor(private http: HttpClient, private auth: AuthService) { }

  saveFile(formData: FormData): Observable<any> {
    return this.http.post<any>(this.url + 'sales/policy/savefile' , formData);
  }
}
