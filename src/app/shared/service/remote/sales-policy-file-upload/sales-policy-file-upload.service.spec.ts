import { TestBed, inject } from '@angular/core/testing';

import { SalesPolicyFileUploadService } from './sales-policy-file-upload.service';

describe('SalesPolicyFileUploadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SalesPolicyFileUploadService]
    });
  });

  it('should be created', inject([SalesPolicyFileUploadService], (service: SalesPolicyFileUploadService) => {
    expect(service).toBeTruthy();
  }));
});
