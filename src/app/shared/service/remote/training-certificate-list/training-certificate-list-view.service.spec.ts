import { TestBed, inject } from '@angular/core/testing';

import { TrainingCertificateListViewService } from './training-certificate-list-view.service';

describe('TrainingCertificateListViewService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrainingCertificateListViewService]
    });
  });

  it('should be created', inject([TrainingCertificateListViewService], (service: TrainingCertificateListViewService) => {
    expect(service).toBeTruthy();
  }));
});
