import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpParams, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

@Injectable({
  providedIn: 'root'
})
export class TrainingCertificateListViewService {

  url = this.auth.APPSERVER + 'training/certificate';
  constructor(private auth: AuthService, private http: HttpClient) { }

  public get(pageSize: number, pageNumber: number, filterText: string, searchText: string) {
    return this.http.get(`${this.url}/list/view`, {
      params: new HttpParams()
        .set('pageSize', pageSize.toString())
        .set('pageNumber', pageNumber.toString())
        .set('filterText', filterText)
        .set('searchText', searchText)
    });
  }

  public trainingCertificatePrint(fromDate: Date, toDate: Date, searchText: string): any {
    return this.http.get<any>(`${this.url}/print`, {
     // responseType: 'blob' as 'json',
      params: new HttpParams()
        .set('fromDate', !fromDate ? '' : fromDate.toISOString())
        .set('toDate', !toDate ? '' : toDate.toISOString())
        .set('searchText', searchText)
    });
  }
}
