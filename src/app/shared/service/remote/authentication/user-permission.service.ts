import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserPermissionService {
  url = this.authService.APPSERVER + 'UserPermission';
  constructor(private http: HttpClient, private authService: AuthService) {}

  public Get(userId: string, isShownAp: string, pageName: string) {
    return this.http.get(this.url, {
      params: new HttpParams()
        .set('userId', userId)
        .set('isShownInAp', isShownAp)
        .set('pageName', pageName)
    });
  }
  public GetByUserId(userId: number, isShownAp: string, pageName: string) {
    return this.http.get(
      this.url +
        '/GetByUserId/' +
        userId +
        '/' +
        isShownAp +
        '/' +
        pageName +
        '/'
    );
  }
  public UserAssignToRole(usersId: string[], rolesId: string[]) {
    return this.http.post(this.url, { usersId, rolesId });
  }
}
