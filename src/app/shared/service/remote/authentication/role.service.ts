import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';
import { Role } from '../../../../entity/role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  url = this.authService.APPSERVER + 'role';
  constructor(private authService: AuthService, private http: HttpClient) {}

  public getAll() {
    return this.http.get(this.url);
  }
  public get(id: string) {
    return this.http.get(this.url, { params: new HttpParams().set('id', id) });
  }
  public save(role: Role) {
    return this.http.put(this.url, role);
  }
  public update(role: Role) {
    return this.http.post(this.url, role);
  }
  public remove(roleId: string) {
    return this.http.delete(this.url, {
      params: new HttpParams().set('id', roleId)
    });
  }
}
