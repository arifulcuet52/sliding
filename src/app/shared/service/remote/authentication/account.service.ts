import { Injectable, EventEmitter } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { UserMenuVm } from '../../../../entity/user-menu';
import { PasswordResetAttemptOptsEnum } from 'src/app/entity/password-reset-attempt-opt-enum';
import { Observable } from 'rxjs/Observable';
import { share, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  userMenus: UserMenuVm[] = [];
  passwordStatus: number;
  message: string;
  unauthrizeMessage: string;
  menuEmitter: EventEmitter<UserMenuVm[]> = new EventEmitter<UserMenuVm[]>();
  public logInEmitter = new EventEmitter<{ isLogedin: boolean }>();
  private isLogedIn = false;
  private _headers = new HttpHeaders().set(
    'Content-Type',
    'application/x-www-form-urlencoded'
  );
  constructor(private authService: AuthService, private http: HttpClient) { }

  public Login(model: { email; password }) {
    this.message = '';
    const postString =
      'username=' +
      model.email +
      '&password=' +
      model.password +
      '&grant_type=password' +
      '&portal=AP';
    // return this.http.post(this.authService.APPSERVER + 'token', postString, {
    //   headers: this._headers
    // });

    const data = this.http.post(
      this.authService.APPSERVER + 'token',
      postString,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json; charset=utf-8',
          'Access-Control-Allow-Origin': '*'
        })
      }
    );

    return data;
  }

  public refreshToken() {
    const postString =
      'grant_type=refresh_token&refresh_token=' + this.getRefreshToken();
    return this.http
      .post(this.authService.APPSERVER + 'token', postString, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json; charset=utf-8',
          'Access-Control-Allow-Origin': '*'
        })
      })
      .pipe(
        share(),
        map(res => {
          this.setRefreshTokenValue(res);
          return res['access_token'];
        })
      );
  }

  setRefreshTokenValue(value) {
    const isExist = JSON.parse(localStorage.getItem('AD'));
    if (isExist) {
      isExist.token = value['access_token'];
      isExist.expires = value['.expires'];
      isExist.refreshToken = value['refresh_token'];
      localStorage.setItem('AD', JSON.stringify(isExist));
    }
  }
  public getRefreshToken(): string {
    const res = JSON.parse(localStorage.getItem('AD'));
    if (res) {
      return res.refreshToken;
    }
    return '';
  }
  public logout(logOutMethod: string) {
    this.isLogedIn = false;
    const token = JSON.parse(localStorage.getItem('AD'));
    this.isLogedIn = false;
    this.logInEmitter.next({ isLogedin: this.isLogedIn });
    if (token) {
      this.http
        .post(this.authService.APPSERVER + 'Account/logout', null, {
          params: new HttpParams()
            .set('logoutMethod', logOutMethod)
            .set('userid', token.userid)
        })
        .subscribe();
      localStorage.removeItem('AD');
      this.message = '';
    }
  }

  public incrementTabCount() {
    let res = JSON.parse(localStorage.getItem('TAB'));
    if (res) {
      res = +res + 1;

      //  console.log(res);
      localStorage.setItem('TAB', JSON.stringify(res));
    } else {
      localStorage.setItem('TAB', (1).toString());
    }
  }
  public decremntTabCount() {
    let res = JSON.parse(localStorage.getItem('TAB'));
    if (res) {
      res = +res - 1;
      //  console.log(res);
      localStorage.setItem('TAB', JSON.stringify(res));
    }
  }

  public tabCount() {
    const res = JSON.parse(localStorage.getItem('TAB'));
    if (res) {
      return res.toString();
    }
    return 0;
  }

  public getUserName(): string {
    const res = JSON.parse(localStorage.getItem('AD'));
    if (res) {
      return res.username.toString();
    }
    return '';
  }
  public getUserEmail(): string {
    const res = JSON.parse(localStorage.getItem('AD'));
    if (res) {
      return res.userid.toString();
    }
    return '';
  }

  public set_token(res) {
    const isExist = localStorage.getItem('AD');
    // localStorage.setItem(
    //   'AD',
    //   JSON.stringify({
    //     userid: JSON.parse(JSON.stringify(res)).userName,
    //     token: JSON.parse(JSON.stringify(res)).access_token,
    //     expires: JSON.parse(JSON.stringify(res))['.expires'],
    //     menus: JSON.parse(JSON.stringify(res)).menus,
    //     username: JSON.parse(JSON.stringify(res)).userFullName,
    //     idelTime: JSON.parse(JSON.stringify(res)).idelTime
    //   })
    // );

    localStorage.setItem(
      'AD',
      JSON.stringify({
        userid: res.userName,
        token: res.access_token,
        expires: res['.expires'],
        menus: res.menus,
        username: res.userFullName,
        idelTime: res.idelTime,
        roles: res.roles,
        refreshToken: res.refresh_token,
        campaignCode: res.campaignCode,
        profilePicUrl: res.profilePicUrl,
        components:
          typeof res.components === 'undefined'
            ? '[]'
            : this.recursive(JSON.parse(res.components))
      })
    );
    // this.incrementTabCount();

    // this.setIdleTime();
    this.isLogedIn = true;
    this.logInEmitter.next({ isLogedin: this.isLogedIn });
    this.menuEmitter.next(this.getUserMenu());
  }
  public isLogIn() {
    if (!(localStorage.getItem('AD') === null) || this.isLogedIn) {
      return true;
    } else {
      return false;
    }
  }
  public getUserMenu(): UserMenuVm[] {
    const res = JSON.parse(localStorage.getItem('AD'));
    if (res) {
      const menus = JSON.parse(res.menus);
      return menus;
    } else {
      return [];
    }
  }
  public getUserRoles(): string[] {
    const res = JSON.parse(localStorage.getItem('AD'));
    if (res) {
      const roles = JSON.parse(res.roles);
      return roles;
    } else {
      return [];
    }
  }
  public getRoleComponents(): SiteComponentVm[] {
    const res = JSON.parse(localStorage.getItem('AD'));
    if (res) {
      const components = res.components;
      return components;
    } else {
      return [];
    }
  }
  public canShow(name: string, parentComponent: string) {
    const res = this.getRoleComponents();
    return (
      res.filter(x => x.name === name && x.parentComponent === parentComponent)
        .length > 0
    );
    // this.recursive(res);
  }

  recursive(node: SiteComponentVm[]) {
    let result = [];
    node.forEach(element => {
      if (element.children.length === 0) {
        result.push(element);
      } else if (element.children.length > 0) {
        const temp = Object.assign({}, element);
        temp.children = [];
        result.push(temp);
      }
      result = result.concat(this.recursive(element.children));
    });
    return result;
  }

  public getIdleTime(): number {
    const res = JSON.parse(localStorage.getItem('AD'));
    return +res.idelTime;
  }

  public setIdleTime() {
    this.http
      .get(this.authService.APPSERVER + 'generalsetting/getuseridletime')
      .subscribe(
        success => {
          const res = JSON.parse(localStorage.getItem('AD'));
          res['idleTime'] = +success;
          localStorage.setItem('AD', JSON.stringify(res));
        },
        error => { }
      );
  }
  public passwordChangeReason(reson: PasswordResetAttemptOptsEnum) {
    this.passwordStatus = reson;
  }
  public getPasswordChangeReason() {
    return this.passwordStatus;
  }
  public changePassowrd(
    email: string,
    oldPassword: string,
    newPassword: string,
    confirmPassword: string
  ) {
    console.log(this.passwordStatus);
    return this.http.post(
      this.authService.APPSERVER + 'Account/ChangePassword',
      {
        oldPassword: oldPassword,
        newPassword: newPassword,
        confirmPassword: confirmPassword,
        changeReason: this.passwordStatus
      }
    );
  }
  public changePassowrdByURL(
    email: string,
    newPassword: string,
    confirmPassword: string,
    token: string
  ) {
    return this.http.post(
      this.authService.APPSERVER + 'Account/ChangePasswordByUrl',
      {
        email: email,
        newPassword: newPassword,
        confirmPassword: confirmPassword,
        changeReason: this.passwordStatus,
        token: token
      }
    );
  }
  public forgetPassowrd(
    email: string,
    oldPassword: string,
    newPassword: string,
    confirmPassword: string,
    otpCode: string
  ) {
    console.log(otpCode);
    return this.http.post(
      this.authService.APPSERVER + 'Account/ForgetPassword',
      {
        email: email,
        newPassword: newPassword,
        confirmPassword: confirmPassword,
        changeReason: this.passwordStatus,
        otpCode: otpCode
      }
    );
  }
  setMessage(message: string) {
    this.message = message;
  }
  getMessage() {
    return this.message;
  }
  setUnauthrize(message: string) {
    this.unauthrizeMessage = message;
  }
  getUnauthrize() {
    return this.unauthrizeMessage;
  }

  isTraingAdmin() {
    let result = false;
    const menus = this.getUserMenu();
    for (let i = 0; i < menus.length; i++) {
      if (menus[i].path.includes('training-admin')) {
        result = true;
        break;
      }
      for (let index = 0; index < menus[i].list.length; index++) {
        if (menus[i].list[index].path.includes('training-admin')) {
          result = true;
          break;
        }
      }
    }
    return result;
  }

  isAgencyServiceAdmin() {
    return this.findInMenu('ap/agent-service-admin');
  }

  isCollectionAdmin() {
    return this.findInMenu('collection-admin-dashboard');
  }

  getDashboardPath() {
    const roles = this.getUserRoles().map(x => x.toLocaleLowerCase());
    if (
      roles.find(
        x => x === 'UM'.toLocaleLowerCase() || x === 'BM'.toLocaleLowerCase()
      )
    ) {
      return 'ap/um-bm-dashboard';
    } else if (
      roles.find(
        x =>
          x === 'Admin'.toLocaleLowerCase() ||
          x === 'UM'.toLocaleLowerCase() ||
          x === 'BM'.toLocaleLowerCase()
      )
    ) {
      return 'ap/admin-dashboard';
    } else {
      return '/ap/dashboard';
    }
  }

  findInMenu(menuName: string) {
    let result = false;
    const menus = this.getUserMenu();
    for (let i = 0; i < menus.length; i++) {
      if (menus[i].path.includes(menuName)) {
        result = true;
        break;
      }
      for (let index = 0; index < menus[i].list.length; index++) {
        if (menus[i].list[index].path.includes(menuName)) {
          result = true;
          break;
        }
      }
    }
    return result;
  }

  validateShortURL(shortCode: string): Observable<any> {
    return this.http.post(
      this.authService.APPSERVER + 'account/validateShortURL',
      null,
      {
        params: new HttpParams().set('key', shortCode)
      }
    );
  }

  setTempEmail(email: string) {
    localStorage.setItem(
      'TempEmail',
      JSON.stringify({
        userid: email
      })
    );
  }

  getTempEmail(): string {
    const res = JSON.parse(localStorage.getItem('TempEmail'));
    if (res) {
      return res.userid.toString();
    }
    return '';
  }

  public getToken(): string {
    const res = JSON.parse(localStorage.getItem('AD'));
    if (res) {
      return res.token.toString();
    }
    return '';
  }
  checkRole(roleName: string): boolean {
    const roles = this.getUserRoles();
    return (
      roles.filter(x => x.toLocaleLowerCase() === roleName.toLocaleLowerCase())
        .length > 0
    );
  }

  getDashboardContent() {
    const res = JSON.parse(localStorage.getItem('AD'));
    if (res.dashboard) {
      return JSON.parse(res.dashboard);
    }
    return [];
  }
  setDashboardContent(value) {
    const isExist = JSON.parse(localStorage.getItem('AD'));
    if (isExist) {
      isExist.dashboard = JSON.stringify(value);
      localStorage.setItem('AD', JSON.stringify(isExist));
    }
  }

  set_profile_pic_url(url: string){
    const obj = JSON.parse(localStorage.getItem('AD'));
    if (obj != null) {
      obj['profilePicUrl'] = url;
      localStorage.setItem('AD', JSON.stringify(obj));
    }
  }

  get_profile_pic_url(){
    const obj = JSON.parse(localStorage.getItem('AD'));
    if (obj != null && obj.hasOwnProperty('profilePicUrl')) {
     return obj['profilePicUrl'];
    }else{
      return '';
    }
  }
}
