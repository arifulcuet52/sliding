import { Injectable } from '@angular/core';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { RolePermission } from '../../../../entity/role-permission';

@Injectable({
  providedIn: 'root'
})
export class RolePermissionService {
  url = this.authService.APPSERVER + 'RolePermission';
  constructor(private authService: AuthService, private http: HttpClient) { }

  GetAll(roleId: string) {
    return this.http.get(this.url, { params: new HttpParams().set("roleId", roleId) });
  }
  menuAssign(rolePermission: RolePermission[]) {
    return this.http.post(this.url, rolePermission);
  }

}
