import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Menu } from '../../../../entity/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  url = this.authService.APPSERVER + 'menu';
  constructor(private authService: AuthService, private http: HttpClient) {}

  public getAll(searchText: string, pageSize: number, pageNumber: number) {
    return this.http.get(this.url, {
      params: new HttpParams()
        .set('searchText', searchText)
        .set('pageSize', pageSize.toString())
        .set('pageNumber', pageNumber.toString())
    });
  }

  public GetDropdownList() {
    return this.http.get(this.url + '/GetDropdownList');
  }
  public get(menuId: string) {
    return this.http.get(this.url, {
      params: new HttpParams().set('id', menuId)
    });
  }
  public insert(menu: Menu) {
    return this.http.put(this.url, menu);
  }
  public update(menu: Menu) {
    return this.http.post(this.url, menu);
  }
  public remove(menuId: string) {
    return this.http.delete(this.url, {
      params: new HttpParams().set('id', menuId)
    });
  }
}
