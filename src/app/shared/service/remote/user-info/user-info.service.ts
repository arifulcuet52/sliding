import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ColleactionAdminVm } from 'src/app/entity/CollectionAdminCreation';

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {
  api = this.auth.APPSERVER + 'userInfo/';


  constructor(private http: HttpClient, private auth: AuthService) { }

  getall(searchText, pageSize: number, pageNumber: number): Observable<any> {
    const url = this.api + 'all/' + pageSize + '/' + pageNumber + '/' + searchText;
    return this.http.get(url);
  }

  getById(id: number): Observable<any> {
    return this.http.get(this.api + 'getById/' + id);
  }

  getByAgentId(id): Observable<any> {
    return this.http.get(this.api + 'getByAgentId/' + id);
  }

  update(obj: any): Observable<any> {
    return this.http.put(this.api + 'updateInfo/', obj);
  }

  create(obj: any): Observable<any> {
    console.log(obj);
    return this.http.post(this.api + 'add/', obj);
  }

  upload_picture(modelId: Number, formdata: FormData): Observable<any> {
    if (formdata.has('modelId')) {
      formdata.delete('modelId');
    }
    formdata.append('modelId', modelId.toString());
    return this.http.post(this.api + 'uploadpic/', formdata);
  }

  saveCollectionAdmin(obj: any) {
    return this.http.post(this.api + 'CreateCollectionAdmin', obj);
  }

  getUmBmUser(): Observable<any>{
    return this.http.get(this.api + 'getUmBm');
  }
}
