import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  public api = this.authService.APPSERVER;
  public endpoint = `${this.api}/contact`;

  constructor(private authService: AuthService, private http: HttpClient) { }

  getAll(searchText: string, pageNumber: number, pageSize: number) {
    const data = this.http.get(this.endpoint, {
      params: new HttpParams()
        .set('searchText', searchText)
        .set('pageNumber', pageNumber.toString())
        .set('pageSize', pageSize.toString())
    });

    return data;
  }
}
