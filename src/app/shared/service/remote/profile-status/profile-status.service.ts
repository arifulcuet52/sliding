import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfileStatusService {
  url = this.auth.APPSERVER + "ProfileStatus";
  public refStatusVm= new Subject();
  public personalStatusVm= new Subject();
  public academicStatusVm= new Subject();
  public bankStatusVm= new Subject();

  constructor(private auth: AuthService, private http: HttpClient) { }

  getByUserId() {
    return this.http.get(this.url + "/getByUserId");
  }

  getAllProfileStatusByCandidate(candidateId: number) {
    return this.http.get(this.url + "/getProfileStatusByCandidate/" + candidateId);
  }
}
