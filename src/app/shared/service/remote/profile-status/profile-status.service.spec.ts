import { TestBed, inject } from '@angular/core/testing';

import { ProfileStatusService } from './profile-status.service';

describe('ProfileStatusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfileStatusService]
    });
  });

  it('should be created', inject([ProfileStatusService], (service: ProfileStatusService) => {
    expect(service).toBeTruthy();
  }));
});
