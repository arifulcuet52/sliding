import { TestBed, inject } from '@angular/core/testing';

import { SecurityPoliciesService } from './security-policies.service';

describe('SecurityPoliciesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SecurityPoliciesService]
    });
  });

  it('should be created', inject([SecurityPoliciesService], (service: SecurityPoliciesService) => {
    expect(service).toBeTruthy();
  }));
});
