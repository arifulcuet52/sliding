import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import { Observable } from 'rxjs';
import { SecurityPolicies } from '../../../../entity/security-policies';

@Injectable({
  providedIn: 'root'
})
export class SecurityPoliciesService {
  api = this.authService.APPSERVER + 'securityPolicy/';
  // api: string = "assets/dummy/";
  constructor(private authService: AuthService, private http: HttpClient) { }

  getAll(): Observable<any>{
    return this.http.get(this.api + 'getAll/');
    // return this.http.get(this.api + "config.json");
  }

  get(id): Observable<any>{
    return this.http.get(this.api + 'getById/' + id);
    // return this.http.get(this.api + "security-policy.json");
  }

  edit(id: number, obj: any): Observable<any>{
    return this.http.put(this.api + 'edit/' + id, obj);
  }

  delete(id): Observable<any>{
    return this.http.delete(this.api + 'delete/' + id);
  }

  save(obj: any): Observable<any>{
    return this.http.post(this.api + 'add/', obj);
  }
}
