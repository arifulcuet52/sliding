import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { Area } from '../../../../entity/area';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  url = this.auth.APPSERVER + 'area/';
  constructor(private auth: AuthService, private http: HttpClient) { }

  public getAllByDistrict(districtId: number) {
    // return this.http.get(this.url + 'get/' + countryCode);
    return this.http.get(this.url + 'all/' + districtId);
  }
  public getAll() {
    // return this.http.get(this.url + 'get/' + countryCode);
    return this.http.get(this.url + 'all/');
  }
  public get(areaId: number) {
    // return this.http.get(this.url + 'get/' + countryCode);
    return this.http.get(this.url + 'get/' + areaId);
  }
  public delete(areaId: number) {
    // return this.http.get(this.url + 'get/' + countryCode);
    return this.http.delete(this.url + 'delete/' + areaId);
  }
  public add(area: Area) {
    // return this.http.get(this.url + 'get/' + countryCode);
    return this.http.post(this.url + 'add', area);
  }
  public update(area: Area) {
    // return this.http.get(this.url + 'get/' + countryCode);
    return this.http.put(this.url + 'edit/' + area.areaId, area);
  }
}
