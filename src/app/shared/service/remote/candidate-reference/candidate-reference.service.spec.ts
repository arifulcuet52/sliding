import { TestBed, inject } from '@angular/core/testing';

import { CandidateReferenceService } from './candidate-reference.service';

describe('CandidateReferenceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CandidateReferenceService]
    });
  });

  it('should be created', inject([CandidateReferenceService], (service: CandidateReferenceService) => {
    expect(service).toBeTruthy();
  }));
});
