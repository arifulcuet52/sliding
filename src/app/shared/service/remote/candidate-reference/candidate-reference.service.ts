import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CandidateReferenceService {
  private url = this.auth.APPSERVER + 'candidate/reference';
  private actionUrl = this.auth.APPSERVER + 'reference/action';

  constructor(private auth: AuthService, private http: HttpClient) {}
  getRefernceVerificationLIst(
    isFirstTimeLoad: boolean,
    searchText: string,
    pageIndex: number,
    pageSize: number
  ): Observable<any> {
    return this.http.get(
      this.url +
        '/getRefrenceVerificationList/' +
        isFirstTimeLoad +
        '/' +
        pageIndex +
        '/' +
        pageSize +
        '/' +
        searchText +
        '/'
    );
  }

  getRefVerificListBy(
    status: string,
    searchText: string,
    pageIndex: number,
    pageSize: number
  ): Observable<any> {
    status = status ? status.replace(/\s/g, '').toLowerCase() : '';
    return this.http.get(
      this.url +
        '/getRefVerificationListBy/' +
        status +
        '/' +
        pageIndex +
        '/' +
        pageSize +
        '/' +
        searchText +
        '/'
    );
  }

  getReferenceInfoByCandidateId(candidateId: number): Observable<any>{
    return this.http.get(this.url + '/getByCandidate/' + candidateId);
  }

  updateReferenceStatus(obj: any): Observable<any>{
    return this.http.put(this.url + '/update/status/' , obj);
  }

  updateRefVerificationQueue(obj: any): Observable<any>{
    return this.http.put(this.url + '/update/refVerificationQueue' , obj);
  }

  //admin action on reference verififcation
  submitReferenceAction(obj: any): Observable<any> {
    return this.http.post(this.actionUrl + "/add", obj);
  }

  getReferenceActions(referenceInfoId: number): Observable<any> {
    return this.http.get(this.actionUrl + "/getByRefInfoId/" + referenceInfoId);
  }

  getReferenceActionsByCandidate(candidateId: number): Observable<any> {
    return this.http.get(this.actionUrl + "/getByCandidateId/" + candidateId);
  }
}
