/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TrainingCertificateService } from './training-certificate.service';

describe('Service: TrainingCertificate', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrainingCertificateService]
    });
  });

  it('should ...', inject([TrainingCertificateService], (service: TrainingCertificateService) => {
    expect(service).toBeTruthy();
  }));
});
