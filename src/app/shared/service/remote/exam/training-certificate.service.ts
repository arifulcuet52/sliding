import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpParams, HttpClient } from '@angular/common/http';
import { SelectedCandidateVm } from 'src/app/entity/selected-candidate.vm';

@Injectable({
  providedIn: 'root'
})
export class TrainingCertificateService {

  url = this.auth.APPSERVER + 'training/certificate';
  constructor(private auth: AuthService, private http: HttpClient) { }

  public get(pageSize: number, pageNumber: number, searchText: string, fromDate: Date = null, toDate: Date = null) {
    if (fromDate != null) {
      fromDate.setHours(new Date().getHours());
    }
    if (fromDate != null) {
      toDate.setHours(new Date().getHours());
    }
    return this.http.get(`${this.url}/list`, {
      params: new HttpParams()
        .set('pageSize', pageSize.toString())
        .set('pageNumber', pageNumber.toString())
        .set('searchText', searchText)
        .set('fromDate', !fromDate ? '' : fromDate.toISOString())
        .set('toDate', !toDate ? '' : toDate.toISOString())
    });
  }

  public zip(candidateId: number) {
    const options = { responseType: 'arraybuffer' as 'json' };
    return this.http.get<any>(`${this.url}/zip/${candidateId}`, options);
  }

  public zipSelected(selectedCandidates: SelectedCandidateVm[]) {
    const options = { responseType: 'arraybuffer' as 'json' };
    return this.http.post<any>(`${this.url}/zip/all`, selectedCandidates, options);
  }

  public trainingCertificatePrint(fromDate: Date, toDate: Date, searchText: string): any {
    return this.http.get<any>(`${this.url}/print`, {
      // responseType: 'blob' as 'json',
      params: new HttpParams()
        .set('fromDate', !fromDate ? '' : fromDate.toISOString())
        .set('toDate', !toDate ? '' : toDate.toISOString())
        .set('searchText', searchText)
    });
  }
}
