import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../../../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class BookingService {


  api = this.auth.APPSERVER + 'exam/booking/';
  constructor(private http: HttpClient, private auth: AuthService) { }

  getall(searchText, pageSize: number, pageNumber: number): Observable<any> {
    const url = this.api + 'all/' + pageSize + '/' + pageNumber + '/' + searchText;
    return this.http.get(url);
  }
}
