import { TestBed, inject } from '@angular/core/testing';

import { ExamScheduleService } from './exam-schedule.service';

describe('ExamScheduleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExamScheduleService]
    });
  });

  it('should be created', inject([ExamScheduleService], (service: ExamScheduleService) => {
    expect(service).toBeTruthy();
  }));
});
