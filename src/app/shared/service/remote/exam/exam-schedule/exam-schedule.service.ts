import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpClientModule, HttpParams } from '@angular/common/http';
import { AuthService } from 'src/app/shared/service/auth/auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExamScheduleService {

  constructor(private authService: AuthService, private http: HttpClient) { }

  private api = this.authService.APPSERVER + 'examSchedule/';

  getAll(): Observable<any>{
    return this.http.get(this.api + 'getAllSchedules/');
  }

  public getBySearch(searchText: string, pageSize: number, pageNumber: number) {
    return this.http.get(this.api + 'getBySearch', {
      params: new HttpParams()
        .set('searchText', searchText)
        .set('pageSize', pageSize.toString())
        .set('pageNumber', pageNumber.toString())
    });
  }

  getById(id): Observable<any>{
    return this.http.get(this.api + 'getById/' + id);
  }

  edit(obj: any): Observable<any>{
    return this.http.put(this.api + 'edit/', obj);
  }

  delete(id): Observable<any>{
    return this.http.delete(this.api + 'delete/' + id);
  }

  save(obj: any): Observable<any>{
    return this.http.post(this.api + 'add/', obj);
  }

}
