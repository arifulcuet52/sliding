import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ExamSetting } from '../../../../entity/exam/exam-setting';

@Injectable({
  providedIn: 'root'
})

export class ExamSettingService {

  url = this.auth.APPSERVER + 'exam/setting';
  constructor(private auth: AuthService, private http: HttpClient) { }

  public get(searchText: string, pageSize: number, pageNumber: number) {
    return this.http.get(this.url, {
      params: new HttpParams()
        .set('searchText', searchText)
        .set('pageSize', pageSize.toString())
        .set('pageNumber', pageNumber.toString())
    });
  }

  public getById(examSettingId: number) {
    return this.http.get(`${this.url}/${examSettingId}`);
  }

  public getDropdown() {
    return this.http.get(`${this.url}/dropdown`);
  }

  public delete(examSettingId: number) {
    return this.http.delete(`${this.url}/${examSettingId}`);
  }

  public add(examSetting: ExamSetting) {
    return this.http.post(this.url, examSetting);
  }

  public update(examSetting: ExamSetting) {
    return this.http.put(this.url, examSetting);
  }
}
