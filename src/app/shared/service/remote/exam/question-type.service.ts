import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpParams, HttpClient } from '@angular/common/http';
import { QuestionType } from '../../../../entity/exam/question-type';

@Injectable({
  providedIn: 'root'
})
export class QuestionTypeService {

  url = this.auth.APPSERVER + 'question/type';
  constructor(private auth: AuthService, private http: HttpClient) { }

  public get(searchText: string, pageSize: number, pageNumber: number) {
    return this.http.get(this.url, {
      params: new HttpParams()
        .set('searchText', searchText)
        .set('pageSize', pageSize.toString())
        .set('pageNumber', pageNumber.toString())
    });
  }

  public getById(moduleId: number) {
    return this.http.get(`${this.url}/${moduleId}`);
  }

  public getDropdown() {
    return this.http.get(`${this.url}/dropdown`);
  }

  public delete(moduleId: number) {
    return this.http.delete(`${this.url}/${moduleId}`);
  }

  public add(module: QuestionType) {
    return this.http.post(this.url, module);
  }

  public update(module: QuestionType) {
    return this.http.put(this.url, module);
  }

}
