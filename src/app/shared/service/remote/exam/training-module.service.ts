import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TrainingModule } from '../../../../entity/training-module';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TrainingModuleService {

  url = this.auth.APPSERVER + 'module';
  constructor(private auth: AuthService, private http: HttpClient) { }

  getall(searchText, pageSize: number, pageNumber: number): Observable<any> {
    const url = this.url + '/getall/' + pageSize + '/' + pageNumber + '/' + searchText;
    return this.http.get(url);
  }

  public getDropdown() {
    return this.http.get(`${this.url}/dropdown`);
  }

  getById(id: number): Observable<any> {
    return this.http.get(this.url + '/get/' + id);
  }

  getByModuleType(type: number): Observable<any> {
    return this.http.get(this.url + '/getByModuleType/' + type);
  }

  get_tree(): Observable<any> {
    return this.http.get(this.url + '/gettree');
  }

  save(obj: any): Observable<any> {
    return this.http.post(this.url + '/add', obj);
  }

  edit(id: number, obj: any): Observable<any> {
    return this.http.put(this.url + '/edit/' + id, obj);
  }

  delete(id): Observable<any> {
    return this.http.delete(this.url + '/delete/' + id);
  }

  changeSortOrder(id: number, obj: any): Observable<any> {
    return this.http.put(this.url + '/changeSortOrder/' + id, obj);
  }

  fnAddLinkedSubModule(model: any) {
    let url = this.auth.APPSERVER + 'linked-training/addSubmodule';
    return this.http.post(url, model);
  }

  fnGetSubModuleInfoByModuleId(moduleId:number){
    let url = this.auth.APPSERVER + 'linked-training/getSubModuleSettingsByModuleId?moduleId='+moduleId;
    return this.http.get(url);
  }

  fnCheckDependencyBeforeDeleteSubModule(linkedSubModuleId:number){
    let url = this.auth.APPSERVER + 'linked-training/checkForRemoveSubModule?linkedSubModuleId='+linkedSubModuleId;
    return this.http.get(url);
  }

  fnGetAllModuleSettins(){
    let url = this.auth.APPSERVER + 'linked-training/GetAllModuleSettingsList';
    return this.http.get(url);
  }
  fnDeleteModuleSettingsById(moduleId:number){
    let url = this.auth.APPSERVER + 'linked-training/DeleteByModuleId?moduleId='+moduleId;
    return this.http.get(url);
  }
}
