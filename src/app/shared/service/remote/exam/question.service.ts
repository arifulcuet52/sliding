import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Question } from '../../../../entity/exam/question';

@Injectable({
  providedIn: 'root'
})

export class QuestionService {

  url = this.auth.APPSERVER + 'bank/question';
  constructor(private auth: AuthService, private http: HttpClient) { }

  public getByModule(moduleId: number) {
    return this.http.get(this.url + 'module/' + moduleId);
  }

  public get(searchText: string, pageSize: number, pageNumber: number) {
    return this.http.get(this.url, {
      params: new HttpParams()
        .set('searchText', searchText)
        .set('pageSize', pageSize.toString())
        .set('pageNumber', pageNumber.toString())
    });
  }

  public getById(questionId: number) {
    return this.http.get(`${this.url}/${questionId}`);
  }

  public getDropdown() {
    return this.http.get(`${this.url}/dropdown`);
  }

  public delete(questionId: number) {
    return this.http.delete(`${this.url}/${questionId}`);
  }

  public add(question: Question) {
    return this.http.post(this.url, question);
  }

  public update(question: Question) {
    return this.http.put(this.url, question);
  }
}
