import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { QuestionPerModule } from '../../../../entity/exam/question-per-module';

@Injectable({
  providedIn: 'root'
})

export class QuestionPerModuleService {

  url = this.auth.APPSERVER + 'exam/question-per-module';
  constructor(private auth: AuthService, private http: HttpClient) { }

  public get(searchText: string, pageSize: number, pageNumber: number) {
    return this.http.get(this.url, {
      params: new HttpParams()
        .set('searchText', searchText)
        .set('pageSize', pageSize.toString())
        .set('pageNumber', pageNumber.toString())
    });
  }

  public getById(questionPerModuleId: number) {
    return this.http.get(`${this.url}/${questionPerModuleId}`);
  }

  public getDropdown() {
    return this.http.get(`${this.url}/dropdown`);
  }

  public delete(questionPerModuleId: number) {
    return this.http.delete(`${this.url}/${questionPerModuleId}`);
  }

  public add(questionPerModule: QuestionPerModule) {
    return this.http.post(this.url, questionPerModule);
  }

  public update(questionPerModule: QuestionPerModule) {
    return this.http.put(this.url, questionPerModule);
  }
}
