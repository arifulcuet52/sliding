/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { QuestionPerModuleService } from './question-per-module.service';

describe('Service: QuestionPerModule', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuestionPerModuleService]
    });
  });

  it('should ...', inject([QuestionPerModuleService], (service: QuestionPerModuleService) => {
    expect(service).toBeTruthy();
  }));
});
