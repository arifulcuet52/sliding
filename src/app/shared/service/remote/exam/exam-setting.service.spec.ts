/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ExamSettingService } from './exam-setting.service';

describe('Service: ExamSetting', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExamSettingService]
    });
  });

  it('should ...', inject([ExamSettingService], (service: ExamSettingService) => {
    expect(service).toBeTruthy();
  }));
});
