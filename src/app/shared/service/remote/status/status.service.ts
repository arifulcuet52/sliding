import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class StatusService {

  private url = this.auth.APPSERVER + 'status';

  constructor(private http: HttpClient, private auth: AuthService) { }

  getAll() {
    return this.http.get(this.url + '/GetAllUserStatus');
  }

  getStatusFee(){
    return this.http.get(`${this.url}/fee`);
  }

  getStatusByType(type){
    return this.http.get(`${this.url}/GetStatus/` + type);
  }

}
