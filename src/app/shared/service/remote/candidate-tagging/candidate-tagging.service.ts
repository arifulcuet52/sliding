import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';
import { StatusEnum } from '../../../../entity/status.enum';

@Injectable({
  providedIn: 'root'
})
export class CandidateTaggingService {

  url = this.authService.APPSERVER;
  constructor(private http: HttpClient,
    private authService: AuthService
  ) { }

  getAllTagRequest(searchText: string, pageIndex: number, pageSize: number) {
    return this.http.get(this.url + 'tagging/TagRequestsByUmBm/' + searchText + '/' + pageIndex + '/' + pageSize);
  }
  changeTagStatus(status: StatusEnum, id: number) {
    return this.http.post(this.url + 'tagging/ChangeTagRequestStatus/' + id,
      null, { params: new HttpParams().set('status', status.toString()) });
  }
  getTag(id: number) {
    return this.http.get(this.url + 'tagging/GetTag/' + id);
  }
}
