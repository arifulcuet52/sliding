import { TestBed, inject } from '@angular/core/testing';

import { CandidateTaggingService } from './candidate-tagging.service';

describe('CandidateTaggingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CandidateTaggingService]
    });
  });

  it('should be created', inject([CandidateTaggingService], (service: CandidateTaggingService) => {
    expect(service).toBeTruthy();
  }));
});
