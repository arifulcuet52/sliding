import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private url = this.auth.APPSERVER;
  constructor(private http: HttpClient, private auth: AuthService) {}

  getUMBMPendingTagRequest() {
    return this.http.get(this.url + 'tagging/UserWisePendingTag');
  }
  getUMBMDashboard() {
    return this.http.get(this.url + 'dashboard/umbm');
  }
}
