import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from '../../auth/auth.service';
import { Observable } from 'rxjs';
import { NotificationTypeEnum } from '../../../../entity/enums/notification-type.enum';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private url = this.auth.APPSERVER ;
  private on_site_notific_url = this.auth.APPSERVER + "usernotification";
  clickORScrollInformEmitter = new EventEmitter<any>();

  constructor(private auth: AuthService, private http: HttpClient) { }

  notifyOnRefReject(candidateId: number): Observable<any>{
    return this.http.get(this.url + '/notificOnRefRejected/' + candidateId);
  }

  notifyOnRefAccept(candidateId: number): Observable<any>{
    return this.http.get(this.url + '/notificOnRefAccepted/' + candidateId);
  }

  notifyOnVerificationComplete(candidateId: number): Observable<any>{
    return this.http.get(this.url + '/notificOnVerificationComplete/' + candidateId);
  }

  //on-site notification
  getNotification(type: NotificationTypeEnum, pageNumber: number) {
    return this.http.get(this.on_site_notific_url + "/getusernotification", {
      params: new HttpParams()
        .set("type", type.toString())
        .set("pageNumber", pageNumber.toString())
    });
  }
  informClickEmitter() {
    this.clickORScrollInformEmitter.next(true);
  }

  updateReadStatus(id: number) {
    return this.http.post(this.on_site_notific_url + "/updatestatus/" + id, null);
  }
  getUserAllPendingNotification() {
    return this.http.get(this.on_site_notific_url + "/getUserAllPendingNotification");
  }
}
