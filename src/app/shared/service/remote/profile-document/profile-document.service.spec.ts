import { TestBed, inject } from '@angular/core/testing';

import { ProfileDocumentService } from './profile-document.service';

describe('ProfileDocumentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfileDocumentService]
    });
  });

  it('should be created', inject([ProfileDocumentService], (service: ProfileDocumentService) => {
    expect(service).toBeTruthy();
  }));
});
