import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { toDate } from '@angular/common/src/i18n/format_date';
import { DocumentVerificationBy } from '../../../../entity/document-verification-by.vm';

@Injectable({
  providedIn: 'root'
})
export class ProfileDocumentService {
  private url = this.auth.APPSERVER + 'profiledocumentdetail';
  constructor(private auth: AuthService, private httpClient: HttpClient) { }

  getDocumentVerificationLIst(
    search: string,
    isFirstTimeLoad: boolean
  ): Observable<any> {
    return this.httpClient.get(
      this.url + '/VerificationDocList/' + isFirstTimeLoad + '/' + search
    );
  }

  getDocumentVerificationBy(status: string, searchText: string, pageNumber: number = 1, pageSize: number = 10): Observable<any> {
    const params = new HttpParams()
      .set('status', status)
      .set('searchText', searchText)
      .set('pageNumber', pageNumber.toString())
      .set('pageSize', pageSize.toString());

    return this.httpClient.get(this.url + '/document-verification-by', { params: params });
  }

  reassign(model: DocumentVerificationBy) {
    const data = this.httpClient.post(this.url + '/reassign', model);

    return data;
  }
}
