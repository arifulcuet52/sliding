import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CertificateFeeCa } from 'src/app/entity/certificate-fee-ca';

@Injectable({
  providedIn: 'root'
})
export class CertificateService {

  public api = this.authService.APPSERVER;

  constructor(private authService: AuthService, private http: HttpClient) { }

  getNextCertificate(): Observable<any> {
    return this.http.get(this.api + 'certificatefee/getnext');
  }

  updateStatus(body: any = null): Observable<any> {
    return this.http.put(this.api + 'certificatefee/statusupdate/', body);
  }

  getFilteredCertificate(searchText: string) {
    const url = this.api + 'certificatefee/all?searchText=' + searchText;
    return this.http.get(url);
  }

  // #region collection supervisor
  getAllCa(status: string, searchText: string, pageIndex: number, pageSize: number) {
    status = status ? status.replace(/\s/g, '').toLowerCase() : '';
    const url = this.api + 'certificatefee/all/ca';
    const data = this.http.get(url, {
      params: new HttpParams()
        .set('status', status)
        .set('searchText', searchText)
        .set('pageIndex', pageIndex.toString())
        .set('pageSize', pageSize.toString())
    });

    return data;
  }

  reassign(model: CertificateFeeCa) {
    const url = this.api + 'certificatefee/reassign';
    const data = this.http.post(url, model);

    return data;
  }
  // #endregion
}
