import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  public loadingEventEmiter: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  showLoader = false;
  constructor() {}
  public start() {
    this.showLoader = true;
    this.loadingEventEmiter.next(true);
  }
  public stop() {
    this.showLoader = false;
    this.loadingEventEmiter.next(false);
  }
  public loader() {
    return this.showLoader;
  }
}
