import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  private url = this.authService.APPSERVER;
  private api_general_seetings = this.authService.APPSERVER + 'generalsetting/';
  private api_profile_document_detail =
    this.authService.APPSERVER + 'profiledocumentdetail';
  private api_document_upload_setting =
    this.authService.APPSERVER + 'documenuploadsetting';

  public accepEmiter: EventEmitter<any> = new EventEmitter<any>();
  public rejectEmiter: EventEmitter<any> = new EventEmitter<any>();
  constructor(private http: HttpClient, private authService: AuthService, private sanitizer: DomSanitizer) { }

  sendAcceptNotification() {
    this.accepEmiter.next(true);
  }
  sendRejectNotification(reason: string) {
    this.rejectEmiter.next(reason);
  }

  get_user_edu_label(userId): Observable<any> {
    return this.http.get(
      this.api_profile_document_detail + '/GetUserEduLabel/' + userId
    );
  }
  get_profileDocumentDetail(userId): Observable<any> {
    return this.http.get(
      this.api_profile_document_detail + '/getByUserId/' + userId
    );
  }
  get_document_upload_setting(): Observable<any> {
    return this.http.get(
      this.api_document_upload_setting + '/getallwithfileSize'
    );
  }
  get_image_settings(): Observable<any> {
    return this.http.get(this.api_general_seetings + 'image');
  }

  get_file_settings(): Observable<any> {
    return this.http.get(this.api_general_seetings + 'file');
  }

  acceptDocument(globalKey: string, candidateId: number) {
    return this.http.post(
      this.api_profile_document_detail +
      '/acceptDocument/' +
      globalKey +
      '/' +
      candidateId.toString(),
      null
    );
  }
  rejectDocument(globalKey: string, reason: string, candidateId: number) {
    return this.http.post(
      this.api_profile_document_detail + '/rejectDocument',
      {
        globalKey: globalKey,
        reason: reason,
        candidateId: candidateId
      }
    );
  }
  get_call_to_actions(portalName: string): Observable<any> {
    return this.http.get(this.url + 'user/getcalltoactions/' + portalName);
  }

  get_timeline_logs(userId: number) {
    return this.http.get(this.url + 'timeline/GetLeadEventsBy/' + userId);
  }

  sendDocumentVerificationNotification(canidateId: number) {
    return this.http.post(
      this.api_profile_document_detail +
      '/SendDocumentVerificationNotification/' +
      canidateId,
      null
    );
  }
  getCandidatesEligibleList(
    pageIndex: number,
    pageSize: number,
    searchText: string
  ) {
    return this.http.get(
      this.url +
      'EligibleCandidateForContract/GetAll/' +
      pageIndex +
      '/' +
      pageSize +
      '/' +
      searchText +
      '/'
    );
  }

  candiateSignin(candaiteId: number) {
    return this.http.post(
      this.url + 'EligibleCandidateForContract/CandidateSigned/' + candaiteId,
      null
    );
  }

  getFile(fileUrl: string, fileName?: string): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => this.http
      .get<Blob>(this.url + 'utility/getfile', {
        params: new HttpParams().set('fileUrl', fileUrl),
        responseType: 'blob' as 'json'
      })
      .subscribe((success: Blob) => {
        if (!fileName) {
          fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
        }
        this.downloadFile(success, success.type, fileName);
        resolve(true);
      },
        (error) => {
          // return new Promise<boolean>((resolve, reject) => { download: false };
          resolve(false);
        }
      ));
  }
  downloadFile(data: any, contentType: string, fileName: string) {
    const blob = new Blob([data], { type: contentType });
    const filename = fileName;
    if (window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      const a = document.createElement('a');
      document.body.appendChild(a);
      const url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = filename;
      a.click();
      setTimeout(() => {
        window.URL.revokeObjectURL(url);
        document.body.removeChild(a);
      }, 0);
    }
  }
  printDom(domId: string) {
    const divToPrint = document.getElementById(domId);
    console.log(divToPrint);
    if (divToPrint['tagName'] && divToPrint['tagName'] === 'IFRAME') {
      const fileUrl = divToPrint['src'];
      if (fileUrl) {
        this.http
          .get<Blob>(this.url + 'utility/getfile', {
            params: new HttpParams().set('fileUrl', fileUrl),
            responseType: 'blob' as 'json'
          }).subscribe((success: Blob) => {

            const blob = new Blob([success], { type: success.type });
            const blobUrl = URL.createObjectURL(blob);
            const iframe = document.createElement('iframe');
            iframe.style.display = 'none';
            iframe.src = blobUrl;
            document.body.appendChild(iframe);
            iframe.contentWindow.print();
          },
            (error) => {
            });
      }


    } else {
      // let htmlToPrint = '';
      // htmlToPrint += divToPrint.outerHTML;
      // const newWin = window.open('');
      // // newWin.document.write('<h3 align=\'center\'>Print Page</h3>');
      // newWin.document.write(htmlToPrint);
      // newWin.print();
      // newWin.close();

      const css = '@media print {' +
        'body * {' +
        ' visibility: hidden;' +
        ' -webkit-print-color-adjust: exact;' +
        ' color-adjust: exact;' +
        '}' +

        'html,body {' +
        ' height: 100vh;' +
        ' margin: 0 !important;padding: 0 !important;overflow: hidden;' +
        '  }' +

        '@page {' +
        '  size: auto;' +
        '  /* auto is the initial value */ ' +
        '  margin: 0mm; ' +
        ' /* this affects the margin in the printer settings */ ' +
        ' }' +
        '#' + domId + ',' + '#' + domId + ' *' +
        ' {' +
        ' visibility: visible;' +
        ' }' +
        '#' + domId + ' {' +
        'position: absolute;' +
        ' top: 0;' +
        ' left: 0;' +
        ' right: 0;' +
        ' margin-left: auto;' +
        ' margin-right: auto;' +
        ' }' +
        ' }'
        ;
      const style = document.createElement('style');
      style.type = 'text/css';
      style.innerHTML = css;
      window.document.getElementsByTagName('head')[0].appendChild(style);
      window.print();

    }

  }
}
