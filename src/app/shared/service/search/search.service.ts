import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  constructor(@Inject(DOCUMENT) private document: any) { }
  public populateLink(searchText: string): string[] {

    let arr = [], l = document.links;
    if (!searchText || searchText.length == 0) {
      return arr;
    }
    for (var i = 0; i < l.length; i++) {
      if (l[i].innerText && l[i].innerText.toLowerCase().includes(searchText.toLowerCase())) {
        if (arr.filter(x => x['innerText'].toLowerCase() == l[i].innerText.toLowerCase()).length == 0) {
          arr.push(l[i]);
        }
      }
    }
    return (arr);
  }
}
