import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { Values,LanguageSetting } from '../../../shared/models/multilingual'
import { Observable,of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MultilingualService {
  api = this.authService.APPSERVER;
  constructor(private authService: AuthService, private http: HttpClient) { }

  
  fnGetValues(language:string, type:string): Observable<any>{
    return this.http.get(this.api + 'multilingual/getData?language='+language+"&type="+type);
  }

  fnDeleteValue(id:number): Observable<any>{
    return this.http.get(this.api + 'multilingual/delete?id=' + id);
  }

  fnPostValue(value:Values): Observable<any>{
    return this.http.post(this.api + 'multilingual/add', value);
  }

  fnGetLanguageList():Observable<any>{
    return this.http.get(this.api + 'multilingual/language');
  }

   //#region Language Setting
   fnDeleteLanguageSetting(id:number): Observable<any>{
    return this.http.get(this.api + 'language-setting/delete?id=' + id);
  }

  fnPostLanguageSetting(obj:LanguageSetting): Observable<any>{
    return this.http.post(this.api + 'language-setting/add', obj);
  }

  fnGetLanguageSettings():Observable<any>{
    return this.http.get(this.api + 'language-setting/getData');
  }
   //#endregion Language Setting

}
