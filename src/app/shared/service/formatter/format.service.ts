import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormatService {

  constructor() { }
  date_format = 'DD/MM/YYYY';
  date_time_format = 'DD/MM/YYYY HH:mm';
}
