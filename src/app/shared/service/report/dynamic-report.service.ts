import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { AuthService } from "../auth/auth.service";

@Injectable({
    providedIn: 'root'
})

export class DynamicReportService {
    constructor(private http: HttpClient, private auth: AuthService) { }
    url = this.auth.APPSERVER + '/report/';

    fnGetReportParams(reportId: number) {
        let apiUrl = this.url + "GetDynamicReportParam?reportId=" + reportId;
        return this.http.get(apiUrl);
    }

    fnGenerateOptions(paramId: number, dataId: number) {
        let apiUrl = this.url + "GetDynamicOptions?paramId=" + paramId + '&dataId=' + dataId;
        return this.http.get(apiUrl);
    }

    fnGetReportData(params: any, reportId:number) {
        let apiUrl = this.url + 'GetDynamicReportData?reportId='+reportId;
        return this.http.get(apiUrl, { params: params})
    }

    fnGetReportTitle(reportId: number){
      const apiUrl = this.url + 'GetDynamicTitle?id=' + reportId;
      return this.http.get(apiUrl);
    }

}
