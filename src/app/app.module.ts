import { ExamBookingComponent } from './module/exam/exam-booking/exam-booking.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { ModalModule } from 'ngx-bootstrap';
import { DataTableModule } from 'angular-6-datatable';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DateparserPipe } from './shared/pipe/date-parser/dateparser.pipe';
import { AuthService } from './shared/service/auth/auth.service';
import { AppComponent } from './app.component';
import { SigninComponent } from './module/authentication/signin/signin.component';
import { RoleComponent } from './module/authentication/role/role.component';
import { UserPermissionComponent } from './module/authentication/user-permission/user-permission.component';
import { HomeComponent } from './module/home/home.component';
import { AuthInterceptor } from './shared/interceptor/auth.Interceptor';
import { RolePermissionComponent } from './module/authentication/role-permission/role-permission.component';
import { MenuComponent } from './module/authentication/menu/menu.component';
import { AdminDashboardComponent } from './module/admin-dashboard/admin-dashboard.component';
import { UserCreateComponent } from './module/user-create/user-create.component';
import { UserListComponent } from './module/user-list/user-list.component';
import { SecurityPoliciesComponent } from './module/security-policy/security-policies/security-policies.component';
import { SecurityPolicyCreateComponent } from './module/security-policy/security-policy-create/security-policy-create.component';
import { UserMenuCanActivateRouteGuardService } from './shared/route-gurd/user-menu-can-activate-route-guard.service';
import { UnauthorizedComponent } from './module/unauthorized/unauthorized.component';
import { NotFoundComponent } from './module/not-found/not-found.component';
import { SearchComponent } from './module/search/search.component';
import { IdelModalComponent } from './module/idel-modal/idel-modal.component';
import { ChangePasswordComponent } from './module/change-password/change-password.component';
import { AccountRecoveryComponent } from './module/account-recovery/account-recovery.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { GlobalSettingsComponent } from './module/global-settings/global-settings.component';
import { LoadingTestComponent } from './module/loading-test/loading-test.component';
import { LoadingSpinnerComponent } from './module/loading-spinner/loading-spinner.component';
import { AgeEligibilityComponent } from './module/eligibility-criteria-configuration/age-eligibility/age-eligibility.component';
// tslint:disable-next-line:max-line-length
import { EducationEligibilityComponent } from './module/eligibility-criteria-configuration/education-eligibility/education-eligibility.component';
import { EligibilityCriteriaComponent } from './module/eligibility-criteria/eligibility-criteria.component';
import { DialogEligibilityCriteriaComponent } from './dialog/dialog-eligibility-criteria/dialog-eligibility-criteria.component';
// import { EditorModule } from '@tinymce/tinymce-angular';

import { DistrictComponent } from './module/district/district.component';
import { AreaComponent } from './module/area/area.component';
import { DialogAgeEligibilityComponent } from './dialog/dialog-age-eligibility/dialog-age-eligibility.component';
import { DialogEducationEligibilityComponent } from './dialog/dialog-education-eligibility/dialog-education-eligibility.component';

import { DialogAreaComponent } from './dialog/dialog-area/dialog-area.component';
import { DialogEligibilityJobComponent } from './dialog/dialog-eligibility-job/dialog-eligibility-job.component';
import { DialogEligibilityMaritalStatusComponent } from './dialog/dialog-eligibility-marital-status/dialog-eligibility-marital-status.component';
import { MaritalStatusEligibilityComponent } from './module/eligibility-criteria-configuration/marital-status-eligibility/marital-status-eligibility.component';
import { JobExperienceEligibilityComponent } from './module/eligibility-criteria-configuration/job-experience-eligibility/job-experience-eligibility.component';
import { DialogDistrictComponent } from './dialog/dialog-district/dialog-district.component';
import { DialogMenuCreateComponent } from './dialog/dialog-menu-create/dialog-menu-create.component';
import { ProfileviewComponent } from './module/profileview/profileview.component';
import { UmBmCreateComponent } from './module/um-bm-user/um-bm-create/um-bm-create.component';
import { CandidateTagRequestComponent } from './module/tagging/candidate-tag-request/candidate-tag-request.component';
import { AccountActivationComponent } from './module/authentication/account-activation/account-activation.component';
import { UmBmListComponent } from './module/um-bm-user/um-bm-list/um-bm-list.component';

//#region
import { QuestionListComponent } from './module/exam/question/question-list/question-list.component';
import { ExamScheduleComponent } from './module/exam/exam-schedule/exam-schedule.component';
import { QuestionDialogComponent } from './module/exam/question/question-dialog/question-dialog.component';
import { DialogExamScheduleComponent } from './dialog/exam/dialog-exam-schedule/dialog-exam-schedule.component';
import { ExamSettingListComponent } from './module/exam/exam-setting/exam-setting-list/exam-setting-list.component';
//#endregion

import { Ng2ImgToolsModule } from 'ng2-img-tools';
import { CandidateprofileviewComponent } from './module/candidateprofileview/candidateprofileview.component';
import { ToastrModule } from 'ngx-toastr';
import { TinyEditorComponent } from './module/tiny-editor/tiny-editor.component';
import { TrainingAdminDashboardComponent } from './module/training-admin-dashboard/training-admin-dashboard.component';
// tslint:disable-next-line:max-line-length
import { QuestionPerModuleListComponent } from './module/exam/question-per-module/question-per-module-list/question-per-module-list.component';
import { ExamSettingDialogComponent } from './module/exam/exam-setting/exam-setting-dialog/exam-setting-dialog.component';
import { DialogTrainingModuleComponent } from './dialog/dialog-training-module/dialog-training-module.component';
import { InternalUserCreationComponent } from './module/internal-user-creation/internal-user-creation.component';
import { CertificateListComponent } from './module/certificate-list/certificate-list.component';
import { TrainingModuleComponent } from './module/training-module/training-module.component';
import { CollectionAdminComponent } from './module/collection-admin/collection-admin.component';
import { AuthComponent } from './auth/auth.component';
import { TrainingCertificateComponent } from './module/training-certificate/training-certificate.component';
import { Ng2FileUploadComponent } from './module/ng2-file-upload/ng2-file-upload.component';
import { FileUploadModule } from 'ng2-file-upload';
import { DocumentViewerComponent } from './module/document-viewer/document-viewer.component';
import { DocumentVerificationListComponent } from './module/document-verification-list/document-verification-list.component';
import { PolicyVerificationComponent } from './module/policy-verification/policy-verification.component';
import { AgencyServiceAdminComponent } from './module/agency-service-admin/agency-service-admin.component';
import { UntaggedleadListComponent } from './module/lead/untaggedlead-list/untaggedlead-list.component';
import { TaggedleadListComponent } from './module/lead/taggedlead-list/taggedlead-list.component';
import { CandidateProfileComponent } from './module/candidate-profile/profile/candidate-profile.component';
import { CandidateDocumentDetailsComponent } from './module/candidate-profile/candidate-document-details/candidate-document-details.component';
import { PolicyNumberComponent } from './module/candidate-profile/policy-number/policy-number.component';
import { ShowDocumentComponent } from './module/show-document/show-document.component';
import { CallToActionComponent } from './module/call-to-action/call-to-action.component';
import { TimelineComponent } from './module/timeline/timeline.component';
import { SalesPolicyFileUploadComponent } from './module/sales-policy-file-upload/sales-policy-file-upload.component';
import { ReferenceVerificationListComponent } from './module/reference-verification-list/reference-verification-list.component';
import { DashboardComponent } from './module/dashboard/dashboard.component';
import { VivaResultComponent } from './module/viva-result/viva-result.component';
import { AgencyExecutiveDashboardComponent } from './module/agency-executive-dashboard/agency-executive-dashboard.component';
import { CallCenterAdminDashboardComponent } from './module/call-center-admin-dashboard/call-center-admin-dashboard.component';
import { DialogVivaResultComponent } from './dialog/dialog-viva-result/dialog-viva-result.component';
import { ReferenceComponent } from './module/candidate-profile/reference/reference.component';
import { BottomRowComponent } from './module/candidate-profile/bottom-row/bottom-row.component';
import { TrainingCertificateListViewComponent } from './module/training-certificate-list-view/training-certificate-list-view.component';
import { CertificateFeeListComponent } from './module/certificate/certificate-fee-list/certificate-fee-list.component';
import { CertificateFeeReassignDialogComponent } from './module/certificate/certificate-fee-reassign-dialog/certificate-fee-reassign-dialog.component';
import { CandidatesEligibleListComponent } from './module/candidates-eligible-list/candidates-eligible-list.component';
import { DialogReferenceRejectComponent } from './dialog/dialog-reference-reject/dialog-reference-reject.component';
import { DocumentVerificationByComponent } from 'src/app/module/document-verification/document-verification-by/document-verification-by.component';
import { TypeheadModule } from './module/typehead/typehead.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatTreeModule } from '@angular/material/tree';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRippleModule } from '@angular/material/core';
import { DocumentVerificationByDialogComponent } from './module/document-verification/document-verification-by-dialog/document-verification-by-dialog.component';
import { ReferenceVerificationListByComponent } from './module/reference-verification-list-by/reference-verification-list-by.component';
import { MatTooltipModule } from '@angular/material';
import { DialogReassignCcAdminComponent } from './dialog/dialog-reassign-cc-admin/dialog-reassign-cc-admin.component';
import { CampaignCreationComponent } from './module/campaign/campaign-creation/campaign-creation.component';
import { EventCalenderComponent } from './module/event-calender/event-calender.component';
import { DialogEventCalenderComponent } from './dialog/dialog-event-calender/dialog-event-calender.component';
import { FaqComponent } from './module/faq/faq.component';
import { DialogFaqComponent } from './dialog/dialog-faq/dialog-faq.component';
import { AppRoutingModule } from './/app-routing.module';
import { ContactComponent } from './module/contact/contact.component';
import { CampaignListComponent } from './module/campaign/campaign-list/campaign-list.component';
import { UmbmDashboardComponent } from './module/umbm-dashboard/umbm-dashboard.component';
import { UserNotificationComponent } from './module/user-notification/user-notification.component';
import { NotificationLoaderComponent } from './module/notification-loader/notification-loader.component';
import { ClickOutsideDirective } from './shared/directive/click-outside.directive';
import { ScrollableDirective } from './shared/directive/scrollable.directive';
import { FilterByPropertyPipe } from './shared/pipe/filterByProperty/filter-by-property.pipe';
import { DialogReferenceActionsComponent } from './dialog/dialog-reference-actions/dialog-reference-actions.component';
import { AnchorActionFilterDirective } from './shared/directive/anchor-disabled/anchor-action-filter.directive';
import swal from 'sweetalert2';
import { BankInformationComponent } from './module/candidate-profile/bank-information/bank-information.component';
import { AcademicQualificationComponent } from './module/candidate-profile/academic-qualification/academic-qualification.component';
import { PersonalInfoComponent } from './module/candidate-profile/personal-info/personal-info.component';
import { SideNavbarComponent } from './module/side-navbar/side-navbar.component';
import { LinkedTrainingModuleComponent } from './module/linked-training-module/linked-training-module.component';
//import { SideNavbarComponent } from './module/side-navbar/side-navbar.component';
@NgModule({
  declarations: [
    AppComponent,
    DateparserPipe,
    SigninComponent,
    RoleComponent,
    UserPermissionComponent,
    HomeComponent,
    RolePermissionComponent,
    MenuComponent,
    AdminDashboardComponent,
    UserCreateComponent,
    UserListComponent,
    SecurityPoliciesComponent,
    SecurityPolicyCreateComponent,
    UnauthorizedComponent,
    NotFoundComponent,
    SearchComponent,
    IdelModalComponent,
    ChangePasswordComponent,
    AccountRecoveryComponent,
    GlobalSettingsComponent,
    LoadingTestComponent,
    LoadingSpinnerComponent,
    AgeEligibilityComponent,
    EducationEligibilityComponent,
    EligibilityCriteriaComponent,
    DialogEligibilityCriteriaComponent,
    DistrictComponent,
    AreaComponent,
    EducationEligibilityComponent,
    DialogAgeEligibilityComponent,
    DialogEducationEligibilityComponent,
    EducationEligibilityComponent,
    DialogAreaComponent,
    DialogEligibilityJobComponent,
    DialogEligibilityMaritalStatusComponent,
    MaritalStatusEligibilityComponent,
    JobExperienceEligibilityComponent,
    DialogDistrictComponent,
    DialogMenuCreateComponent,
    ProfileviewComponent,
    UmBmCreateComponent,
    CandidateTagRequestComponent,
    AccountActivationComponent,
    UmBmListComponent,
    CandidateprofileviewComponent,
    TinyEditorComponent,
    QuestionListComponent,
    QuestionDialogComponent,
    ExamSettingListComponent,
    ExamSettingDialogComponent,
    QuestionPerModuleListComponent,
    ExamScheduleComponent,
    DialogExamScheduleComponent,
    TrainingAdminDashboardComponent,
    CollectionAdminComponent,
    CertificateListComponent,
    TrainingModuleComponent,
    InternalUserCreationComponent,
    DialogTrainingModuleComponent,
    ExamBookingComponent,
    DocumentViewerComponent,
    AuthComponent,
    TrainingCertificateComponent,
    Ng2FileUploadComponent,
    DocumentVerificationListComponent,
    PolicyVerificationComponent,
    AgencyServiceAdminComponent,
    Ng2FileUploadComponent,
    // Ng2FileUploadComponent,
    UntaggedleadListComponent,
    TaggedleadListComponent,
    CandidateProfileComponent,
    CandidateDocumentDetailsComponent,
    PolicyNumberComponent,
    ShowDocumentComponent,
    CallToActionComponent,
    TimelineComponent,
    SalesPolicyFileUploadComponent,
    ReferenceVerificationListComponent,
    DashboardComponent,
    VivaResultComponent,
    AgencyExecutiveDashboardComponent,
    CallCenterAdminDashboardComponent,
    DialogVivaResultComponent,
    ReferenceComponent,
    BottomRowComponent,
    TrainingCertificateListViewComponent,
    CertificateFeeListComponent,
    CertificateFeeReassignDialogComponent,
    CandidatesEligibleListComponent,
    DialogReferenceRejectComponent,
    DocumentVerificationByDialogComponent,
    DocumentVerificationByComponent,
    ClickOutsideDirective,
    ReferenceVerificationListByComponent,
    DialogReassignCcAdminComponent,
    CampaignCreationComponent,
    EventCalenderComponent,
    DialogEventCalenderComponent,
    FaqComponent,
    DialogFaqComponent,
    ContactComponent,
    CampaignListComponent,
    UmbmDashboardComponent,
    UserNotificationComponent,
    NotificationLoaderComponent,
    ScrollableDirective,
    FilterByPropertyPipe,
    DialogReferenceActionsComponent,
    AnchorActionFilterDirective,
    BankInformationComponent,
    AcademicQualificationComponent,
    PersonalInfoComponent,
    //SideNavbarComponent,
    LinkedTrainingModuleComponent,
    PersonalInfoComponent
    //SideNavbarComponent

  ],

  imports: [
    // EditorModule,
    InfiniteScrollModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgIdleKeepaliveModule.forRoot(),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    DataTableModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatTooltipModule,
    MatCardModule,
    MatTableModule,
    MatSortModule,
    MatDialogModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatSelectModule,
    MatRadioModule,
    MatCheckboxModule,
    MatInputModule,
    MatTreeModule,
    MatDatepickerModule,
    MatRippleModule,
    ReactiveFormsModule,
    Ng2ImgToolsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: false,
      closeButton: true,
      enableHtml: true
    }),
    FileUploadModule,
    TypeheadModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    UserMenuCanActivateRouteGuardService
  ],
  bootstrap: [AppComponent],

  entryComponents: [
    IdelModalComponent,
    DialogEligibilityCriteriaComponent,
    DialogEligibilityJobComponent,
    DialogEligibilityMaritalStatusComponent,
    DialogAreaComponent,
    DialogDistrictComponent,
    DialogAgeEligibilityComponent,
    DialogEducationEligibilityComponent,
    DialogMenuCreateComponent,
    QuestionDialogComponent,
    DialogExamScheduleComponent,
    ExamSettingDialogComponent,
    DialogTrainingModuleComponent,
    Ng2FileUploadComponent,
    DocumentViewerComponent,
    ShowDocumentComponent,
    DialogVivaResultComponent,
    CertificateFeeReassignDialogComponent,
    DialogReferenceRejectComponent,
    DocumentVerificationByDialogComponent,
    DialogReassignCcAdminComponent,
    DialogEventCalenderComponent,
    DialogFaqComponent,
    DialogReferenceActionsComponent
  ]
})
export class AppModule {
  constructor(private auth: AuthService, private http: HttpClient) {
    // this.http.get('../assets/config.json').subscribe( res => {
    //   if (res != null ){
    //     environment['apiEndPoint'] = ((environment.production) ? res['remoteServer'] : res['devServer']);

    //   }
    //  });
    this.auth.APPSERVER = environment.production
      ? environment.remoteServer
      : environment.devServer;
  }
}
